/*
trigger: AccountTriggerPopulateAuthorizedPartner 
Author: Chris C
Created Date: 7/10/2014
*/
trigger AccountTriggerPopulateAuthorizedPartner on Account ( after update ) {
    if(trigger.isAfter && trigger.isUpdate)
    {
        AccountTriggerPopAuthPartnerHelper.populateAuthorizedPartner( trigger.new, trigger.oldMap );
       
    }
    
}