trigger CaseTrigger on Case (before insert, after insert) {
    if(trigger.isAfter) {
        if(trigger.isInsert) {
            CaseTriggerHelper.offworkSupport(Trigger.new);
        }
    }
}