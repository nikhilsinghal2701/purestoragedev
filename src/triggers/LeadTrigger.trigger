/*
trigger: LeadTrigger
author: Jaya
Created Date: 11/20/2013
*/
trigger LeadTrigger on Lead (after insert, before insert) 
{
	
	if(Trigger.isBefore)
	{
		if(Trigger.isInsert)
		{
			LeadTriggerHandler beforeCall = new LeadTriggerHandler();
			beforeCall.beforeInsertAction(Trigger.new);
		}
	}
	if(Trigger.isAfter)
	{	if(Trigger.isInsert)
		{
			LeadTriggerHandler afterCall = new LeadTriggerHandler();
			afterCall.afterInsertAction(Trigger.new);
		}
	}
}