trigger celigoUpdateOpportunity on Opportunity (before insert, before update) {
	List < Id > OpportunityIds = new List < Id > ();
	List < Id > AccountIdsToBeSynced = new List < Id > ();
    for (Opportunity opportunity: System.Trigger.new) {
        if (opportunity.Celigo_Update__c) {
            opportunity.Celigo_Update__c = false;
            continue;
        }
        
        if (Trigger.isInsert) {
            opportunity.StageNameState__c = null;
            opportunity.Send_to_NetSuite__c = false;
            opportunity.Generate_Estimate__c = false;
            opportunity.Generate_Sales_Order__c = false;
            opportunity.NetSuite_Quote__c = null;
            opportunity.NS_Sync__c = null;
        }
        
		if (Trigger.isInsert)
			continue;
		OpportunityIds.add(opportunity.Id);
		if (opportunity.Distributor_Account__c != null)
			AccountIdsToBeSynced.add(opportunity.Distributor_Account__c);
		if (opportunity.Partner_Account__c != null)
			AccountIdsToBeSynced.add(opportunity.Partner_Account__c);
        //please see Sales Order and Transfer Order button logic
        //the NS_BillTo_Customer_Lookup__c field (new) also renders the NS_BillTo_Customer__c obsolete 
        /*
		if(opportunity.Partner_Account__c != null) {
            opportunity.NS_BillTo_Customer__c = opportunity.Partner_Account__c;  
            opportunity.NS_BillTo_Customer_Lookup__c = opportunity.Partner_Account__c;
		} else {            
            opportunity.NS_BillTo_Customer__c = opportunity.AccountId; 
            opportunity.NS_BillTo_Customer_Lookup__c = opportunity.AccountId; 
		} 
		*/ 
      
    }
	System.debug('AccountIdsToBeSynced -- ' + AccountIdsToBeSynced);
	if (AccountIdsToBeSynced != null && AccountIdsToBeSynced.size() > 0) {
		List < Account > accountsToBeSynced = [select Id, NetSuite_Push__c, NetSuite_Pull__c from Account where Id IN : AccountIdsToBeSynced];
		List < Account > acctsToUpdate = new List < Account > ();
		for (Account accountToBeSynced : accountsToBeSynced) {
			accountToBeSynced.NetSuite_Push__c = true;
			accountToBeSynced.NetSuite_Pull__c = true;
			acctsToUpdate.add(accountToBeSynced);
		}
		if (acctsToUpdate == null || acctsToUpdate.size() < 1)
			return;
		System.debug('----account ids to be updated----' + acctsToUpdate);
		update acctsToUpdate;
	}
}