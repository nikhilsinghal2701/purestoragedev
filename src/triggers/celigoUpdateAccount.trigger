trigger celigoUpdateAccount on Account(before insert, before update) {
		for (Account account: System.Trigger.new) {

			if(account.BillingStreet !=null && account.BillingStreet != ''  && account.BillingStreet.indexOf('<br>') != -1)
				account.BillingStreet = account.BillingStreet.replace('<br>','\n');

			if(account.ShippingStreet !=null && account.ShippingStreet != ''  && account.ShippingStreet.indexOf('<br>') != -1)
				account.ShippingStreet = account.ShippingStreet.replace('<br>','\n');

			if (account.Celigo_Update__c) {
				account.Celigo_Update__c = false;
				continue;
			}       
			
			if (Trigger.isInsert) {
				account.NetSuite_Id__c = null;
				account.NetSuite_Locked__c = false;
				account.NS_Sync__c = null;						
			}
	
			if(account.NetSuite_Locked__c)
				continue;
			
			if(Trigger.isUpdate){		
					
				if(account.BillingStreet !=null && account.BillingStreet != '' ){										                   
					String BillingStreet = account.BillingStreet;
					List<String> BillStreet = BillingStreet.split('\\\n',3);			
					account.NS_BillAddr_Line_1__c = BillStreet[0];
					if(BillStreet.size() > 1 )
						account.NS_BillAddr_Line_2__c = BillStreet[1];		
				}  
					
				if(account.ShippingStreet != null && account.ShippingStreet != '' ){
					String ShippingStreet = account.ShippingStreet;
					List<String> ShipStreet = ShippingStreet.split('\\\n',3);			
					account.NS_ShipAddr_Line_1__c = ShipStreet[0];
					if(ShipStreet.size() > 1 )
						account.NS_ShipAddr_Line_2__c = ShipStreet[1];		
				}
			}
			
			if (account.NetSuite_Id__c == null)
            	continue;
				
			if (Trigger.isUpdate) {
                Account oldAccountMap = System.Trigger.oldMap.get(account.Id);
                Account newAccounttMap = System.Trigger.newMap.get(account.Id);
                List < String > fields = new List < String > {
                    'NetSuite_Type_Id__c',
                    'Fax',
                    'Name',
                    'Phone',
                    'Phone'
                };
                boolean isAnyFieldChanged = false;
                for (String field : fields) {
                    if (oldAccountMap.get(field) != newAccounttMap.get(field))
                        isAnyFieldChanged = true;
                }
                if (isAnyFieldChanged == false)
                    continue;
            }	
        
        account.NetSuite_Push__c = true;
        account.NetSuite_Pull__c = true;
    }
}