trigger OpportunityTrigger on Opportunity (before insert, after insert, before update) {

    //instantiate the processor
    OpportunityProcessor processor = new OpportunityProcessor();
    
    //handle the before events
    if(trigger.isBefore)
    {
        //handle inserts
        if(trigger.isInsert)
        {
            //pass trigger to worker/helper class(es) for logic
            //step 1
            processor.processBeforeInsert(trigger.new);
        }
        
        //handle updates
        if(trigger.isUpdate)
        {
            //pass trigger to worker/helper class(es) for logic
            //step 1
            processor.processBeforeUpdate(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);                                
        }
    }
    if(trigger.isAfter)
    {   
        //handle inserts
        if(trigger.isInsert)
        {
            //pass trigger to worker/helper class(es) for logic
            //step 1
            processor.processAfterInsert(trigger.new);                              
        }   
    }
}