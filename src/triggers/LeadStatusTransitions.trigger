trigger LeadStatusTransitions on Lead (after update) 
{

    //handle the before events
	if(trigger.isAfter)
	{
		//handle inserts
		if(trigger.isUpdate)
		{
			//pass trigger to worker/helper class(es) for logic
			//step 1
			LeadStatusTransitionsProcessor.checkStatusTransition(trigger.new, trigger.oldMap);
		}
    }
    
}