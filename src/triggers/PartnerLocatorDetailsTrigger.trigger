/*************************************************
Class: PartnerLocatorDetailsTrigger 
Description: Trigger on Partner Locator Details
             1. populate Channel Account Manager field
Auther: Christine Wang
Date:   8/25/2014

Revision History:
----------------------------------------------------     

***************************************************/
trigger PartnerLocatorDetailsTrigger on Partner_Locator_Details__c (after insert) {
    // code to fire on insert of Training Evaluation
    if( trigger.isInsert ){
        if( trigger.isBefore ){
            // code to fire on Before Insert
            
        }else{
            // code to fire on After Insert
            PartnerLocatorDetailsTriggerHelper.populateChannelAccountManagerOnInsert(trigger.new);
        }
    }
}