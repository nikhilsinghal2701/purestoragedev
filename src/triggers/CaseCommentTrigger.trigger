trigger CaseCommentTrigger on CaseComment (after insert, before update, after update) {
	if(trigger.isAfter) {
		if(trigger.isInsert){
			CaseCommentTriggerHelper.eventProcess(Trigger.new);
		}
	}
}