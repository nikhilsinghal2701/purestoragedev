trigger AccountTrigger on Account (after insert, before update, after update) 
{
//instantiate the processor
	AccountProcessor processor = new AccountProcessor();
	
	//handle the before events
	if(trigger.isBefore)
	{
		//handle updates
		if(trigger.isUpdate)
		{
			//pass trigger to worker/helper class(es) for logic
			//step 1
			processor.processRecords(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, true, false);
		}
        
	}
	if(trigger.isAfter)
	{	
		//handle inserts
		if(trigger.isInsert)
		{
			processor.processRecords(trigger.new, null, trigger.newMap, null, false, true);				
		}
        //handle updates
		if(trigger.isUpdate)
		{
			//pass trigger to worker/helper class(es) for logic
			//step 1
			processor.processRecords(trigger.new, trigger.old, trigger.newMap, trigger.oldMap, false,true);
		}
	}
}