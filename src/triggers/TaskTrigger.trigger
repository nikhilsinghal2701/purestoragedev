trigger TaskTrigger on Task (after insert, after update, after delete) 
{
    //handle the after events
	if(trigger.isAfter)
	{
		//handle inserts
		if(trigger.isInsert || trigger.isUpdate)
		{
			//pass trigger to worker/helper class(es) for logic
			//step 1
			LeadStatusTransitionsProcessor.taskIsApex = true;
            TaskLeadProcessor.computeNumberOfActivities(trigger.new, trigger.newMap);
		}
        if(trigger.isDelete)
        {
            LeadStatusTransitionsProcessor.taskIsApex = true;
            TaskLeadProcessor.computeNumberOfActivities(trigger.old, trigger.oldMap);
        }
        
	}

}