trigger AccountShareTrigger on Account (after insert, after update, before update) {

    if(trigger.isAfter) {
        AccountUtil.svarAccountShare(trigger.new, trigger.oldMap);
    }
    
}