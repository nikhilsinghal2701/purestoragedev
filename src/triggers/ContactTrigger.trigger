/*
Trigger: AcontactTrigger
Author: Jaya
Created Date: 11/21/2013
Updated Date: 09/17/2014; MD added processor for soft deletes
*/
trigger ContactTrigger on Contact (before insert ,before update,after update) 
{
    
    if(trigger.isBefore)
    {
        ContactTriggerHelper obj = new ContactTriggerHelper();
        if(trigger.isInsert)
        {  
            obj.beforeInsertAction(trigger.new);
        }
        if(trigger.isUpdate)
        {
            obj.beforeUpdateAction(trigger.new, trigger.oldMap);
            ContactDeletionProcessor.contactSoftUndelete(trigger.new, trigger.oldMap);
        }
    }
  
    if(trigger.isAfter && trigger.isUpdate)
    {
        ContactTriggerHelper obj = new ContactTriggerHelper();
        obj.afterUpdateAction(trigger.new, trigger.oldMap);
    }
}