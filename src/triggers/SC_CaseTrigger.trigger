/***********************************************
 * Description : On before insert & before update - splits the emails from case collaborators field into individual email fields
                 On after insert, create Account's CC enabled Contacts as case team members
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

trigger SC_CaseTrigger on Case (before insert, before update, after insert, after update) 
{
    if(trigger.isBefore)
    {
        if(trigger.isInsert)
        {
            system.debug('-----> before insert');
            SC_CaseTriggerHandler.isInsert = true;
            SC_CaseTriggerHandler.onBeforeInsert(trigger.new);
        }
        
        if(trigger.isUpdate)
        {
            system.debug('-----> before update');
            SC_CaseTriggerHandler.onBeforeUpdate(trigger.oldMap, trigger.newMap);
        }
    }
    
    if(trigger.isAfter)
    {
        if(trigger.isInsert)
        {
            system.debug('-----> after insert');
            SC_CaseTriggerHandler.onAfterInsert(trigger.newMap);
        }
        
        if(trigger.isUpdate)
        {
            system.debug('-----> after update');
            SC_CaseTriggerHandler.onAfterUpdate(trigger.oldMap, trigger.newMap);
        }
    }
}