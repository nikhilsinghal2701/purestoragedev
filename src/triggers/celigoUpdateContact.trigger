trigger celigoUpdateContact on Contact(before insert, before update) {
	List < Id > ids = new List < Id > ();
	for (Contact contact : System.Trigger.new) {
		if (contact.AccountId != null && !contact.NetSuite_Locked__c && !contact.Celigo_Update__c)
			ids.add(contact.AccountId);
	}
	List < Account > acs = new List < Account > ();

	if (!ids.isEmpty())
		acs = [select Id, NetSuite_Id__c from account where id IN : ids];

	if (Trigger.isInsert) {
		for (Contact contact : System.Trigger.new) {

			if (contact.AccountId == null || contact.NetSuite_Locked__c)
				continue;

			if (contact.MailingStreet != null && contact.MailingStreet != '' && contact.MailingStreet.indexOf('<br>') != -1)
				contact.MailingStreet = contact.MailingStreet.replace('<br>', '\n');

			if (contact.Celigo_Update__c) {
				contact.Celigo_Update__c = false;
				continue;
			}

			contact.NetSuite_Id__c = null;
			contact.NetSuite_Locked__c = false;
			contact.NS_Sync__c = null;

			if (contact.NetSuite_Id__c == null && contact.Email == null) {
				contact.NetSuite_Push__c = false;
				contact.NetSuite_Pull__c = false;
				continue;
			}

			if (contact.MailingStreet != null && contact.MailingStreet != '') {
				String MailingStreet = contact.MailingStreet;
				List < String > MailStreet = MailingStreet.split('\\\n', 3);
				contact.NS_MailAddr_Line_1__c = MailStreet[0];
				if (MailStreet.size() > 1)
					contact.NS_MailAddr_Line_2__c = MailStreet[1];
			}

			Integer index = -1;
			for (Integer i = 0; i < acs.size(); i++) {
				if (acs[i].Id == contact.AccountId) {
					index = i;
					break;
				}
			}

			if (index == -1 || acs[index].NetSuite_Id__c == null) {
				continue;
			}

			contact.NetSuite_Push__c = true;
			contact.NetSuite_Pull__c = true;
		}
	} else {

		List < Contact > newContacts = System.Trigger.new;
		List < Contact > oldContacts = System.Trigger.old;

		for (Integer i = 0; i < newContacts.size(); i++) {
			if (newContacts[i].AccountId == null || newContacts[i].NetSuite_Locked__c)
				continue;

			if (newContacts[i].MailingStreet != null && newContacts[i].MailingStreet != '' && newContacts[i].MailingStreet.indexOf('<br>') != -1)
				newContacts[i].MailingStreet = newContacts[i].MailingStreet.replace('<br>', '\n');

			if (newContacts[i].Celigo_Update__c) {
				newContacts[i].Celigo_Update__c = false;
				continue;
			}

			if (newContacts[i].NetSuite_Id__c == null && newContacts[i].Email == null) {
				newContacts[i].NetSuite_Push__c = false;
				newContacts[i].NetSuite_Pull__c = false;
				continue;
			}
			//To avoid the creation of a new address lines on contact record in NS,each time the contact is updated in SF
			if (newContacts[i].MailingStreet != null && newContacts[i].MailingStreet != '' && newContacts[i].MailingStreet != oldContacts[i].MailingStreet) {
				String MailingStreet = newContacts[i].MailingStreet;
				List < String > MailStreet = MailingStreet.split('\\\n', 3);
				newContacts[i].NS_MailAddr_Line_1__c = MailStreet[0];
				if (MailStreet.size() > 1)
					newContacts[i].NS_MailAddr_Line_2__c = MailStreet[1];
			}

			Integer index = -1;
			for (Integer j = 0; j < acs.size(); j++) {
				if (acs[j].Id == newContacts[j].AccountId) {
					index = j;
					break;
				}
			}

			if (index == -1 || acs[index].NetSuite_Id__c == null) {
				continue;
			}

			if (Trigger.isUpdate) {
				Contact oldContactMap = System.Trigger.oldMap.get(newContacts[i].Id);
				Contact newContactMap = System.Trigger.newMap.get(newContacts[i].Id);
				List < String > fields = new List < String > {
					'Description',
					'AccountId',
					'Email',
					'Fax',
					'FirstName',
					'HomePhone',
					'LastName',
					'MobilePhone',
					'Phone',
					'Title'
				};
				boolean isAnyFieldChanged = false;
				for (String field : fields) {
					if (oldContactMap.get(field) != newContactMap.get(field))
						isAnyFieldChanged = true;
				}
				if (isAnyFieldChanged == false)
					continue;
			}

			newContacts[i].NetSuite_Push__c = true;
			newContacts[i].NetSuite_Pull__c = true;

		}
	}
}