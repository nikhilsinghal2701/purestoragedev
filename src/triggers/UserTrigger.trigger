/*************************************************
Class: UserTrigger
Description: Trigger on User
             Update associated contact owner field when account owner changes
Auther: Christine Wang
Date:   7/3/2014

Revision History:
----------------------------------------------------     

***************************************************/
trigger UserTrigger on User( after insert, after update ) {
     // code to fire on insert of Training Evaluation
    if( trigger.isInsert ){
        if(trigger.isAfter) {
            //pass trigger to worker/helper class(es) for logic
            //step 1
            new UserProcessor().processRecords(trigger.new, null, trigger.newMap, null);
        }
        if( trigger.isBefore ){
            // code to fire on Before Insert    
        }else{
            // code to fire on After Insert
            UserTriggerHelper.methodOnInsert( trigger.new );   
        }
    }
    
    // code to fire on update of Training Evaluation
    if( trigger.isUpdate ){
        if( trigger.isBefore ){
            // code to fire on Before Update
        }else{
            // code to fire on After Update
            UserTriggerHelper.methodOnUpdate( trigger.new, trigger.oldMap );     
        }
    }
}