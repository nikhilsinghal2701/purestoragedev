/*
 * Description: trigger to create a survey record when a case is closed. The survey record will then be
 *              picked up by SatMetrix to the send the survey out to customers.
 */

trigger SatMetrixSurveyTrigger on Case (after update) 
{
    if(trigger.isAfter && trigger.isUpdate)
    {
        SatMetrixSurveyTriggerHandler.OnAfterUpdate(trigger.oldMap, trigger.newMap);
    }
}