trigger ZenOrgCreateAfterUpdateOrInsert on Opportunity (after update,after insert) {
    for (Opportunity op : Trigger.new) {
        if (!Test.isRunningTest() && op.accountId != null && ( op.StageName == 'Stage 4 - POC/EVAL Engaged' || op.StageName == 'Stage 8 - Closed/Won' )){
            Account acc = [Select ac.Id,ac.Name,ac.Zendesk_Organization__c,ac.Support_Must_Not_Contact_End_Customer__c from Account ac where ac.id =: op.AccountId];
            if(!acc.Support_Must_Not_Contact_End_Customer__c){
                if(acc.Zendesk_Organization__c == null || acc.Zendesk_Organization__c.length() == 0){
                    String accountName = acc.Name;
                    ZenWebServiceCallout.createOrganization(accountName,op.id,acc.id);
                    acc.Zendesk_Organization__c = acc.Name;
                    update acc;
                }
            }
        }
    }
}