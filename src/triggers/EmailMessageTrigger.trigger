trigger EmailMessageTrigger on EmailMessage (before insert, after insert, before update, after update) {
    EmailMessageTriggerHelper.eventProcess(trigger.new);

}