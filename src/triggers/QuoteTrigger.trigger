trigger QuoteTrigger on Quote (after update, after insert) 
{

    //instantiate the processor
    QuoteProcessor processor = new QuoteProcessor();
    
    //handle the after events
    if(trigger.isAfter)
    {
        //handle updates
        if(trigger.isUpdate || trigger.isInsert)
        {
            //pass trigger to worker/helper class(es) for logic
            //step 1
            processor.processRecords(trigger.new, trigger.old, trigger.newMap, trigger.oldMap);
        }
        if(trigger.isUpdate)
        {
            processor.copyApprovalDetails(trigger.oldMap, trigger.newMap);
        }
    }

    /*for (Quote q : trigger.new) 
    {
        if(trigger.OldMap.get(q.id).Status != trigger.NewMap.get(q.Id).Status && trigger.Newmap.get(q.Id).Status == '1st Approval')
        {
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
    
            app.setObjectId(q.id);
    
            Approval.ProcessResult result = Approval.process(app);
        }
    }*/
    
}