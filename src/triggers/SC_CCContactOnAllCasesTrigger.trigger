/***********************************************
 * Description : On after insert & after update - if the contact is checked/unchecked as CC Email, this trigger would create/remove them as 
                 case team members
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

trigger SC_CCContactOnAllCasesTrigger on Contact (after insert, after update) 
{
    if(trigger.isAfter)
    {
        if(trigger.isInsert)
        {
            SC_CCContactOnAllCasesTriggerHandler.onAfterInsert(trigger.newMap);
        }
        
        if(trigger.isUpdate)
        {
            SC_CCContactOnAllCasesTriggerHandler.onAfterUpdate(trigger.oldMap, trigger.newMap);
        }
    }
}