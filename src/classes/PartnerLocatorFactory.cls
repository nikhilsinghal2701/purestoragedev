/*****************************************************************************
 * Description : A factory that will return an instance of the Partner Locator Service
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/26/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorFactory {

	private static final String SERVICE_IMPL = 'PartnerLocatorServiceImpl';
	
	/**
	* Return an instance of the Partner Locator Service
	*/
	public static PartnerLocatorService getInstance(){
		Type t = Type.forName(SERVICE_IMPL);
		return (PartnerLocatorService)t.newInstance();
	}

}