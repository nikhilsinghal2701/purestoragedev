@isTest( seeAllData=true )
public class OpportunityExchangeProcessorTest
{
    private static DatedConversionRate currencyRateGBPnow;
    private static DatedConversionRate currencyRateGBPprev;
    private static DatedConversionRate currencyRateUSD;
    
    static testMethod void testcalculateOpportunityExchangeRate()
    {
        // Updating the custom setting to not invoke the trigger and set the currency rates
        setupSettings();
        
        Opportunity myTestObject1 = createOpportunity( 'GBP', System.today() );
        addTestProducts( myTestObject1 );
        
        Opportunity myTestObject2 = createOpportunity( 'GBP', currencyRateGBPprev.NextStartDate.addDays( -1 ));
        addTestProducts( myTestObject2 );
        
        Opportunity myTestObject3 = createOpportunity( 'USD', System.today() );
        addTestProducts( myTestObject3 );
        
        // Pre-calculated amount for the opportunity
        Decimal calculatedAmount = (167.12 * 2) + (788.24 * 3);
        
        test.startTest();
        
        // Refetching the data to refresh the amount
        myTestObject1 = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount FROM Opportunity WHERE id = :myTestObject1.id];
        
        // Calculating the GBP amount that is from recent currency rate
        myTestObject1.Exch_Rate_USD__c = OpportunityExchangeProcessor.getConversionRate( myTestObject1.CurrencyIsoCode, myTestObject1.CloseDate );
        myTestObject1.Converted_Amount_USD__c = OpportunityExchangeProcessor.calculateOpportunityExchangeRate( myTestObject1.Amount, myTestObject1.Exch_Rate_USD__c );
        System.assertEquals( currencyRateGBPnow.ConversionRate, myTestObject1.Exch_Rate_USD__c );
        System.assertEquals( calculatedAmount / currencyRateGBPnow.ConversionRate, myTestObject1.Converted_Amount_USD__c );
        
        // Refetching the data to refresh the amount
        myTestObject2 = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount FROM Opportunity WHERE id = :myTestObject2.id];
                
        // Calculating the GBP amount that is from oldest currency rate
        myTestObject2.Exch_Rate_USD__c = OpportunityExchangeProcessor.getConversionRate( myTestObject2.CurrencyIsoCode, myTestObject2.CloseDate );
        myTestObject2.Converted_Amount_USD__c = OpportunityExchangeProcessor.calculateOpportunityExchangeRate( myTestObject2.Amount, myTestObject2.Exch_Rate_USD__c );
        System.assertEquals( currencyRateGBPprev.ConversionRate, myTestObject2.Exch_Rate_USD__c );
        System.assertEquals( calculatedAmount / currencyRateGBPprev.ConversionRate, myTestObject2.Converted_Amount_USD__c );
        
        // Refetching the data to refresh the amount
        myTestObject3 = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount FROM Opportunity WHERE id = :myTestObject3.id];
                
        // Calculating the USD amount to make sure it mathes
        myTestObject3.Exch_Rate_USD__c = OpportunityExchangeProcessor.getConversionRate( myTestObject3.CurrencyIsoCode, myTestObject3.CloseDate );
        myTestObject3.Converted_Amount_USD__c = OpportunityExchangeProcessor.calculateOpportunityExchangeRate( myTestObject3.Amount, myTestObject3.Exch_Rate_USD__c );
        System.assertEquals( currencyRateUSD.ConversionRate, myTestObject3.Exch_Rate_USD__c );
        System.assertEquals( calculatedAmount, myTestObject3.Converted_Amount_USD__c );
        
        // Testing the mapping of conversion
        Map<String, List<DatedConversionRate>> myMapping = OpportunityExchangeProcessor.getConversionRate();
        
        System.assertEquals( true, myMapping.containsKey( 'USD' ));
        System.assertEquals( true, myMapping.containsKey( 'GBP' ));
        test.stopTest();
    }
   
    public static void setupSettings()
    {
        currencyRateGBPnow = [SELECT ConversionRate, StartDate, NextStartDate FROM DatedConversionRate WHERE IsoCode = 'GBP' ORDER BY NextStartDate desc limit 1];
        currencyRateGBPprev = [SELECT ConversionRate, StartDate, NextStartDate FROM DatedConversionRate WHERE IsoCode = 'GBP' ORDER BY NextStartDate asc limit 1];
        currencyRateUSD = [SELECT ConversionRate, StartDate, NextStartDate FROM DatedConversionRate WHERE IsoCode = 'USD' ORDER BY NextStartDate asc limit 1];
        
        // Disabling the trigger so that opportunity currency information doesn't update automatically... (other test will cover the trigger)
        Apex_Code_Settings__c cs = [SELECT id, Oppty_Currency_Cal_Trigger_Enabled__c  FROM Apex_Code_Settings__c];
        cs.Oppty_Currency_Cal_Trigger_Enabled__c = false;
        update cs;
    }
   
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account', Support_Must_Not_Contact_End_Customer__c = true );
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate);
        
        insert myOpp;
        return myOpp;
    }
    
    
    public static void addTestProducts( Opportunity myOpp )
    {
        PricebookEntry pbe1 = createPriceBookEntry( 'Product 1', 167.12, myOpp.CurrencyIsoCode );
        PricebookEntry pbe2 = createPriceBookEntry( 'Product 2', 788.24, myOpp.CurrencyIsoCode );
        
        OpportunityLineItem oli1 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe1.Id, Quantity = 2, UnitPrice = 167.12 );
        OpportunityLineItem oli2 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe2.Id, Quantity = 3, UnitPrice = 788.24 );
        
        insert oli1;
        insert oli2;
    }
    
    
    public static PricebookEntry createPriceBookEntry( String productName, Decimal price, String currencyCode )
    {
        Product2 myProduct = new Product2(Name = productName);
        insert myProduct;

        PricebookEntry pbe = new PricebookEntry( isActive = true, Product2Id = myProduct.Id, Pricebook2Id = getStandardPriceBookID(), CurrencyIsoCode = currencyCode, UnitPrice = price );
        insert pbe;
        
        return pbe;
    }
    
    
    public static ID getStandardPriceBookID()
    {
        return [select Id from Pricebook2 where isActive = true and isStandard = true limit 1].ID;
    }
}