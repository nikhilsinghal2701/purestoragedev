/*****************************************************************************
 * Description : Implementation class for Partner Locator Service
 *				 This service operates on the sObject Partner_Locator_Details__c 
 *				 and handles all the queries related to filter select options, search etc.
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/25/2014
 * Version     : 1.0 
 *
 * 
 *****************************************************************************/
public class PartnerLocatorServiceImpl implements PartnerLocatorService {

	private static final String PARTNER_LOCATOR_SQL_SELECT = 
							' SELECT ID, NAME, APPLICATION_EXPERTISE__C, COMPANY_LOGO__C '
						+	', CONTACT_INFORMATION_EMAIL__C, CONTACT_INFORMATION_PHONE__C, CONTACT_INFORMATION_WEBSITE_URL__C '
						+	', DESCRIPTION__C, INDUSTRY_FOCUS__C, PARTNER_ACCOUNT__C, PARTNER_NAME__C, PARTNER_TIER__C, PLATFORMS__C '
						+	', PRIMARY_LOCATION_CITY__C, PRIMARY_LOCATION_COUNTRY__C, PRIMARY_LOCATION_STATE_PROVINCE__C '
						+	', PRIMARY_LOCATION_STREET__C, PRIMARY_LOCATION_ZIP_POSTAL_CODE__C, PUBLISHED_DATE__C '
						+	', STATUS__C, SUBMIT_FOR_APPROVAL__C, TARGET_MARKET_SIZE__C ';
	private static final String PARTNER_LOCATOR_SQL_FROM = ' FROM PARTNER_LOCATOR_DETAILS__C ';
	private static final String PARTNER_LOCATOR_SQL_WHERE = ' WHERE STATUS__C = \'Published\' ';	
	private static final String PARTNER_LOCATOR_SQL_AND = ' AND ';	
	private static final String PARTNER_LOCATOR_SQL_FILTER_COUNTRY = ' PRIMARY_LOCATION_COUNTRY__C = :selectedCountry ';
	private static final String PARTNER_LOCATOR_SQL_FILTER_STATE_OR_PROVINCE = ' PRIMARY_LOCATION_STATE_PROVINCE__C = :selectedStateOrProvince ';
	private static final String PARTNER_LOCATOR_SQL_FILTER_INDUSTRY_FOCUS = ' INDUSTRY_FOCUS__C INCLUDES '; 
	private static final String PARTNER_LOCATOR_SQL_FILTER_APPLICATION_EXPERTISE = ' APPLICATION_EXPERTISE__C INCLUDES '; 
	private static final String PARTNER_LOCATOR_SQL_FILTER_PARTNER_TIERS = ' PARTNER_TIER__C IN :selectedPartnerTiers ';
	private static final String PARTNER_LOCATOR_SQL_ORDER_BY = ' ORDER BY PARTNER_TIER__C ASC, PARTNER_NAME__C ASC ';
	private static final String PARTNER_LOCATOR_SQL_FILTER_NAME = ' NAME = :name ';

  /**
  	*	Purpose of this operation is to 
  	*	1. Read the search filter or criteria from the input Apex object PartnerLocatorSearchBean
  	*	2. Prepare the SOQL query to retrieve results from the sObject Partner_Locator_Details__c
  	*	3. Apply any filter or criteria to the SOQL as read the Search Bean instance
  	*	4. Retrieve the Collection of results of sObject type Partner_Locator_Details__c
  	*	5. Convert the results from sObject type Partner_Locator_Details__c to a custom object bean PartnerLocatorBean
  	*	6. Return the Collection of results of custom bean type PartnerLocatorBean
  	*/
	public List<PartnerLocatorBean> search(PartnerLocatorSearchBean bean) {
		List<Partner_Locator_Details__c> details = new List<Partner_Locator_Details__c>();
		// begin - prepare the query variables by extracting values from the search bean
		String selectedCountry = ( (bean!=NULL) && (!String.isBlank(bean.selectedCountry)) )
								? bean.selectedCountry
								: '';
		String selectedApplicationExpertise = ( (bean!=NULL) && (!String.isBlank(bean.selectedApplicationExpertise)) )
								? bean.selectedApplicationExpertise
								: '';
		String selectedIndustryFocus = ( (bean!=NULL) && (!String.isBlank(bean.selectedIndustryFocus)) )
								? bean.selectedIndustryFocus
								: '';
		List<String> selectedPartnerTiers = ( (bean!=NULL) && (!bean.selectedPartnerTiers.isEmpty()) )
								? bean.selectedPartnerTiers
								: new List<String>();
		String selectedStateOrProvince = ( (bean!=NULL) && (!String.isBlank(bean.selectedCountry)) && (!String.isBlank(bean.selectedStateOrProvince)) )
								? bean.selectedStateOrProvince
								: '';																								
		// end - prepare the query variables by extracting values from the search bean
		String sql = PARTNER_LOCATOR_SQL_SELECT + PARTNER_LOCATOR_SQL_FROM + PARTNER_LOCATOR_SQL_WHERE;
		if ( !String.isBlank(selectedCountry) ){
			sql = sql 
				+ PARTNER_LOCATOR_SQL_AND
				+ PARTNER_LOCATOR_SQL_FILTER_COUNTRY;
		}
		if ( !String.isBlank(selectedApplicationExpertise) ){
			sql = sql 
				+ PARTNER_LOCATOR_SQL_AND
				+ PARTNER_LOCATOR_SQL_FILTER_APPLICATION_EXPERTISE
				+ '(\''
				+ selectedApplicationExpertise
				+ '\')';				
		}
		if ( !String.isBlank(selectedIndustryFocus) ){
			sql = sql 
				+ PARTNER_LOCATOR_SQL_AND
				+ PARTNER_LOCATOR_SQL_FILTER_INDUSTRY_FOCUS
				+ '(\''
				+ selectedIndustryFocus
				+ '\')';
		}
		if ( !selectedPartnerTiers.isEmpty() ){
			sql = sql 
				+ PARTNER_LOCATOR_SQL_AND
				+ PARTNER_LOCATOR_SQL_FILTER_PARTNER_TIERS;
		}		
		if ( !String.isBlank(selectedStateOrProvince) ){
			sql = sql 
				+ PARTNER_LOCATOR_SQL_AND
				+ PARTNER_LOCATOR_SQL_FILTER_STATE_OR_PROVINCE;
		}
		sql = sql + PARTNER_LOCATOR_SQL_ORDER_BY;
		for (Partner_Locator_Details__c detail : Database.query(sql)){
			details.add(detail);
		}						
		return PartnerLocatorUtility.convertDetailsToBeans(details);
	}
  /**
  	*	Return the Partner Locator Detail record for the input parameter [Name]
  	*	Return value is a custom bean object - PartnerLocatorBean
  	*/
	public PartnerLocatorBean getPartnerLocatorByName(String name) {
		if (String.isBlank(name)) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid input parameter name.'));
			return null;
		}
		String sql = PARTNER_LOCATOR_SQL_SELECT + PARTNER_LOCATOR_SQL_FROM
					+ PARTNER_LOCATOR_SQL_WHERE + PARTNER_LOCATOR_SQL_AND + PARTNER_LOCATOR_SQL_FILTER_NAME;
		for (Partner_Locator_Details__c detail : Database.query(sql)){
			return PartnerLocatorUtility.convertDetailToBean(detail);
		}
		ApexPages.addmessage(
			new ApexPages.message(ApexPages.severity.ERROR, 'Unable to locate Partner Locator Details for input parameter name - ' + name)
		);						
		return null;
	}	
  /**
  	*	Return the Select Options for the enum value 
  	*	1. Retrieve countries list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.COUNTRY
  	*	2. Retrieve industry focus list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.INDUSTRYFOCUS
  	*	3. Rrtrieve application expertise list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.APPLICATIONEXPERTISE
  	*	4. Retrieve partner tier list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.PARTNERTIER
  	*	5. Retrieve state or province list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.STATE
  	*		a. Controlling field value for Country is the expected additional/optional input parameter
  	*/
	public List<SelectOption> getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD selectOptionField
													, String optionalControllingFieldValue
													, SelectOption defaultSelectOption
												){
		if (selectOptionField.equals(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.COUNTRY)){
			return PartnerLocatorUtility.getPickListOptions(
											Partner_Locator_Details__c.Primary_Location_Country__c.getDescribe().getPicklistValues()
											, defaultSelectOption //PartnerLocatorConstants.DEFAULT_SELECT_OPTION_COUNTRY	// Include default
											, new Set<String>()			// No exclusions
										);
		}
		if (selectOptionField.equals(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.INDUSTRYFOCUS)){
			return PartnerLocatorUtility.getPickListOptions(
											Partner_Locator_Details__c.Industry_Focus__c.getDescribe().getPicklistValues()
											, defaultSelectOption // PartnerLocatorConstants.DEFAULT_SELECT_OPTION 	// Include default
											, new Set<String>()			// No exclusions
										);
		}
		if (selectOptionField.equals(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.APPLICATIONEXPERTISE)){
			return PartnerLocatorUtility.getPickListOptions(
											Partner_Locator_Details__c.Application_Expertise__c.getDescribe().getPicklistValues()
											, defaultSelectOption // PartnerLocatorConstants.DEFAULT_SELECT_OPTION 	// Include default
											, new Set<String>()			// No exclusions
										);
		}		
		if (selectOptionField.equals(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.PARTNERTIER)){
			return PartnerLocatorUtility.getPickListOptions(
											Account.Partner_Tier__c.getDescribe().getPicklistValues()
											, defaultSelectOption // NULL 					// Do not include default
											, PartnerLocatorConstants.PARTNER_TIER_EXCLUSIONS 		// Introduce the exclusion
										);
		}
		if (selectOptionField.equals(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.STATE)){
			return getStatesOrProvincesSelectOptions(optionalControllingFieldValue
													, defaultSelectOption // PartnerLocatorConstants.DEFAULT_SELECT_OPTION_STATE 	// Include default
										);
		}
		return new List<SelectOption>();
	}

	private  List<SelectOption> getStatesOrProvincesSelectOptions(String selectedCountry, SelectOption defaultOption){
		List<SelectOption> statesOrProvinces = new List<SelectOption>();
		if (defaultOption!=NULL){
			statesOrProvinces.add(defaultOption);
		}
		Map<String, List<String>> statesOrProvincesByCountry = TStringUtils.GetDependentOptions(
			PartnerLocatorConstants.PARTNER_LOCATOR_DETAILS_SOBJECT_NAME //'Partner_Locator_Details__c' //object name
			,PartnerLocatorConstants.PRIMARY_LOCATION_COUNTRY_SOBJECTFIELD_NAME //,'Primary_Location_Country__c' //controlling field name
			,PartnerLocatorConstants.PRIMARY_LOCATION_STATE_SOBJECTFIELD_NAME //,'Primary_Location_State_Province__c' //dependent field name
		);
		for (String country : statesOrProvincesByCountry.keySet()){
			if (!String.isBlank(country) && !String.isBlank(selectedCountry) && country.equals(selectedCountry) ) {
				for (String state : statesOrProvincesByCountry.get(country)){
					statesOrProvinces.add(new SelectOption(state, state));
				}
			}
		}
		return statesOrProvinces;
	}	

}