public class ZenUser{
    public String id {set;get;}
    public String email {set;get;}
    public string name {set;get;}   
    public string organizationId {set;get;}   
    
    public ZenUser(){
        //Empty Constructor
    }
    
    public ZenUser (String userId,boolean isApexTest) { 
               
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');
        
        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
       
        String url = configuration.Zendesk_URL__c + '/api/v2/users/'+userId+'.json';
        //String url = configuration.Zendesk_URL__c + '/users/'+userId+'.xml'; //Deprecated
        req.setEndpoint(url);       

        Blob headerValue = Blob.valueOf(configuration.Zendesk_Username__c+':'+configuration.Zendesk_Password__c);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        
        HTTPResponse resp;
        
        if(isApexTest){
            resp = new HTTPResponse();
            //resp.setBody('XML'); Deprecated
        	resp.setBody('{"user":{"id":256204479,"url":"https://pure.zendesk.com/api/v2/users/256204479.json","name":"Picpure001-ct0","email":"picpure001-ct0@barclays.com","created_at":"2013-09-24T15:54:49Z","updated_at":"2013-09-25T00:32:25Z","time_zone":"Pacific Time (US & Canada)","phone":"","photo":null,"locale_id":1,"locale":"en-US","organization_id":20919772,"role":"end-user","verified":false,"external_id":null,"tags":[],"alias":"","active":true,"shared":false,"shared_agent":false,"last_login_at":null,"signature":null,"details":"","notes":"","custom_role_id":null,"moderator":false,"ticket_restriction":"requested","only_private_comments":false,"restricted_agent":true,"suspended":false,"user_fields":{"array_id":null,"serial_number":null}}}');
        }else{
            resp = http.send(req);
        }  
        
        /*XML Parser Deprecated
        dom.Document doc = resp.getBodyDocument();

        dom.XmlNode [] nodes = doc.getRootElement().getChildElements(); 

        for(dom.XMLNode node :nodes){
            
            if(node.getName() == 'id') 
                this.id = node.getText();
            else if(node.getName() == 'name') 
                this.name = node.getText();
            else if(node.getName() == 'email') 
                this.email = node.getText();
            else if(node.getName() == 'organization-id') 
                this.organizationId = node.getText();
        }  
        System.debug('#######################requester: '+this.id+ ' name: ' +this.name + '('+this.organizationId+')');
        
        */
        
        JSONParser parser = JSON.createParser(resp.getBody());
        
        parser.nextToken();
        while (parser.nextToken() != null)  
        {              
            if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
            {                                                  
                while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                {  
                    if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                    {  
                        parser.nextToken();      
                        this.id = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'name'))  
                    {  
                        parser.nextToken();  
                        this.name = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'email'))  
                    {  
                        parser.nextToken();  
                        this.email = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'organization_id'))  
                    {  
                        parser.nextToken();  
                        this.organizationId = parser.getText();  
                    }                    
                    else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                    {  
                        parser.nextToken();
                        if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                            while(parser.nextToken() != JSONToken.END_ARRAY){
                                //Ignore nested Arrays
                            }
                        }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                            while(parser.nextToken() != JSONToken.END_OBJECT){
                                //Ignore nested objects
                                if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                    while(parser.nextToken() != JSONToken.END_OBJECT){
                                        //Ignore double nested objects
                                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                            while(parser.nextToken() != JSONToken.END_OBJECT){
                                                //Ignore triple nested objects
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }else{
                            //Ignore Other Tokens
                        }
                    }                               
                }                                 
            }    
        }          
    } 
}