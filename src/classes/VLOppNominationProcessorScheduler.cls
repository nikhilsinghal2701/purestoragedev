global class VLOppNominationProcessorScheduler {
   
    public static void createJobsEveryFiveMin(){
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 00', '0 00 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 05', '0 05 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 10', '0 10 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 15', '0 15 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 20', '0 20 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 25', '0 25 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 30', '0 30 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 35', '0 35 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 40', '0 40 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 45', '0 45 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 50', '0 50 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync 55', '0 55 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
    }
    
    public static void createJobEveryHour(){
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        System.schedule('Satmetrix Integration Job - Process Opportunity Nominaton Sync Hly', '0 0 * * 1-12 ? *',  New VLStatusSchedulerContextOpportunity());
    }
}