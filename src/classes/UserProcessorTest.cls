@isTest
public class UserProcessorTest
{

    static testMethod void verifyUserCreateDate()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.User_Trigger_Enabled__c = true;
        insert cs;
        
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Chatter Free User'].Id;
        
        Integer NumOfUsers = 100;
        List<User> usrs = new List<User>();
        for(Integer x=0; x<NumOfUsers; x++)
        {
            usrs.add(new User(LastName = 'Test' + String.valueOf(x),
                            FirstName = 'Emp',
                            Email = 'emptest@pure.test'  +String.valueOf(x),
                            Username = 'emptest@pure.test'  +String.valueOf(x),
                            CommunityNickName = 'emptst'+String.valueOf(x),
                            Alias = String.valueOf(x),
                            EmailEncodingKey = 'ISO-8859-1',
                            TimeZoneSidKey = 'America/New_York',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = profileId));
        }
        test.startTest();
        insert usrs;
        test.stopTest();
        for(User u: [select Id, User_Created_Date__c from User where Email like 'emptest@pure.test%' limit :NumOfUsers])
        {
            system.assertEquals(system.today(),u.User_Created_Date__c);
        }
        
    }
}