@isTest
public class AccountProcessorTest
{

     static testMethod void testSegmentSync()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Account_Segment_Sync_Enabled__c = true;
        insert cs;

        Account a = new Account(Name = 'Some Account',
                               	AnnualRevenue = 9999999999999999.00,
                               	Company_Size__c = '10000+');
        test.startTest();
        insert a;
        test.stopTest();
        Account aUpdated = [select Segment_Calculated__c, Segment__c from Account where Id =: a.Id];
        System.assertEquals(aUpdated.Segment_Calculated__c, aUpdated.Segment__c);
    }        
    
    static testMethod void updateAccountShippingAddress()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Account_Upsert_Address_Enabled__c = true;
        insert cs;
        
        
        Account a = new Account(Name = 'Some Account',
                               	ShippingStreet = '123 Main St',
                                ShippingCity = 'Washington',
                               	ShippingState = 'DC',
                               	ShippingPostalCode = '20001',
                                ShippingCountry = 'United States',
                               	Ship_To_Contact_Name__c = 'Joe',
                               	Ship_To_Contact_Phone__c = '867-5309');
        insert a;
       
        list<Address__c> shipAddr = [select Id, Type__c, Street__c, City__c, State_Province__c, Postal_Code__c, Country__c,
                              Location_Contact__c, Location_Contact_Phone__c, Primary__c
                              from Address__c where Account__c =: a.Id];
        
        system.assertEquals(1, shipAddr.size());
        system.assertEquals(a.ShippingStreet, shipAddr[0].Street__c);
        system.assertEquals(a.ShippingCity, shipAddr[0].City__c);
        system.assertEquals(a.ShippingState, shipAddr[0].State_Province__c);
        system.assertEquals(a.ShippingPostalCode, shipAddr[0].Postal_Code__c);
        system.assertEquals(a.ShippingCountry, shipAddr[0].Country__c);
        system.assertEquals(a.Ship_To_Contact_Name__c, shipAddr[0].Location_Contact__c);
        system.assertEquals(a.Ship_To_Contact_Phone__c, shipAddr[0].Location_Contact_Phone__c);
        system.assertEquals(true, shipAddr[0].Primary__c);
        
        test.startTest();
            a.Ship_To_Contact_Name__c = 'Jane';
        	a.Ship_To_Contact_Phone__c = '1-800-555-1212';
        	a.ShippingStreet = '123 Street';
        	a.ShippingCity = 'Boston';
        	a.ShippingState = 'MA';
        	a.ShippingPostalCode = '02109';
        	a.ShippingCountry = 'USA';
        	update a;
        test.stopTest();
        
        Account acctUpdated = [select a.Id, a.Ship_To_Contact_Name__c, a.Ship_To_Contact_Phone__c,
                              a.ShippingStreet, a.ShippingCity, a.ShippingState, a.ShippingPostalCode,
                              a.ShippingCountry
                              from Account a where id =: a.Id];
        
		list<Address__c> shipAddr2 = [select Id, Type__c, Street__c, City__c, State_Province__c, Postal_Code__c, Country__c,
                              Location_Contact__c, Location_Contact_Phone__c, Primary__c
                              from Address__c where Account__c =: a.Id];
        
        system.assertEquals(1, shipAddr2.size());
        system.assertEquals(acctUpdated.ShippingStreet, shipAddr2[0].Street__c);
        system.assertEquals(acctUpdated.ShippingCity, shipAddr2[0].City__c);
        system.assertEquals(acctUpdated.ShippingState, shipAddr2[0].State_Province__c);
        system.assertEquals(acctUpdated.ShippingPostalCode, shipAddr2[0].Postal_Code__c);
        system.assertEquals(acctUpdated.ShippingCountry, shipAddr2[0].Country__c);
        system.assertEquals(acctUpdated.Ship_To_Contact_Name__c, shipAddr2[0].Location_Contact__c);
        system.assertEquals(acctUpdated.Ship_To_Contact_Phone__c, shipAddr2[0].Location_Contact_Phone__c);
        system.assertEquals(true, shipAddr2[0].Primary__c);             
    }
    
    static testMethod void createAccount()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Account_Upsert_Address_Enabled__c = true;
        insert cs;
        
        
        Account a = new Account(Name = 'Some Account',
                               	ShippingStreet = '123 Main St',
                                ShippingCity = 'Washington',
                               	ShippingState = 'DC',
                               	ShippingPostalCode = '20001',
                                ShippingCountry = 'United States',
                               	Ship_To_Contact_Name__c = 'Joe',
                               	Ship_To_Contact_Phone__c = '867-5309');
        test.startTest();
        insert a;
        test.stopTest();
        
        list<Address__c> shipAddr = [select Id, Type__c, Street__c, City__c, State_Province__c, Postal_Code__c, Country__c,
                              Location_Contact__c, Location_Contact_Phone__c, Primary__c
                              from Address__c where Account__c =: a.Id];
        
        system.assertEquals(1, shipAddr.size());
        system.assertEquals(a.ShippingStreet, shipAddr[0].Street__c);
        system.assertEquals(a.ShippingCity, shipAddr[0].City__c);
        system.assertEquals(a.ShippingState, shipAddr[0].State_Province__c);
        system.assertEquals(a.ShippingPostalCode, shipAddr[0].Postal_Code__c);
        system.assertEquals(a.ShippingCountry, shipAddr[0].Country__c);
        system.assertEquals(a.Ship_To_Contact_Name__c, shipAddr[0].Location_Contact__c);
        system.assertEquals(a.Ship_To_Contact_Phone__c, shipAddr[0].Location_Contact_Phone__c);
        system.assertEquals(true, shipAddr[0].Primary__c);
        
        
    }
    
    
}