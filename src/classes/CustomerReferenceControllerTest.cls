@isTest
public class CustomerReferenceControllerTest
{

    static testMethod void testCustomerReferencePage()
    {
        setUpCustomSettings();
        
        // Create a test opportunity with exiting action item
        Opportunity testOpp = createOpportunity( 'USD', System.today() );
        
        // instantiate the controller and assert that the defaults set
        ApexPages.StandardController controller = new ApexPages.StandardController(new Opportunity());
        CustomerReferenceController crController = new CustomerReferenceController(controller);
        
        system.assert(crController.allIndustries.size()>0);
        system.assert(crController.allStages.size()>0);
        system.assert(crController.allSubDivisions.size()>0);
        system.assert(crController.getSomeCustomerReferencePickVals().size()>0);
        
        
        // execute a search that returns no results
        crController.selectedCR = 'Something';
        crController.selectedIndustry = 'Will Not Be Found';
        crController.selectedSubDivision = '--None--';
        crController.search();
        
        system.assert(crController.opptys.size() == 0);
        system.assert(crController.opptys2.size() == 0);
        
        
        // execute a search that returns the expected results
        crController.selectedCR = 'Other';
        crController.selectedIndustry = 'Test Industry';
        crController.selectedSubDivision = '--None--';
        crController.search(); 
        
		system.assert(crController.opptys.size()>0);
		system.assert(crController.opptys2.size()>0);
        
        crController.selectedOpptyObj = testOpp.Id;
        crController.requestReference();
        
        system.assert(crController.retURL.length()>0);
		        
        
        
        
        
        
        
         
    }
    
    
     public static void setUpCustomSettings()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Ref_Req_Account__c = '123456789012345';
        cs.Ref_Req_AE_Approver__c = '123456789012345';
        cs.Ref_Req_Opportunity__c = '123456789012345';
        insert cs;
    }
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account', Industry = 'Test Industry' );
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        /*
         * Id, Name, StageName, CloseDate, Competition__c, Environment__c, Environment_detail__c, ' + 
            			'OwnerId, Owner.Name, Owner.Email, Reasons_for_Loss__c, Reasons_for_Win__c, Customer_Reference__c, ' + 
            			'Enviornment_Primary_Server__c, AccountId, Account.Name, Account.Industry, ' + 
            			'SE_Opportunity_Owner__c, Business_value__c, Technical_value__c, Operational_value__c ' + 
            			'From Opportunity
         */
        
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate,
                                           	 Competition__c = 'Other',
                                             Environment__c = 'Other',
                                           	 Environment_detail__c = 'Other',
                                           	 Reasons_for_Loss__c = 'Testing',
                                             Reasons_for_Win__c = 'Testing',
                                             Customer_Reference__c = 'Other',
                                             Business_value__c = 'Testing Business Value',
                                             Technical_value__c = 'Technical Value',
                                             Operational_value__c = 'Operational Value');
        
        insert myOpp;
        return myOpp;
    }
    
}