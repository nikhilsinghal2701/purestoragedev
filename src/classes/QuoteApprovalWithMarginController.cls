public without sharing class QuoteApprovalWithMarginController
{
	public String quoteId {get;set;}
	public string debugIsInside {get;set;}
	public list<cQLI>  qliList {get;set;}
    
    //wrapper class needed to provide access to fields that are being suppressed by fls in vf
    public class cQLI
    {
        public string prodName {get;set;}
        public boolean isHardware {get;set;}
        public double quant {get;set;}
        public double sub {get;set;}
        public double listTotal {get;set;}
        public double total {get;set;}
        public double disc {get;set;}
        public string lineMargin {get;set;}
        public string currencyIsoCode {get;set;}
        
        public cQLI(QuoteLineItem inputQLI)
        {
            this.prodName = inputQLI.PricebookEntry.Name;
            this.isHardware = inputQLI.PricebookEntry.Product2.Is_Hardware__c;
            this.quant = inputQLI.Quantity;
            this.sub = inputQLI.Subtotal;
            this.listTotal = inputQLI.Total_List_Price__c;
            this.total = inputQLI.TotalPrice;
            this.disc = inputQLI.Discount_for_Approval__c;
            this.lineMargin = inputQLI.Line_Item_Margin_w_Percent__c;
            this.currencyIsoCode = inputQLI.CurrencyIsoCode;
            
        }
    }
    
        
    public list<cQLI> getMyQuoteLineItem()
    {
        if (quoteId != null) 
          {
              debugIsInside = 'inside getMyQuoteLineItem';
              qliList = new list<cQLI>();
              try 
              {
                  List<QuoteLineItem> relatedQLI = [Select Id, Line_Item_Margin_w_Percent__c, PricebookEntry.Name, Total_List_Price__c,
                                           PricebookEntry.Product2.Is_Hardware__c, Quantity, Subtotal, CurrencyIsoCode,
                                           TotalPrice, Discount_for_Approval__c,QuoteId from QuoteLineItem where QuoteId = :quoteId];
                  for(QuoteLineItem qli: relatedQLI)
                  {
                      debugIsInside = debugIsInside + ' inside forloop' + qli;
                      qliList.add(new cQLI(qli));
                      debugIsInside = debugIsInside + ' wrapper ' + qliList;   
                  }
                  return qliList;
                  system.debug('DEBUG:: qli: ' + qliList);
              }
              catch (Exception e)
              {
                  return qliList;
              }
          }
        return qliList;
    }
        
        
    public double getmyMargin() 
    {
        double myMargin = 0;  
        if (quoteId != null) 
          {
              try 
              {
                  Quote quote = [Select Id, Margin__c from Quote where Id = :quoteID];
                  system.debug('DEBUG:: quote: ' + quote);
                  if(quote.Margin__c != null)
                    myMargin = quote.Margin__c;
                      
                  return myMargin;
              } 
              catch (Exception e) 
              {
                  return myMargin;
              }        
          }
        return myMargin;
    }
            
}