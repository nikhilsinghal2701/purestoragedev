/* Test class for SC_LithiutmSSORedirectController
 * to test the functionality of fetching the customer community Id
 */

@isTest
private class SC_LithiumSSORedirectControllerTest
{
	private static testMethod void testFetchingCommunityId()
	{
		SC_LithiumSSORedirectController obj = new SC_LithiumSSORedirectController();
		system.assertNotEquals(null, obj.communityId, 'Community Id should not be null');
	}
}