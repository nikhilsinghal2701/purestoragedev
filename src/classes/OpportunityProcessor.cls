public with sharing class OpportunityProcessor {
    
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance(); 
    public static final boolean OCR_TRIGGER_ENABLED = customSettings.Oppty_Primary_Contact_Trigger_Enabled__c;
    public static final double OCR_TRIGGER_STAGE = customSettings.Oppty_Primary_Contact_Trigger_Stage__c;
    public static final string OCR_TRIGGER_MSG = customSettings.Oppty_Primary_Contact_Trigger_Msg__c;
    public static final boolean GARTNER_VR_ENABLED = customSettings.Oppty_Gartner_VR_Enabled__c;
    public static final double GARTNER_VR_AMOUNT = customSettings.Oppty_Gartner_VR_Amount__c;
    public static final string GARTNER_VR_MSG = customSettings.Oppty_Gartner_VR_Msg__c;
    public static final double GARTNER_VR_STAGE = customSettings.Oppty_Gartner_VR_Stage_Number__c;
    Public Static final Boolean OCCR_TRIGGER_ENABLED = customSettings.Oppty_Currency_Cal_Trigger_Enabled__c;
    public static final string ENV_TRIGGER_MSG = customSettings.Env_Trigger_Msg__c;
    public static final Boolean ENV_TRIGGER_ENABLED = customSettings.Env_Trigger_Enabled__c;
    
    public void processBeforeInsert( List<Opportunity> newRecs )
    {
        list<Opportunity> recsToUpdate = new list<Opportunity>();
        // The system will go through the new records and calculate the USD amount using the dated currency rate
        for( Opportunity o: newRecs )
        {
            updateCurrencyExchangeRates( o );
            recsToUpdate.add(o);
        }
		if(recsToUpdate.size()>0)
        {
            defaultShippingAddr(recsToUpdate);
        }
    }
    

    public void processAfterInsert(List<Opportunity> newRecs)
    {
        System.debug(LoggingLevel.info,'DEBUG:: processAfterInsert newRecs: ' + newRecs);
        //rule: only process inserts that are caused by a clone event.  There is no official way to determine if the record is cloned
        //but we can inspect fields populated via workflow, like System_id__c, to see if the record is cloned.  WF runs after triggers so
        //System_Id__c should be blank for new inserts and not blank during a clone
        list<Opportunity> clonedRecs = new list<Opportunity>();
        for(Opportunity o: newRecs)
        {
            if(o.System_Id__c != null)
                clonedRecs.add(o);
        }
        
        system.debug('DEBUG:: cloned recs: ' + clonedRecs);
        //checkPrimaryOCR(newRecs,true);
        if(clonedRecs.size()>0)
            checkPrimaryOCR(clonedRecs,true);
    }
    
    public void processBeforeUpdate(List<Opportunity> newRecs, List<Opportunity> oldRecs, Map<Id,Opportunity> newMap, Map<Id,Opportunity> oldMap)
    {
        System.debug(LoggingLevel.info,'DEBUG:: processBeforeUpdate newRecs: ' + newRecs);
        list<Opportunity> recsWithStageChange = new list<Opportunity>();
        list<Opportunity> recsWithStageChangeOrGartnerBlank = new list<Opportunity>();
        list<Opportunity> recsWithAcctChange = new list<Opportunity>();
        for(Opportunity o: newRecs)
        {
            //clear the Shipping_Address__c lookup
            if((oldMap.get(o.Id).AccountId != o.AccountId && oldMap.get(o.Id).Shipping_Address__c != null) ||
              	(oldMap.get(o.Id).Distributor_Account__c != o.Distributor_Account__c))
            {
                recsWithAcctChange.add(o);
            }
            
            // Validating the amount or close date to make sure they are same (if not, invoke the calculation)
            if (oldMap.get( o.Id ).Amount != o.Amount || oldMap.get( o.Id ).CloseDate != o.CloseDate)
            {
                updateCurrencyExchangeRates( o );
            }
            
            if(o.StageName != oldMap.get(o.Id).StageName)
                recsWithStageChange.add(o);
            
            if((o.StageName != oldMap.get(o.Id).StageName) || (o.Engaged_Prospect_with_Gartner__c == null && oldMap.get(o.Id).Engaged_Prospect_with_Gartner__c != null))
                recsWithStageChangeOrGartnerBlank.add(o);
            
        }
        //clear out the shipping addr
        clearShippingAddr(recsWithAcctChange);
        //only check if the stage has changed
        checkPrimaryOCR(recsWithStageChange,false);
        //only check if the stage has changed or the Gartner field has been blanked out
        checkGartnerRequiredField(recsWithStageChangeOrGartnerBlank);
        //check the Environment has been specified
        checkEnvironment(recsWithStageChange);
    }
    
    public void checkEnvironment(list<Opportunity> recsWithStageChange)
    {
        if(ENV_TRIGGER_ENABLED)
        {
            list<Opportunity> oppsToProcess = new list<Opportunity>();
            for(Opportunity o: recsWithStageChange)
            {
                if(getStageNumber(o.StageName) >= 4)
                {
                    if(o.StageName != 'Stage 8 - Closed/No Decision' && o.StageName != 'Stage 8 - Closed/ Low Capacity' && o.StageName != 'Stage 8 - Closed/ Disqualified')
                    {
                        oppsToProcess.add(o);
                    }
                }
                    
            }
            
            if(oppsToProcess.size()>0)
            {
                set<id> oppIds = new set<id>();
                map<id,Environment__c> mapOppToEnv = new map<id,Environment__c>();
                for(Opportunity o: oppsToProcess)
                {
                    oppIds.add(o.Id);
                }
                for(Environment__c e: [select Id, Opportunity__c from Environment__c where Opportunity__c in: oppIds])
                {
                    mapOppToEnv.put(e.Opportunity__c, e);
                }
                if(!mapOppToEnv.isEmpty())
                {
                    for(Opportunity o: oppsToProcess)
                    {
                        if(mapOppToEnv.get(o.Id)== null)
                            o.addError(ENV_TRIGGER_MSG);
                    }
                }
                else //map is empty so there are no related Env recs, so we should throw the alert;
                {
                    for(Opportunity o: oppsToProcess)
                    {
                        o.addError(ENV_TRIGGER_MSG);
                    }
                }
            }
        }
    }
    
    
    public void defaultShippingAddr(list<Opportunity> recsToProcess)
    {
        list<Opportunity> opptysToUpd = new list<Opportunity>();
        set<id> opptyAccountIds = new set<id>();
        map<id,id> mapAcctPrimaryAddr = new map<id,id>();
        for(Opportunity o: recsToProcess)
        {
            if(o.AccountId != null)
            {
            	opptyAccountIds.add(o.AccountId);
            }
            if(o.Distributor_Account__c != null && (o.Distributor_Account__c != o.AccountId))
            {
                opptyAccountIds.add(o.Distributor_Account__c);
            }
        }
        
        if(opptyAccountIds.size()>0)
        {
            for(Address__c addr: [select Account__c, Id from Address__c where Primary__c = true and Account__c in: opptyAccountIds])
            {
                mapAcctPrimaryAddr.put(addr.Account__c, addr.Id);
            }
            
            for(Opportunity o: recsToProcess)
            {
                if(o.Shipping_Address__c == null && o.Distributor_Account__c == null && o.AccountId != null)
                {
                	o.Shipping_Address__c = mapAcctPrimaryAddr.get(o.AccountId);
                }
                else if(o.Shipping_Address__c == null && o.Distributor_Account__c != null)
                {
                    o.Shipping_Address__c = mapAcctPrimaryAddr.get(o.Distributor_Account__c);
                }
            }
            
        }
        
        
    }
    public void clearShippingAddr(list<Opportunity> recsToProcess)
    {
        list<Opportunity> opptysToUpd = new list<Opportunity>();
        for(Opportunity o: recsToProcess)
        {
            o.Shipping_Address__c = null;
            opptysToUpd.add(o);
        }
        defaultShippingAddr(recsToProcess);
    }
    
    
    public map<string,double> getExchangeRateMap(set<string> inputCurrency)
    {
        map<string,double> retExchRates = new map<string,double>();
        for(CurrencyType ct: [select IsoCode, ConversionRate FROM CurrencyType where isActive = true and IsoCode in: inputCurrency])
        {
            retExchRates.put(ct.IsoCode, ct.ConversionRate);
        }
        system.debug('DEBUG:: retExchRates: ' + retExchRates);
        return retExchRates;
    }
    
    
    public void checkGartnerRequiredField(list<Opportunity> recsToProcess)
    {
        //use the custom setting to turn off the trigger for emergency purposes
        if(GARTNER_VR_ENABLED)
        {
            set<string> currencies = new set<string>();
            for(Opportunity o: recsToProcess)
            {
                currencies.add(o.CurrencyIsoCode);
            }
            
            map<string,double> exchRateMap = getExchangeRateMap(currencies);
            
            system.debug('DEBUG:: exchRateMap: ' + exchRateMap);
            
            for(Opportunity o: recsToProcess)
            {
                system.debug('DEBUG:: o.Engaged_Prospect_with_Gartner__c: ' + o.Engaged_Prospect_with_Gartner__c);
                system.debug('DEBUG:: o.amount: ' + o.amount);
                system.debug('DEBUG:: o.StageName: ' + o.StageName);
                system.debug('DEBUG:: o.CurrencyIsoCode: ' + o.CurrencyIsoCode);
                system.debug('DEBUG:: GARTNER_VR_STAGE: ' + GARTNER_VR_STAGE);
                if(o.Engaged_Prospect_with_Gartner__c == null && o.Amount != null && o.StageName != null && getStageNumber(o.StageName)>= GARTNER_VR_STAGE)
                {
                    if(o.CurrencyIsoCode != 'USD')
                    {
                        if ((o.Amount / exchRateMap.get(o.CurrencyIsoCode)) > GARTNER_VR_AMOUNT)
                            o.Engaged_Prospect_with_Gartner__c.addError(GARTNER_VR_MSG);
                    }
                    else
                    {
                        if(o.Amount > GARTNER_VR_AMOUNT)
                            o.Engaged_Prospect_with_Gartner__c.addError(GARTNER_VR_MSG);
                    }
                }
            }
        }
    }
    
    
    public integer getStageNumber(string inputStage)
    {
        integer retVal;
        
        if(inputStage.contains('Stage 1 -'))
            retVal = 1;
        else if(inputStage.contains('Stage 2 -'))
            retVal = 2; 
        else if(inputStage.contains('Stage 3 -'))
            retVal = 3;         
        else if(inputStage.contains('Stage 4 -'))
            retVal = 4;     
        else if(inputStage.contains('Stage 5 -'))
            retVal = 5;     
        else if(inputStage.contains('Stage 6 -'))
            retVal = 6; 
        else if(inputStage.contains('Stage 7 -'))
            retVal = 7; 
        else if(inputStage.contains('Stage 8 -'))
            retVal = 8;     
        else
            retVal = 0;
            
        return retVal;
    }
    
    public void checkPrimaryOCRfromClones(map<Id,Opportunity> recsToProcessMap,list<Opportunity> recsToProcessList)
    {
        //since this is coming from a clone we need to check if the orig record had roles; if yes, add them to the new oppty; if not, do
        //not raise an error.  PS does not want error on any inserts missing roles but does want the roles to come over if they exist
        set<Id> oldOpptyIds = new set<Id>();
        map<Id,Id> oldOpptyIdToNewOpptyId = new map<Id,Id>();
        list<OpportunityContactRole> oldOCRList = new list<OpportunityContactRole>();
        map<Id,OpportunityContactRole> newOpptyIdToOldOCRMap = new map<Id,OpportunityContactRole>();
        list<OpportunityContactRole> newOCRList = new list<OpportunityContactRole>();
        System.Debug('DEBUG::::::::::::::::::::::::::::::::::::::::::::::::::::::::::');
        //go through each eligible oppty and determine if the System_ID field was populated by wf
        //if it was populated, it is a clone because the wf always fires after triggers
        //get a list of orig/cloned opptys to process
        for(Opportunity o: recsToProcessList)
        {
            system.debug('DEBUG:: got here inside recsToProcessList for loop with o.System_Id__c = ' + o.System_Id__c);
            if(o.System_Id__c != '')
            {
                oldOpptyIds.add(o.System_Id__c);
                oldOpptyIdToNewOpptyId.put(o.System_Id__c, o.Id);
            }
        }
        
        //for each of the orig/cloned opptys get the related roles, if any and associate to the new oppty
        if(oldOpptyIds.size()>0)
        {
            system.debug('DEBUG:: got here inside oldOpptyIds if ' + oldOpptyIds.size());
            oldOCRList = [select Id, IsPrimary, Role, ContactId, OpportunityId from OpportunityContactRole where OpportunityId in: oldOpptyIds];
            if(oldOCRList.size()>0)
            {
                for(OpportunityContactRole ocr: oldOCRList)
                {
                    system.debug('DEBUG:: got here with ocrOpptyId = ' + oldOpptyIdToNewOpptyId.get(String.valueof(ocr.OpportunityId).substring(0,15)));
                    system.debug('DEBUG:: and the old System_Id = ' + oldOpptyIdToNewOpptyId);
                    //use the map of orig/cloned ids to new ids to do the update
                    if(oldOpptyIdToNewOpptyId.get(String.valueof(ocr.OpportunityId).substring(0,15)) != null)
                    {
                        OpportunityContactRole ocrToLoad = new OpportunityContactRole();
                        ocrToLoad.OpportunityId = oldOpptyIdToNewOpptyId.get(ocr.OpportunityId);
                        ocrToLoad.ContactId = ocr.ContactId;
                        ocrToLoad.Role = ocr.Role;
                        ocrToLoad.IsPrimary = ocr.IsPrimary;
                        newOCRList.add(ocrToLoad);
                    }
                }
                
                insert newOCRList;
            }
            
        }
        
    }
    
    
    public void checkPrimaryOCRfromUpdates(map<Id,Opportunity> recsToProcessMap,list<Opportunity> recsToProcessList)
    {
        map<Id, OpportunityContactRole> oppycontactroles = new map<Id, OpportunityContactRole>();
        
        //select OpportunityContactRoles for the opportunities with contact role required 
        list<OpportunityContactRole> roles = [select OpportunityId, IsPrimary 
                                              from OpportunityContactRole 
                                              //where (OpportunityContactRole.IsPrimary = True 
                                              //Dec 16, 2003 - change logic to check for any related cons, not just primaries
                                              where OpportunityContactRole.OpportunityId in :recsToProcessMap.keySet()];
        
        for (OpportunityContactRole ocr : roles) 
        {
            oppycontactroles.put(ocr.OpportunityId,ocr);
        }
               
        for (Opportunity o : recsToProcessList) 
        {
            if (!oppycontactroles.containsKey(o.id))
            {
                o.addError(OCR_TRIGGER_MSG);       
            }
        } 
    }
    
    
    public void checkPrimaryOCR(List<Opportunity> recs, boolean isClone)
    {
        
        
        //use the custom setting to turn off the trigger for emergency purposes
        if(OCR_TRIGGER_ENABLED)
        {
            
            map<Id,Opportunity> recsToProcessMap = new map<Id,Opportunity>();
            list<Opportunity> recsToProcessList = new list<Opportunity>();
            //see if the record should be excluded from the rule
            for(Opportunity o: recs)
            {
                system.debug('DEBUG:: getStageNumber: ' + getStageNumber(o.StageName));
                system.debug('DEBUG:: csetting: ' + OCR_TRIGGER_STAGE);
                if(!o.Exclude_from_Primary_Contact_Role_Rule__c && getStageNumber(o.StageName) >= OCR_TRIGGER_STAGE)
                {
                    recsToProcessMap.put(o.Id,o);
                    recsToProcessList.add(o);   
                }
            }
            
            if(isClone)
            {
                system.debug('DEBUG:: checking clones');
                checkPrimaryOCRfromClones(recsToProcessMap, recsToProcessList);
            }
            else
            {
                checkPrimaryOCRfromUpdates(recsToProcessMap, recsToProcessList);
            }
            
        }
    }
    
    //orig commenting for backup
    /*public void checkPrimaryOCR(List<Opportunity> recs)
    {
        map<Id,Opportunity> recsToProcessMap = new map<Id,Opportunity>();
        list<Opportunity> recsToProcessList = new list<Opportunity>();
        //use the custom setting to turn off the trigger for emergency purposes
        if(OCR_TRIGGER_ENABLED)
        {
            //see if the record should be excluded from the rule
            for(Opportunity o: recs)
            {
                if(!o.Exclude_from_Primary_Contact_Role_Rule__c && getStageNumber(o.StageName) >= OCR_TRIGGER_STAGE)
                {
                    recsToProcessMap.put(o.Id,o);
                    recsToProcessList.add(o);   
                }
            }
            
            //check if the primary ocr 
            map<Id, OpportunityContactRole> oppycontactroles = new map<Id, OpportunityContactRole>();

            //select OpportunityContactRoles for the opportunities with contact role required 
            list<OpportunityContactRole> roles = [select OpportunityId, IsPrimary 
                                                    from OpportunityContactRole 
                                                    //where (OpportunityContactRole.IsPrimary = True 
                                                    //Dec 16, 2003 - change logic to check for any related cons, not just primaries
                                                    where OpportunityContactRole.OpportunityId in :recsToProcessMap.keySet()];
            
            for (OpportunityContactRole ocr : roles) 
            {
                oppycontactroles.put(ocr.OpportunityId,ocr);
            }
               
            for (Opportunity o : recsToProcessList) 
            {
                if (!oppycontactroles.containsKey(o.id))
                {
                    o.addError(OCR_TRIGGER_MSG);       
                }
            }  
            
        }
    }*/
    
    
    public void updateCurrencyExchangeRates( Opportunity myOpp )
    {
        // Checking the custom setting to make sure this trigger is enabled
        if (OCCR_TRIGGER_ENABLED)
        {
            myOpp.Exch_Rate_USD__c = OpportunityExchangeProcessor.getConversionRate( myOpp.CurrencyIsoCode, myOpp.CloseDate );
            myOpp.Converted_Amount_USD__c = OpportunityExchangeProcessor.calculateOpportunityExchangeRate( myOpp.Amount, myOpp.Exch_Rate_USD__c );
        }
    }
}