@isTest
public class TaskLeadProcessorTest 
{

    static testMethod void testActivityCounter()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Lead_Status_Transitions_Enabled__c = true;
        cs.Lead_Status_Transitions_Exception_Group__c = '123455';
        cs.Lead_Status_Transitions_Go_Live_Date__c = system.now();
        insert cs;
        
        Lead myLead = new Lead();
        myLead.FirstName = 'TestFirst';
        myLead.LastName = 'TestLast';
        myLead.Email = 'testfirst@testlast.test';
        myLead.Company = 'testCo';
        
        insert myLead;
        
        Lead myLeadInsert = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Open', myLeadInsert.Status);
        
        
        /********************ADD TASK TO UPD TO WORKING 1**************************/
        test.StartTest();
        Task t1 = new Task();
        t1.WhoId = myLead.Id;
        t1.Status = 'Completed';
        insert t1;
        test.StopTest();
        Lead myLeadUpd1 = [select id, Status, Number_of_Related_Activities__c from Lead where id =: myLead.Id];
        system.assertEquals('Working 1', myLeadUpd1.Status);
        system.assertEquals(1, myLeadUpd1.Number_of_Related_Activities__c);
    }
}