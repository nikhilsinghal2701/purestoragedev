/**
 * 
 * Class: CaseTriggerHelper
 * Author: Fred
 * Created Date: 07/02/2014
 *
 */

public class CaseTriggerHelper {

    public static final String PURE_STORAGE_SUPPORT = 'PureStorage Support';

    /*
     *  1. if the case created between 6:00 pm and 9:00 am pacific time, 
     *  2. if the case created at weekend;
     *  this after insert trigger will check the on call events;
     *  send the email with the user custom email address, pull out the dynamic case fields;
     */
    public static void offworkSupport(List<Case> cases) {
        Map<Id, Case> supportCaseMap = new Map<Id, Case> ();
        for(Case supportCase : [Select Id, 
                                       CaseNumber, 
                                       Account.Name, 
                                       Subject, 
                                       Contact.Name,
                                       Contact.Phone,
                                       CreatedDate,
                                       Description,
                                       (Select CommentBody, Id from CaseComments ORDER BY CreatedDate desc LIMIT 1) 
                                       From Case Where Id IN: cases]) {
            //if(offworkSupport(supportCase.CreatedDate)) {
                supportCaseMap.put(supportCase.id, supportCase);
            //}
        }
        if(supportCaseMap.isEmpty()) {
            return;
        }
        //Map<Id, Event> onCallEventMap = new Map<Id, Event> ([Select Id, Subject, OwnerId From Event Where Subject = 'On Call' limit 50000]);
        Set<Id> ownerIdMap = new Set<Id> ();
        for(Event event: [Select Id, Subject, OwnerId From Event Where Subject = 'On Call' And StartDateTime <=: System.now() AND EndDateTime >=: System.now() limit 50000]) {
            ownerIdMap.add(event.OwnerId);
        }

        if(ownerIdMap.isEmpty()) {
          return;
        }
        List<String> toAddresses = new List<String>();
        for(User user: [Select Id, SMS_Email__c From User where IsActive = true and Id IN: ownerIdMap limit 50000]) {
            if(user.SMS_Email__c != null) {
                toAddresses.add(user.SMS_Email__c);
            }
        }
        /**
         * Email Content
         * Subject: Case.CaseNumber Case.Account.Name Case.Subject
         * Case.Contact.Name
         * Case.Contact.Phone
         * Link to Case
         * Last Case Comment
         */
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for(Case emailCase : supportCaseMap.values()) {
               Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
               email.setReplyto(Label.Pure_Storage_Support_Email);
               email.setSenderDisplayName(PURE_STORAGE_SUPPORT);
               email.setToAddresses(toAddresses);
               String subject = 'Service Cloud: ' + emailCase.CaseNumber + '  ';
               if(emailCase.Account.Name != null) {
                    subject += emailCase.Account.Name + '  ';
               }
               if(emailCase.Subject != null) {
                    subject += emailCase.Subject + '  ';
               }
               email.setSubject(subject);
               String content = '\n';
               if(emailCase.Contact.Name != null) {
                    content += 'Contact Name: ' + emailCase.contact.Name + '\n';
               }
               if(emailCase.Contact.phone != null) {
                    content += 'Contact Phone: ' + emailCase.Contact.phone + '\n';
               }
               if(emailCase.Description != null) {
                    content += 'Description: ' + emailCase.Description + '\n';
               }
               content += 'Case link: ' + 'https://' + System.URL.getSalesforceBaseUrl().getHost().remove('-api' ) + '/'+emailCase.id + '\n';
               if(emailCase.CaseComments != null && !emailCase.CaseComments.isEmpty()) {
                    content += 'Last Case Comment: ' + emailCase.CaseComments[0].CommentBody + '\n';
               }
               email.setPlainTextBody(content);
               emails.add(email);
        }
        try{
            // Emails are sent only if it production org. If you want to send it from this sandbox,
            // update the SC_ProductionOrgId with this sandbox's 18 digit org id
            if(userinfo.getOrganizationId() == system.label.SC_ProductionOrgId)
                Messaging.sendEmail(emails);
        }
        catch(EmailException e) {
                Util.SendAdminEmail('CaseTriggerHelper Send Email to On Call group failed', e.getStackTraceString() + '\n' +  e.getMessage(), Label.Help_Desk_Email);
        }

    }

    public static Boolean offworkSupport(Datetime createdDateTime) {
        String dayOfWeek = createdDateTime.format('E');
        //for the unit test only, as salesforce doesn't support to set createdDate time 
        if(Test.isRunningTest()) {
            dayOfWeek = 'Sun';
        }
        if(isWeekend(dayOfWeek)) {
            return true;
        }
        /*
        else if(createdDateTime.hour() >= 9 && createdDateTime.hour() < 18) {
            return false;
        }
        */
        return false;
    }

    public static Boolean isWeekend(String dayString) {
        if(dayString == 'Sat'){
            return true;
        }else if( dayString == 'Sun') {
            return true;
        }
        return false;
    }
    
}