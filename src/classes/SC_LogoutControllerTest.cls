/* Test Class for SC_LogoutController
 * When users clicks on sign out either in lithium or in support community, they will taken to 
 * the custom login page which is SC_Logout. The SC_LogoutController will have logic to log out the Customers, 
 * SVARs and Visitors and it will take the internal users to the internal salesforce home page.
 */

@isTest
private class SC_LogoutControllerTest
{
	// test log out funcionality of internal users
	private static testMethod void testLogoutAsInternalUser()
	{
		// load the domains custom settings data using static resource
		Test.loadData(SC_Domains__c.sObjectType, 'SC_TestClass_Domains');
		Profile p = SC_TestUtil.getProfile('System Administrator');

		// run as internal user
		system.runAs(SC_TestUtil.createUser(p))
		{
			Test.startTest();

			// set the custom logout page
			Test.setCurrentPage(Page.SC_Logout);
			ApexPages.currentPage().getParameters().put('isFromSFDC', 'true');
			SC_LogoutController obj = new SC_LogoutController();
			obj.logoutUsers();

			Test.stopTest();

			// assert that the users are taken to the home page of internal salesforce
			system.assertEquals(obj.getDomain() + system.label.SC_CommunityEmployeeRedirectURL, obj.LogoutURL, 
									'Internal users shoudl be taken to Internal Saleforce Home Page on logout from support community');
		}
	}

	// test log out funcionality of any other user
	private static testMethod void testLogoutAsCustomer()
	{
		// load the domains custom settings data using static resource
		Test.loadData(SC_Domains__c.sObjectType, 'SC_TestClass_Domains');
		Profile p = SC_TestUtil.getProfile('PS Customer Community User');

		Account acc = SC_TestUtil.createAccount(true);
		Contact con = SC_TestUtil.createContact(true, acc);

		// run as a customer
		system.runAs(SC_TestUtil.createCustomerUser(p, con))
		{
			Test.startTest();

			Test.setCurrentPage(Page.SC_Logout);
			SC_LogoutController obj = new SC_LogoutController();
			obj.logoutUsers();

			Test.stopTest();

			// assert that the customers are logged out of the community
			system.assert(obj.LogoutURL.contains('/secur/logout.jsp'), 
									'Customers shoudl be logged out of support community');
		}
	}


}