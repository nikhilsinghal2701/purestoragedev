global with sharing class SC_PopulateCCFieldsController
{   
    
    global SC_PopulateCCFieldsController(ApexPages.StandardController controller) 
    {}
    
    @RemoteAction
    global static string populateExternalEmails(string caseId)
    {
        set<string> uniqueEmails = new set<string>();
        set<Id> conIds = new set<Id>();
        string externalEmails = '';
        
        Case c = [Select Id, Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, ContactId, Contact.Email
                        from Case where Id = :caseId];
        
        // collect from Case Collaborators
        if(c.Case_Collaborator_1__c != null && c.Case_Collaborator_1__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_1__c);
        }
        if(c.Case_Collaborator_2__c != null && c.Case_Collaborator_2__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_2__c);
        }
        if(c.Case_Collaborator_3__c != null && c.Case_Collaborator_3__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_3__c);
        }
        if(c.Case_Collaborator_4__c != null && c.Case_Collaborator_4__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_4__c);
        }
        if(c.Case_Collaborator_5__c != null && c.Case_Collaborator_5__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_5__c);
        }
        if(c.Case_Collaborator_6__c != null && c.Case_Collaborator_6__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_6__c);
        }
        if(c.Case_Collaborator_7__c != null && c.Case_Collaborator_7__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_7__c);
        }
        if(c.Case_Collaborator_8__c != null && c.Case_Collaborator_8__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_8__c);
        }
        if(c.Case_Collaborator_9__c != null && c.Case_Collaborator_9__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_9__c);
        }
        if(c.Case_Collaborator_10__c != null && c.Case_Collaborator_10__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_10__c);
        }
        
        if(c.ContactId != null)
        {
            uniqueEmails.add(c.Contact.Email);
        }
        
        for(CaseTeamMember cm : [Select MemberId from CaseTeamMember where ParentId = :c.Id and TeamRole.Name = :system.label.SC_CCContactRole])
        {
            conIds.add(cm.MemberId);
        }
        
        for(Contact con : [Select Email from Contact where Id in :conIds and Email != null])
        {
            uniqueEmails.add(con.Email);
        }
        
        for(User u : [Select Email from User where Id in :conIds])
        {
            uniqueEmails.add(u.Email);
        }
        
        // form the cc email string
        for(string s : uniqueEmails)
        {
            if(externalEmails == '')
                externalEmails += s;
            else
                externalEmails += ',' + s;
        }
        
        return externalEmails;
    }
    
    @RemoteAction
    global static string populateInternalEmails(string caseId)
    {
        set<string> uniqueEmails = new set<string>();
        set<Id> userIds = new set<Id>();
        string internalEmails = '';
        
        Case c = [Select Id, OwnerId, Owner.Email from Case where Id = :caseId];
        
        if(string.valueOf(c.OwnerId).startsWith('005'))
        {
            uniqueEmails.add(c.Owner.Email);
        }
        
        for(CaseTeamMember cm : [Select MemberId from CaseTeamMember where ParentId = :c.Id 
                                                                    and (TeamRole.Name = 'Account Executive'
                                                                         or TeamRole.Name = 'Systems Engineer')])
        {
            userIds.add(cm.MemberId);
        }
        
        for(User u : [Select Email from User where Id in :userIds])
        {
            uniqueEmails.add(u.Email);
        }
        
        // form the cc email string
        for(string s : uniqueEmails)
        {
            if(internalEmails == '')
                internalEmails += s;
            else
                internalEmails += ',' + s;
        }
        
        return internalEmails;
    }
}