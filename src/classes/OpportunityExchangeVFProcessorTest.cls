@isTest( seeAllData=true )
public class OpportunityExchangeVFProcessorTest
{
    static testMethod void testOpportunityExchangeVFProcessor()
    {
        system.debug('DEBUG:: OpportunityExchangeProcessor.getConversionRate().keySet(): ' + OpportunityExchangeProcessor.getConversionRate().keySet());
        for (String isoCode : OpportunityExchangeProcessor.getConversionRate().keySet())
        {
            // Since KPW currency is inactive, we cannot create a test record for it
            //if (!isoCode.equals( 'KPW' ))
            if(isoCode.equals('EUR'))
            {
                createOpportunity( isoCode, System.today() );
            }
        }
        
        test.startTest();
        OpportunityExchangeVFProcessor testClass = new OpportunityExchangeVFProcessor();
        
        // Without filter
        //testClass.processOpportunities();
        
        // With filter
        testClass.dateFilter = System.today().format() + '-' + System.today().addDays( +1 ).format();
        testClass.processOpportunities();
        
        testClass.dateFilter = System.today().format() + '-' + System.today().addDays( +1 ).format();       
        String testDates = testClass.getFilter();
        System.assert( testClass.myConversionMapping.keySet().size() > 0 );
        System.assert( !testDates.contains( '1/1/0001-12/31/9999' ));   
        
        test.stopTest();
    }
    
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account', Support_Must_Not_Contact_End_Customer__c = true, Zendesk_Organization__c = '123' );
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate);
        
        insert myOpp;
        return myOpp;
    }
    
    
    public static void addTestProducts( Opportunity myOpp )
    {
        PricebookEntry pbe1 = createPriceBookEntry( 'Product 1', 167.12, myOpp.CurrencyIsoCode );
        PricebookEntry pbe2 = createPriceBookEntry( 'Product 2', 788.24, myOpp.CurrencyIsoCode );
        
        OpportunityLineItem oli1 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe1.Id, Quantity = 2, UnitPrice = 167.12 );
        OpportunityLineItem oli2 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe2.Id, Quantity = 3, UnitPrice = 788.24 );
        
        insert oli1;
        insert oli2;
    }
    
    
    public static PricebookEntry createPriceBookEntry( String productName, Decimal price, String currencyCode )
    {
        Product2 myProduct = new Product2(Name = productName);
        insert myProduct;

        PricebookEntry pbe = new PricebookEntry( isActive = true, Product2Id = myProduct.Id, Pricebook2Id = getStandardPriceBookID(), CurrencyIsoCode = currencyCode, UnitPrice = price );
        insert pbe;
        
        return pbe;
    }
    
    
    public static ID getStandardPriceBookID()
    {
        return [select Id from Pricebook2 where isActive = true and isStandard = true limit 1].ID;
    }
}