/*
Class: AccountTriggerHelper
Author: Jaya
Created Date: 11/20/2013
*/
public class AccountTriggerHelper 
{

	public void afterupdateAction(List<Account> accountList, Map<Id,Account> accountOldMap)
	{
		
		List<Approval.ProcessSubmitRequest> approvalRequestList = new List<Approval.ProcessSubmitRequest>();
    	Approval.ProcessSubmitRequest req;
     	List<Approval.ProcessResult> approvalResults;
     	Map<Id, ProcessInstance> pendingPIMap = New Map<id, ProcessInstance>();
     	
     	List<Account> acctList = new List<Account>();
		List<Id> acctIdList = new List<Id>();
		for(Account acct: accountList)
		{
			Account oldAcct = new Account();
			oldAcct = accountOldMap.get(acct.Id);
			if(acct.PRM_Address_Approval__c && !oldAcct.PRM_Address_Approval__c)
			{
				acctList.add(acct);	
				acctIdList.add(acct.Id);			
			}
		}
		if(!acctList.isEmpty())
		{
			Map<Id,ProcessInstance> targetObjPI = new Map<Id,ProcessInstance>();
			for(ProcessInstance pInstance : [SELECT Id,TargetObjectID FROM ProcessInstance 
								WHERE  TargetObjectId in :acctIdList AND Status=:'Pending'])
			{
				targetObjPI.put(pInstance.TargetObjectID, pInstance);
			}
			//Approval req creation
			for(Account acct : acctList)
			{
				if(targetObjPI.get(acct.Id) == null)
				{
					req = new Approval.ProcessSubmitRequest();
		            req.setComments('Submitting request for approval.');
		            req.setObjectId(acct .Id);
		            approvalRequestList.add(req);
				}
				System.debug('account:'+acct);
			}
			
			// Approval submission
			if(approvalRequestList.size() > 0)
            {
             System.debug('************ Approvalid2: ************ ' +approvalRequestList);
             approvalResults = Approval.process(approvalRequestList);
             System.debug('approvalResults:'+approvalResults);
            }
				
		}
	}
}