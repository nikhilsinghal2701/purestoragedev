/**
* Class: EmailMessageTriggerHelper
* Author: Fred
* Created Date: 06/27/2014
* Description: A helper class when EmailMessage trigger fires
*/
public without sharing class EmailMessageTriggerHelper {
    

    public static final String CASE_EMAIL_ALIAS = 'daizhixia@gmail.com';

    public static final String PID_CUSTOMER_COMMUNITY_LOGIN = 'PID_Customer_Community_Login';

    public static final String SYSTEM_ADMINISTRATOR = 'System Administrator';

    public static final String PURE_STORAGE_SUPPORT = 'Purestorage Support';
    //from custom setting
    //public static ContactEmail__c caseEmailAlias = ContactEmail__c.getInstance('Case Email Contact');

    /*
        when new Email message get inserted, will check the case owner, 
        if the owner is out of office, will send out email to the case alias;
        1. create a dummy contact and put case email alias value in the contact email;
        2. create a custom setting, put the contact id in the custom setting;
     */
    public static void eventProcess(List<EmailMessage> emailMessages) {

        List<UserLicense> licenses = [Select Id, LicenseDefinitionKey from UserLicense WHERE LicenseDefinitionKey =: PID_CUSTOMER_COMMUNITY_LOGIN limit 1];
        
        // Sorna: added SVARs to the query
        Map<Id, User> customUsers = new Map<Id, User>([Select Id, email, Contact.email FROM User where (User.Profile.UserLicenseId =: licenses[0].id or Lithium_Role__c = :system.label.SC_LithiumRole_SVAR)
                                                                              AND ContactId != null
                                                                              AND IsActive = true ]);
        Set<String> fromEmail = new Set<String> ();
        for(User user: customUsers.values()){
            fromEmail.add(user.Contact.email);
        }
        Set<Id> caseId = new Set<Id>();
        for(EmailMessage emailMessage : emailMessages) {
            //support user create a email message 
            if(emailMessage.Incoming && fromEmail.contains(emailMessage.FromAddress)) {
                caseId.add(emailMessage.parentId);
            }
        }
        if(caseId.isEmpty()) {
            return;
        }
        Map<Id, Id> caseOwnerMap = new Map<Id, Id> ();

        for(Case eventCase :[Select Id, ownerId FROM Case where Id IN: caseId limit 50000]) {
            if(string.valueOf(eventCase.ownerId).startsWith('005'))
                caseOwnerMap.put(eventCase.Id, eventCase.ownerId);          
        }

        if(caseOwnerMap == null || caseOwnerMap.isEmpty()) {
            return;
        }
        System.debug('case owner map is ...' + caseOwnerMap);
        Set<Id> eventOwnerId = new Set<Id> ();
        Set<Id> businessOwnerId = new Set<Id> ();
        for(Event event: [Select Id, ownerId, Subject From Event where ownerId IN: caseOwnerMap.values() And (ShowAs = 'OutOfOffice' OR Subject = 'Business Hours') AND StartDateTime <=: System.now() AND EndDateTime >=: System.now()  limit 50000]) {
            if(event.Subject == 'Business Hours') {
                businessOwnerId.add(event.ownerId);
            }else {
                eventOwnerId.add(event.ownerId);
            }
        }

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<EmailTemplate> templates = [Select Id from EmailTemplate where DeveloperName = 'Out_of_the_office_Case_Notification' limit 1];
        for(Id id : caseOwnerMap.keySet()) {
            if(eventOwnerId.contains(caseOwnerMap.get(id)) || !businessOwnerId.contains(caseOwnerMap.get(Id))) {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setReplyto(Label.Pure_Storage_Support_Email);
                email.setSenderDisplayName(PURE_STORAGE_SUPPORT);
                email.setTargetObjectId(Label.Case_Contact_Email);
                email.setWhatId(id);
                email.setTemplateId(templates[0].id);
                email.saveAsActivity = false;
                emails.add(email);
            }
        }
        try{
            // Emails are sent only if it production org. If you want to send it from this sandbox,
            // update the SC_ProductionOrgId with this sandbox's 18 digit org id
            if(userinfo.getOrganizationId() == system.label.SC_ProductionOrgId)
                Messaging.sendEmail(emails);
        }
        catch(EmailException e) {
                Util.SendAdminEmail('EmailMessageTriggerHelper Send Email to Case Alias failed', e.getStackTraceString() + '\n' +  e.getMessage(), Label.Help_Desk_Email);
        }
    }

    public static void sendAdminEmail(String sSubject, String sMessage, string adminEmail) {
        if (sMessage != '') {
               List<String> toAddresses = new list<String>();
               toAddresses.add(adminEmail);
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               mail.setToAddresses(toAddresses);
               mail.setSubject(sSubject);
               mail.setPlainTextBody(sMessage);
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
    }
}