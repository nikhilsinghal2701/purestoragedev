/* Test Class for SC_SendRMA
 * to test the Send RMA functionality
 */

@isTest
private class SC_SendRMAControllerTest
{
    // test the RMA functionality from start to end
    private static testMethod void testSendRMA()
    {
        // test data of parts matrix
        Test.loadData(Parts_Matrix__c.sObjectType, 'SC_TestClass_PartsMatrix');
        // test data for sla types
        Test.loadData(SC_Send_RMA_SLA_Types__c.sObjectType, 'SC_TestClass_SLATypes');

        // create test data
        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        // setup a source support contact whom the RMA email will be sent
        Contact sourceSupportContact = SC_TestUtil.createSourceSupportContact();

        // test Parent Array
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        // child assets for the parent array - 2 controllers and 2 shelves
        List<Asset> childAssets = new List<Asset>();
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0010-00', 'CT-0',
                                                        'Controller 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSCONTROLLER0'));
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0010-01', 'CT-1',
                                                        'Controller 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSCONTROLLER1'));
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0013-00', 'SH-0',
                                                        'Shelf 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSSHELF0'));
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0022-00', 'SH-1',
                                                        'Shelf 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSSHELF1'));
        insert childAssets;

        // open a ticket for the parent array
        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        // run the complete RMA process as a TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            // set the current page as Send RMA Page
            Test.setCurrentPage(Page.SC_SendRMA);
            ApexPages.currentPage().getParameters().put('id', c.Id);
            SC_SendRMAController obj = new SC_SendRMAController(new ApexPages.StandardController(c));

            // when the page loads, there should 4 options in the components group which are basically the 4 child assets
            system.assertEquals(5, obj.assetGroups.size(), 'There should be 4 child assets in the picklist with one --None-- value');

            // choose the first controller: CT-0
            obj.selectedGroup = obj.assetGroups[1].getValue();
            obj.showComponents();
            // get the list of size of all the components of CT-0 - used later to assert
            Integer controller0_size = obj.componentsList.size();
            // assert the sla type and the default sla for CT-0
            system.assertEquals(2, obj.deliveryOptions.size(), 'There should be 2 SLA types in the picklist');
            system.assertEquals(obj.deliveryOptions[0].getValue(), obj.selectedDelivery, '4 Hours should be the default SLA Type');

            // choose the first component of CT-0
            obj.componentsList[0].IsSelected = true;
            obj.addSelected();
            // the selected component should be removed from available table and added to selected table
            system.assertEquals(controller0_size - 1, obj.componentsList.size(),'Selected Component should be removed from the available table');
            system.assertEquals(1, obj.selectedComponentsList.size(),'Selected Component should be added to the selected table');

            // choose the second controller: CT-1
            obj.selectedGroup = obj.assetGroups[2].getValue();
            obj.showComponents();
            // get the list of size of all the components of CT-1 - used later to assert
            Integer controller1_size = obj.componentsList.size();
            system.assertEquals(2, obj.deliveryOptions.size(), 'There should be 2 SLA types in the picklist');
            system.assertEquals(obj.deliveryOptions[0].getValue(), obj.selectedDelivery, '4 Hours should be the default SLA Type');

            // choose the first component of CT-1
            obj.componentsList[0].IsSelected = true;
            obj.addSelected();
            // the selected component should be removed from available table and added to selected table
            system.assertEquals(controller1_size - 1, obj.componentsList.size(),'Selected Component should be removed from the available table');
            system.assertEquals(2, obj.selectedComponentsList.size(),'Selected Component should be added to the selected table');

            // choose the first shelf: SH-0
            obj.selectedGroup = obj.assetGroups[3].getValue();
            obj.showComponents();
            // get the list of size of all the components of SH-0 - used later to assert
            Integer shelf0_size = obj.componentsList.size();
            system.assertEquals(2, obj.deliveryOptions.size(), 'There should be 2 SLA types in the picklist');
            system.assertEquals(obj.deliveryOptions[0].getValue(), obj.selectedDelivery, '4 Hours should be the default SLA Type');

            // choose the first component of SH-0
            obj.componentsList[0].IsSelected = true;
            obj.addSelected();
            // the selected component should be removed from available table and added to selected table
            system.assertEquals(shelf0_size - 1, obj.componentsList.size(),'Selected Component should be removed from the available table');
            system.assertEquals(3, obj.selectedComponentsList.size(),'Selected Component should be added to the selected table');

            // choose the second shelf: SH-1
            obj.selectedGroup = obj.assetGroups[4].getValue();
            obj.showComponents();
            // get the list of size of all the components of SH-1 - used later to assert
            Integer shelf1_size = obj.componentsList.size();
            system.assertEquals(2, obj.deliveryOptions.size(), 'There should be 2 SLA types in the picklist');
            system.assertEquals(obj.deliveryOptions[0].getValue(), obj.selectedDelivery, '4 Hours should be the default SLA Type');

            obj.changeDelivery();

            // choose all the components of SH-1 - the reason is to test the PartsMatrixSort functionality - which means
            // the selected tables list has a custom sort functionality
            for(Integer i=0; i < obj.componentsList.size(); i++)
            {
                obj.componentsList[i].IsSelected = true;
            }
            obj.addSelected();
            // all components of SH-1 should be removed from available table and added to selected table
            system.assertEquals(0, obj.componentsList.size(),'Selected Component should be removed from the available table');
            system.assertEquals(shelf1_size + 3, obj.selectedComponentsList.size(),'Selected Component should be added to the selected table');

            // test the remove selection functionality
            obj.selectedComponentsList[0].item1.IsSelected = true;
            obj.removeSelected();
            system.assertEquals(shelf1_size +2,  obj.selectedComponentsList.size(),'Removed Component should be removed from the selected table');

            obj.selectedGroup = obj.assetGroups[1].getValue();
            obj.showComponents();
            system.assertEquals(controller0_size, obj.componentsList.size(), 'Removed Component should be added back to the available table');

            // set other flags
            obj.hwEscalated = true;
            obj.onsite = true;

            // navigate to second screen where the TSE is shown a drafted email of all parts he has chosen
            obj.nextStep();

            // assert the to address and other flags
            system.assertEquals(sourceSupportContact.Email, obj.toAddress, 'To Address of the email should be source support');
            system.assertEquals(false, obj.isStep1);
            system.assertEquals(true, obj.isStep2);
            system.assertEquals(false, obj.isEditEmail);

            // try navigating between 2 screens
            obj.goBackToStep1();
            system.assertEquals(true, obj.isStep1);
            system.assertEquals(false, obj.isStep2);

            // try editing the drafted email
            obj.nextStep();
            obj.editEmail();
            system.assertEquals(true, obj.isEditEmail);

            obj.cancelEdit();
            system.assertEquals(false, obj.isEditEmail);

            // send email
            obj.sendEmail();
            // assert the success flag and success msg
            system.assertEquals(true, obj.isEmailSuccess, 'Email should successfully sent to source support');
            system.assertEquals(system.label.SC_SUCCESS_MSG, obj.result, 'Success Message should be displayed to the user');

            Test.stopTest();
        }
    }

    // test the custom comparision functionality to compare 2 diff versions - the reason for 
    // a custom functionaity is version can be 3.1.2
    private static testMethod void testComparisionMethodsAndNoAssetMsg()
    {
        SC_SendRMAController obj = new SC_SendRMAController(new ApexPages.StandardController(new Case()));
        system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_NO_ARRAY_FOUND), 
                                    'No Array found message should be added to the page');

        // test the greater or equal method
        system.assertEquals(true, obj.isGreaterOrEqual('3.1.2', '3.1.1'), '3.1.2 is greater than 3.1.1');
        system.assertEquals(true, obj.isGreaterOrEqual('3.1.2', '3.1'), '3.1.2 is greater than 3.1');
        system.assertEquals(true, obj.isGreaterOrEqual('3.1.0', '3.1.0'), '3.1.0 is equal to 3.1.0');
        system.assertEquals(false, obj.isGreaterOrEqual('3.1.0', '3.1.1'), '3.1.0 is less than 3.1.1');
        system.assertEquals(false, obj.isGreaterOrEqual('3.1', '3.1.6'), '3.1 is less than 3.1.6');

        // test the lesser or equal method
        system.assertEquals(false, obj.isLesserOrEqual('3.1.2', '3.1.1'), '3.1.2 is greater than 3.1.1');
        system.assertEquals(false, obj.isLesserOrEqual('3.1.2', '3.1'), '3.1.2 is greater than 3.1');
        system.assertEquals(true, obj.isLesserOrEqual('3.1.0', '3.1.0'), '3.1.0 is equal to 3.1.0');
        system.assertEquals(true, obj.isLesserOrEqual('3.1.0', '3.1.1'), '3.1.0 is less than 3.1.1');
        system.assertEquals(true, obj.isLesserOrEqual('3.1', '3.1.6'), '3.1 is less than 3.1.6');       
    }

    // test the validation error msg
    private static testMethod void testNoChildAssetFoundMsg()
    {
        // load test data
        Test.loadData(Parts_Matrix__c.sObjectType, 'SC_TestClass_PartsMatrix');
        Test.loadData(SC_Send_RMA_SLA_Types__c.sObjectType, 'SC_TestClass_SLATypes');

        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Contact sourceSupportContact = SC_TestUtil.createSourceSupportContact();

        // create the array with no child assets
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');

        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_SendRMA);
            ApexPages.currentPage().getParameters().put('id', c.Id);
            SC_SendRMAController obj = new SC_SendRMAController(new ApexPages.StandardController(c));

            // as there are no child assets, there is nothing to replace
            system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_NO_CHILDASSET_FOUND), 
                                    'No Child Asset found message should be added to the page');

            Test.stopTest();
        }
    }

    // test validation error msgs
    private static testMethod void testNoPartsMatrixFoundMsg()
    {
        // load test data
        Test.loadData(Parts_Matrix__c.sObjectType, 'SC_TestClass_PartsMatrix');
        Test.loadData(SC_Send_RMA_SLA_Types__c.sObjectType, 'SC_TestClass_SLATypes');

        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Contact sourceSupportContact = SC_TestUtil.createSourceSupportContact();

        // create parent array and child assets
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');

        List<Asset> childAssets = new List<Asset>();
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0010-00', 'CT-0',
                                                        'Controller 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSCONTROLLER0'));
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0010-01', 'CT-1',
                                                        'Controller 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSCONTROLLER1'));
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, '80-0013-00', 'SH-0',
                                                        'Shelf 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSSHELF0'));
        // create a child asset with a platform PN that is not existing in the loaded parts matrix
        childAssets.add(SC_TestUtil.createChildAsset(false, parentArray, acc, 'TESTPN', 'SH-1',
                                                        'Shelf 24/7 4HR 1 Year Domestic Support',
                                                        'TESTCLASSSHELF1'));
        insert childAssets;

        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_SendRMA);
            ApexPages.currentPage().getParameters().put('id', c.Id);
            SC_SendRMAController obj = new SC_SendRMAController(new ApexPages.StandardController(c));

            // system should capture the missing platform no and notify the user
            system.assert(Apexpages.getMessages().size() == 1, 
                                    'No Parts Matrix found message should be added to the page');

            Test.stopTest();
        }
    }
}