public without sharing class ContactDeletionProcessor 
{
	public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static string HIDDEN_ACCT_ID = customSettings.Hidden_Account_Id__c;
    public static string HIDDEN_OWNER_ID = customSettings.Hidden_Owner_Id__c;
    public static boolean HIDE_AND_DEACTIVATE_USER = customSettings.Hide_And_Deactivate_User__c;
    
    public static void contactSoftUndelete(list<Contact> updContactList, map<id,Contact> updContactMap)
    {
        system.debug('DEBUG:: contact undelete this contact: ' + updContactList);
        system.debug('DEBUG:: contact undelete this contact old version: ' + updContactMap);
        list<Contact> contToUndelete = new list<Contact>();
        for(Contact c: updContactList)
        {
            if(c.Delete__c == false && c.Delete__c != updContactMap.get(c.Id).Delete__c)
            {
                system.debug('DEBUG:: c.Delete__c is: ' + c.Delete__c);
                system.debug('DEBUG:: c.Delete__c was: ' + updContactMap.get(c.Id).Delete__c);
                if(c.Prior_Account__c != null)
                	c.AccountId = c.Prior_Account__c;
                
                if(c.Prior_Owner__c != null)
                    c.OwnerId = c.Prior_Owner__c;
                
                c.Prior_Account__c = null;
                c.Prior_Owner__c = null;
                
                contToUndelete.add(c);
            }
        }
        /*if(contToUndelete.size()>0)
        	update contToUndelete;*/
    }
    
    public static void executeContactSoftDeletion()
    {
		list<Contact> contToUpd = new list<Contact>();
        set<id> contUserIdsToInactivate = new set<id>();
        User backupUsr = [select Id from User where Profile.Name = 'System Administrator' and isActive = true limit 1];
        
        for(Contact c: [select Id, AccountId, Prior_Account__c, Delete__c, Prior_Owner__c, OwnerId 
                        from Contact 
                        where Delete__c = true 
                        and AccountId !=: HIDDEN_ACCT_ID
                        limit 10000])
        {
            c.Prior_Account__c = c.AccountId;
            c.Prior_Owner__c = c.OwnerId;
            if(HIDDEN_ACCT_ID != null)
            {
            	c.AccountId = HIDDEN_ACCT_ID;
            }
            else
            {
                c.AccountId = null;
            }
            if(HIDDEN_OWNER_ID != null)
            {
                c.OwnerId = HIDDEN_OWNER_ID;
            }
            else
            {
                c.OwnerId = backupUsr.Id;
            }
            contToUpd.add(c);
            contUserIdsToInactivate.add(c.Id);
        }
        
        update contToUpd;
        
        if(contUserIdsToInactivate.size() > 0 && HIDE_AND_DEACTIVATE_USER)
        {
            inactivateRelatedContactUser(contUserIdsToInactivate);
        }
    }
    
    @future
    public static void inactivateRelatedContactUser(set<id> contUserIds)
    {
        list<User> usersToInactivate = new list<User>();
        
        for(User u: [select Id from User where ContactId in: contUserIds limit 10000])
        {
        	u.IsActive = false;    
            usersToInactivate.add(u);
        }
        
        if (usersToInactivate.size()>0)
            update usersToInactivate;
    }
}