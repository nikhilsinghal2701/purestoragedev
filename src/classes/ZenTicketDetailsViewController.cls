public class ZenTicketDetailsViewController {

    public ZenTicket ticket {get;set;}
    private Map<String,ZenUser> usersMap;
    
    private static String SERVICE_JSON_URL; 
    private static String SERVICE_XML_URL; 
    private static String USERNAME;
    private static String PASSWORD;
    
    public boolean isApexTest = false;
    
    public ZenTicketDetailsViewController(){
        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
                      
        SERVICE_XML_URL = configuration.Zendesk_URL__c; 
        SERVICE_JSON_URL = configuration.Zendesk_URL__c + '/api/v2';       
        USERNAME = configuration.Zendesk_Username__c;
        PASSWORD = configuration.Zendesk_Password__c;
        
        usersMap = new Map<String,ZenUser>{};
        
        if(SERVICE_XML_URL == 'http://testURL.com') this.isApexTest = true;
        
        this.ticket = loadTicket(); 
        loadComments();  //Requires for API V2 - JSON
    }

     private void loadComments() {     
        
        List<ZenComment> comments = new List<ZenComment>();
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');
 
        String url = SERVICE_JSON_URL + '/tickets/'+this.ticket.id+'/comments.json';
        req.setEndpoint(url);       

        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
		HTTPResponse resp;
        if(isApexTest){
            resp = new HTTPResponse();
            resp.setBody('{"comments":[{"id":8145277449,"type":"Comment","author_id":268009639,"body":"test","html_body":"test","public":true,"trusted":true,"attachments":[],"via":{"channel":"email","source":{"from":{"address":"den2purea01-ct0@photobucket.com","name":"DEN2PUREA01-ct0","original_recipients":["den2purea01-ct0@photobucket.com","james@purestorage.com","support@purestorage.com","atelles@purestorage.com"]},"to":{"address":"support@purestorage.com","name":"Pure Storage Support"},"rel":null}},"metadata":{"system":{"message_id":"<20131029021618.CB66022005A@DEN2PUREA01-ct0>","raw_email_identifier":"71699/caebb13624759835071f3c08690d18a08b9cd8d5-7431.eml"},"custom":{},"suspension_type_id":null},"created_at":"2013-10-29T02:16:03Z"},{"id":8274846734,"type":"Comment","author_id":261594844,"body":"This request was closed and merged into request #11185  Photobucket - unable to phonehome ","html_body":"<p>This request was closed and merged into request &quot;Photobucket - unable to phonehome&quot;.</p>","public":false,"trusted":true,"attachments":[],"via":{"channel":"system","source":{"to":{},"from":{"ticket_id":11185,"subject":"Photobucket - unable to phonehome"},"rel":"merge"}},"metadata":{"system":{"location":"Ogden, UT, United States","latitude":41.24709999999999,"longitude":-112.1316},"custom":{}},"created_at":"2013-10-29T02:19:20Z"}],"next_page":null,"previous_page":null,"count":2}'); 
        }else{
            resp = http.send(req);
        } 
         
        JSONParser parser = JSON.createParser(resp.getBody());
        
         while (parser.nextToken() != null)  
        {  
            if (parser.getCurrentToken() == JSONToken.START_ARRAY)  
            {  
               
                while (parser.nextToken() !=  JSONToken.END_ARRAY)  
                {  
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
                    {  
                          
                        ZenComment comment = new ZenComment();  
                        
                        while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                        {  
                                if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                                {  
                                    parser.nextToken();      
                                    comment.id= parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'created_at'))  
                                {  
                                    parser.nextToken();  
                                    comment.created_at = parser.getText();  
                                }
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'author_id'))  
                                {  
                                    parser.nextToken();  
                                    comment.author = parser.getText();  
                                }  
                            	else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'html_body'))  
                                {  
                                    parser.nextToken();  
                                    comment.html_body = parser.getText();  
                                }
                           		else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'public')) 
                                {
                                    parser.nextToken();
                                    comment.isPublic = parser.getText();  
                                }                                
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'type')) 
                                {
                                    parser.nextToken();
                                    comment.type = parser.getText();  
                                }   
                            	else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                                {  
                                    parser.nextToken();
                                    if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                        while(parser.nextToken() != JSONToken.END_ARRAY){
                                            //Ignore nested Arrays
                                        }
                                    }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                            //Ignore nested objects
                                            if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                while(parser.nextToken() != JSONToken.END_OBJECT){
                                                    //Ignore double nested objects
                                                    if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                                            //Ignore triple nested objects
                                                        }
                                                    }
                                                }
                                            }
                                        
                                        }
                                    }else{
                                        //Ignore Other Tokens
                                    }
                                }
                        } 
                        if(comment.isPublic == 'true')
                        {
                            comments.add(comment);
                        }
                    }  
                }  
            }  
        }
        ticket.comments = comments;  
    }
    
    public ZenTicket loadTicket() {    
    
        ZenTicket ticket = new ZenTicket();
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');
        
        String ticketNumber = System.currentPagereference().getParameters().get('id');
        
        String url = SERVICE_JSON_URL + '/tickets/'+ ticketNumber +'.json';
        //String url = SERVICE_XML_URL + '/tickets/'+ ticketNumber +'.xml';
        req.setEndpoint(url);       

        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        
        HTTPResponse resp;
        
        if(isApexTest){
            resp = new HTTPResponse();
            resp.setBody('{"ticket":{"url":"https://pure.zendesk.com/api/v2/tickets/11200.json","id":11200,"external_id":null,"via":{"channel":"email","source":{"from":{"address":"den2purea01-ct0@photobucket.com","name":"DEN2PUREA01-ct0"},"to":{"address":"support@purestorage.com","name":"Pure Storage Support"},"rel":null}},"created_at":"2013-10-29T02:16:03Z","updated_at":"2013-10-29T02:19:20Z","type":"incident","subject":"Phonehome failed on DEN2PUREA01-ct0","description":"test","priority":null,"status":"closed","recipient":"support@pure.zendesk.com","requester_id":268009639,"submitter_id":268009639,"assignee_id":373470663,"organization_id":22983719,"group_id":128914,"collaborator_ids":[250249525,243533459],"forum_topic_id":null,"problem_id":null,"has_incidents":false,"due_at":null,"tags":["arraysystem","closed_by_merge"],"custom_fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":""},{"id":21171050,"value":""},{"id":20007202,"value":""},{"id":20010486,"value":""},{"id":20520488,"value":null},{"id":22064668,"value":""},{"id":20018901,"value":null},{"id":21666866,"value":""},{"id":22064678,"value":""},{"id":21092354,"value":null},{"id":21101625,"value":null},{"id":21072274,"value":""},{"id":20172612,"value":null}],"satisfaction_rating":null,"sharing_agreement_ids":[],"fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":""},{"id":21171050,"value":""},{"id":20007202,"value":""},{"id":20010486,"value":""},{"id":20520488,"value":null},{"id":22064668,"value":""},{"id":20018901,"value":null},{"id":21666866,"value":""},{"id":22064678,"value":""},{"id":21092354,"value":null},{"id":21101625,"value":null},{"id":21072274,"value":""},{"id":20172612,"value":null}],"followup_ids":[],"ticket_form_id":null}}'); 
        }else{
            resp = http.send(req);
        }
        // JSON Parser
        JSONParser parser = JSON.createParser(resp.getBody());
        
        parser.nextToken();
        while (parser.nextToken() != null)  
        {              
            if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
            {                                                  
                while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                {  
                    if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                    {  
                        parser.nextToken();      
                        ticket.id= parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'subject'))  
                    {  
                        parser.nextToken();  
                        ticket.subject = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'description'))  
                    {  
                        parser.nextToken();  
                        ticket.description = parser.getText();  
                    } 
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'type'))  
                    {  
                        parser.nextToken();  
                        ticket.type = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'status'))  
                    {  
                        parser.nextToken();  
                        ticket.status = parser.getText();  
                    } 
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'priority'))  
                    {  
                        parser.nextToken();  
                        ticket.priority = parser.getText();  
                    } 
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'updated_at'))  
                    {  
                        parser.nextToken();  
                        ticket.updated_at = parser.getText();  
                    }  
                    else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'created_at'))  
                    {  
                        parser.nextToken();  
                        ticket.created_at = parser.getText();  
                    }
                    else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                    {  
                        parser.nextToken();
                        if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                            while(parser.nextToken() != JSONToken.END_ARRAY){
                                //Ignore nested Arrays
                            }
                        }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                            while(parser.nextToken() != JSONToken.END_OBJECT){
                                        //Ignore nested objects
                                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                            while(parser.nextToken() != JSONToken.END_OBJECT){
                                                //Ignore double nested objects
                                                if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                    while(parser.nextToken() != JSONToken.END_OBJECT){
                                                        //Ignore triple nested objects
                                                    }
                                                }
                                            }
                                        }
                                    
                                    }
                        }else{
                            //Ignore Other Tokens
                        }  
                    }                               
                } 
                                 
            }    
        } 
         
        
        /*
        //XML Parsing loads ticket with comments no longer supported. - Ignore      
        dom.Document doc = resp.getBodyDocument();

        dom.XmlNode [] nodes = doc.getRootElement().getChildElements(); 

        for(dom.XMLNode node :nodes){

            if(node.getName() == 'nice-id') 
                ticket.id= node.getText();
            else if(node.getName() == 'subject') 
                ticket.subject = node.getText();
            else if(node.getName() == 'description') 
                ticket.description = node.getText();    
            else if(node.getName() == 'ticket-type-id') 
                ticket.type = node.getText();
            else if(node.getName() == 'status-id') 
                ticket.status = node.getText();
            else if(node.getName() == 'priority-id') 
                ticket.priority = node.getText();
            else if(node.getName() == 'assignee-id'){
                if(!this.usersMap.containsKey(node.getText()))
                    usersMap.put(node.getText(),new ZenUser(node.getText(),isApexTest));
                ticket.assignee = (usersMap.get(node.getText())).name; 
            }    
            else if(node.getName() == 'updated-at') 
                ticket.updated_at = node.getText();
            else if(node.getName() == 'created-at') 
                ticket.created_at = node.getText();
            else if (node.getName() == 'comments'){
                ticket.comments = new List<ZenComment>();
                dom.XmlNode [] commentNodes = node.getChildElements();
                for(dom.XMLNode commentNode :commentNodes){
                    ZenComment comment = new ZenComment();
                    dom.XmlNode [] commentNodeItems = commentNode.getChildElements();
                    for(dom.XMLNode commentNodeItem :commentNodeItems){
                        if(commentNodeItem.getName() == 'author-id'){
                            if(!this.usersMap.containsKey(commentNodeItem.getText()))
                                usersMap.put(commentNodeItem.getText(),new ZenUser(commentNodeItem.getText(),isApexTest));
                            comment.author = (usersMap.get(commentNodeItem.getText())).name;                      
                        }else if(commentNodeItem.getName() == 'created-at')
                            comment.created_at = commentNodeItem.getText();
                        else if(commentNodeItem.getName() == 'value')
                            comment.value = commentNodeItem.getText();
                        else if(commentNodeItem.getName() == 'is-public')
                            comment.isPublic = commentNodeItem.getText();
                    }
                    if (comment.isPublic == 'true')
                        ticket.comments.add(comment);
                 }
             }
        }
		*/
        return ticket;
    }
}