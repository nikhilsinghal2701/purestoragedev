global class ScheduleApprovalReminderEmails implements Schedulable {
 	
	global void execute(SchedulableContext ctx) {   
		ApprovalReminderHelper.executeQuotesReminders(); 
	}
}