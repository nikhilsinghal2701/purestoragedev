@isTest
public class OpportunityWelcomeKitControllerTest
{
    static testMethod void testOpportunityWelcomeKitControllerExisting()
    {
        // Create a test opportunity with exiting action item
        Opportunity testOpp = createOpportunity( 'USD', System.today() );
        
        // Create a test contact to associate with opportunity 
        Contact testContact = new Contact( LastName='test', email='test@test.com', AccountId=testOpp.AccountId, T_Shirt_Size__c='' );
        insert testContact;
        
        // Create a action item for opportunity that can be tested during the update process        
        Opportunity_Action_Item__c oaic = new Opportunity_Action_Item__c();
        oaic.Opportunity__c = testOpp.id;
        oaic.Recipient__c = testContact.id;
        oaic.type__c = 'Welcome Kit';
        insert oaic;
        
        test.startTest();
        
        // Set the URL paramter for Opportunity ID
        ApexPages.currentPage().getParameters().put( 'oppID', testOpp.id );
        OpportunityWelcomeKitController testClass = new OpportunityWelcomeKitController();
        
        // Making sure that the action item is same
        System.assertEquals( oaic, testClass.oaic );
        
        // Making sure that there is only one contact that is tied to this opportunity
        System.assertEquals( 1, testClass.getOppContacts().size() );
        
        // Setting the ID for selected ID to fetch the record
        testClass.selectedContactID = testContact.Id;
        testClass.selectContact();
        
        // Making sure to the fetched contact record matches (T Shirt Size should be XL)
        testContact.T_Shirt_Size__c = 'XL';
        System.assertEquals( testContact, testClass.selectedContact );
        
        testClass.selectedContact.Email = 'test1@test.com';
        testClass.save();
        
        testClass.cancel();
        System.assertEquals( false, testClass.errorDetected );
        
        // Refetch the updated contact to compare
        testContact = [SELECT id, FirstName, LastName, Email, AccountID, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, T_Shirt_Size__c FROM Contact WHERE id = :testContact.Id];
        System.assertEquals( testContact, testClass.selectedContact );
        
        // Saving the opp Welcome kit opt out function
        testClass.welcomeKitOptOutVal = 'Info To Be Provided';
        testClass.saveOpp();
        
        testOpp = [SELECT Welcome_Kit_Opt_Out__c FROM Opportunity WHERE id = :testOpp.Id];
        System.assertEquals( 'Info To Be Provided', testOpp.Welcome_Kit_Opt_Out__c );
        
        testClass.clearContact();
        System.assertEquals( new Opportunity_Action_Item__c( Opportunity__c=testOpp.id ), testClass.oaic );
        
        test.stopTest();
    }
    
    
    static testMethod void testOpportunityWelcomeKitController()
    {
        // Creates a test opportunity without existing action item
        Opportunity testOpp = createOpportunity( 'USD', System.today() );

        test.startTest();
        // Set the URL paramter for Opportunity ID
        ApexPages.currentPage().getParameters().put( 'oppID', testOpp.id );
        OpportunityWelcomeKitController testClass = new OpportunityWelcomeKitController();
        
        testClass.newContact();
        
        // Making sure that new contact we create contains the same account ID
        //System.assertEquals( new Contact( accountId=testOpp.AccountId ), testClass.selectedContact );
        System.assertEquals( testOpp.AccountId , testClass.selectedContact.AccountId );

        test.stopTest();
    }
        
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account' );
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate);
        
        insert myOpp;
        return myOpp;
    }
}