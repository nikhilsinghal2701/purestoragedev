@isTest
private class TestEmailMessageTriggerHelper {
	
  @isTest static void eventProcess() {

        TestDataFactory dataCreation = new TestDataFactory();
        UserLicense license = dataCreation.getLicense('PID_Customer_Community_Login');
        System.debug('license  is ...' + license);
        Profile profile = dataCreation.getPrifleByLicense('PID_Customer_Community_Login');
        System.debug('Profile is ...' + profile);
        Account testAccount = dataCreation.getAccount();
        insert testAccount;
        Contact testContact = dataCreation.getContact(testAccount);
        testContact.email = 'email@email.com.test';
        insert testContact;
        User customUser1 = dataCreation.getUser(profile, testContact);
        insert customUser1;
			
        Case testCase = new Case(Origin = 'Email', contactId = testContact.id, Asset_Not_Listed__c = true);
        insert testCase;
   	    System.debug('test case is ...' + testCase);
   	    Date today = System.today();
        Event testEvent = new Event(subject = 'Call', IsVisibleInSelfService = true, StartDateTime = today.addDays(-2), EndDateTime  = today.addDays(2), ShowAs = 'OutOfOffice', WhoId = testContact.id);
        insert testEvent;
        System.debug('The test event is ...' + testEvent);
        List<Case> result = [select id from Case where Id=: testCase.id limit 1];
        System.debug('test case is ...' + result);
        List<Event> events = [Select Id from Event where Id=: testEvent.id limit 1];
        System.assertEquals(1, result.size());
        EmailMessage emailMessage = new EmailMessage(ParentId = testCase.Id, Incoming = true, TextBody = 'Test', subject = 'Test', fromAddress = 'email@email.com.test', ToAddress = 'fred.dai@perficient.com');
        insert emailMessage;
        }
	
    @isTest static void sendEmail() {
        EmailMessageTriggerHelper.sendAdminEmail('test', 'test', 'daizhixia@gmail.com');
    }
	
}