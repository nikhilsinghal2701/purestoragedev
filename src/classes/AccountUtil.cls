/*
Class: AccountUtil
Author: Fred
Created Date: 06/27/2014
*/

public with sharing class AccountUtil {

    public static final String PID_CUSTOMER_COMMUNITY_LOGIN = 'PID_Customer_Community_Login';

    public static final String PID_PARTNER_COMMUNITY_LOGIN = 'PID_Partner_Community_Login';

    /**
     * When adds svar Account, all of the users under the account should be able to see/read the current account.
     * And when the current account owner get changed, re-do the action logic to put all the sharing rules again.
     * Once the svar account value get deleted/reset/edit, we need to remove the account sharing rule which are added earlier, and update to the new one.
     * PID_Partner_Community_Login user license
    */
    public static void svarAccountShare(List<Account> accounts, map<Id, Account> triggerOldMap) {
        Set<Id> svarAccountId = new Set<Id>();
        for(Account account: accounts) {
            //support for add and edit operation
            if(triggerOldMap == null || account.SVAR_Account__c != triggerOldMap.get(account.id).SVAR_Account__c || account.ownerId != triggerOldMap.get(account.id).ownerId) {
                // remove sharing
                if(account.SVAR_Account__c == null && triggerOldMap != null) {
                    svarAccountId.add(triggerOldMap.get(account.id).SVAR_Account__c);   
                }else if(account.SVAR_Account__c != null) {
                    svarAccountId.add(account.SVAR_Account__c);
                }
            }
        }

        if(svarAccountId.isEmpty()) {
            return;
        }

        List<UserLicense> licenses = [Select LicenseDefinitionKey, Id from UserLicense WHERE LicenseDefinitionKey =: PID_CUSTOMER_COMMUNITY_LOGIN 
                                                                                          OR LicenseDefinitionKey =: PID_PARTNER_COMMUNITY_LOGIN limit 2];
        Map<Id, User> userMap = new Map<Id, User> ();
        for(User user: [Select Id, Name, ContactId, AccountId, Contact.AccountId, IsActive From User where (User.Profile.UserLicenseId =: licenses[0].id OR User.Profile.UserLicenseId =: licenses[1].id)
                                                                          AND ContactId != null
                                                                          AND Contact.AccountId != null
                                                                          AND Contact.AccountId IN: svarAccountId
                                                                          AND IsActive = true]) {
            userMap.put(user.Id, user);

        }
        Map<Id, Id> acct2UserRole = new Map<Id, Id>();
        Map<Id, Id> role2Group = new Map<Id, Id>();

        for(UserRole role: [Select Id, PortalRole, PortalAccountId, Name From UserRole where PortalAccountId IN: svarAccountId And PortalRole = 'Worker' AND PortalAccountId != null limit 1000]) {
            acct2UserRole.put(role.PortalAccountId, role.id);
        }

        for(Group record : [Select Id, relatedId from Group where relatedId IN: acct2UserRole.values() and type = 'Role' limit 1000]) {
            role2Group.put(record.relatedId, record.id);

        }
        System.debug('Roles will get the account ...' + acct2UserRole);
        if(userMap.isEmpty() || acct2UserRole.isEmpty()) {
            return;
        }
        
        List<AccountShare> accountSharesDel = new List<AccountShare> ();
        for(AccountShare accountShare : [Select Id from AccountShare where AccountId IN: accounts and RowCause = 'Manual' limit 1000]) {
            accountSharesDel.add(AccountShare);
        }
        
        if(!accountSharesDel.isEmpty()) {
            delete accountSharesDel;
        }

        List<AccountShare> accountShares = new List<AccountShare> ();
        for(Account account: accounts) {
            if(account.SVAR_Account__c != null && acct2UserRole.get(account.SVAR_Account__c) != null){
                    AccountShare accountShare = new AccountShare();
                    accountShare.accountId  = account.id;
                    accountShare.AccountAccessLevel = 'Edit';
                    accountshare.OpportunityAccessLevel = 'None';
                    accountshare.CaseAccessLevel = 'Edit';
                    accountshare.UserOrGroupId = role2Group.get(acct2UserRole.get(account.SVAR_Account__c));
                    accountShares.add(accountShare);
            }
        }
        if(!accountShares.isEmpty()){
            try{
                insert accountShares;
            }
            catch(DmlException e) {
                Util.SendAdminEmail('AccountUtil svarAccountShare failed', e.getStackTraceString() + '\n' +  e.getMessage(), Label.Help_Desk_Email);
            }
        }
    }
}