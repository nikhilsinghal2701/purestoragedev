/*****************************************************************************
 * Description : Partner Locator Context Class for Partner Locator functionality
 *
 *				Purpose of this class is to maintain the current context for Partner Locator functionality
 *
 *				Partner Locator User Interface state may be represented in the following ways
 *				1. Search
 *				2. List
 *				3. Details 
 *
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/27/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorContext {

   /**
    * Define the current context for Partner Locator functionality
    */
  	private enum CURRENT_STATE {PARTNER_LOCATOR_SEARCH, PARTNER_LOCATOR_LIST, PARTNER_LOCATOR_DETAILS }

  	public String UI_State { 
  		get {
  			return (currentState!=NULL) ? currentState.name() : CURRENT_STATE.PARTNER_LOCATOR_SEARCH.name();
  		}
  	}

	private CURRENT_STATE currentState { 
		get;
		set;
	}

	public PartnerLocatorContext() {
		setContextAsSearch();
	}

	public void setContextAsSearch() {
		currentState = CURRENT_STATE.PARTNER_LOCATOR_SEARCH;
	}	

	public void setContextAsList() {
		currentState = CURRENT_STATE.PARTNER_LOCATOR_LIST;
	}

	public void setContextAsDetails() {
		currentState = CURRENT_STATE.PARTNER_LOCATOR_DETAILS;
	}

	public boolean isContextSearch() {
		return currentState.equals(CURRENT_STATE.PARTNER_LOCATOR_SEARCH);
	}		

}