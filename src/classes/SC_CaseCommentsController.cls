/***********************************************
 * Description : Controller class for SC_CaseCommentsCustomView. This class has methods to insert public/private comments.
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

public with sharing class SC_CaseCommentsController
{
    // Variable Declarations
    public string commentBody {get;set;}
    public string selectedOption {get;set;}
    public list<SelectOption> options {get;set;}
    public boolean isError {get;set;}

    private static final string PRIVATE_COMMENT = 'Private';
    private static final string PUBLIC_COMMENT = 'Public';
    
    // current case record
    private Case c;
    
    // Constructor
    public SC_CaseCommentsController(Apexpages.StandardController controller)
    {
        c = (Case)controller.getRecord();
        
        selectedOption = PRIVATE_COMMENT; // defaults to private
        isError = false; // defaults to false
        
        // select options for radio buttons
        options = new list<SelectOption>();
        options.add(new SelectOption(PRIVATE_COMMENT,PRIVATE_COMMENT));
        options.add(new SelectOption(PUBLIC_COMMENT,PUBLIC_COMMENT));
    }
    
    // Method to insert the case comment as public or private
    public void insertCaseComment()
    {
        try
        {
            isError = false;
            
            // if commentbody is not null, create case comment and associate it to the parent case
            if(commentBody != null && commentBody != '')
            {
                CaseComment comment = new CaseComment();
                comment.ParentID = c.Id;
                comment.CommentBody = commentBody;
                
                if(selectedOption == PUBLIC_COMMENT)
                {
                    comment.IsPublished = true;
                }
                else
                {
                    comment.IsPublished = false;
                }
                
                insert comment;
                
                // reset the comment body text box
                commentBody = null;
            }
            else
            {
                // add "Please enter comments" msg
                isError = true;
                Apexpages.addMessage(new ApexPages.Message(APEXPAGES.SEVERITY.ERROR, system.label.SC_EnterComments));
            }
        }
        catch(Exception e)
        {
            // add error message if there is any error in creating the case comment
            isError = true;
            Apexpages.addMessage(new ApexPages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
        }
    }
}