global class OpportunityWelcomeKitScheduler implements Schedulable
{
    global void execute( SchedulableContext ctx )
    {
        processClosedOpportunity();
    }
    
    
    global static String processClosedOpportunity()
    {
        // Retrieve all of the custom settings for Dates and Stage
        Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
        Decimal firstNotificationSetting = customSettings.Welcome_Kit_First_Email_Notification__c;
        Decimal secondNotificationSetting = customSettings.Welcome_Kit_Second_Email_Notification__c;
        String stageName = customSettings.Welcome_Kit_Stage__c;
        String groupID = customSettings.Welcome_Kit_Notification_Group_ID__c;
        String emailTemplateName = customSettings.Welcome_Kit_Email_Template__c;
        Date wkGoLiveDate = customSettings.Welcome_Kit_Go_Live_Date__c;
        
        //Fetching Email Template
        EmailTemplate et = [SELECT id FROM EmailTemplate WHERE Name = :emailTemplateName];
        
        // Convert the date range into a date for SOQL comparison
        Date firstNotificationDate = Date.today().addDays( -Integer.valueOf( firstNotificationSetting ));
        Date secondNotificationDate = Date.today().addDays( -Integer.valueOf( secondNotificationSetting ));
        
        List<Messaging.SingleEmailMessage> listOfFirstNotificationEmails = new List<Messaging.SingleEmailMessage>();
        
        // First Query the opportunities that have been closed for longer than first notification setting
        // but didn't reach the second setting
        for(Opportunity overdueOpp : [SELECT id, OwnerId
                                        FROM Opportunity
                                       WHERE CloseDate >= :wkGoLiveDate
                                      AND Type = 'New Business'
                                         AND CloseDate <= :firstNotificationDate
                                         AND CloseDate > :secondNotificationDate
                                         //AND (Count_of_Welcome_Kits__c = 0 OR Welcome_Kit_Opt_Out__c != 'Customer Opt Out')
                                         AND Count_of_Welcome_Kits__c = 0
                                         AND Welcome_Kit_Opt_Out__c != 'Customer Opt Out'
                                         AND StageName = :stageName])
                                         //AND OwnerId = '00560000001faf9'])//for testing
                                         //AND CreatedByID = '005g0000001AAmK'])
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId( overdueOpp.OwnerId );
            mail.setTemplateId( et.id );
            mail.setWhatId( overdueOpp.id );
            mail.saveAsActivity = false;
            
            listOfFirstNotificationEmails.add( mail );
        }
        
        // Sends the email in mass for first notification
        Messaging.sendEmail( listOfFirstNotificationEmails );
        
        List<Messaging.SingleEmailMessage> listOfSecondNotificationEmails = new List<Messaging.SingleEmailMessage>();
        
        // Fetching the Custom Group
        List<String> mailCCAddresses = new List<String>();
        List<Id> userIDList = new List<Id>();
        
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE id = :groupID];
        
        
        
        // Fetching the user ID from the group
        for (GroupMember gm : g.groupMembers)
        {
            userIDList.add( gm.userOrGroupId );
        }
        
        // Inserting the email address to a list
        for(User u : [SELECT email FROM user WHERE id IN :userIDList])
        {
            mailCCAddresses.add( u.email );
        }
        
        // First Query the opportunities that have been closed for longer than second notification setting
        for(Opportunity overdueOpp : [SELECT id, OwnerId
                                        FROM Opportunity
                                       WHERE CloseDate >= :wkGoLiveDate
                                      AND Type = 'New Business'
                                         AND CloseDate <= :secondNotificationDate
                                         //AND (Count_of_Welcome_Kits__c = 0 OR Welcome_Kit_Opt_Out__c != 'Customer Opt Out')
                                         AND Count_of_Welcome_Kits__c = 0
                                         AND Welcome_Kit_Opt_Out__c != 'Customer Opt Out'                                         
                                         AND StageName = :stageName])
                                         //AND OwnerID = '00560000001faf9'])
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId( overdueOpp.OwnerId );
            mail.setTemplateId( et.id );
            mail.setWhatId( overdueOpp.id );
            mail.setCcAddresses( mailCCAddresses );
            mail.saveAsActivity = false;
            
            listOfSecondNotificationEmails.add( mail );
        }
        
        // Sends the email in mass for second notification
        Messaging.sendEmail( listOfSecondNotificationEmails );
        
        return 'processed';
    }
}