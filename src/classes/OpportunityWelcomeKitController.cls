public class OpportunityWelcomeKitController
{
    public Opportunity opp{ get; set; }
    public Contact selectedContact{ get; set; }
    public String accountName{ get; set; }
    public Id selectedContactID{ get; set; }
    public Opportunity_Action_Item__c oaic{ get; set; }
    public boolean existingWelcomeKit{ get; set; }
    public boolean errorDetected{ get; set; }
    public String welcomeKitOptOutVal{ get; set; }
    public boolean displayClearBtn{ get; set; }
    
    public OpportunityWelcomeKitController()
    {
        errorDetected = false;
        ID oppID = ApexPages.currentPage().getParameters().get( 'oppID' );
        
        // Fetch the opportunity using the URL parameter
        opp = [SELECT id, CloseDate, Name, AccountID, StageName, Welcome_Kit_Opt_Out__c FROM Opportunity WHERE id = :oppID];

        // Fetch the account name to display the information in Visualforce page
        accountName = [SELECT name FROM Account WHERE id = :opp.AccountID].name;
        
        try
        {
            // Try to fetch the existing action item... if it doesn't it wil create a new action item tied to the opportunity
            oaic = [SELECT Recipient__c, Type__c, Opportunity__c, Notes__c 
                      FROM Opportunity_Action_Item__c 
                     WHERE Opportunity__c = :opp.id 
                       AND Type__c = 'Welcome Kit'];
            
            // Since it does exist, it will fetch the contact informatiom
            selectedContact = [SELECT id, FirstName, LastName, Email, AccountID, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Phone, T_Shirt_Size__c
                                 FROM Contact 
                                WHERE id = :oaic.Recipient__c];
            if (selectedContact.T_Shirt_Size__c == null || selectedContact.T_Shirt_Size__c.length() == 0)
            {
                selectedContact.T_Shirt_Size__c = 'XL';             
            }                    
            
            existingWelcomeKit = true;
            displayClearBtn = true;
                    
        }
        catch(QueryException e)
        {
            oaic = new Opportunity_Action_Item__c();
            oaic.Opportunity__c = opp.id;
            existingWelcomeKit = false;
            displayClearBtn = false;
        }   
    }
    
    
    // Fetch all of the contact that tie to the opportunity using the accountID
    public List<Contact> getOppContacts()
    {
        Id accountID = opp.accountID;
        String mySOQL = 'SELECT id, FirstName, LastName, Email, AccountID, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Phone FROM Contact WHERE AccountID = :accountID';
        
        return Database.query( mySOQL );
    }
    
    
    // Create a new contact for the account using the opportunity
    public void newContact()
    {
        selectedContact = new Contact();
        selectedContact.AccountID = opp.AccountID;
        selectedContact.T_Shirt_Size__c = 'XL'; 
        
        displayClearBtn = true;
    }
    
    
    // Since the user selected a contact record, it will populate the information using SOQL
    public void selectContact()
    {
        selectedContact = [SELECT id, FirstName, LastName, Email, AccountID, MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry, Phone, T_Shirt_Size__c FROM Contact WHERE id = :selectedContactID];
        
        if (selectedContact.T_Shirt_Size__c == null || selectedContact.T_Shirt_Size__c.length() == 0)
        {
            selectedContact.T_Shirt_Size__c = 'XL';             
        }
        
        displayClearBtn = true;
    }
    
    
    public void saveOpp()
    {
        // Update the welcome kit opt out dropdown
        opp.Welcome_Kit_Opt_Out__c = welcomeKitOptOutVal;
        update opp;
    }
    
    // it will either insert/update the contact and action record
    public void save()
    {
        try
        {
            upsert selectedContact;
            
            oaic.Recipient__c = selectedContact.id;
            oaic.Type__c = 'Welcome Kit';
            oaic.Status__c = 'Requested';
            upsert oaic;
            
            opp.Welcome_Kit_Opt_Out__c = '';
            update opp;
        }
        catch(DMLException e)
        {
            errorDetected = true;
            ApexPages.addMessages( e );
        }
    }
    
    
    public void cancel()
    {
        errorDetected = false;
    }
    
    
    public void clearContact()
    {
        if (oaic.id != null)
        {
            delete oaic;
        }
        
        oaic = new Opportunity_Action_Item__c();
        oaic.Opportunity__c = opp.id;
        
        selectedContact = new Contact();
        selectedContact.AccountID = opp.AccountID;
        displayClearBtn = false;
    }
}