public with sharing class PRM_MDFRequestTriggerHelper{
	
	
	   public static void populateFieldsOnMDFRequests( List<MDF_Request__c > MDFRequests){
	   	
        if(UserInfo.getUserType() != 'Standard'){
        Map<String,Id>	uidMap= new Map<String,Id>();
        Set<String> uNames = new Set<String>();	
        
        
         User u = [SELECT Accountid,ContactId FROM User 
         			WHERE (UserType ='Partner' OR UserType='PowerPartner')
                	AND Id = : UserInfo.getUserId() 
         ];
        
        for(MDF_Request__c c : MDFRequests){
        	if(c.Field_Marketing_Manager__c != null)uNames.add(c.Field_Marketing_Manager__c);
        }
        
        for(User uid: [SELECT Id,name FROM user where Name In: uNames]){
        	uidMap.put(uid.name,uid.Id);
        }
        
        for(MDF_Request__c c : MDFRequests){
            c.Partner_Company_Name__c = u.Accountid;
            c.Requestor_Name__c = u.ContactId;
            if(uidMap.containskey(c.Field_Marketing_Manager__c))
            c.Field_Marketing_Manager_User__c=uidMap.get(c.Field_Marketing_Manager__c);
        }
        
      }  
       
   }
   
   public static void createCampaignOnMDFRequests( List<MDF_Request__c > MDFRequests,Map<Id,MDF_Request__c>OldMDFRequests){
   	List<Campaign> CampaignList = new List<Campaign>();
   	Set<Id> aid  = new Set<id>();
   	Map<Id,String>	aidMap= new Map<Id,String>();
   	
   	 for(MDF_Request__c c : MDFRequests){
        	if(c.Partner_Company_Name__c != null)aid.add(c.Partner_Company_Name__c);
        }
        
     for(Account a: [SELECT Id,name FROM Account where Id In: aid]){
        	aidMap.put(a.Id,a.name);
        }   
        	
   	 for(MDF_Request__c c : MDFRequests){
   	 	if(c.Create_Campaign_and_Send_to_NetSuite__c == true 
   	 	&& c.Status__c =='Approved'
   	 	&& (c.Status__c != OldMDFRequests.get(c.Id).Status__c 
   	 		||c.Create_Campaign_and_Send_to_NetSuite__c != OldMDFRequests.get(c.Id).Create_Campaign_and_Send_to_NetSuite__c)){
   	 		CampaignList.add(new Campaign(OwnerId = c.Field_Marketing_Manager_User__c
   	 										,Type=c.Category__c
   	 										,Name=Date.Today().Year()+'.'+Date.Today().Month()+'.'+Date.Today().day()+'-'+c.Program_Event_Location__c+'-'+(aidMap.containsKey(c.Partner_Company_Name__c)?(aidMap.get(c.Partner_Company_Name__c) != null?aidMap.get(c.Partner_Company_Name__c):''):'')
   	 										,Partner_Campaign__c=true
   	 										,IsActive=true
   	 										,Partner_Account_Association__c=c.Partner_Company_Name__c
   	 										,Description=c.Program_Event_Description__c
   	 										,Status='In Progress'
   	 										,Region__c=c.Program_Event_Location__c
   	 										,Solution_Category__c=c.Target_Market_Segment__c
   	 										,EndDate=c.Program_Event_End_Date__c
   	 										,StartDate=c.Program_Event_Start_Date__c
   	 										,BudgetedCost=c.Total_Estimated_Cost_of_Program__c
   	 										,CurrencyIsoCode='USD'
   	 										,MDF_Request__c=c.id
   	 										));
   	 	}
   	 }
   	 
   	 if(!CampaignList.isEmpty())insert CampaignList;
   	 
   }

}