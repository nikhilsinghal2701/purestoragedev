@isTest
private class QuoteProcessorTest
{
    public static Account generateAccount()
    {
        Account retVal = new Account(Name = 'testAccount');
        return retVal;
    }
    
    public static Product2 generateProduct2()
    {
        Product2 retVal = new Product2(Name = 'testProduct', 
                                        Is_Hardware__c = true,
                                        Product_Cost__c = 100.00,
                                        IsActive = true);
        return retVal;
    }
    
    public static PricebookEntry generatePricebookEntry(id inputProduct2Id) 
    {
        Id pbId = Test.getStandardPricebookId();
        PricebookEntry retVal = new PricebookEntry(Pricebook2Id = pbId,
                                                    IsActive = true,
                                                    Product2Id = inputProduct2Id,
                                                    UnitPrice = 100.00,
                                                    CurrencyIsoCode = 'USD');
        return retVal;
    }
    
    public static Opportunity generateOpportunity(id inputAccountId)
    {
        Opportunity retVal = new Opportunity(Name = 'Some Oppty',
                                                AccountId = inputAccountId,
                                                CloseDate = System.Today(),
                                                CurrencyIsoCode = 'USD',
                                                StageName = 'TBD');
        return retVal;
    }
    
    public static OpportunityLineItem generateOLI(id inputOpportunity, id inputPricebookEntryId)
    {
        OpportunityLineItem retVal = new OpportunityLineItem(OpportunityId = inputOpportunity,
                                                                PricebookEntryId = inputPricebookEntryId,
                                                                Quantity = 1,
                                                                UnitPrice = 100.00);
        return retVal;
    }
    
    public static Quote generateQuote(id inputOpptyId)
    {
        Quote retVal = new Quote(Name = 'Some Quote',
                                 OpportunityId = inputOpptyId,
                                 Status = 'Draft',
                                 Pricebook2Id = Test.getStandardPricebookId());
        return retVal;
    }
    
    public static QuoteLineItem generateQLI(id inputQuoteId, id inputPBEId)
    {
        QuoteLineItem retVal = new QuoteLineItem(PriceBookEntryId = inputPBEId,
                                 QuoteId = inputQuoteId,
                                 Quantity = 1,
                                 UnitPrice = 100.00);
        return retVal;
    }
    
    @istest(SeeAllData = true)
    static void computeMargin()
    {
        Account acct = generateAccount();
        insert acct;
        
        Product2 p = generateProduct2();
        insert p;
        
        PricebookEntry pbe = generatePricebookEntry(p.Id);
        insert pbe;
        
        Opportunity o = generateOpportunity(acct.id);
        insert o;
        
        OpportunityLineItem oli = generateOLI(o.Id, pbe.Id);
        insert oli;
        
        Quote q = generateQuote(o.Id);
        insert q;
        
        QuoteLineItem qli = generateQLI(q.Id, pbe.Id);
        insert qli;
        
        Test.startTest();
            q.Status = 'Approved - Pending GVP + Pres';
            update q;
        
            list<ProcessInstance> pis = [select Id, Status, TargetObjectId from ProcessInstance where TargetObjectId = : q.id];
        
            system.assert(pis.size()>0);
        
        Test.stopTest();
        
    }
    
    @istest(SeeAllData = false)
    static void QuoteTriggerCopyApprovalDetails() {
        Apex_Code_Settings__c cs = Apex_Code_Settings__c.getInstance();
        cs.Quote_Approval_Copy_Enabled__c = true;
        insert cs;
		test.startTest();
			//create the opportunity
			Account acct = generateAccount();
        	insert acct;
        	
        	Product2 p = generateProduct2();
        	insert p;
        
        	PricebookEntry pbe = generatePricebookEntry(p.Id);
        	insert pbe;
        
        	Opportunity o = generateOpportunity(acct.id);
        	o.OwnerId = UserInfo.getUserId();
        	insert o;
        	//check that the opportunity was inserted	
			System.assertNotEquals(null, o.id); 
        
        	Quote q = generateQuote(o.Id);
        	insert q;
        	//check that the Quote was inserted	
			System.assertNotEquals(null, q.id);	
			
        	QuoteLineItem qli = generateQLI(q.Id,pbe.Id);
        	qli.Discount = 59;
        	qli.Discount_for_Approval__c = 59;
        	insert qli;
        
			// Create an approval request for the quote
	        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
	        req1.setComments('Submitting request for approval.');
	        req1.setObjectId(q.id);
	        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
	        // Submit the approval request for the account
	        Approval.ProcessResult result = Approval.process(req1);
	        // Verify the result
	        System.assert(result.isSuccess());
	        System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
	        
	        // Approve the submitted request
	        // First, get the ID of the newly created item
	        List<Id> newWorkItemIds = result.getNewWorkitemIds();
	        
			// Instantiate the new ProcessWorkitemRequest object and populate it
        	Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
	        req2.setComments('Approving request.');
	        req2.setAction('Approve');
	        req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
	        
	        // Use the ID from the newly created item to specify the item to be worked
	        req2.setWorkitemId(newWorkItemIds.get(0));
	        
	        // Submit the request for approval
	        Approval.ProcessResult result2 =  Approval.process(req2);
	        
        	test.stopTest();
        
	        // Verify the results
	        System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
	        
	        System.assertEquals('Approved', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());
	        
			//Check that Approval Comments record is created
	        List<Approval_Process_Comments__c> quotesApproval = [Select Id, Comments__c, Date_Time_of_the_approval__c, Opportunity__c, 
                                                                 Approver__c From Approval_Process_Comments__c 
                                                                 Where Quote__c =: q.Id];
	        // Verify the results
	        System.assertEquals('Approving request.', quotesApproval[0].Comments__c);
	        System.assertNotEquals(Null, quotesApproval[0].Date_Time_of_the_approval__c);
	        System.assertEquals(o.Id, quotesApproval[0].Opportunity__c);
	        System.assertEquals(UserInfo.getUserId(), quotesApproval[0].Approver__c);
		
	}
	
	 /*
    * creates and returns a User
    */
    // generate active users
    public static User generateUser(String profileName, String role, String firstName, String lastName) {   
        UserRole userRole = createUserRole(role);
        Profile profile = getProfileByName(profileName);
        
        insert userRole;
        
        // ensure userRole was created properly and expected profile was fetched
        System.assertNotEquals(userRole.Id, null); 
        System.assertNotEquals(profile.Id, null);
        
        //creates a user
        User user = new User(
            Username = firstName + '@test.com.psem',
            LastName = lastName,
            Email = firstName + '@test.com',
            Alias = firstName,
            CommunityNickname = firstName, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserRoleId = userRole.Id,
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = profile.id);
 
        insert user;
        System.assertNotEquals(user.Id, null);
        
        return user;
    }
    
    // get profile by name
    public static Profile getProfileByName(String profileName) {
        return [Select Id, Name From Profile Where Name= :profileName];
    }
 
     // create a User Role
    public static UserRole createUserRole(String name) {
        return new UserRole(Name = name);
    }

    
    
}