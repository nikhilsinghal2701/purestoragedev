global class VLCampaignNomination {
    global static void campaignNomination(String strCampID,String strSurveyID,String strSurveyName)
     {
         CampaignMember[] arrCampaignMember=[select ContactID FROM CampaignMember WHERE CampaignId =: strCampID];
         List <Feedback__c> feedbackList = new List<Feedback__c>(); 
         Long lgSeed = System.currentTimeMillis();
         for (CampaignMember campaignMember : arrCampaignMember)
         {
              lgSeed =  lgSeed + 1;
              Feedback__c feedback = new Feedback__c();  
              feedback.Name = 'P_' + lgSeed;
              feedback.Campaign__c = strCampID;
              feedback.Contact__c = campaignMember.ContactID;                
              feedback.DataCollectionId__c = strSurveyID;
              feedback.Status__c = 'Nominated';               
              feedback.DataCollectionName__c = strSurveyName;
              feedbackList.add(feedback);             
          }
        insert feedbackList;
        Campaign campaign = [select Status from Campaign where Id =: strCampID];
        campaign.Survey_Status__c = 'In Progress';
        Update campaign;        
      } 
      
      /*** Test Methods ***/
      @isTest(SeeAllData=true)
      static void testCampaignNomination(){
       String strCampaignId = prepareTestData();
       campaignNomination(strCampaignId,'Test12345','Test Survey');
       clearTestData();
      }
      
      static String prepareTestData(){
             
        Account a = new Account(Name='SMX Test Account');
        insert a;
        Contact c1 = new Contact(FirstName='SMX TestFName1', LastName='SMX TestLName1', AccountID=a.id, Email='this.is.a.smx.test@verafin.com');
        insert c1;

        Opportunity o = new Opportunity(Name='SMX Test Opportunity', StageName='Prospect',CloseDate=Date.newInstance(2020,12,31),AccountID=a.id);
        insert o;
        
        Campaign campaign = new Campaign(Name='SMX Test Campaign',StartDate=Date.today());
        insert campaign;
        
        CampaignMember cm1 = new CampaignMember(ContactID=c1.id, CampaignID=campaign.ID);
        insert cm1;
       
        return campaign.Id;          
      }
      
      static void clearTestData(){
       Opportunity oppn = [SELECT Id from OPPORTUNITY WHERE NAME = 'SMX Test Opportunity'];
       delete oppn;
       Campaign campaign = [SELECT Id from CAMPAIGN WHERE NAME = 'SMX Test Campaign'];
       delete campaign;
       Contact contact = [SELECT Id from CONTACT WHERE FirstName = 'SMX TestFName1'];
       delete contact;
      }   
}