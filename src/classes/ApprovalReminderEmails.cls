global class ApprovalReminderEmails implements Database.Batchable<sObject>,Database.Stateful {
	
	global Database.QueryLocator start(Database.BatchableContext bc){
		//generate wrappers
		ApprovalReminderHelper.populateApproverWrappers();
		String stringIds = '';
		//convert for where clause in the dynamic query
		if(ApprovalReminderHelper.wrapperMap != null){
			for(Id actorId : ApprovalReminderHelper.wrapperMap.keySet()){
				stringIds += '\'' + actorId + '\'' + ', ';
			}
		}
		if(stringIds != ''){ 
			//remove the last 2 characters ", "
			stringIds = stringIds.substring(0, stringIds.length() - 2);
		}
		
		String queryUsers = 'SELECT Id, FirstName, LastName, Email FROM User WHERE Id in (' + stringIds + ')' ;  
   		return  Database.getQueryLocator(queryUsers);
   	}
    
	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		// generate wrappers again because static vars are reseted every time a batch is executed
		ApprovalReminderHelper.populateApproverWrappers();
		//loop thru the users and send email
		for (SObject s : scope) {
   			User u = (User)s;
   			if(ApprovalReminderHelper.wrapperMap.get(u.Id) != null){
   				ApprovalReminderHelper.SendReminderEmail(ApprovalReminderHelper.wrapperMap.get(u.Id));
   			}
		} 
	} 
 
 	global void finish(Database.BatchableContext info) {
	   
	} // finish
 	
} // end class