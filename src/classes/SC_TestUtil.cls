/* Test Util Class to create test records */

public class SC_TestUtil 
{
    // create test account
    public static Account createAccount(boolean isInsert)
    {
        Account acc = new Account(Name = 'Test Class Account', Industry = 'Banking & Securities-IDC',
                                  Type = 'Customer', Company_Size__c = '0-499', BillingCountry = 'United States');
        if(isInsert)
        {
            insert acc;
        }

        return acc;
    }
    
    // set the given user as an account team member
    public static void createAccountTeamMember(Account acc, User u)
    {
        AccountTeamMember am = new AccountTeamMember(TeamMemberRole = system.label.SC_CCAccountRole,
                                                     AccountID = acc.ID, UserID = u.Id);
        insert am;
    }
    
    public static void createAccountShare(Account acc, User u)
    {
        AccountShare accShare = new AccountShare(AccountId = acc.Id, UserOrGroupId = u.Id, AccountAccessLevel = 'Edit',
                                                 CaseAccessLevel = 'Read', OpportunityAccessLevel = 'Edit');
        insert accShare;
    }

    // create a contact for the given account
    public static Contact createContact(boolean isInsert, Account acc)
    {
        Contact con = new Contact(FirstName = 'Test Class', LastName = 'Contact', AccountId = acc.Id, 
                                  Email = 'testcontact@testclass.com', LeadSource = 'Channel');
        if(isInsert)
        {
            insert con;
        }

        return con;
    }

    // create a source support contact - a contact setup in the system with source support email id 
    // to whom the RMA emails will be sent. So, this is used to test the Send RMA functionality
    public static Contact createSourceSupportContact()
    {
        Account acc = new Account(Name = 'Service Cloud Account For Dev Purpose (Do not delete)', Industry = 'Banking & Securities-IDC',
                                  Type = 'Customer', Company_Size__c = '0-499');
        insert acc;

        Contact con = new Contact(FirstName = 'Source Support', LastName = 'Email (Do not delete)', AccountId = acc.Id, 
                                  Email = 'sourcesupport@testclass.com', LeadSource = 'Channel');
        insert con;

        return con;
    }

    // create a parent array for the given account with details like purity version, status, array id, etc
    public static Asset createArray(boolean isInsert, string arrayName, Account acc, string arrayID, string purityVersion, string status, string warranty)
    {
        Asset parentArray = new Asset(Name = arrayName, AccountId = acc.Id, Array_ID__c = arrayID, Purity_Version__c = purityVersion,
                                Status = status, Pure_Warranty_Description__c = warranty,
                                Street_Address__c = 'test street', City__c = 'San Francisco',
                                state_province__c = 'California', Postal_Code__c = '90000', 
                                Country__c = 'United States');
        if(isInsert)
        {
            insert parentArray;
        }

        return parentArray;
    }

    // create a child asset for the given parent array
    public static Asset createChildAsset(boolean isInsert, Asset parentAsset, Account acc, string platformPN, string index, 
                                            string wty, string serialNo)
    {
        Asset a = new Asset(Parent_Asset__c = parentAsset.Id, AccountId = acc.Id, Platform_PN__c = platformPN,
                            Index__c = index, Pure_Warranty_Description__c = wty, Serial_Number__c = serialNo,
                            Name = serialNo);
        if(isInsert)
        {
            insert a;
        }

        return a;
    }

    // create a case
    public static Case createCase(boolean isInsert, Account acc, Contact con, Asset parentArray)
    {
        Case c = new Case(AccountID = acc.ID, ContactID = con.Id, AssetId = parentArray.Id,
                          Status = 'New', Origin = 'Email');
        if(isInsert)
        {
            insert c;
        }

        return c;
    }
    
    /* Not used
    public static CaseComment createCaseComment(boolean isInsert, Case c, boolean isPublic)
    {
        CaseComment cm = new CaseComment(ParentId = c.Id, IsPublished = isPublic, 
                                         CommentBody = 'Test Case Comment');
        if(isInsert)
        {
            insert cm;
        }

        return cm;
    }*/

    // create a test attachment
    public static Attachment createattachment(boolean isInsert, Id parentID, string contentType)
    {
        Attachment a = new Attachment(Name = 'test Attachment', ParentID = parentID, 
                                      Description = 'from test class', Body = Blob.valueOf('test class'),
                                      contentType = contentType);
        if(isInsert)
        {
            insert a;
        }

        return a;
    }

    // create a email message for the given case
    public static EmailMessage createEmailMessage(boolean isInsert, Id parentID, string fromAddress,
                                                string toAddress, string ccAddress, string subject)
    {
        EmailMessage em = new EmailMessage(ParentId = parentId, FromAddress = fromAddress,
                                           ToAddress = toAddress, CCAddress = ccAddress,
                                           Subject = subject);
        if(isInsert)
        {
            insert em;
        }

        return em;
    }

    // query the profile for the given profile name
    public static Profile getProfile(string name)
    {
        return [Select Id, Name from Profile where Name = :name];
    }

    // create a test user to run the test method as
    public static User createUser(Profile profile)
    {
        User u = new User(Alias = 'tesClass', Email='testuser@testclass.com', 
                          EmailEncodingKey='UTF-8', LastName='TestClassUser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profile.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser@testclass.com',
                          SMS_Email__c = 'testuser@testclass.com.sms');
        return u;
    }

    // create a customer user to run the test method as
    public static User createCustomerUser(Profile profile, Contact con)
    {
        User u = new User(Alias = 'tesClass', Email='testuser@testclass.com', 
                          EmailEncodingKey='UTF-8', LastName='TestClassUser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profile.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testuser@testclass.com',
                          ContactId = con.Id);
        return u;
    }
}