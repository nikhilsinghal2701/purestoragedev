global class ZenSurveyPoster implements Schedulable{

    global void execute(SchedulableContext ctx){
    
        loadTickets(false);
    }

    @future(callout=true)
    webService static void loadTickets(boolean isApexTest) {
       
        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
                      
        String SERVICE_URL = configuration.Zendesk_URL__c;      
        String USERNAME = configuration.Zendesk_Username__c;
        String PASSWORD = configuration.Zendesk_Password__c;
    
        List<ZenTicket> tickets = new List<ZenTicket>();
        Integer totalTickets = 0;
        
               
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');
 
        //String url = SERVICE_URL+'/rules/29879973.xml';
        //System.debug('XMLURL:'+url);
        String url = SERVICE_URL+'/api/v2/views/29879973/tickets.json?include=users,organizations';
       
        req.setEndpoint(url);

        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);         
        
        HTTPResponse response;

        if(isApexTest){
            response = new HTTPResponse();
            response.setBody('{"tickets":[{"url":"https://pure.zendesk.com/api/v2/tickets/4472.json","id":4472,"external_id":null,"via":{"channel":"email","source":{"from":{"address":"kelvinw@purestorage.com","name":"Kelvin Ward"},"to":{"address":"support@purestorage.com","name":"Pure Storage Support"},"rel":null}},"created_at":"2013-05-10T01:02:25Z","updated_at":"2013-05-14T02:01:50Z","type":"incident","subject":"Internal - Dogfood: Warning - pure-b6: Communication Failure [18]","description":"Looking into this now. Anyone working on it","priority":"normal","status":"closed","recipient":"support@pure.zendesk.com","requester_id":376851876,"submitter_id":376851876,"assignee_id":376851876,"organization_id":21577342,"group_id":128914,"collaborator_ids":[377053777,325435597,389611003],"forum_topic_id":null,"problem_id":null,"has_incidents":false,"due_at":null,"tags":["40","administrivia","na","other","phonehome"],"custom_fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":20007202,"value":"na"},{"id":20010486,"value":"other"},{"id":20520488,"value":"n/a"},{"id":22064668,"value":"phonehome"},{"id":20172612,"value":"Investigate Alert"},{"id":20018901,"value":null},{"id":21666866,"value":"40"},{"id":22064678,"value":"administrivia"}],"satisfaction_rating":null,"sharing_agreement_ids":[],"fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":20007202,"value":"na"},{"id":20010486,"value":"other"},{"id":20520488,"value":"n/a"},{"id":22064668,"value":"phonehome"},{"id":20172612,"value":"Investigate Alert"},{"id":20018901,"value":null},{"id":21666866,"value":"40"},{"id":22064678,"value":"administrivia"}],"followup_ids":[]}],"users":[{"url":"https://pure.zendesk.com/api/v2/users/325435597.json","id":325435597,"external_id":null,"name":"John Flagg","alias":"","created_at":"2013-01-14T18:59:09Z","updated_at":"2013-05-03T18:55:04Z","active":true,"verified":true,"shared":false,"locale_id":null,"locale":"en-US","time_zone":"Pacific Time (US & Canada)","last_login_at":"2013-05-09T23:41:17Z","email":"flagg@purestorage.com","phone":"408-316-2620","signature":"RegardsJohn Flagg","details":"","notes":"","organization_id":21577342,"role":"admin","custom_role_id":null,"moderator":true,"ticket_restriction":null,"only_private_comments":false,"tags":["tse"],"suspended":false,"photo":{"url":"https://pure.zendesk.com/api/v2/attachments/57859622.json","id":57859622,"file_name":"logo.png","content_url":"https://pure.zendesk.com/system/photos/5785/9622/logo.png","content_type":"image/png","size":7912,"thumbnails":[{"url":"https://pure.zendesk.com/api/v2/attachments/57859632.json","id":57859632,"file_name":"logo_thumb.png","content_url":"https://pure.zendesk.com/system/photos/5785/9622/logo_thumb.png","content_type":"image/png","size":7912}]}},{"url":"https://pure.zendesk.com/api/v2/users/376851876.json","id":376851876,"external_id":null,"name":"Kelvin Ward","alias":"","created_at":"2013-04-05T23:01:30Z","updated_at":"2013-04-09T16:27:13Z","active":true,"verified":true,"shared":false,"locale_id":null,"locale":"en-US","time_zone":"Mountain Time (US & Canada)","last_login_at":"2013-05-13T18:50:07Z","email":"kelvinw@purestorage.com","phone":"","signature":null,"details":"","notes":"","organization_id":21577342,"role":"agent","custom_role_id":900271,"moderator":false,"ticket_restriction":null,"only_private_comments":false,"tags":[],"suspended":false,"photo":{"url":"https://pure.zendesk.com/api/v2/attachments/67018318.json","id":67018318,"file_name":"logo.jpg","content_url":"https://pure.zendesk.com/system/photos/6701/8318/logo.jpg","content_type":"image/jpeg","size":2464,"thumbnails":[{"url":"https://pure.zendesk.com/api/v2/attachments/67018328.json","id":67018328,"file_name":"logo_thumb.jpg","content_url":"https://pure.zendesk.com/system/photos/6701/8318/logo_thumb.jpg","content_type":"image/jpeg","size":2457}]}},{"url":"https://pure.zendesk.com/api/v2/users/377053777.json","id":377053777,"external_id":null,"name":"Escalation Engineering","alias":"","created_at":"2013-04-10T20:31:17Z","updated_at":"2013-04-10T21:25:28Z","active":true,"verified":true,"shared":false,"locale_id":null,"locale":"en-US","time_zone":"America/Los_Angeles","last_login_at":"2013-04-10T21:25:28Z","email":"escalations@purestorage.com","phone":"","signature":null,"details":"","notes":"","organization_id":21577342,"role":"agent","custom_role_id":746611,"moderator":false,"ticket_restriction":"groups","only_private_comments":false,"tags":[],"suspended":false,"photo":null},{"url":"https://pure.zendesk.com/api/v2/users/389611003.json","id":389611003,"external_id":null,"name":"Pure-b6-ct1","alias":null,"created_at":"2013-05-08T16:11:50Z","updated_at":"2013-05-08T16:11:50Z","active":true,"verified":false,"shared":false,"locale_id":null,"locale":"en-US","time_zone":"America/Los_Angeles","last_login_at":null,"email":"pure-b6-ct1@purestorage.com","phone":null,"signature":null,"details":null,"notes":null,"organization_id":21577342,"role":"end-user","custom_role_id":null,"moderator":false,"ticket_restriction":"requested","only_private_comments":false,"tags":[],"suspended":false,"photo":null}],"next_page":null,"previous_page":null,"count":1}');
        }else{
            response = http.send(req);
        }
        
        tickets = parseJSON(response);
        
        
        /*     OLD XML PARSER -- IGNORE   
        HTTPResponse response;
        
        if(isApexTest){
            response = new HTTPResponse();
            response.setBody('<?xml version="1.0" encoding="UTF-8"?><records type="array" count="4"><record><assigned-at type="datetime">2011-04-11T09:34:45-07:00</assigned-at><assignee-id type="integer">28704645</assignee-id><base-score type="integer">0</base-score><created-at type="datetime">2011-04-11T09:34:45-07:00</created-at><current-collaborators nil="true"></current-collaborators><current-tags>6705 vmware</current-tags><description>Alpha 3 works</description><due-date type="datetime" nil="true"></due-date><entry-id type="integer" nil="true"></entry-id><external-id nil="true"></external-id><group-id type="integer">128914</group-id><initially-assigned-at type="datetime">2011-04-11T09:34:45-07:00</initially-assigned-at><latest-recipients nil="true"></latest-recipients><nice-id type="integer">1006</nice-id><organization-id type="integer" nil="true"></organization-id><original-recipient-address nil="true"></original-recipient-address><priority-id type="integer">4</priority-id><recipient nil="true"></recipient><requester-id type="integer">51852997</requester-id><resolution-time type="integer">0</resolution-time><solved-at type="datetime">2011-04-11T10:02:55-07:00</solved-at><status-id type="integer">4</status-id><status-updated-at type="datetime">2011-04-15T10:57:41-07:00</status-updated-at><subject>Alpha 3 Problem</subject><submitter-id type="integer">51852997</submitter-id><ticket-type-id type="integer">2</ticket-type-id><updated-at type="datetime">2011-04-15T10:57:41-07:00</updated-at><updated-by-type-id type="integer">0</updated-by-type-id><via-id type="integer">0</via-id><score type="integer">0</score><problem-id nil="true"></problem-id><has-incidents type="boolean">false</has-incidents><comments type="array"><comment><author-id type="integer">51852997</author-id><created-at type="datetime">2011-04-11T09:34:45-07:00</created-at><is-public type="boolean">true</is-public><type>Comment</type><value>Alpha 3 works</value><via-id type="integer">0</via-id><attachments type="array"/></comment><comment><author-id type="integer">28704645</author-id><created-at type="datetime">2011-04-11T09:54:24-07:00</created-at><is-public type="boolean">false</is-public><type>Comment</type><value>Alpha 3 works..for 24 hours.</value><via-id type="integer">0</via-id><attachments type="array"/></comment><comment><author-id type="integer">28704645</author-id><created-at type="datetime">2011-04-11T10:02:55-07:00</created-at><is-public type="boolean">false</is-public><type>Comment</type><value>Thanks, I close this ticket.</value><via-id type="integer">0</via-id><attachments type="array"/></comment></comments><linkings type="array"/><ticket-field-entries type="array"><ticket-field-entry><ticket-field-id type="integer">20006988</ticket-field-id><value>non</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20007202</ticket-field-id><value>6705</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20009798</ticket-field-id><value>Michael Cornwell</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20010481</ticket-field-id><value>408-722-4144</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20010486</ticket-field-id><value>vmware</value></ticket-field-entry></ticket-field-entries><channel nil="true"></channel><permissions></permissions></record><record><assigned-at type="datetime">2011-04-08T15:33:40-07:00</assigned-at><assignee-id type="integer">28704645</assignee-id><base-score type="integer">0</base-score><created-at type="datetime">2011-04-08T15:33:40-07:00</created-at><current-collaborators>Allen Lu | Pure Storage</current-collaborators><current-tags>6702 other</current-tags><description>I want an HA array</description><due-date type="datetime" nil="true"></due-date><entry-id type="integer" nil="true"></entry-id><external-id nil="true"></external-id><group-id type="integer">128914</group-id><initially-assigned-at type="datetime">2011-04-08T15:33:40-07:00</initially-assigned-at><latest-recipients nil="true"></latest-recipients><nice-id type="integer">1004</nice-id><organization-id type="integer" nil="true"></organization-id><original-recipient-address nil="true"></original-recipient-address><priority-id type="integer">4</priority-id><recipient nil="true"></recipient><requester-id type="integer">52211291</requester-id><resolution-time type="integer">10</resolution-time><solved-at type="datetime">2011-04-09T01:06:28-07:00</solved-at><status-id type="integer">4</status-id><status-updated-at type="datetime">2011-04-13T01:54:20-07:00</status-updated-at><subject>array is HA</subject><submitter-id type="integer">52211291</submitter-id><ticket-type-id type="integer">2</ticket-type-id><updated-at type="datetime">2011-04-13T01:54:20-07:00</updated-at><updated-by-type-id type="integer">0</updated-by-type-id><via-id type="integer">0</via-id><score type="integer">0</score><problem-id nil="true"></problem-id><has-incidents type="boolean">false</has-incidents><comments type="array"><comment><author-id type="integer">52211291</author-id><created-at type="datetime">2011-04-08T15:33:40-07:00</created-at><is-public type="boolean">true</is-public><type>Comment</type><value>I want an HA array</value><via-id type="integer">0</via-id><attachments type="array"/></comment><comment><author-id type="integer">30654662</author-id><created-at type="datetime">2011-04-08T16:26:55-07:00</created-at><is-public type="boolean">true</is-public><type>Comment</type><value>Our HA offerings will be available by 4th quarter of this year.</value><via-id type="integer">21</via-id><attachments type="array"/></comment><comment><author-id type="integer">28704645</author-id><created-at type="datetime">2011-04-09T01:06:28-07:00</created-at><is-public type="boolean">false</is-public><type>Comment</type><value>Contacted our PM regarding customer request for HA.</value><via-id type="integer">0</via-id><attachments type="array"/></comment></comments><linkings type="array"/><ticket-field-entries type="array"><ticket-field-entry><ticket-field-id type="integer">20006988</ticket-field-id><value>none</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20007202</ticket-field-id><value>6702</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20009798</ticket-field-id><value>johncolgrove</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20010481</ticket-field-id><value>650-823-4454</value></ticket-field-entry><ticket-field-entry><ticket-field-id type="integer">20010486</ticket-field-id><value>other</value></ticket-field-entry></ticket-field-entries><channel nil="true"></channel><permissions></permissions></record></records>');
        }else{
            response = http.send(req);
        } 
        
        dom.Document doc = response.getBodyDocument();
        totalTickets = Integer.valueOf(doc.getRootElement().getAttribute('count',null));
        dom.XmlNode [] nodes = doc.getRootElement().getChildElements(); 
        for(dom.XMLNode node :nodes){
            ZenTicket ticket = new ZenTicket();
            dom.XmlNode [] nodeItems = node.getChildElements();
            for(dom.XMLNode nodeItem :nodeItems){
                if(nodeItem.getName() == 'nice-id') 
                    ticket.id = nodeItem.getText();
                else if(nodeItem.getName() == 'subject') 
                    ticket.subject = nodeItem.getText();
                else if(nodeItem.getName() == 'ticket-type-id') 
                    ticket.type = nodeItem.getText();
                else if(nodeItem.getName() == 'status-id') 
                    ticket.status = nodeItem.getText();
                else if(nodeItem.getName() == 'priority-id') 
                    ticket.priority = nodeItem.getText();
                else if(nodeItem.getName() == 'assignee-id'){
                    if(!usersMap.containsKey(nodeItem.getText()))
                        usersMap.put(nodeItem.getText(),new ZenUser(nodeItem.getText(),isApexTest));
                    ticket.assignee = (usersMap.get(nodeItem.getText())).name;  
                }
                else if(nodeItem.getName() == 'requester-id'){
                    if(!requestersMap.containsKey(nodeItem.getText()))
                        requestersMap.put(nodeItem.getText(),new ZenUser(nodeItem.getText(),isApexTest));
                    ticket.requester_name = (requestersMap.get(nodeItem.getText())).name; 
                    ticket.requester_email = (requestersMap.get(nodeItem.getText())).email; 
                    ticket.requester_organizationId = (requestersMap.get(nodeItem.getText())).organizationId;
                }
                else if(nodeItem.getName() == 'resolution-time') 
                    ticket.resolution_time = nodeItem.getText();    
                else if(nodeItem.getName() == 'updated-at') 
                    ticket.updated_at = nodeItem.getText();
                else if(nodeItem.getName() == 'created-at') 
                    ticket.created_at = nodeItem.getText();
                else if(nodeItem.getName() == 'status-updated-at') 
                    ticket.status_updated_at = nodeItem.getText();
            }
            tickets.add(ticket);
            //System.debug('Here the ticket:'+ticket);           
        }
        */
        //Success
        
        for(ZenTicket tk :tickets){
            if(tk.requester_email.contains('@purestorage.com') || tk.requester_email.contains('pure_internal@')){
                System.debug('Internal Pure User: ' +tk.requester_name + '('+tk.requester_email+') Not sending Survey.');
            }else{
                List<Contact> contacts = [select id,AccountId from Contact where Email =: tk.requester_email];
                //List<Contact> contacts = [select id from Contact where id ='003W00000066r1y'];
                Contact contact;
                if(contacts.isEmpty()){
                    System.debug('Could not find a contact match with user: '+tk.requester_name + '('+tk.requester_email+')');
                    
                    if(tk.requester_organizationId.length()>0){
                        List<Account> accs = [Select ac.Id,ac.Name,ac.Zendesk_Organization_Id__c from Account ac where ac.Zendesk_Organization_Id__c =: tk.requester_organizationId];
                        Account acc;
                        if(accs.size()>0){
                            acc = accs.get(0);
                            System.debug('Account: '+acc.Name + ' Account Org ID: ' +acc.Zendesk_Organization_Id__c + 'matched with('+tk.requester_organizationId+')');
                        }                        
                        if(acc != null){
                            System.debug('Found Account: '+acc.Name + ' for: ' +tk.requester_name + '('+tk.requester_email+')');
                            contact = new Contact();
                            List<String> names = tk.requester_name.split(' ',2); 
                            if(names.size()>0){
                                contact.Firstname = names.get(0);
                            }else{
                                contact.Firstname = 'NULL';
                            }
                            if(names.size()>1){
                                contact.Lastname = names.get(1);
                            }else{
                                contact.Lastname = 'NULL';
                            }
                            contact.Email = tk.requester_email;
                            contact.AccountId = acc.id;
                            insert contact;                    
                            System.debug('Created Contact: '+contact.id+ ' for: ' +tk.requester_name + '('+tk.requester_email+')');
                        }else{
                            System.debug('Could not find an account for: ' +tk.requester_name + '('+tk.requester_email+') with Zendesk Organization ID: ' + tk.requester_organizationId); 
                        }
                     }else{
                         System.debug('Zendesk user: ' +tk.requester_name + '('+tk.requester_email+') does not belong to any Organization');
                     }
                }else{
                    contact = contacts.get(0);
                    System.debug('Found a matching Contact: '+contact.id+ ' for: ' +tk.requester_name + '('+tk.requester_email+')');
                }
                if(contact != null){ 
                    List<Account> accs = [Select ac.Id, ac.Support_Must_Not_Contact_End_Customer__c from Account ac where ac.id =: contact.AccountId]; 
                    if(accs.size()>0){
                        Account acc = accs.get(0);
                        if(!acc.Support_Must_Not_Contact_End_Customer__c){          
                            SatmetrixNominationUtil util = new SatmetrixNominationUtil();
                            Feedback__c feedback = util.nominateContactsForSupportSurvey(contact, tk.id, tk.priority, tk.assignee, tk.resolution_time); 
                            System.debug('Survey queued');  
                        }else{
                            System.debug('Account is set to Support Must Not Contact End Customer - Survey ignored');
                        }
                    }else{
                       System.debug('Contact is not associated with any account - Survey ignored'); 
                    }
                }else{
                    System.debug('Contact was null - Survey ignored'); 
                }  
            }
        }
        
    }
    
    private static List<ZenTicket> parseJSON(HTTPResponse response){    
        
        List<ZenTicket> tickets = new List<ZenTicket>(); 
        Map<String,ZenUser> usersMap = new Map<String,ZenUser>{};
        ZenUser user;
        
        JSONParser parser = JSON.createParser(response.getBody());
        
        system.debug(response.getBody());
        
        while (parser.nextToken() != null)  
        {  
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'tickets')) 
            {               
                while (parser.nextToken() !=  JSONToken.END_ARRAY)  
                {  
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
                    {  
                          
                        ZenTicket ticket = new ZenTicket();  
                        
                        while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                        {  
                              
                                if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                                {  
                                    parser.nextToken();      
                                    ticket.id= parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'requester_id'))  
                                {  
                                    parser.nextToken();  
                                    ticket.requester_id = parser.getText();  
                                }
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'subject'))  
                                {  
                                    parser.nextToken();  
                                    ticket.subject = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'type'))  
                                {  
                                    parser.nextToken();  
                                    ticket.type = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'url'))  
                                {  
                                    parser.nextToken();  
                                    ticket.status = parser.getText();  
                                } 
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'priority'))  
                                {  
                                    parser.nextToken();  
                                    ticket.priority = parser.getText();  
                                } 
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'updated_at'))  
                                {  
                                    parser.nextToken();  
                                    ticket.updated_at = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'created_at'))  
                                {  
                                    parser.nextToken();  
                                    ticket.created_at = parser.getText();  
                                }
                                else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                                {  
                                    system.debug('Field is: ' + parser.getText());
                                    parser.nextToken();
                                    if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                        while(parser.nextToken() != JSONToken.END_ARRAY){
                                            //Ignore nested Arrays
                                            if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                                while(parser.nextToken() != JSONToken.END_ARRAY){
                                                    //Ignore double nested Arrays
                                                    if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                                        while(parser.nextToken() != JSONToken.END_ARRAY){
                                                            //Ignore triple nested Arrays
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                            //Ignore nested objects
                                            if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                while(parser.nextToken() != JSONToken.END_OBJECT){
                                                    //Ignore double nested objects
                                                    if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                                            //Ignore triple nested objects
                                                        }
                                                    }
                                                }
                                            }
                                        
                                        }
                                    }else{
                                        //Ignore Other Tokens
                                    }  
                                }                               
                        } 
                        
                        system.debug('Ticket number: ' + ticket.id); 
                        tickets.add(ticket);  
                    }  
                } 
            }
            else if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'users')) 
            {               
                while (parser.nextToken() !=  JSONToken.END_ARRAY)  
                {              
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
                    {    
                        user = new ZenUser();                                              
                        while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                        {  
                            if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                            {  
                                parser.nextToken();      
                                user.id = parser.getText();  
                            }  
                            else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'name'))  
                            {  
                                parser.nextToken();  
                                user.name = parser.getText();  
                            }  
                            else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'email'))  
                            {  
                                parser.nextToken();  
                                user.email = parser.getText();  
                            }  
                            else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'organization_id'))  
                            {  
                                parser.nextToken();  
                                user.organizationId = parser.getText();  
                            }                    
                            else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                            {  
                                parser.nextToken();
                                if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                    while(parser.nextToken() != JSONToken.END_ARRAY){
                                        //Ignore nested Arrays
                                    }
                                }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                    while(parser.nextToken() != JSONToken.END_OBJECT){
                                        //Ignore nested objects
                                        if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                            while(parser.nextToken() != JSONToken.END_OBJECT){
                                                //Ignore double nested objects
                                                if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                    while(parser.nextToken() != JSONToken.END_OBJECT){
                                                        //Ignore triple nested objects
                                                    }
                                                }
                                            }
                                        }
                                    
                                    }
                                }else{
                                    //Ignore Other Tokens
                                }  
                            }                               
                        }
                        if(!usersMap.containsKey(user.id))
                        usersMap.put(user.id,user);                                  
                    }    
                }                 
            }  
        }  
         
        for(ZenUser Iuser : usersMap.values()){
            system.debug('=============User: ' + Iuser.id + ' ' + Iuser.name + ' ' + Iuser.email + ' ' + Iuser.organizationId);
        }
        for(ZenTicket Iticket : tickets){
            system.debug('=====BAMBAM========Ticket: ' + Iticket.id + ' ' + Iticket.subject+ ' ' + Iticket.status+ ' ' + Iticket.requester_id);
            ZenUser Iuser = usersMap.get(Iticket.requester_id);
            Iticket.requester_name = Iuser.name;
            Iticket.requester_email = Iuser.email;
            Iticket.requester_organizationId = Iuser.organizationId;            
        }  
        
        system.debug('=============COUNT:'+tickets.size());
        for(ZenTicket Iticket : tickets){
            system.debug('=============Final Ticket: ' + Iticket.id + ' ' + Iticket.subject+ ' ' + Iticket.requester_name+ ' ' + Iticket.requester_email+ ' ' + Iticket.requester_organizationId);
            ZenUser Iuser = usersMap.get(Iticket.requester_id);
            Iticket.requester_name = Iuser.name;
            Iticket.requester_email = Iuser.email;
            Iticket.requester_organizationId = Iuser.organizationId;            
        }
        
        return tickets;
    }
    
}