/****************************************************************************************
  Class: PartnerLocatorDetailsTriggerTest
  Test Class for PartnerLocatorDetails Trigger
  Author - Christine Wang
  Date - 8/25/2014
  Revision History
****************************************************************************************/

@isTest
private class PartnerLocatorDetailsTriggerTest {

    static testMethod void myUnitTest(){
        Partner_Locator_Details__c locator;
    
        // create partner account
        Account testAccount = new Account(Name = 'Account-TestRecord');
        insert testAccount;  
      
        testAccount = [SELECT Id FROM Account WHERE Id =: testAccount.Id];
        testAccount.IsPartner = true;
        update testAccount;
        
        // create a contact
        Contact con = new Contact(LastName ='testCon',
                                  AccountId = testAccount.Id);
        insert con;
        
        // create a user
        User u;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            Profile p = [SELECT Id FROM Profile WHERE Name LIKE '%Partner%' LIMIT 1]; 
            u = new User( Alias = 'standt', 
                          Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', 
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id,
                          ContactId = con.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='tuser@perficient123.p3');
            insert u;
        }
        
        System.runAs(u) {
            // create Partner Locator Details
            locator = new Partner_Locator_Details__c(Partner_Account__c = testAccount.Id);
            insert locator;
        }
        
        locator = [SELECT Channel_Account_Manager__c FROM Partner_Locator_Details__c WHERE Id = :locator.Id];
        System.assertEquals(UserInfo.getUserId(), locator.Channel_Account_Manager__c );
    }
}