/*
Class: ContactTriggerHelper
Purpose: Supports trigger actions on Contact record
Author: Jaya
Created Date: 11/21/2013
*/
public class ContactTriggerHelper 
{
	
	// before insert action
	public void beforeInsertAction(List<Contact> contactList)
	{
		List<Id> acctIdList = new List<Id>();			
		for(Contact contact: contactList)
		{
			acctIdList.add(contact.AccountId);
		}
		// query for account
		Map<Id,Account> acctIdMap = new Map<Id,Account>([SELECT Id,OwnerId FROM Account WHERE Id IN: acctIdList]);
		if(!acctIdMap.isEmpty())
		{
			for(Contact contact: contactList)
			{
				if( acctIdMap.get(contact.AccountId) != null ){
					contact.Account_Owner__c = acctIdMap.get(contact.AccountId).OwnerId;
				}
			}			
			
		}
	}
	// Before update action
	public void beforeUpdateAction(List<Contact> contactList, Map<Id,Contact> contactMap)
	{
		List<Contact> cList = new List<Contact>();
		for(Contact contact: contactList)
		{
			Contact oldContact = contactMap.get(contact.Id); 
			//if(contact.AccountId != oldContact.AccountId)
			//{
				cList.add(contact);
			//}
		}
		if(!cList.isEmpty())
		{
			beforeInsertAction(cList);
		}
	}
    
    public void afterUpdateAction(List<Contact> contactList, Map<Id,Contact> oldContactMap)
    {
        List<Approval.ProcessSubmitRequest> approvalRequestList = new List<Approval.ProcessSubmitRequest>();
        Approval.ProcessSubmitRequest req;
        List<Approval.ProcessResult> approvalResults;
        Map<Id, ProcessInstance> pendingPIMap = New Map<id, ProcessInstance>();
        
        List<Contact> contacts = new List<Contact>();
        List<Id> contactIdList = new List<Id>();
        for(Contact c: contactList)
        {
            Contact oldContact = new Contact();
            oldContact = oldContactMap.get(c.Id);
            if(c.PRM_Contact_Approval__c && !oldContact.PRM_Contact_Approval__c)
            {
                contacts.add(c);    
                contactIdList.add(c.Id);            
            }
        }
        if(!contacts.isEmpty())
        {
            Map<Id,ProcessInstance> targetObjPI = new Map<Id,ProcessInstance>();
            for(ProcessInstance pInstance : [SELECT Id,TargetObjectID FROM ProcessInstance 
                                WHERE  TargetObjectId in :contactIdList AND Status=:'Pending'])
            {
                targetObjPI.put(pInstance.TargetObjectID, pInstance);
            }
            //Approval req creation
            for(Contact c : contacts)
            {
                if(targetObjPI.get(c.Id) == null)
                {
                    req = new Approval.ProcessSubmitRequest();
                    req.setComments('Submitting request for approval.');
                    req.setObjectId(c .Id);
                    approvalRequestList.add(req);
                }
                System.debug('Contact:'+c);
            }
            
            // Approval submission
            if(approvalRequestList.size() > 0)
            {
             System.debug('************ Approvalid2: ************ ' +approvalRequestList);
             approvalResults = Approval.process(approvalRequestList);
             System.debug('approvalResults:'+approvalResults);
            }
                
        }
    }

}