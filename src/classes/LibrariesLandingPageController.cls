public with sharing class LibrariesLandingPageController {
    
    private Integer pageNumber;
    private Integer pageSize;
    private Integer totalPageNumber;
    private List<ContentVersion> assets ;
    private String FEATURED_ITEMS = 'Featured Items';
    
    public List<ContentVersion> featuredAssets {get;set;}
    public List<Content_Thumbnail__c> contentThumbnails {get;set;}
    public string searchString {get;set;}
    public Boolean viewingAllResources {get;set;}
    public string selectedType {get;set;}
    public String soql {get;set;}
    public String selectedLibrary {get;set;}
    public Integer currentPageNumberClicked {get;set;}
    public String soqlsort {get;set;}
    public List<ContentVersion> pageAssets { get;set;}
    public LibrariesLandingPageController cont { get { return this; } }
    
    public String featuredItems { get{ return FEATURED_ITEMS; } 
                                  set;}
    public List<Integer> pageNumbersList {get{
                                            integer left_max = 1;
                                            integer total_max = 3;
                                            integer begPg = 1;
                                            integer endPg = getTotalPageNumber();
                                            if (currentPageNumberClicked - left_max > begPg) {
                                                begPg = currentPageNumberClicked - left_max;
                                            }
                                            if ( (begPg + total_max - 1) < endPg) {
                                                endPg = begPg + total_max - 1;
                                            }
                                            pageNumbersList = new List<Integer>();
                                            for( integer i=begPg; i<= endPg; i++ ){
                                                pageNumbersList.add(i);                                             
                                            }
                                            pageNumbersList.sort();
                                            return pageNumbersList;
                                         }set;}
    
    public Map<String, String> typeToURL {get{
                                            typeToURL = new Map<String, String>();
                                            typeToURL.put( 'Sales', '/sfc/#workspaceView?selectedWorkspaceId=058g00000008SDx');
                                            typeToURL.put( 'Marketing', '/sfc/#workspaceView?selectedWorkspaceId=058g00000008SDx');
                                            typeToURL.put( 'Training', '/sfc/#workspaceView?selectedWorkspaceId=058g00000008SDx');
                                            typeToURL.put( 'Technology', '/sfc/#workspaceView?selectedWorkspaceId=058g00000008SDx');
                                            typeToURL.put( 'Featured Items', '/sfc/#workspaceView?selectedWorkspaceId=058g00000008SDx');
                                            return typeToURL;
                                        }set;}
    
    
    public String sortField { get  { if (sortField == null) {sortField = 'Title'; } return sortField; }
                              set{
                                   if (value == sortField)
                                     sortDir = ( sortDir == 'ASC')? 'DESC' : 'ASC';
                                   else
                                     sortDir = 'ASC';
                               sortField = value;
                             }}
                             
    public String sortDir { get  
                            { if (sortDir == null) {  sortDir = 'ASC'; } return sortDir;}
                            set;}
    
    public Integer getPageSize(){
        return pageSize;
    }
    
    public Integer getTotalPageNumber(){
        if (totalPageNumber == 0 && assets !=null)
        {
            totalPageNumber = assets.size() / pageSize;
            Integer mod = assets.size() - (totalPageNumber * pageSize);
            if (mod > 0){
                totalPageNumber++;
            }
        }
        return totalPageNumber;
    }

    public Integer getPageNumber(){
        return pageNumber;
    }
    
    
    public Boolean hasPrevious {
        get {
            return ( currentPageNumberClicked > 1 ) ;
        }
        
    }

    public Boolean hasNext {
        get {
            return ( currentPageNumberClicked < totalPageNumber );
        }
    }
    
    
    public LibrariesLandingPageController(){
        contentThumbnails = [ SELECT Id, Image__c, Library__c, Description_Rich__c 
                              FROM Content_Thumbnail__c 
                              WHERE Thumbnail_Type__c = 'Library'];
        searchString = Apexpages.currentPage().getParameters().get('searchStr');
        selectedLibrary = Apexpages.currentPage().getParameters().get('lib');
        if( ( searchString == null || searchString == '' ) && ( selectedLibrary == null || selectedLibrary == '' ) ){
            viewingAllResources = true;
        }
        pageNumber = 0;
        totalPageNumber = 0;
        pageSize = 20;
        totalPageNumber = 0;
        currentPageNumberClicked = 1;
        featuredAssets = [ SELECT Id, Title, Featured_Asset__c
                           FROM ContentVersion 
                           WHERE Featured_Asset__c = true LIMIT 5 ];
        if( selectedLibrary != null && selectedLibrary != '' ){
            getLibraryResources();
        }
        if( searchString != null && searchString != '' ){
            searchResources();
        }
    }
    
    public PageReference searchResources(){
        assets = null;
        bindData(1);
        viewingAllResources = false;
        if( assets == null || assets.isEmpty() ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'There are no results for your search.'));
        }
        return null;
    }
    
    public PageReference getLibraryResources(){
        pageAssets = new List<ContentVersion>();
        String query = 'SELECT Id, Title, FileType, Description, TagCsv, Content_Thumbnail__r.Image__c, ContentUrl, Content_Thumbnail__r.Library__c FROM ContentVersion';
        query += ' WHERE isLatest = true AND Content_Thumbnail__r.Library__c = \'' + selectedLibrary + '\'' + 
                 ' ORDER BY ' + sortField + ' ' + sortDir + ' LIMIT 200'; 
        assets = Database.query( query );
        if( assets!= null && !assets.isEmpty() ){
            bindData(1);
        }
        viewingAllResources = false;
        if( assets == null || assets.isEmpty() ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available in this library.'));
        }
        return null;
    }
    
    private void bindData(Integer newPageIndex){
        try{
            if ( assets == null || assets.isEmpty() ){
                soql = 'FIND \'*' + searchString + '*\' IN ALL FIELDS RETURNING ContentVersion ( Id, Title, FileType, Description, TagCsv, ContentUrl, ';
                soql += 'Content_Thumbnail__r.Image__c, Content_Thumbnail__r.Library__c ';
                soql += ' WHERE isLatest = true ';
                List<List<Sobject>> searchResults = Search.query( soql + ' ORDER BY ' + sortField + ' ' + sortDir + ')' + ' LIMIT 200');
                assets = ( List<ContentVersion> ) searchResults[0];
            }
            pageAssets = new List<ContentVersion>();
            Transient Integer counter = 0;
            Transient Integer min = 0;
            Transient Integer max = 0;
            if (newPageIndex > pageNumber){
                min = pageNumber * pageSize;
                max = newPageIndex * pageSize;
            }else{
                max = newPageIndex * pageSize;
                min = max - pageSize;
            }
            for(ContentVersion cv : assets){
                counter++;
                if (counter > min && counter <= max){
                    pageAssets.add( cv );
                }   
                pageNumber = newPageIndex;
                if( pageAssets == null || pageAssets.size() <= 0){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Data not available for this view.'));
                }
            }
        }
        
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
        }
    }
    
    public Pagereference gotoPage( ){
        BindData( currentPageNumberClicked );
        return null;
    }
    
    public PageReference previous(){
    	currentPageNumberClicked = currentPageNumberClicked - 1;
        BindData( currentPageNumberClicked );
        return null;
    }
    
    public PageReference next(){
    	currentPageNumberClicked = currentPageNumberClicked + 1;
        BindData( currentPageNumberClicked );
        return null;
    }

    
    public Pagereference toggleSort() {
        if( searchString != null && searchString != '' ){
            soql = 'FIND \'*' + searchString + '*\' IN ALL FIELDS RETURNING ContentVersion ( Id, Title, FileType, Description, ContentUrl, TagCsv, Content_Thumbnail__r.Image__c, ';
            soql += ' Content_Thumbnail__r.Library__c WHERE isLatest = true ';
            List<List<Sobject>> searchResults = Search.query( soql + ' ORDER BY ' + sortField + ' ' + sortDir + ') LIMIT 200' );
            assets = searchResults[0];
            bindData(1);
        }else{
            getLibraryResources();
        }
        return null;
    }
    
}