global class VLStatusBatchParty implements Database.Batchable<Account>, Database.AllowsCallouts{

//Account [] AssetsToUpdate = [select Id,Name,AccountNumber,CreatedDate from Account where CreatedDate >= YESTERDAY or SystemModstamp >= YESTERDAY or XPSynced__c = 'N'];
Account [] AssetsToUpdate = [select Id,Name,AccountNumber,CreatedDate from Account where CreatedDate >= YESTERDAY or XPSynced__c = 'N'];

global Iterable<Account> start(database.batchablecontext BC){
    return (AssetsToUpdate);    
}

global void execute(Database.BatchableContext BC, List<Account> scope){
    for(Account a : scope){
            VLStatusParty.PartyCreate_Update(a.Id);
    }    
}

global void finish(Database.BatchableContext info){
    }//global void finish loop

}