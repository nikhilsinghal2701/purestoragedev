@isTest
public class LeadStatusTransitionsProcessorTest 
{

    static testMethod void testStatusTransitions()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Lead_Status_Transitions_Enabled__c = true;
        cs.Lead_Status_Transitions_Exception_Group__c = '123455';
        cs.Lead_Status_Transitions_Go_Live_Date__c = system.now();
        insert cs;
        
        test.StartTest();
        Lead myLead = new Lead();
        myLead.FirstName = 'TestFirst';
        myLead.LastName = 'TestLast';
        myLead.Email = 'testfirst@testlast.test';
        myLead.Company = 'testCo';
        
        insert myLead;
        
        Lead myLeadInsert = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Open', myLeadInsert.Status);
        
        
        /******************CANNOT UPD TO WORKING 2 MANUALLY**************************/
        myLeadInsert.Status = 'Working 2';
        string myErrorMsg = '';
        try{
            update myLeadInsert;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        /********************ADD TASK TO UPD TO WORKING 1**************************/
        Task t1 = new Task();
        t1.WhoId = myLead.Id;
        t1.Status = 'Completed';
        insert t1;
        
        Lead myLeadUpd1 = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Working 1', myLeadUpd1.Status);
        
        Task t2 = new Task();
        t2.WhoId = myLead.Id;
        t2.Status = 'Completed';
        insert t2;
        
        myLeadUpd1.Status = 'Working 6';
        myErrorMsg = '';
        try{
            update myLeadUpd1;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        Task t3 = new Task();
        t3.WhoId = myLead.Id;
        t3.Status = 'Completed';
        insert t3;
        
        myLeadUpd1.Status = 'Working 6';
        myErrorMsg = '';
        try{
            update myLeadUpd1;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        Task t4 = new Task();
        t4.WhoId = myLead.Id;
        t4.Status = 'Completed';
        insert t4;
        
        myLeadUpd1.Status = 'Working 6';
        myErrorMsg = '';
        try{
            update myLeadUpd1;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        Task t5 = new Task();
        t5.WhoId = myLead.Id;
        t5.Status = 'Completed';
        insert t5;
        
        myLeadUpd1.Status = 'Working 6';
        myErrorMsg = '';
        try{
            update myLeadUpd1;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        /*************************UPD TO NURTURE**************************/
        myLeadUpd1.Status = 'Nurture';
        update myLeadUpd1;
        
        Lead myLeadUpd2 = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Nurture', myLeadUpd2.Status);
        
        myLeadUpd2.Status = 'Working 1';
        myErrorMsg = '';
        try{
            update myLeadUpd2;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        /*************************UPD TO FORECAST**************************/
        myLeadUpd2.Status = 'Forecast';
        update myLeadUpd2;
        Lead myLeadUpd3 = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Forecast', myLeadUpd3.Status);
        myLeadUpd3.Status = 'Working 1';
        
        myErrorMsg = '';
        try{
            update myLeadUpd3;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        /*************************UPD TO QUALIFIED**************************/
        myLeadUpd3.Status = 'Qualified';
        update myLeadUpd3;
        Lead myLeadUpd4 = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('Qualified', myLeadUpd4.Status);
        myLeadUpd4.Status = 'Working 1';
        
        myErrorMsg = '';
        try{
            update myLeadUpd4;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        /**************************UPD TO DEAD**************************/
        myLeadUpd4.Status = 'DEAD';
        update myLeadUpd4;
        Lead myLeadUpd5 = [select id, Status from Lead where id =: myLead.Id];
        system.assertEquals('DEAD', myLeadUpd5.Status);
        myLeadUpd5.Status = 'Working 1';
        
        myErrorMsg = '';
        try{
            update myLeadInsert;
        }
        catch(DMLException e){
            myErrorMsg = e.getMessage();
        }
        system.assertNotEquals('', myErrorMsg);
        
        test.stopTest();
        
        
    }
    
}