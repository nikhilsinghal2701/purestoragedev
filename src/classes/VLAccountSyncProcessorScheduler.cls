global class VLAccountSyncProcessorScheduler {
   
    public static void createJobsEveryFiveMin(){
        System.schedule('Satmetrix Integration Job - Process Party Sync 00', '0 00 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 05', '0 05 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 10', '0 10 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 15', '0 15 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 20', '0 20 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 25', '0 25 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 30', '0 30 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 35', '0 35 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 40', '0 40 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 45', '0 45 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 50', '0 50 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
        System.schedule('Satmetrix Integration Job - Process Party Sync 55', '0 55 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
    }
    
    public static void createJobEveryHour(){
        //Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        System.schedule('Satmetrix Integration Job - Process Party Sync Hly', '0 0 * * 1-12 ? *',  New VLStatusPartySchedulerContext());
    }
}