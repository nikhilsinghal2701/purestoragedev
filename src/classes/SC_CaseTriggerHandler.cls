/***********************************************
 * Description : Handler for SC_CaseTrigger. It has before insert & before update methods to 
                 split the comma separted email address from the case collaborators field into individual email fields;
                 It has after insert future method to create contacts that are marked to be CCed as case team members.
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/
public without sharing class SC_CaseTriggerHandler
{
    public static final string CC_CONTACT_ROLENAME = system.label.SC_CCContactRole;
    //public static final string CC_ACCOUNTTEAM_ROLENAME = system.label.SC_CCAccountTeamRole;
    public static final string ERROR_EMAIL_SUBJECT = system.label.SC_CCContactOnEmailsErrorMsgSubject;
    public static final string ERROR_EMAIL_STR = system.label.SC_CCContactOnEmailsErrorMsgBody + '\n' + '\n';
    public static final string ACCOUNTTEAM_ERROR_EMAIL_SUBJECT = system.label.SC_CopyAccountTeamsIntoCaseTeamsErrorSubject;
    public static final string ACCOUNTTEAM_ERROR_EMAIL_STR = system.label.SC_CopyAccountTeamsIntoCaseTeamsErrorBody + '\n' + '\n';
    public static final string CASECREATION_ERROR_EMAIL_SUBJECT = system.label.SC_CaseCreationEmailErrorSubject;
    public static final string CASECREATION_ERROR_EMAIL_BODY = system.label.SC_CaseCreationEmailErrorBody + '\n \n';
    
    // this flag is used to avoid creating case team members redundantly, because when a case is inserted,
    // insert trigger will create case team, and if workflow updates any fields, the update trigger will also try to 
    // create case team
    public static boolean isInsert = false;
    
    // on before insert, split the emails into individual fields
    public static void onBeforeInsert(List<Case> newCases)
    {   
        for(Case c : newCases)
        {
            if(c.Case_Collaborators__c != null && c.Case_Collaborators__c.contains(','))
            {
                splitEmails(c);
            }
        }
    }
    
    // on before update, split the emails into individual fields
    public static void onBeforeUpdate(Map<Id, Case> oldCases, Map<Id, Case> newCases)
    {
        for(Case c : newCases.values())
        {
            if(oldCases.get(c.Id).Case_Collaborators__c != c.Case_Collaborators__c)
            {
                splitEmails(c);
            }
        }
    }
    
    // on after insert, collect the case id and account id and send it to future method to create cc contacts and account teams as case team members
    public static void onAfterInsert(Map<Id, Case> newCasesMap)
    {
        Set<Id> accIdsToCreateCCContacts = new Set<Id>();
        Set<Id> accIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();
        
        for(Case c : newCasesMap.values())
        {
            system.debug('-----> Is AccountId is coming? : ' + c.AccountID);
            
            if(c.AccountId != null)
            {
                accIdsToCreateCCContacts.add(c.AccountId);
                accIds.add(c.AccountId);
                caseIds.add(c.Id);
            }
            
            if(c.Cloud_Assist_Account__c != null)
            {
                accIds.add(c.Cloud_Assist_Account__c);
                caseIDs.add(c.ID);
            }
        }
        
        system.debug('-----> To Create Contacts: ' + accIdsToCreateCCContacts);
        system.debug('-----> To Create Accounts: ' + accIds);
        
        if(accIdsToCreateCCContacts.size() > 0)
        {
            createCaseTeam(caseIds, accIdsToCreateCCContacts);
        }
        
        if(accIds.size() > 0)
        {
            createAccTeamAsCaseTeamFuture(caseIds, accIds);
        }
    }
    
    // on after updated, if the standard account or cloud assist account is updated, 
    // delete the existing case team members and create new
    public static void onAfterUpdate(Map<Id, Case> oldMap, Map<Id, Case> newMap)
    {
        Set<Id> accIds = new Set<Id>();
        Set<Id> caseIds = new Set<Id>();
        
        for(Case c : newMap.values())
        {
            if(c.AccountId != oldMap.get(c.Id).AccountId)
            {
                accIds.add(c.AccountId);
                caseIds.add(c.Id);
            }
            
            if(c.Cloud_Assist_Account__c != oldMap.get(c.Id).Cloud_Assist_Account__c)
            {
                accIds.add(c.Cloud_Assist_Account__c);
                caseIds.add(c.Id);
            }
        }
        
        system.debug('-----> To Update Accounts: ' + accIds);
        
        if(!isInsert && accIds.size() > 0)
        {
            updateCaseTeam(caseIds, accIds);
        }
    }
    
    // splits case collaborators field by comma, populate them into each email field
    public static void splitEmails(Case c)
    {
        SObject s;
        List<string> emailsList = new List<string>();
        
        nulloutAllEmailFields(c);
        
        if(c.Case_Collaborators__c != null && c.Case_Collaborators__c.contains(','))
        {
            emailsList = c.Case_Collaborators__c.split(',');

            if(emailsList.size() <= 10)
            {
                s = (SObject) c;
                
                for(Integer i = 0; (i < 10 && i < emailsList.size()); i++)
                {
                    s.put('Case_Collaborator_' + (i + 1) + '__c', emailsList[i]);
                }
            }
            else if(emailsList.size() > 10)
            {
                c.Case_Collaborators__c.addError(system.label.SC_CaseCollaborators_ErrorMsg);
            }
        }
        else if(c.Case_Collaborators__c != null)
        {
            c.Case_Collaborator_1__c = c.Case_Collaborators__c;
        }
    }
    
    // reset all the email fields before update
    public static void nulloutAllEmailFields(Case c)
    {
        c.Case_Collaborator_1__c = c.Case_Collaborator_2__c = c.Case_Collaborator_3__c = null;
        c.Case_Collaborator_4__c = c.Case_Collaborator_5__c = c.Case_Collaborator_6__c = null;
        c.Case_Collaborator_7__c = c.Case_Collaborator_8__c = c.Case_Collaborator_9__c = c.Case_Collaborator_10__c = null;
    }
    
    // future method to create case team members - without a future method, if a case is created by a customer community user the trigger cannot create case team
    @future
    public static void createCaseTeam(set<Id> caseIds, Set<Id> accIds)
    {
        Map<Id, Account> accountsMap = new Map<Id, Account>();
        List<Case> cases = new List<Case>();
        List<CaseTeamMember> members = new List<CaseTeamMember>();
        List<Database.SaveResult> results;

        string emailBody = ERROR_EMAIL_STR;
        boolean isError = false;
        
        Id caseTeamRoleID = getCCContactRoleID();
        
        // query on the case
        cases = [Select Id, AccountId, OwnerId from Case where Id in : caseIds];
        
        // query case's account with all CC enabled contacts
        accountsMap = new Map<Id, Account>([Select Id, (Select Id from Contacts where CC_On_All_Cases__c = :true) from Account where Id in :accIds]);
        
        for(Case c : cases)
        {
            if(accountsMap.get(c.AccountId) != null && accountsMap.get(c.AccountId).Contacts != null && accountsMap.get(c.AccountId).Contacts.size() > 0)
            {
                for(Contact con : accountsMap.get(c.AccountId).Contacts)
                {
                    members.add(populateCaseTeamMemberValues(c, con.Id, caseTeamRoleID));
                }
            }
        }
        
        // create case team members
        if(members.size() > 0)
        {
            results = Database.insert(members, false);
            
            for(Integer i = 0; i < results.size(); i++)
            {
                if(!results[i].IsSuccess())
                {
                    emailBody += members[i].MemberID + '  ' + members[i].ParentID + '  ' + results[i].getErrors()[0].getMessage() + '\n';
                    isError = true;
                }
            }

            if(isError)
            {
                Util.SendAdminEmail(ERROR_EMAIL_SUBJECT, emailBody, Label.Help_Desk_Email);
            }
        }
    }
    
    
    // a future method to create accoun team as case team members
    @future
    public static void createAccTeamAsCaseTeamFuture(Set<Id> caseIds, Set<Id> accIds)
    {
        createAccTeamAsCaseTeam(caseIds, accIds, true);
    }
    
    // method that creates account teams with roles AE and SE as case team members and updates a flag on case
    // after successful creation, so that an email will be sent out to notify AE and SE that a case is created
    public static void createAccTeamAsCaseTeam(Set<Id> caseIds, Set<Id> accIds, boolean sendCaseCreationEmail)
    {
        Map<Id, Account> accountsMap = new Map<Id, Account>();
        Map<Id, Case> casesMap = new Map<Id, Case>();
        Set<Id> caseIdsToUpdate = new Set<Id>();
        List<Case> cases = new List<Case>();
        List<CaseTeamMember> members = new List<CaseTeamMember>();
        List<Database.SaveResult> results;
        List<Database.SaveResult> caseUpdateResults;
        Id accId;

        string emailBody = ACCOUNTTEAM_ERROR_EMAIL_STR;
        boolean isError = false;
        string caseUpdateEmailBody = CASECREATION_ERROR_EMAIL_BODY;
        boolean isCaseUpdateError = false;
        
        Map<string, Id> accTeamRoles = getAccountTeamRoles();
        
        // query on the case
        casesMap = new Map<Id, Case>([Select Id, AccountId, Cloud_Assist_Account__c, OwnerId, Case_Creation_Email__c from Case where Id in : caseIds]);
        
        // query case's account with all CC enabled contacts
        accountsMap = new Map<Id, Account>([Select Id, 
                                                   //(Select Id from Contacts where CC_On_All_Cases__c = :true), 
                                                   (Select AccountId, UserId, TeamMemberRole from AccountTeamMembers where TeamMemberRole = 'Account Executive' or TeamMemberRole = 'Systems Engineer') from Account where Id in :accIds]);
        
        
        for(Case c : casesMap.values())
        {
            accId = null;
            
            if(c.AccountId != null)
                accID = c.AccountId;
            else if(c.Cloud_Assist_Account__c != null)
                accId = c.Cloud_Assist_Account__c;
            
            if(accId != null)
            {
                if(accountsMap.get(accId) != null && accountsMap.get(accId).AccountTeamMembers != null && accountsMap.get(accId).AccountTeamMembers.size() > 0)
                {
                    for(AccountTeamMember am : accountsMap.get(accId).AccountTeamMembers)
                    {
                        members.add(populateCaseTeamMemberValues(c, am.UserId, accTeamRoles.get(am.TeamMemberRole)));
                    }
                }
            }
        }
        
        // create case team members
        if(members.size() > 0)
        {
            results = Database.insert(members, false);
            
            for(Integer i = 0; i < results.size(); i++)
            {
                if(!results[i].IsSuccess())
                {
                    emailBody += members[i].MemberID + '  ' + members[i].ParentID + '  ' + results[i].getErrors()[0].getMessage() + '\n';
                    isError = true;
                }
                else
                {
                    if(!caseIdsToUpdate.contains(members[i].ParentId))
                    {
                        casesMap.get(members[i].ParentId).Case_Creation_Email__c = true;
                        cases.add(casesMap.get(members[i].ParentId));
                        caseIdsToUpdate.add(members[i].ParentId);
                    }
                }
            }

            if(isError)
            {
                Util.SendAdminEmail(ACCOUNTTEAM_ERROR_EMAIL_SUBJECT, emailBody, Label.Help_Desk_Email);
            }
            
            // if it is an insert, update the case flag which fires an workflow to notify the AE and SE that a case is created
            if(sendCaseCreationEmail && cases.size() > 0)
            {
                caseUpdateResults = Database.update(cases, false);
                
                for(Integer i = 0; i < results.size(); i++)
                {
                    if(!results[i].IsSuccess())
                    {
                        caseUpdateEmailBody += cases[i].Id + '  ' + results[i].getErrors()[0].getMessage() + '\n';
                        isCaseUpdateError = true;
                    }
                }
                
                if(isCaseUpdateError)
                {
                    Util.SendAdminEmail(CASECREATION_ERROR_EMAIL_SUBJECT, caseUpdateEmailBody, Label.Help_Desk_Email);
                }
            }
        }
    }
    
    // a future method which deletes the exising old account's team from case team and create new ones
    @future
    public static void updateCaseTeam(Set<Id> caseIds, Set<Id> accIds)
    {
        List<CaseTeamMember> caseteamMembers = new List<CaseTeamMember>();
        Map<string, Id> accTeamRoles = getAccountTeamRoles();
        List<Database.DeleteResult> results;
        
        string emailBody = ACCOUNTTEAM_ERROR_EMAIL_STR;
        boolean isError = false;
        
       for(CaseTeamMember ctm : [Select Id, TeamRoleId, TeamRole.Name from CaseTeamMember where ParentId in :caseIds])
       {
           if(accTeamRoles.containskey(ctm.TeamRole.Name))
           caseTeamMembers.add(ctm);
       }
        
        if(caseTeamMembers.size() > 0)
        {
            results = Database.delete(caseTeamMembers, false);
            createAccTeamAsCaseTeam(caseIds, accIds, false);
            
            for(Integer i = 0; i < results.size(); i++)
            {
                if(!results[i].IsSuccess())
                {
                    emailBody += caseTeamMembers[i].MemberID + '  ' + caseTeamMembers[i].ParentID + '  ' + results[i].getErrors()[0].getMessage() + '\n';
                    isError = true;
                }
            }

            if(isError)
            {
                Util.SendAdminEmail(ACCOUNTTEAM_ERROR_EMAIL_SUBJECT, emailBody, Label.Help_Desk_Email);
            }
        }
        else
        {
            createAccTeamAsCaseTeam(caseIds, accIds, false);
        }
    }
    
    // populate case team member record
    public static CaseTeamMember populateCaseTeamMemberValues(Case c, Id conId, ID caseTeamRoleID)
    {
        CaseTeamMember cm = new CaseTeamMember(ParentId = c.ID, MemberId = conId, TeamRoleID = caseTeamRoleID);
        return cm;
    }
    
    // gets the contact role for CC enabled contacts
    public static Id getCCContactRoleID()
    {
        try
        {
            CaseTeamRole ctr = [Select Id from CaseTeamRole where Name = :CC_CONTACT_ROLENAME];
            return ctr.Id;
        }
        catch(Exception e)
        {
            for(Case c : (List<Case>) trigger.new)
            {
                c.addError(e);
            }
            
            return null;
        }
    }
    
    // gets the Account Team role
    public static Map<string, Id> getAccountTeamRoles()
    {
        Map<string, Id> teamRoleMap = new Map<string, Id>();
        
        try
        {
            for(CaseTeamRole ctr : [Select Id, Name from CaseTeamRole where Name = 'Account Executive' or Name = 'Systems Engineer'])
            {
                teamRoleMap.put(ctr.Name, ctr.Id);
            }
            
            return teamRoleMap;
        }
        catch(Exception e)
        {
            for(Case c : (List<Case>) trigger.new)
            {
                c.addError(e);
            }
            
            return null;
        }
    }
}