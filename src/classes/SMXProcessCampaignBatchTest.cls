@istest
global class SMXProcessCampaignBatchTest
{

       public static testMethod void testBatch(){ 
       String strCampaignId;
       Test.startTest();
       strCampaignId=prepareTestData();
       SMXProcessCampaignBatch StrBatch= new SMXProcessCampaignBatch() ;
       ID batchprocessid = Database.executeBatch(StrBatch);     
       clearTestData();
       Test.stopTest(); 
       }

     static String prepareTestData(){
             
        Account a = new Account(Name='SMX Test Account', Type = 'Customer');
        insert a;
        Contact c1 = new Contact(FirstName='SMX TestFName1', LastName='SMX TestLName1', AccountID=a.id, Email='this.is.a.smx.test@acmedemo.com');
        insert c1;

        
        Campaign campaign = new Campaign(Name='SMX Test Campaign1',StartDate=Date.today(),Survey_Status__c='Approved');
        insert campaign;
        
        CampaignMember cm1 = new CampaignMember(ContactID=c1.id, CampaignID=campaign.ID);
        insert cm1;
       
        return campaign.Id;          
      }
      
      static void clearTestData(){
       Campaign campaign = [SELECT Id from CAMPAIGN WHERE NAME = 'SMX Test Campaign1'];
       delete campaign;
       Contact contact = [SELECT Id from CONTACT WHERE FirstName = 'SMX TestFName1'];
       delete contact;
      }  
       
}