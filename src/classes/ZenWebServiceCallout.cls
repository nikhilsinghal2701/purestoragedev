public class ZenWebServiceCallout{
    @Future(callout=true)
    public static void createOrganization(String name, String oppId, String accId){
        
        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
                              
        String USERNAME = configuration.Zendesk_Username__c;
        String PASSWORD = configuration.Zendesk_Password__c;
        HttpRequest req = new HttpRequest();
        Http http = new Http();
    
        req.setMethod('POST');
    
        String url = configuration.Zendesk_URL__c + '/api/v2/organizations.json';  //JSON
        //String url = configuration.Zendesk_URL__c + '/organizations.xml';  //XML Deprecated
        req.setEndpoint(url);       
    
        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        req.setHeader('Content-Type', 'application/json');
        req.setBody('{"organization":{"name":"'+name+'"}}');  //JSON
        //req.setBody('<organization><name>'+name+'</name></organization>');  //XML Deprecated
        System.debug(req.getBody());
        HTTPResponse resp;
        if(name!='isTest'){ // Test Method Bypass
            resp = http.send(req);
        }else{
            resp = new HTTPResponse();
            resp.setBody('{"organization":{"url":"https://pure.zendesk.com/api/v2/organizations/23532100.json","id":23532100,"name":"Test","shared_tickets":false,"shared_comments":false,"external_id":null,"created_at":"2013-11-12T20:37:43Z","updated_at":"2013-11-12T20:37:43Z","domain_names":[],"details":null,"notes":null,"group_id":null,"tags":[],"organization_fields":{}}}');
            //resp.setBody('XML'); //Deprecated
        }
        System.debug(resp.getBody());
        
        JSONParser parser = JSON.createParser(resp.getBody());
        
        parser.nextToken();
        while (parser.nextToken() != null)  
        {              
            if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
            {                                                  
                while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                {  
                    if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                    {  
                        parser.nextToken();      
                        String orgId = parser.getText();  
                        System.debug('orgId:'+orgId ); 
                        Account acc = [Select ac.Id,ac.Name,ac.Zendesk_Organization__c,ac.Support_Must_Not_Contact_End_Customer__c from Account ac where ac.id =: accId];
                        
                        if(!acc.Support_Must_Not_Contact_End_Customer__c){
                            List<OpportunityContactRole> contactRoles = [Select contactid from OpportunityContactRole where OpportunityId=:oppId];        
                            for(OpportunityContactRole cr : contactRoles ){
                                Contact c = [Select c.id, c.name, c.email from Contact c where c.id =: cr.contactid]; 
                                System.debug('Contact name:'+c.name);
                                createEndUser(c.name, c.email, orgId);
                    		}
                		}
                        acc.Zendesk_Organization_Id__c = orgId;
                        update acc;
                    }                                      
                }
            }
        }
        
        /* XML Parser Deprecated
        dom.Document doc = resp.getBodyDocument();
        dom.XmlNode [] nodes = doc.getRootElement().getChildElements(); 

        for(dom.XMLNode node :nodes){
            if(node.getName() == 'id'){
                String orgId = node.getText();
                //System.debug('orgId:'+orgId ); 
                Account acc = [Select ac.Id,ac.Name,ac.Zendesk_Organization__c,ac.Support_Must_Not_Contact_End_Customer__c from Account ac where ac.id =: accId];
                
                if(!acc.Support_Must_Not_Contact_End_Customer__c){
                    List<OpportunityContactRole> contactRoles = [Select contactid from OpportunityContactRole where OpportunityId=:oppId];        
                    for(OpportunityContactRole cr : contactRoles ){
                        Contact c = [Select c.id, c.name, c.email from Contact c where c.id =: cr.contactid]; 
                        System.debug('Contact name:'+c.name);
                        createEndUser(c.name, c.email, orgId);
                    }
                }
                acc.Zendesk_Organization_Id__c = orgId;
                update acc;
            }
        }
		*/
    }
    
        
    public static void createEndUser(String name, String email,String orgId){
    
        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
                              
        String USERNAME = configuration.Zendesk_Username__c;
        String PASSWORD = configuration.Zendesk_Password__c;
        HttpRequest req = new HttpRequest();
        Http http = new Http();
    
        req.setMethod('POST');
    
        String url = configuration.Zendesk_URL__c + '/api/v2/users.json';  //JSON
        //String url = configuration.Zendesk_URL__c + '/users.xml';  //XML Deprecated
        req.setEndpoint(url);       
    
        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        req.setHeader('Content-Type', 'application/json');
        //req.setBody('<user><email>'+email+'</email><name>'+name+'</name><organization-id>'+orgId+'</organization-id><roles>0</roles><restriction-id>2</restriction-id></user>');  //XML Deprecated
        req.setBody('{"user": {"name": "'+name+'", "email": "'+email+'","organization_id":'+orgId+',"role":"end-user"}}'); // JSON
        if(email != 'test@istest.com'){ //Test Method Bypass
            HTTPResponse resp = http.send(req);
        	System.debug(resp.getBody());
        }
    }
}