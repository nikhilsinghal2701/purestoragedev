/*
Class: TestDataFactory
Purpose: to build data for test class
Author: Jaya
Created Date: 11/21/2013
*/
@isTest
public class TestDataFactory 
{
	public Account getAccount()
	{
		Account acct = new Account(Type = 'Analyst', Name = 'TestAccountData', Company_Size__c = '0-499');
		return acct;
	}
	public Contact getContact(Account acct)
	{
		Contact contact = new Contact(LastName = 'testCntName', Email = 'cntTest@c.com', AccountId = acct.Id);
		return contact;
	}
	public Lead getLead()
	{
		Lead lead = new Lead(Company = 'TestCompany',LastName='LastNameTest');
		return lead;
	}
	public Profile getProfile(String strProfile)
	{
		Profile p =  [SELECT Id, Name FROM Profile WHERE Name =: strProfile];
		return p;
	}
	public User getUser(Profile p, Contact contact)
	{
		User user = new User(Alias = 'standt', Email='standarduser@testorg.com', 
						      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
						      LocaleSidKey='en_US', ProfileId = p.Id, 
						      TimeZoneSidKey='America/Los_Angeles', UserName='testU@testorg.com' );
		 if( contact != null ){
	      	user.ContactId = contact.Id;
	     }
		return user;
	}

	public UserLicense getLicense(String key) {
		List<UserLicense> licenses = [Select LicenseDefinitionKey, Id from UserLicense WHERE LicenseDefinitionKey =: key limit 1];
		return licenses[0];
	}

	public Profile getPrifleByLicense(String key) {
		UserLicense license = getLicense(key);
		if(license != null) {
			for(Profile profile :[Select Id, UserLicenseId from Profile Where UserLicenseId =: license.id order by CreatedDate desc limit 10]) {
				return profile;				
			}
		}
		return null;
	}

    public Case createCase()
    {
        Case caseObj = new Case(Status = 'New',  Origin = 'Web');
        return caseObj;
    }
    
    public Opportunity createOpportunity(string aid,string es)
    {
	//Opportunity o = new Opportunity(Competition__c='Other',LeadSource='Web',Eligible_for_the_Love_Your_Storage_Prog__c='Yes',Customer_Reference__c='Public Reference',AccountId=aid, Name='testopportunityName',StageName='Stage 8 - Closed/No Decision', CloseDate=Date.today(),Extension_Status__c=es);        
    Opportunity o = new Opportunity(Channel_Account_Manager__c=UserInfo.getUserId(),AccountId=aid, Name='testopportunityName',StageName='Prequalified', CloseDate=Date.today(),Extension_Status__c=es,Registration__c='test123');        
    
    insert o;
    
    return o;
    }
	
}