/*****************************************************************************
 * Description : Interface class for Partner Locator Service
 *				 This service operates on the sObject Partner_Locator_Details__c 
 *				 and handles all the queries related to filter select options, search etc.
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/25/2014
 * Version     : 1.0 
 *
 * 
 *****************************************************************************/
public interface PartnerLocatorService {
  /**
  	*	Purpose of this operation is to 
  	*	1. Read the search filter or criteria from the input Apex object PartnerLocatorSearchBean
  	*	2. Prepare the SOQL query to retrieve results from the sObject Partner_Locator_Details__c
  	*	3. Apply any filter or criteria to the SOQL as read the Search Bean instance
  	*	4. Retrieve the Collection of results of sObject type Partner_Locator_Details__c
  	*	5. Convert the results from sObject type Partner_Locator_Details__c to a custom object bean PartnerLocatorBean
  	*	6. Return the Collection of results of custom bean type PartnerLocatorBean
  	*/
	List<PartnerLocatorBean> search(PartnerLocatorSearchBean bean);
  /**
    * Return the Partner Locator Detail record for the input parameter [Name]
    * Return value is a custom bean object - PartnerLocatorBean
    */
  PartnerLocatorBean getPartnerLocatorByName(String name);  
  /**
  	*	Return the Select Options for the enum value 
  	*	1. Retrieve countries list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.COUNTRY
  	*	2. Retrieve industry focus list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.INDUSTRYFOCUS
  	*	3. Rrtrieve application expertise list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.APPLICATIONEXPERTISE
  	*	4. Retrieve partner tier list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.PARTNERTIER
  	*	5. Retrieve state or province list for PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.STATE
  	*		a. Controlling field value for Country is the expected additional/optional input parameter
  	*/
	List<SelectOption> getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD selectOptionField
													, String optionalControllingFieldValue
                          , SelectOption defaultSelectOption
												);	 
}