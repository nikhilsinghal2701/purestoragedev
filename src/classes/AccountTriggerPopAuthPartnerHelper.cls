/*
Class: AccountTriggerHelper
Author: Chris c
Created Date: 7/10/2014
Histroy Revision:
    Create method populateAuthorizedPartner
*/
public class AccountTriggerPopAuthPartnerHelper {

    //Method to populate AuthorizedPartner field
    public static void populateAuthorizedPartner( List<Account> accountList, Map<Id,Account> accountOldMap ){
        List<Account> accountToUpdate = new List<Account>(); 
        List<Id> acctIdList = new List<Id>();
        //Different there: accountList != newAccounts, unknow issue
        List<Account> newAccounts = [ SELECT IsPartner FROM Account WHERE Id IN :accountList ];
        //get accounts that field IsPartner changes
        for(Account acct: newAccounts ){
            Account oldAcct = new Account();
            oldAcct = accountOldMap.get(acct.Id);
            if( acct.IsPartner != oldAcct.IsPartner ){
                acctIdList.add( acct.Id );   
            }
        }
        List<Account> acctList = [SELECT Authorized_Partner__c,IsPartner FROM Account WHERE Id IN:acctIdList];
        //populate field Authorized_Partner__c 
        for( Account a : acctList ){ 
            a.Authorized_Partner__c = a.IsPartner;
            accountToUpdate.add( a );
        }

        Database.update( accountToUpdate );
    }

  
   
    
}