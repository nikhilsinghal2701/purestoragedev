/**********************
Requirements:

1. Upload Marketing POPs

2. Upload Financial POPs

3. Upload Invoice

4. Upload Leads

Author: Nikhil @ Perficient

Date:11/4/2014

*******************/

public with sharing class PRM_MDF_PopUploadController {
	
	public String mdFid {get; set;}   
	public MDF_Request__c mdf {get; set;}
	public Proof_of_Performance__c POP {get; set;}
	public String uploadType { get; set; }
	List<Proof_of_Performance__c> deletePOPList = new List<Proof_of_Performance__c>();
	List <CampaignMember> cm = new list<CampaignMember>();
	public List<Attachment> attchmentList {get; set;}
	public Id removePopId {get; set;}
	List<List<String>> parsedCSV = new List<List<String>>();
	public Blob leadCsvFile {get;set;}
	public String nameFile  {get;set;}
	public List<LeadWrapper> leadDisplayWrapper {get;set;}
	public Integer rowCount {get;set;}
	public Integer DupCount {get;set;}
	public Integer invalidCount {get;set;}
	public Integer blankCount {get;set;}
	public Integer lastNameBlankCount {get;set;}
	List<Lead> saveList = new List<Lead>();
	public Boolean hidelead  {get;set;}

	public Attachment attachment {
	  get {
	      if (attachment == null)
	        attachment = new Attachment();
	      return attachment;
	    }
	  set;
	  }   
	
	public PRM_MDF_PopUploadController(){
		mdFid=ApexPages.currentPage().getParameters().get('MdfId');
		mdf=[select id,Category__c,Campaign__c,Campaign__r.name from MDF_Request__c where id=: mdFid];
		uploadType=ApexPages.currentPage().getParameters().get('Type');
		attchmentList= new List<Attachment>();
		deletePOPList=new List<Proof_of_Performance__c>();
		POP= new Proof_of_Performance__c();
		cm = new list<CampaignMember>();
		attchmentList=getPOPattachment(mdFid,uploadType);
		leadDisplayWrapper=	new List<LeadWrapper>();
		saveList = new List<Lead>();
		rowCount = 0;
		DupCount = 0;
	    invalidCount = 0;
	    blankCount = 0;
	    lastNameBlankCount=0;
	    hidelead=false;
	}
	
	
  	public PageReference upload() {
  		
  		if(leadCsvFile != null)
        {
        	String fileString = leadCsvFile.toString();
            parsedCSV = PRM_MDF_ParseCsvtoLead.parseCSV(fileString, true);
            rowCount = parsedCSV.size();
            System.debug('parsedCSV:'+parsedCSV);
            leadDisplayWrapper=getResults(parsedCSV,mdf);
            for(LeadWrapper ldw :leadDisplayWrapper){
            	if(ldw.status == System.label.PRM_MDF_LeadBlank)blankCount+=1;
                if(ldw.status == System.label.PRM_MDF_LeadInvalidEmail)invalidCount+=1;
                if(ldw.status == System.label.PRM_MDF_DuplicateLead)DupCount+=1;
                if(ldw.status == System.label.PRM_MDF_LeadNameBlank)lastNameBlankCount+=1;
            }
            
        	return page.PRM_MDF_LeadDisplay;
        }
        
        
  		if (attachment.name == null){
 	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select File'));
      	return null;
  		}
    	if (attachment.Body.size() > 5242880){
 	    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment : Max attachment size is 5 Mb'));
      	return null;
  		}
		POP.Marketing_Funds_Request__c=mdFid;
		POP.Type__c=uploadType;
	    attachment.OwnerId = UserInfo.getUserId();
	    try {
	      insert POP;
	      attachment.ParentId = POP.id;
	      insert attachment;
	    } catch (DMLException e) {
	      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
	      return null;
	    } finally {
	      deletePOPList.add(POP);
	      attachment = new Attachment();
	      POP= new Proof_of_Performance__c();
	    }
	    System.debug('removePopId: '+deletePOPList);
	    
		if(uploadType.equalsIgnoreCase('Financial POP')){
			
			if(mdf.Category__c != null){
				
					if(mdf.Category__c.equalsIgnoreCase('Enablement') || mdf.Category__c.equalsIgnoreCase('Other')){
						ApexPages.currentPage().getParameters().put('Type','Invoice');
						uploadType='Invoice';
						attchmentList=getPOPattachment(mdFid,uploadType);
						return Page.PRM_MDF_InvoicePopUpload;	
					}else if(uploadType.equalsIgnoreCase('Financial POP') && (mdf.Category__c.equalsIgnoreCase('Social Media') || mdf.Category__c.equalsIgnoreCase('Online Advertising')
																|| mdf.Category__c.equalsIgnoreCase('Web') || mdf.Category__c.equalsIgnoreCase('Advertisement')
																|| mdf.Category__c.equalsIgnoreCase('Banner Ads') || mdf.Category__c.equalsIgnoreCase('Public Relations'))){
						hidelead=true;
						ApexPages.currentPage().getParameters().put('Type','Marketing POP');
						uploadType='Marketing POP';
						attchmentList=getPOPattachment(mdFid,uploadType);
						return Page.PRM_MDF_MarketingPopUpload;	
					}else {
						ApexPages.currentPage().getParameters().put('Type','Marketing POP');
						uploadType='Marketing POP';
						attchmentList=getPOPattachment(mdFid,uploadType);
						return Page.PRM_MDF_MarketingPopUpload;	
					
					}
						
						
			}else {
				ApexPages.currentPage().getParameters().put('Type','Marketing POP');
				uploadType='Marketing POP';
				attchmentList=getPOPattachment(mdFid,uploadType);
				return Page.PRM_MDF_MarketingPopUpload;	
			
			}
		}else if(uploadType.equalsIgnoreCase('Marketing POP')){
			ApexPages.currentPage().getParameters().put('Type','Invoice');
			uploadType='Invoice';
			attchmentList=getPOPattachment(mdFid,uploadType);
			return Page.PRM_MDF_InvoicePopUpload;	
		}else if(uploadType.equalsIgnoreCase('Invoice')){
			
			////Start Lead insert
			
				if(!leadDisplayWrapper.isEmpty())
	        {
	            for(LeadWrapper obj: leadDisplayWrapper)
	            {
	               
	                     if(!obj.status.equalsIgnoreCase(System.label.PRM_MDF_LeadInvalidEmail) && !obj.status.equalsIgnoreCase(System.label.PRM_MDF_LeadNameBlank))saveList.add(obj.leadrow);
	                
	            }
	        }
	        if(!saveList.isEmpty())
	        {
	            try{
	                Database.insert(saveList,false);
	                for(Lead l : saveList){
	                	if(l.id != null && mdf.Campaign__c != null){
	                	CampaignMember cml = new CampaignMember();
                        cml.campaignid = mdf.Campaign__c;
                        cml.leadid = l.id;
                        cm.add(cml);
	                	}
	                }
	                
	                if(!cm.isEmpty()){
	                	Database.insert(cm,false);
		            }
	            }
	            catch(Exception e)
	            {
	                ApexPages.addMessages(e);
	                System.debug('Exception :'+e.getMessage());
	                return null;
	            }
	            
	        }
		////End Lead insert
					
	    PageReference pageRef = new PageReference('/apex/PRM_MDF_ViewRequests');
		pageRef.setRedirect(true);
		return pageRef;
		}else{
	    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
	    return null;
		}
	  }
	  
	  
	   public PageReference remove() {
        List<Proof_of_Performance__c> listPop = [Select Id From Proof_of_Performance__c Where id =: removePopId];
		delete listPop;
        return null;
     }
    
      private static List<Attachment> getPOPattachment(String mdFid,string type) {
      		List<Proof_of_Performance__c> POPList = new List<Proof_of_Performance__c>();
      		List<Attachment> aList = new List<Attachment>();
      		POPList=[SELECT Name,Id,Type__c,(SELECT ParentId,Name,Id FROM Attachments) FROM Proof_of_Performance__c 
					where Marketing_Funds_Request__c =: mdFid
					and Type__c=: type];
			for(Proof_of_Performance__c pp : POPList){
				if(!pp.Attachments.IsEmpty())
				aList.add(pp.Attachments);
			}			
            return aList;
       }
	
	     
       public PageReference Cancel() {
       	System.debug('CancelPopId: '+deletePOPList);
       	if(!deletePOPList.IsEmpty()) delete deletePOPList;
        PageReference pageRef = new PageReference('/apex/PRM_MDF_ViewRequests');
		pageRef.setRedirect(true);
		return pageRef;
       }
       
       public PageReference leadBack() {
       	ApexPages.currentPage().getParameters().put('Type','Marketing POP');
		uploadType='Marketing POP';
		attchmentList=getPOPattachment(mdFid,uploadType);
		attachment = new Attachment();
	    POP= new Proof_of_Performance__c();
	    leadCsvFile=null;
	    leadDisplayWrapper=	new List<LeadWrapper>();
	    rowCount = 0;
		DupCount = 0;
	    invalidCount = 0;
	    blankCount = 0;
	    lastNameBlankCount=0;
		return Page.PRM_MDF_MarketingPopUpload;	
       }
       
        public PageReference leadNext() {
       	ApexPages.currentPage().getParameters().put('Type','Invoice');
		uploadType='Invoice';
		attachment = new Attachment();
		leadCsvFile=null;
		rowCount = 0;
		DupCount = 0;
	    invalidCount = 0;
	    blankCount = 0;
	    lastNameBlankCount=0;
	    POP= new Proof_of_Performance__c();
		attchmentList=getPOPattachment(mdFid,uploadType);
		return Page.PRM_MDF_InvoicePopUpload;
       }
       
       
       
       public static List<LeadWrapper> getResults(List<List<String>> parsedCSV,MDF_Request__c mdf){
        	List<LeadWrapper> leadDisplayWrapperlist = new List<LeadWrapper>();
        	map<string,id> leadMap= new map<string,id>();
            set<string> Emailset = new set<string>();
            for (List<String> row : parsedCSV)
            {
            	if(row[2].trim() != null && !row[2].isWhitespace())Emailset.add(row[2].trim());
            }
            if(!Emailset.IsEmpty()){
            for (lead l :[Select Id,email from lead where email in:Emailset])
            {
            	leadMap.put(l.email.toUpperCase(),l.id);
            }
            }
            
            for (List<String> row : parsedCSV)
            {
                
                if(row.size() > 0)
                {
                   if(!row[0].startsWith('Name'))
                    {
                        String status =PRM_MDF_ParseCsvtoLead.validate(row,leadMap);
                        Lead leadItem = createLead(row,status,mdf);
                        LeadWrapper wrapperObj = new LeadWrapper(leadItem,status);
                        leadDisplayWrapperlist.add(wrapperObj);
                    }
   
                }
                
            }
           return leadDisplayWrapperlist; 
              
    }
    
     /*
        creating Lead's using wrapper class
    */
    public static lead createLead(List<String> str,string status,MDF_Request__c mdf)
    {
        lead li = new lead();
        if(!str[0].startsWith('Name'))
        {
        	li.FirstName  =str[0] != null && str[0] != ''?String.ValueOf(str[0]):'';
            li.LastName  =str[1] != null && str[1] != ''?String.ValueOf(str[1]):'';
            li.Email=str[2] != null && str[2] != ''?String.ValueOf(str[2]):'';
            li.Title= str[3] != null && str[3] != ''?String.ValueOf(str[3]):'';
            li.Company= str[4] != null && str[4] != ''?String.ValueOf(str[4]):'';
            li.Street=str[5] != null && str[5] != ''?String.ValueOf(str[5]):'';
            li.City=str[6] != null && str[6] != ''?String.ValueOf(str[6]):'';
            li.State=str[7] != null && str[7] != ''?String.ValueOf(str[7]):'';
            li.Country=str[8] != null && str[8] != ''?String.ValueOf(str[8]):'';
            li.PostalCode=str[9] != null && str[9] != ''?String.ValueOf(str[9]):'';
            li.Phone=str[10] != null && str[10] != ''?String.ValueOf(str[10]):'';
            li.LeadSource='Channel';
            li.Lead_Source_Detail__c='Channel';
            li.Lead_Source_Most_Recent__c=mdf.Campaign__r.name;
            li.Lead_Source_Most_Recent_Detail__c=mdf.Campaign__r.name;
            if(status == System.label.PRM_MDF_DuplicateLead)li.Partner_Lead_Accepted__c=true;
            
       }
        return li;
    }
    
       
       
        /*
        wrapper class
    */
    public class LeadWrapper
    {
        public Lead leadrow   {get;set;}
        public String status   {get;set;}
        public LeadWrapper(lead leadrow, String str)
        {
            this.leadrow = leadrow;
            this.status = str;
        }   
    }
	
}