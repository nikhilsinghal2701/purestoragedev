/****************************************************
 * Description: For the users coming from Lithium, this class will redirect them to the pages 
                depending on their Lithium Role
   Author     : Sorna (Perficient)
 ****************************************************/
 
public class SC_CommunityRedirectController
{
    // redirects the user to the corresponding page
    public PageReference redirectUsersToAccount()
    //public SC_CommunityRedirectController()
    {
        try
        {
            User u = [Select Id, contactId, contact.AccountId, Lithium_Role__c from User where Id = :userInfo.getUserId()];
            
            // if employee, redirect to home page
            if(u.Lithium_Role__c == system.label.SC_LithiumRole_Employee)
            {
                return new pageReference(site.getPathPrefix() + system.label.SC_CommunityEmployeeRedirectURL);
            }
            // if visitor, show insufficient privilege msg
            else if(u.Lithium_Role__c == system.label.SC_LithiumRole_Visitor)
            {
                Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, system.label.SC_CommunityVisitorErrorMsg));
                return null;
            }
            // if customer or support provider, redirect to their account
            else
            {
                return new pageReference(site.getPathPrefix() + '/' + u.contact.AccountId);
            }
        }
        // throw any error
        catch(Exception e)
        {
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
            return null;
        }
    }
}