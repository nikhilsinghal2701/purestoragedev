/****************************************************
 * Description: If users logs out, this page will log them out of lithium too. Also if the users click sign out 
                in lithium, they will be redirected to this page and they will get logged out of both systems by this page.
                If the user's lithium role is employee, then this page will take them to internal salesforce instead of log them out.
   Author     : Sorna (Perficient)
 ****************************************************/
 
public class SC_LogoutController
{
    // boolean to say if the user click signout from salesforce or from lithium
    public boolean isFromSFDC {get;set;}
    
    // variable to say what should be logout url depending on the user's role
    public string logoutURL {get;set;}
    
    // constructor
    public SC_LogoutController()
    {
        if(Apexpages.currentPage().getParameters().get('isFromSFDC') != null
            && Apexpages.currentPage().getParameters().get('isFromSFDC') == 'true')
        {
            isFromSFDC = true;
        }
        else
        {
            isFromSFDC = false;
        }
    }
    
    // redirects the user to the corresponding page
    public void logoutUsers()
    {
        try
        {
            User u = [Select Id, contactId, contact.AccountId, Lithium_Role__c from User where Id = :userInfo.getUserId()];
            
            // if employee, redirect to internal salesforce
            if(u.Lithium_Role__c == system.label.SC_LithiumRole_Employee)
            {
                logoutURL =  getDomain() + system.label.SC_CommunityEmployeeRedirectURL;
            }
            
            // if any other user, logout
            else
            {
                logoutURL = site.getPathPrefix() + '/secur/logout.jsp';
            }
        }
        // throw any error
        catch(Exception e)
        {
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
        }
    }
    
    public string getDomain()
    {
        for(SC_Domains__c d : [Select Name, Domain__c, Org_ID__c from SC_Domains__c])
        {
            if(d.Org_ID__c == userInfo.getOrganizationID())
            {
                return d.Domain__c;
            }
        }
        
        return '';
    }
}