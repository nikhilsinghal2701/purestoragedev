/***********************************************
 * Description : Controller class for SC_SendRMA page. 
                 This class will 1) get the current case's parent array and child assets along with their 
                 platform part no and spare part no, 2) allow user's choose parts of the array, 3) summarize the chosen parts
                 as an email and 4) sends the email to source support for RMA process
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/
public with sharing class SC_SendRMAController
{
    // variable declarations
    public Case c {get;set;}
    public Asset parentAsset {get;set;}
    
    public string selectedGroup {get;set;}
    public List<SelectOption> assetGroups {get;set;}
    
    public List<Asset> childAssets;
    public Map<string, List<Asset>> childAssetsMap;
    public Map<string, List<Parts_Matrix__c>> partsMap;
    
    public Asset selectedChildAsset {get;set;}
    public List<Components> componentsList {get;set;}
    public list<PartsMatrixSort> selectedComponentsList {get;set;}
    public Map<string, Components> selectedComponentsMap;
    
    public boolean hwEscalated {get;set;}
    public boolean onsite {get;set;}
    
    public boolean isStep1 {get;set;}
    public boolean isStep2 {get;set;}
    public boolean isEditEmail {get;set;}
    
    public Contact sourceSupportContact {get;set;}
    public string fromAddress {get;set;}
    public string toAddress {get;set;}
    public string subject {get;set;}
    public string body {get;set;}
    
    public string selectedDelivery {get;set;}
    public List<SelectOption> deliveryOptions {get;set;}
    private Map<Id, string> assetDeliveryMap;

    public boolean isEmailSuccess {get;set;}
    public string result {get;set;}

    // constants
    private static final string NO_ARRAY_FOUND = system.label.SC_NO_ARRAY_FOUND;
    private static final string NO_CHILDASSET_FOUND = system.label.SC_NO_CHILDASSET_FOUND;
    private static final string NO_PARTSMATRIX_FOUND = system.label.SC_NO_PARTSMATRIX_FOUND;
    private static final string VARIABLE_COUNT_VALUE = system.label.SC_VARIABLE_COUNT_VALUE;
    private static final string NOTAPPLICABLE_SLOT_VALUE = system.label.SC_NOTAPPLICABLE_SLOT_VALUE;
    private static final string ALL_OS_VALUE = system.label.SC_ALL_OS_VALUE;
    private static final string SHELF_INDEX_PREFIX = system.label.SC_SHELF_INDEX_PREFIX;

    private static final string FROM_EMAIL = system.label.SC_FROM_EMAIL;
    private static final string FROM_NAME = system.label.SC_FROM_NAME;
    private static final string SOURCESUPPORT_CONTACT = system.label.SC_SOURCESUPPORT_CONTACT;
    private static final string NO_SOURCESUPPORT_CONTACT = system.label.SC_NO_SOURCESUPPORT_CONTACT;
    private static final string SUCCESS_MSG = system.label.SC_SUCCESS_MSG;

    private static final  string EMAIL_SUBJECT = system.label.SC_EMAIL_SUBJECT;
    private static final  string INITIAL_EMAIL_CONTENT_TEXT = system.label.SC_INITIAL_EMAIL_CONTENT_TEXT;
    private static final  string EMAILCONTENT_CUSTOMERCONTACT = system.label.SC_EMAILCONTENT_CUSTOMERCONTACT;
    private static final  string EMAILCONTENT_SUPPORTNO = system.label.SC_EMAILCONTENT_SUPPORTNO;
    private static final  string EMAILCONTENT_LOCATION = system.label.SC_EMAILCONTENT_LOCATION + ' ';
    private static final  string EMAILCONTENT_SLA = system.label.SC_EMAILCONTENT_SLA;
    private static final  string EMAILCONTENT_TSE = system.label.SC_EMAILCONTENT_TSE;
    private static final  string EMAILCONTENT_FAREQUIRED = system.label.SC_EMAILCONTENT_FAREQUIRED;
    private static final  string EMAILCONTENT_TECHNICIANREQURIED = system.label.SC_EMAILCONTENT_TECHNICIANREQURIED;
    private static final  string EMAILCONTENT_CASENUMBER = system.label.SC_EMAILCONTENT_CASENUMBER;

    // constructor
    public SC_SendRMAController(Apexpages.StandardController controller)
    {
        try
        {
            // get current case detail
            c = (Case) controller.getRecord();
            // get necessary details from the case like array, child assets, parts matrix
            init();
        }
        catch(Exception e)
        {
            isStep1 = false;
            isStep2 = false;
            // add message to the page if any error
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
        }
    }

    // gets the array details and parts matrix
    public void init()
    {
        isStep1 = true;
        isStep2 = false;
        isEditEmail = false;
        
        parentAsset = new Asset();
        
        // one group per each child asset
        selectedGroup = '';
        assetGroups = new List<SelectOption>();
        assetGroups.add(new SelectOption('', '--None--'));
        
        assetDeliveryMap = new Map<Id,string>();

        childAssets = new List<Asset>();
        childAssetsMap = new Map<string, List<Asset>>();
        partsMap = new Map<string, List<Parts_Matrix__c>>();
        
        hwEscalated = onsite = false;
        
        // if the case has an array tied to it, get the array details
        if(c.AssetID != null)
        {
            getArrayDetails();
        }
        // if the case doesnot have an array, throw an error to the page
        else
        {
            isStep1 = false;
            isStep2 = false;
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, NO_ARRAY_FOUND));
        }
    }
    
    // get the parent array and child assets details
    public void getArrayDetails()
    {
        set<string> shelvesName = new set<string>();
        //string wty;
        // query on parent array
        parentAsset = [Select Id, Array_ID__c, Purity_Version__c, AccountId, Account.Name, Pure_Warranty_Description__c, 
                              Street_Address__c, City__c, state_province__c, Country__c, Postal_Code__c from Asset where Id = :c.AssetID];
            
        //deliveryOptions = new List<SelectOption>();
        
        // get the child assets
        for(Asset a : [Select Id, Serial_Number__c, Name, Platform_PN__c, Index__c, Pure_Warranty_Description__c from Asset where Parent_Asset__c = :c.AssetId order by Index__c])
        {
            //assetGroups.add(new SelectOption(a.Id, a.Name + a.Index__c));
            // form a map of platform part no -> child asset
            if(childAssetsMap.containsKey(a.Platform_PN__c))
            {
                childAssetsMap.get(a.Platform_PN__c).add(a);
            }
            else
            {
                childAssetsMap.put(a.Platform_PN__c, new List<Asset>{a});
            }
            
            childAssets.add(a);
        }
        
        if(childAssetsMap.size() > 0)
        {
            // get parts matrix for the child assets platform part no
            getPartsDetails();

            for(Asset a : childAssets)
            {
                // special use case: if the child asset is an shelf add a new part to represent the shelf as it is not 
                // part of the parts matrix now
                if(a.Index__c.contains(SHELF_INDEX_PREFIX))
                {
                    if(partsMap.get(a.Platform_PN__c) != null && partsMap.get(a.Platform_PN__c).size() > 0)
                    {
                        // if there are 2 shelves for this array let's say both are 11TB, then the available parts list
                        // will have Shelf added twice. So, adding a check to add the 11 TB shelf only once for that scenario
                        if(!shelvesName.contains(partsMap.get(a.Platform_PN__c)[0].Platform_Description__c))
                        {
                            partsMap.get(a.Platform_PN__c).add(0, new Parts_Matrix__c(Platform_PN__c = a.Platform_PN__c,
                                                            Platform_Description__c = partsMap.get(a.Platform_PN__c)[0].Platform_Description__c, 
                                                            Spare_PN__c = '',
                                                            Spare_Category__c = 'Shelf',
                                                            Spare_Description__c = partsMap.get(a.Platform_PN__c)[0].Platform_Description__c,
                                                            Count__c = '1', Slot_Location__c = NOTAPPLICABLE_SLOT_VALUE));
                            shelvesName.add(partsMap.get(a.Platform_PN__c)[0].Platform_Description__c);
                        }
                    }
                }
            }
        }
        else
        {
            // if there are no child assets, throw an error
            isStep1 = false;
            isStep2 = false;
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, NO_CHILDASSET_FOUND));
        }
    }
    
    // get parts matrix details
    public void getPartsDetails()
    {
        // query the parts matrix for the child assets
        for(Parts_Matrix__c p : [Select Platform_PN__c, Platform_Description__c, Spare_PN__c, Spare_Description__c, Spare_Category__c, 
                                        OS__c, Count__c, Default_Part__c, Slot_Location__c from Parts_Matrix__c where Platform_PN__c in : childAssetsMap.keyset() order by Spare_Description__c, Slot_Location__c])
        {
            // get the appropriate parts matrix based on the os version - ex: if purity version of the array is 3.4, 
            // get the parts that has OS version 3.2+ or 3.5-
            if(p.OS__c == ALL_OS_VALUE 
                || (p.OS__c != ALL_OS_VALUE && p.OS__c.contains('-') && isGreaterOrEqual(p.OS__c.substring(0,p.OS__c.indexOf('-')), parentAsset.Purity_Version__c))
                || (p.OS__c != ALL_OS_VALUE && p.OS__c.contains('+') && isLesserOrEqual(p.OS__c.substring(0,p.OS__c.indexOf('+')), parentAsset.Purity_Version__c)))
            {
                if(partsMap.containsKey(p.Platform_PN__c))
                {
                    partsMap.get(p.Platform_PN__c).add(p);
                }
                else
                {
                    partsMap.put(p.Platform_PN__c, new List<Parts_Matrix__c>{p});
                }
            }
        }
        
        if(partsMap.size() > 0)
        {
            // form the group picklist
            getAssetGroupOptions();
        }
    }
    
    // form the group picklist
    public void getAssetGroupOptions()
    {
        for(Asset a : childAssets)
        {
            if(partsMap.get(a.Platform_PN__c) != null && partsMap.get(a.Platform_PN__c).size() > 0)
            {
                assetGroups.add(new SelectOption(a.Id,  a.Index__c + ' - ' + partsMap.get(a.Platform_PN__c)[0].Platform_Description__c));
            }
            else
            {
                // if a child asset's parts no is not found in parts matrix, throw an error
                isStep1 = false;
                isStep2 = false;
                ApexPages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, NO_PARTSMATRIX_FOUND.replace('(Asset SN)', a.Serial_Number__c).replace('(Platform PN)', a.Platform_PN__c)));
            }
        }
    }
    
    // method to determine the higher version - for ex: 3.1.2 > 3.1.1
    public boolean isGreaterOrEqual(string v1, string v2)
    {
        boolean isGreater = false;
        boolean isEqual = false;
        List<string> str1 = new List<string>();
        List<string> str2 = new List<string>();
        
        if(v1.contains('.'))
            str1 = v1.split('[.]');
        if(v2.contains('.'))
            str2 = v2.split('[.]');
        
        for(Integer i = 0; (i < str1.size() && i < str2.size()); i++)
        {
            if(Integer.valueOf(str1[i]) > Integer.valueOf(str2[i]))
            {
                isGreater = true;
                break;
            }
            else if(Integer.valueOf(str1[i]) == Integer.valueOf(str2[i]))
            {
                isEqual = true;
            }
            else if(Integer.valueOf(str1[i]) < Integer.valueOf(str2[i]))
            {
                isGreater = false;
                isEqual = false;
                break;
            }
        }
        
        if(isEqual)
        {
            if(str1.size() > str2.size())
            {
                isGreater = true;
                isEqual = false;
            }
            else if(str1.size() == str2.size())
            {
                isGreater = false;
                isEqual = true;
            }
            else if(str1.size() < str2.size())
            {
                isGreater = false;
                isEqual = false;
            }
        }
        
        return (isGreater || isEqual);
    }

    // determine the lowest os version for ex: 3.1.1 < 3.1.2
    public boolean isLesserOrEqual(string v1, string v2)
    {
        boolean isLesser = false;
        boolean isEqual = false;
        List<string> str1 = new List<string>();
        List<string> str2 = new List<string>();
        
        if(v1.contains('.'))
            str1 = v1.split('[.]');
        if(v2.contains('.'))
            str2 = v2.split('[.]');
        
        for(Integer i = 0; (i < str1.size() && i < str2.size()); i++)
        {
            if(Integer.valueOf(str1[i]) < Integer.valueOf(str2[i]))
            {
                isLesser = true;
                break;
            }
            else if(Integer.valueOf(str1[i]) == Integer.valueOf(str2[i]))
            {
                isEqual = true;
            }
            else if(Integer.valueOf(str1[i]) > Integer.valueOf(str2[i]))
            {
                isLesser = false;
                isEqual = false;
                break;
            }
        }
        
        if(isEqual)
        {
            if(str1.size() < str2.size())
            {
                isLesser = true;
                isEqual = false;
            }
            else if(str1.size() == str2.size())
            {
                isLesser = false;
                isEqual = true;
            }
            else if(str1.size() > str2.size())
            {
                isLesser = false;
                isEqual = false;
            }
        }
        
        return (isLesser || isEqual);
    }

    // form a list of all parts to be displayed in the UI
    public void showComponents()
    {
        componentsList = new List<Components>();
        
        if(selectedComponentsMap == null)
            selectedComponentsMap = new Map<string, Components>();
        
        List<string> slots;
        Integer i, j;
        
        // if user selected a group, get the parts for that group
        if(selectedGroup != '' && selectedGroup != null)
        {   
            selectedChildAsset = null;

            for(Asset a : childAssets)
            {
                if(a.Id == selectedGroup)
                {
                    selectedChildAsset = a;
                    break;
                }
            }

            deliveryOptions = new List<SelectOption>();

            // fetch the available sla options for the selected child asset based on the warranty
            for(SC_Send_RMA_SLA_Types__c sla : SC_Send_RMA_SLA_Types__c.getAll().values())
            {
                if(selectedChildAsset.Pure_Warranty_Description__c.toLowerCase().contains(sla.Name.toLowerCase()))
                {
                    // if the child asset id already exists in the map, which means TSE has already choosen the delivery option
                    if(assetDeliveryMap.containsKey(selectedChildAsset.Id))
                    {
                        selectedDelivery = assetDeliveryMap.get(selectedChildAsset.Id);
                    }
                    // if the child asset id is not in the map, populate it with the default sla type
                    else
                    {
                        selectedDelivery = sla.Default_Value__c;
                        assetDeliveryMap.put(selectedChildAsset.Id, sla.Default_Value__c);
                    }

                    // split the comma separated available delivery options for the selected child asset
                    for(string s : sla.Available_Values__c.split(','))
                    {
                        deliveryOptions.add(new SelectOption(s, s));
                    }

                    break;
                }
            }
            
            // form a list of parts from parts matrix for the group chosen by the user
            for(Parts_Matrix__c p : partsMap.get(selectedChildAsset.Platform_PN__c))
            {
                slots = new List<string>();
                i = j = 0;
                
                if(p.Slot_Location__c != null)
                {
                    // if slot no is a range, for ex: 1-10, show the parts 10 times with slot no 1, 2, ,3, etc
                    if(p.Slot_Location__c.contains('-'))
                    {
                        slots = p.Slot_Location__c.split('-');
                        i = Integer.valueOf(slots[0]);
                        j = Integer.valueOf(slots[1]);
                        
                        for(Integer k = i; k <= j; k++)
                        {
                            if(!selectedComponentsMap.containsKey(selectedChildAsset.Index__c + '-' + p.Platform_PN__c + '-' + p.Spare_PN__c + '-' + string.valueOf(k)))
                                componentsList.add(new Components(string.valueOf(k), selectedChildAsset, p));
                        }
                    }

                    // if the slot no is a comma separated value, for ex: 1,3,5 , show the part 3 times with slot 1, 3 and 5
                    else if(p.Slot_Location__c.contains(','))
                    {
                        for(string s : p.Slot_Location__c.split(','))
                        {
                            if(!selectedComponentsMap.containsKey(selectedChildAsset.Index__c + '-' + p.Platform_PN__c + '-' + p.Spare_PN__c + '-' + s))
                                componentsList.add(new Components(s, selectedChildAsset, p));
                        }
                    }

                    // if the slot no is 'N/A', then show the part once
                    else if(p.Slot_Location__c == NOTAPPLICABLE_SLOT_VALUE)
                    {
                        if(!selectedComponentsMap.containsKey(selectedChildAsset.Index__c + '-' + p.Platform_PN__c + '-' + p.Spare_PN__c + '-' + NOTAPPLICABLE_SLOT_VALUE))
                            componentsList.add(new Components(NOTAPPLICABLE_SLOT_VALUE, selectedChildAsset, p));
                    }
                }
            }
        }
    }
    
    // sets the changed delivery option on the asset delivery map which will be later used to draft the email
    public void changeDelivery()
    {
        if(selectedChildAsset != null && selectedDelivery != null && selectedDelivery != '')
        {
            assetDeliveryMap.put(selectedChildAsset.Id, selectedDelivery);
        }
    }

    // add the selected parts to a selection list to be showed in a different table in the UI
    public void addSelected()
    {
        // finds the selected part
        for(Components comp : componentsList)
        {
            if(comp.IsSelected)
            {
                // add the selected part to a map
                selectedComponentsMap.put(comp.asset.Index__c + '-' + comp.PlatformNo + '-' + comp.SpareNo + '-' + comp.Slot, new Components(comp.Slot, comp.Asset, comp.part));
            }
        }
        
        selectedComponentsList = new List<PartsMatrixSort>();
        
        // collect the values from the map and custom sort them as per the group and description
        for(Components c : selectedComponentsMap.values())
        {
            selectedComponentsList.add(new PartsMatrixSort(c));
        }
        
        if(selectedComponentsList.size() > 0)
        {
           selectedComponentsList.sort(); 
        }

        // re-form the available components list
        showComponents();
    }
    
    // remove a part from the selection list
    public void removeSelected()
    {
        // finds the chosed part to be removed
        for(PartsMatrixSort comp : selectedComponentsList)
        {
            if(comp.item1.IsSelected)
            {
                // once found remove from the selection map
                selectedComponentsMap.remove(comp.item1.asset.Index__c + '-' + comp.item1.PlatformNo + '-' + comp.item1.SpareNo + '-' + comp.item1.Slot);
            }
        }
        
        selectedComponentsList = new List<PartsMatrixSort>();
        
        // follow the same procedure as addSelected() method
        for(Components c : selectedComponentsMap.values())
        {
            selectedComponentsList.add(new PartsMatrixSort(c));
        }
        
        if(selectedComponentsList.size() > 0)
        {
           selectedComponentsList.sort(); 
        }
        
        showComponents();
    }

    // on click of next, summarize the user selections in an email and display the email draft to the user
    public void nextStep()
    {
        try
        {
            string address = '';
            // source support contact to whom the email has to be sent - a static contact setup in the system
            sourceSupportContact = [Select Id, Email from Contact where Name = :SOURCESUPPORT_CONTACT];
            isStep1 = false;
            isStep2 = true;
            
            // from & to email addresses
            fromAddress = FROM_EMAIL;
            toAddress = sourceSupportContact.Email;
            //toAddress = 'support@sourcesupport.com.perf2';

            // email subject
            subject = EMAIL_SUBJECT.replace('(Account Name)', parentAsset.Account.Name);
            /* No HTML Table because Source Support Email to Case adds only plain text to the description field
            // summarize info in email body - a table to display all the selection
            body = INITIAL_EMAIL_CONTENT_TEXT;
            body += '<table cellspacing="5" cellpadding="5"> <thead> <tr> <th> Description </th> <th> Platform Part Number </th> <th> Spare Part Number </th> <th> Slot No </th> <th> Controller/Shelf S/N </th> <th> Customer SLA Type </th> </tr> </thead>';
            body += '<tbody>';
            integer i=0;
            for(PartsMatrixSort comp : selectedComponentsList)
            {
                body += '<tr> <td> ' + comp.item1.part.Spare_Description__c + ' </td>';
                body += '<td> ' + comp.item1.part.Platform_PN__c + ' </td>';
                body += '<td> ' + comp.item1.part.Spare_PN__c + ' </td>';
                body += '<td> ' + comp.item1.Slot + ' </td>';
                body += '<td> ' + comp.item1.Asset.Serial_Number__c + ' </td> ';
                body += '<td>';

                if(assetDeliveryMap.get(comp.item1.Asset.Id) != null)
                {
                    body += assetDeliveryMap.get(comp.item1.Asset.Id);
                }

                body += '</td> </tr>';
                i++;
            }
            
            body += '</tbody> </table> <br/>';

            // after the table, add additional info about the case, array and case contact
            body += INITIAL_EMAIL_CONTENT_TEXT + '<br/>';
            body += EMAILCONTENT_CUSTOMERCONTACT + C.Contact.Name + '<br/>';
            body += EMAILCONTENT_SUPPORTNO + '<br/>';
            
            address += EMAILCONTENT_LOCATION;
            if(parentAsset.Street_Address__c != null)
                address += parentAsset.Street_Address__c + ', ';
            if(parentAsset.City__c != null)
                address += parentAsset.City__c + ', ';
            if(parentAsset.state_province__c != null)
                address += parentAsset.state_province__c + ', ';
            if(parentAsset.Postal_Code__c != null)
                address += parentAsset.Postal_Code__c + ', ';
            if(parentAsset.Country__c != null) 
                address += parentAsset.Country__c;
            if(address.length() > 1 && address.substring(address.length() - 2) == ', ')
                address = address.substring(0, address.length() - 2);
            body += address + '<br/>';
            
            //if(selectedDelivery == null) selectedDelivery = '';
            //body += EMAILCONTENT_SLA + selectedDelivery + '<br/>';

            if(hwEscalated)
                body += EMAILCONTENT_FAREQUIRED + '<br/>';
            if(onsite)
                body += EMAILCONTENT_TECHNICIANREQURIED + '<br/>';

            body += EMAILCONTENT_CASENUMBER + c.CaseNumber + '<br/>';
            */
            
            integer i=1;
            body = INITIAL_EMAIL_CONTENT_TEXT + '<br/> <br/> <br/>';
            
            for(PartsMatrixSort comp : selectedComponentsList)
            {
                body += i + ') ' + 'Description' + '<br/>';
                body += comp.item1.part.Spare_Description__c + '<br/> <br/>';
                body += 'Platform Part Number' + '<br/>';
                body += comp.item1.part.Platform_PN__c + '<br/> <br/>';
                body += 'Spare Part Number' + '<br/>';
                body += comp.item1.part.Spare_PN__c + '<br/> <br/>';
                body += 'Slot No' + '<br/>';
                body += comp.item1.Slot + '<br/> <br/>';
                body += 'Controller/Shelf S/N' + '<br/>';
                body += comp.item1.Asset.Serial_Number__c + '<br/> <br/>';
                body += 'Customer SLA Type' + '<br/>';
                body += assetDeliveryMap.get(comp.item1.Asset.Id) + '<br/> <br/>';
                
                i++;
            }
            
            body += EMAILCONTENT_CUSTOMERCONTACT + C.Contact.Name + '<br/>';
            body += EMAILCONTENT_SUPPORTNO + '<br/>';
            
            address += EMAILCONTENT_LOCATION;
            if(parentAsset.Street_Address__c != null)
                address += parentAsset.Street_Address__c + ', ';
            if(parentAsset.City__c != null)
                address += parentAsset.City__c + ', ';
            if(parentAsset.state_province__c != null)
                address += parentAsset.state_province__c + ', ';
            if(parentAsset.Postal_Code__c != null)
                address += parentAsset.Postal_Code__c + ', ';
            if(parentAsset.Country__c != null) 
                address += parentAsset.Country__c;
            if(address.length() > 1 && address.substring(address.length() - 2) == ', ')
                address = address.substring(0, address.length() - 2);
            body += address + '<br/>';
            
            //if(selectedDelivery == null) selectedDelivery = '';
            //body += EMAILCONTENT_SLA + selectedDelivery + '<br/>';

            if(hwEscalated)
                body += EMAILCONTENT_FAREQUIRED + '<br/>';
            if(onsite)
                body += EMAILCONTENT_TECHNICIANREQURIED + '<br/>';

            body += EMAILCONTENT_CASENUMBER + c.CaseNumber + '<br/>';
        }
        catch(QueryException e)
        {
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, NO_SOURCESUPPORT_CONTACT));
        }
    }
    
    // go to previous screen
    public void goBackToStep1()
    {
        isStep2 = false;
        isStep1 = true;
    }
    
    // to edit the drafted email
    public void editEmail()
    {
        isEditEmail = true;
    }
    
    // redo the edited content
    public void cancelEdit()
    {
        isEditEmail = false;
    }
    
    // sends email
    public void sendEmail()
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress orgWideEmail;
        try
        {
            //mail.setReplyTo(fromAddress);
            //mail.setSenderDisplayName(FROM_NAME);
            
            orgWideEmail = [Select Id, Address from OrgWideEmailAddress where Address = :FROM_EMAIL];
            
            mail.setOrgWideEmailAddressId(orgWideEmail.Id);
            mail.setTargetObjectId(sourceSupportContact.Id);
            mail.setWhatID(c.Id);
            mail.setSubject(subject + ' ' + getThreadId(c.Id));
            //mail.setReferences('[ ref:_00D117yUm._500113KkEtAAK:ref ]');
            
            mail.setHtmlBody(body);
            
            // Send the email you have created.
            // Emails are sent only if it production org. If you want to send it from this sandbox,
            // update the SC_ProductionOrgId with this sandbox's 18 digit org id
            if(userinfo.getOrganizationId() == system.label.SC_ProductionOrgId)
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

            // if succuss, show success msg
            isEmailSuccess = true;
            result = SUCCESS_MSG;
        }
        catch(EmailException e)
        {
            // if error, show the error msg
            isEmailSuccess = false;
            result = e.getDMLMessage(0);
        }
        catch(QueryException e)
        {
            isEmailSuccess = false;
            result = 'No Organization Wide Email is created as ' + FROM_EMAIL;
        }
    }
    
    private String getThreadId(string caseId)
    {
        string orgId = userinfo.getOrganizationId();
        string threadId = '';
        
        if(orgId != null && caseId != null)
        {
            threadId += 'ref:_' + orgId.substring(0,5) + orgId.substring(5,15).replaceAll('0','')
                        + '._' + caseId.substring(0,5) + caseId.substring(5,10).replaceAll('0','')
                        + caseId.substring(10,15) + ':ref';
        }
        
        return '[' + threadId + ']';
    }
    
    // wrapper class used to show the parts of the array in UI
    public class Components
    {
        public boolean isSelected {get;set;}
        public string slot {get;set;}
        public Integer count {get;set;}
        public boolean isVariable {get;set;}
        public string name {get;set;}
        public string childAssetName {get;set;}
        public Id childAssetId {get;set;}
        
        public string platformNo {get;set;}
        public string spareNo {get;set;}
        public Asset asset {get;set;}
        public Parts_Matrix__c part {get;set;}
        
        // constructor
        public Components(string slotNo, Asset a, Parts_Matrix__c p)
        {
            isSelected = false;
            slot = slotNo;
            if(p.Count__c != VARIABLE_COUNT_VALUE)
            {
                count = Integer.valueOf(p.count__c);
                isVariable = false;
            }
            else
            {
                count = 0;
                isVariable = true;
            }
            name = p.Spare_Description__c;
            //childAssetName = a.Index__c + ' - ' + p.Platform_Description__c;
            childAssetId = a.Id;
            
            platformNo = p.Platform_PN__c;
            spareNo = p.Spare_PN__c;
            
            childAssetName = a.Index__c + ' - ' +  p.Platform_Description__c;


            asset = a;
            part = p;
        }
    }
}