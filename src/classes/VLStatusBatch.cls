global class VLStatusBatch implements Database.Batchable<Feedback__c>, Database.AllowsCallouts
{
    Contact [] c = [select Id from Contact where Account.XPSynced__c =: 'Y' ];
    //XP_Party__c
    Feedback__c [] AssetsToUpdate = [select Feedback__c.Contact__c,Feedback__c.Name,Feedback__c.DataCollectionId__c from Feedback__c where Feedback__c.Status__c ='Nominated' and Feedback__c.Contact__c in (select Id from Contact where Account.XPSynced__c =: 'Y')];
    
    global Iterable<Feedback__c> start(database.batchablecontext BC)
    {
        return (AssetsToUpdate);
    }
    global void execute(Database.BatchableContext BC, List<Feedback__c> scope)
    {
        for(Feedback__c a : scope)
        {
            VLStatus.FeedbackUpdate(a.Name);
        }
    }
    global void finish(Database.BatchableContext info)
    {
    }
    //global void finish loop
}