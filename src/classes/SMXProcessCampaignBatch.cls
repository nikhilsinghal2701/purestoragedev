global class SMXProcessCampaignBatch implements Database.Batchable<Account>, Database.AllowsCallouts { 
    Account [] AssetsToUpdate = [select id from Account where Type = 'Customer'];
     
    // Start Method
    global Iterable<Account> start(database.batchablecontext BC){
    
    return (AssetsToUpdate);    
    }
    
    //Execute Method   
    global void execute(Database.BatchableContext BC, List<Account> scope){
        String strSurveyID='PURESTORAGE_99896';
        String strSurveyName='Pure Storage Inc';
           
        Datetime myDatetime = Datetime.now();
        String myDatetimeStr = myDatetime.format('MMMM d,  yyyy');
        
        Campaign objCampaign = new Campaign();
        objCampaign.Name= 'Relationship Survey - '+ myDatetimeStr;
        objCampaign.SurveyID__c=strSurveyID;
        objCampaign.Survey_Name__c=strSurveyName;
        
        insert(objCampaign);    

        List <CampaignMember> lstCM = new list<CampaignMember>();
        
        for(Account a : scope){
            List <Contact> lstContact= [select id,Account.Id from Contact where Account.Id =: a.Id AND id Not in(select Contact__c from Feedback__c where CreatedDate = LAST_N_DAYS:180)];

            if(!lstContact.isEmpty())
            {             
               for(Contact c : lstContact)
                {
                   CampaignMember cm = new CampaignMember();
                   cm.ContactId = c.id;
                   cm.campaignid = objCampaign.id;
                   lstCM.add(cm);
                  
                }            
            }
            
        }
        
        insert(lstCM);
        
        objCampaign.Survey_Status__c='Approved';
        update objCampaign;
        
        
    }

    global void finish(Database.BatchableContext info){
    }//global void finish loop

}