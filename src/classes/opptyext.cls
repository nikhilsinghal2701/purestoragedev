public class opptyext{

    private final Opportunity oppty;
    public Opportunity opptyfs {get;set;}
    ApexPages.StandardController controller;
    public string shippingStreet {get;set;}
    public string shippingCity {get;set;}
    public string shippingState {get;set;}
    public string shippingPostalCode {get;set;}
    public string shippingCountry {get;set;}
    public string shippingContact {get;set;}
    public string shippingContactPhone {get;set;}
    public boolean isDistributor {get;set;}
    public Id shippingAccountId {get;set;}
    public string selectedId {get; set;}
    public boolean copyBilling {get;set;}
    public string resultsmsg {get;set;}
    public boolean isPrimary {get;set;}
    string errMsg = '';
    public static final string DEFAULT_RESULTS_MSG = 'No existing shipping addresses were found.  Use the form below or copy the billing address to create a shipping address for the opportunity\'s account.';
    public static final string INCOMPLETE_ADDR_MSG = 'To save the shipping address, please provide a Street and City';
    public static final string MISSING_COUNTRY_MSG = 'Please enter a value for Shipping Country';
    
    public opptyext(ApexPages.StandardController controller) 
    {
        this.oppty = (Opportunity)controller.getRecord();
        isDistributor = false;
        isPrimary = false;
        
        
        if(oppty.Distributor_Account__c != null)
        {    
            shippingAccountId = oppty.Distributor_Account__c;
            isDistributor = true;
        }
        else
        {
            shippingAccountId = oppty.AccountId;
        }
        
        
        if(oppty.Shipping_Address__c != null)
        {
            selectedId = oppty.Shipping_Address__c;
            shippingStreet = oppty.Shipping_Address__r.Name;
            shippingCity = oppty.Shipping_City__c;
            shippingState = oppty.Shipping_State_Province__c;
            shippingPostalCode = oppty.Shipping_Postal_Code__c;
            shippingCountry = oppty.Shipping_Country__c;
            shippingContact = oppty.Shipping_Contact__c;
            shippingContactPhone = oppty.Shipping_Contact_Phone__c;
        }
        
        //this.opptyfs = getOpptys();
        resultsmsg = DEFAULT_RESULTS_MSG;
        string showAddressPopup = Apexpages.currentPage().getParameters().get('showAddressPopup');
        system.debug('DEBUG:: Stage_Number: ' + oppty.Stage_Number__c);
        system.debug('DEBUG:: Shipping_Address__c: ' + oppty.Shipping_Address__c);
        if(oppty.Stage_Number__c >=4 && oppty.Shipping_Address__c == null)
        {
            if(showAddressPopup == 'false')
            {
                closePopup();
            }
            else
            {
                showPopup();
            }
            //displayPopup=true;
        }
        else if(showAddressPopup == 'true')
        {
            showPopup();
        }
        else
        {
            closePopup();
            //displayPopup=false;
        }
        

    }

    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;
  
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
    public pageReference clearAddr()
    {
        selectedId = '';
        shippingStreet = '';
        shippingCity = '';
        shippingState = '';
        shippingPostalCode = '';
        shippingCountry = '';
        shippingContact = '';
        shippingContactPhone = '';
        
        if(oppty.Shipping_Address__c != null)
        {
            oppty.Shipping_Address__c = null;
            update oppty;
        }
        closePopup();
        PageReference opptyPage = new ApexPages.StandardController(oppty).view();
        opptyPage.setRedirect(true);
        return opptyPage;
    }
    
    public string validateAddr()
    {
        
        if(shippingStreet == '' || shippingCity == '')
            errMsg = INCOMPLETE_ADDR_MSG;
     
        if(shippingCountry == '')
            errMsg = MISSING_COUNTRY_MSG;
        
        return errMsg;     
    }
    
    public pageReference saveAddr()
    {
        errMsg = '';
        Address__c addr;
        system.debug('DEBUG:: selectedId: ' + selectedId);
        
        if(selectedId == null || selectedId == '' || copyBilling == true)
        {
             addr = new Address__c();
        }
        else
        {
             addr = [select Id, Type__c, Street__c, City__c, Postal_Code__c, 
                     State_Province__c, Country__c, Location_Contact__c, Location_Contact_Phone__c
                     from Address__c where Id =: selectedId limit 1];
        }
        
        system.debug('DEBUG:: addr: ' + addr);
        errMsg = validateAddr();
        
        if(errMsg == '')
        {
            if(isPrimary == true)
                addr.Primary__c = true;
            
            addr.Street__c = shippingStreet;
            addr.City__c = shippingCity;
            addr.State_Province__c = shippingState;
            addr.Postal_Code__c = shippingPostalCode;
            addr.Country__c = shippingCountry;
            addr.Name = shippingStreet;
            //addr.Account__c = oppty.AccountId;
            addr.Account__c = shippingAccountId;
            addr.Type__c = 'Shipping';
            addr.Location_Contact__c = shippingContact;
            addr.Location_Contact_Phone__c = shippingContactPhone;
            try
            {  
                upsert addr;
                system.debug('DEBUG:: addr.Id: ' + addr.Id);
                oppty.Shipping_Address__c = addr.Id;
                update oppty;
                
                closePopup();
                PageReference opptyPage = new ApexPages.StandardController(oppty).view();
                opptyPage.setRedirect(true);
                return opptyPage;
            }
            catch(Exception e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                ApexPages.addMessage(myMsg);
                return null;
            }
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errMsg);
            ApexPages.addMessage(myMsg);
            return null;
        }
        
    }
    
    public void cancelAddr()
    {
        closePopup();
    }
    
    public void selectAddr()
    {
        /*
        //closePopup();
        return null;*/
        system.debug('DEBUG:: selectedId = ' + selectedId);
    }
    /*
    public pageReference save()
    {
        system.debug('DEBUG:: got here in save()');
        //put in business logic to check oppty's account's shipping addr, if missing show popup
        //if(oppty.Shipping_Address__c == null && needsShippingStage(oppty.StageName) )
        if(oppty.Shipping_Address__c == null && oppty.Stage_Number__c>4)
        {
            system.debug('DEBUG:: got here in save() inside if');
            showPopup();
            return null;
        }
        else
        {
            update oppty;
            //pageReference pg = new pageReference('/' + oppty.Id);
            //return pg;
            PageReference opptyPage = new ApexPages.StandardController(oppty).view();
                opptyPage.setRedirect(true);
                return opptyPage;
        }

    }
    */
    public boolean needsShippingStage (string inputStage)
    {
        boolean retVal = false;
        if(inputStage.left(7) == 'Stage 4' || inputStage.left(7) == 'Stage 5' || inputStage.left(7) == 'Stage 6' || inputStage.left(7) == 'Stage 7' || inputStage.left(7) == 'Stage 8')
            retVal = true;
        
        return retVal;
    }
    
    public List<Address__c> getAddrs()
    {
        List<Address__c> returnAddrs = new List<Address__c>([select Id, Type__c, Street__c, City__c, Postal_Code__c, 
                                                             State_Province__c, Country__c, Location_Contact__c, Location_Contact_Phone__c
                                                                from Address__c
                                                                where Account__c =: shippingAccountId]);
        if (returnAddrs.size()==0)
            isPrimary = true;
        
        return returnAddrs;
    }


}