public class CustomerReferenceController{

    private final Opportunity oppty;
    public list<Opportunity> opptys {get;set;}
    public list<opptyObj> opptys2 {get;set;}
    public list<selectOption> someCRPickVals {get;set;}
    public string selectedCR {get;set;}
    public list<selectOption> allStages {get;set;}
    public list<selectOption> selectedStages {get;set;}
    public list<selectOption> allIndustries {get;set;}
    public string selectedIndustry {get;set;}
    public list<selectOption> allSubDivisions {get;set;}
    public string selectedSubDivision {get;set;}
    public string resultsMsg {get;set;}
    public id selectedOpptyObj {get;set;}
    public string reqStatusMsg {get;set;}
    public boolean noneFound {get;set;}
    public id dummyId {get;set;}
    //public map<string,id> mapRefReqFields {get;set;}
    public string refReqKeyPrefix {get;set;}
    public string retURL {get;set;}
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance(); 
    public static final string RR_AE_APPROVER = customSettings.Ref_Req_AE_Approver__c;
    public static final string RR_ACCOUNT = customSettings.Ref_Req_Account__c;
    public static final string RR_OPPTY = customSettings.Ref_Req_Opportunity__c;
    
    
    public class opptyObj
    {
        public Opportunity o {get;set;}
        //public string winComments {get;set;}
        public string winlossSummary {get;set;}
        public boolean hasPendingRequest {get;set;}
        
        public opptyObj(Opportunity oppty)
        {
            o = oppty;
            if(oppty.Reference_Requests__r.size()>0)
                hasPendingRequest = true;
            
            /*winComments = 'Business Value: ' + cleanNulls(oppty.Business_value__c) + '<p/>' +
                        'Technical Value: ' + cleanNulls(oppty.Technical_value__c) + '<p/>' +
                        'Operational Value: ' + cleanNulls(oppty.Operational_value__c);*/
            winlossSummary = '';
            if(oppty.Reasons_for_Win__c != '' && oppty.Reasons_for_Win__c != null)
            {
                winlossSummary = 'Reason for Win: ' + oppty.Reasons_for_Win__c;
            }
            if(oppty.Reasons_for_Loss__c != ''&& oppty.Reasons_for_Loss__c  != null)
            {
                if(winlossSummary != '')
                {
                    winlossSummary += '<p/>';
                }
                
                winlossSummary += 'Reason for Loss: ' + oppty.Reasons_for_Loss__c;     
            }
            if(oppty.Reasons_for_Loss__c == '' && oppty.Reasons_for_Loss__c  == null &&
                oppty.Reasons_for_Win__c == '' && oppty.Reasons_for_Win__c == null)
                {
                    winlossSummary = '';
                }
        }
    }
    

    /*private static string cleanNulls (string inputString)
    {
        string retVal;
        if(inputString == null)
        {
            retVal = '';
        }
        else
        {
            retVal = inputString;
        }
        return retVal;
    }*/
    
    
    public list<selectOption> getSomeCustomerReferencePickVals()
    {
        someCRPickVals = new list<selectOption>();        
        Schema.DescribeFieldResult fieldResult = Opportunity.Customer_Reference__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        someCRPickVals.add(new SelectOption('--None--', '--None--'));
        
        for( Schema.PicklistEntry f : ple)
        {
            if(f.getLabel() != 'Unknown' && f.getLabel() != 'Support Renewal')
                someCRPickVals.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return someCRPickVals;
    }
    
    public list<selectOption> getallSubDivisions()
    {
        allSubDivisions = new list<selectOption>();
        Schema.DescribeFieldResult fieldResult = User.Sub_Division__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        allSubDivisions.add(new SelectOption('--None--', '--None--'));
        
        for( Schema.PicklistEntry f : ple)
        {
            allSubDivisions.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return allSubDivisions;
    }
    
    public list<selectOption> getallIndustries()
    {
        allIndustries = new list<selectOption>();
        Schema.DescribeFieldResult fieldResult = Account.Industry.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        allIndustries.add(new SelectOption('--None--', '--None--'));
        
        for( Schema.PicklistEntry f : ple)
        {
            allIndustries.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return allIndustries;
    }
    
    public list<selectOption> getallStages()
    {
        allStages = new list<selectOption>();        
        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
        for( Schema.PicklistEntry f : ple)
        {
            allStages.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return allStages;
    }
    
    public CustomerReferenceController(ApexPages.StandardController controller) 
    {
        this.oppty = (Opportunity)controller.getRecord();
        resultsMsg = 'No records to display';
        selectedStages = new list<selectOption>();
        selectedIndustry = '';
        selectedSubDivision = '';
        selectedCR = '';
        getSomeCustomerReferencePickVals();
        getallStages();
        getallIndustries();
        getallSubDivisions();
        //mapRefReqFields = getReferenceRequestFields();
        Schema.DescribeSObjectResult r = Reference_Request__c.sObjectType.getDescribe();
        refReqKeyPrefix = r.getKeyPrefix();
        dummyId = null;
        noneFound = false;
    }
    

	/* can't use this until the tooling api is a separate permission; right now a user needs access to config to use this
    public map<string,id> getReferenceRequestFields()
    {
        //borrowed from http://andyinthecloud.com/2014/01/05/querying-custom-object-and-field-ids-via-tooling-api/
        map<string,id> retMap = new map<string,id>();
        // Constructs the Tooling API wrapper (default constructor uses user session Id)
        ToolingAPI toolingAPI = new ToolingAPI();
         
        // Query CustomObject object by DeveloperName (note no __c suffix required)
        List<ToolingAPI.CustomObject> customObjects = (List<ToolingAPI.CustomObject>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Reference_Request\'').records;
         
        // Query CustomField object by TableEnumOrId (use CustomObject Id not name for Custom Objects)
        ToolingAPI.CustomObject customObject = customObjects[0];
        Id customObjectId = customObject.Id;
        List<ToolingAPI.CustomField> customFields = (List<ToolingAPI.CustomField>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix, TableEnumOrId From CustomField Where TableEnumOrId = \'' + customObjectId + '\'').records;
         
        // Dump field names (reapply the __c suffix) and their Id's
        System.debug(customObject.DeveloperName + '__c : ' + customObject.Id);
        for(ToolingAPI.CustomField customField : customFields)
        {
             System.debug(
                  customObject.DeveloperName + '__c.' +
                  customField.DeveloperName + '__c : ' +
                  customField.Id);
        
            retMap.put(customField.DeveloperName + '__c', customField.Id);
        }
    
        return retMap;
        
    }*/
    
    public pageReference requestReference()
    {
        if(selectedOpptyObj != null)
        {
            Opportunity o = [select Id, Name, AccountId, Account.Name, OwnerId, Owner.Name from Opportunity where Id =: selectedOpptyObj];
            /*retURL = '/' + refReqKeyPrefix + '/e?CF' + string.valueOf(mapRefReqFields.get('Opportunity__c')).substring(0,15) + '_lkid=' + o.Id + '&' + 
                                                    'CF' + string.valueOf(mapRefReqFields.get('Opportunity__c')).substring(0,15) + '=' + o.Name + '&' + 
                                                    'Name=' + o.Name + '&' +
                                                    'CF' + string.valueOf(mapRefReqFields.get('AE_Approver__c')).substring(0,15) + '=' + o.Owner.Name + '&' +
                                                    'CF' + string.valueOf(mapRefReqFields.get('AE_Approver__c')).substring(0,15) + '_lkid=' + o.OwnerId + '&' +
                                                    'CF' + string.valueOf(mapRefReqFields.get('Account__c')).substring(0,15) + '=' + o.Account.Name + '&' +
                                                    'CF' + string.valueOf(mapRefReqFields.get('Account__c')).substring(0,15) + '_lkid=' + o.AccountId;*/
            retURL = '/' + refReqKeyPrefix + '/e?CF' + RR_OPPTY.substring(0,15) + '_lkid=' + o.Id + '&' + 
                                                    'CF' + RR_OPPTY.substring(0,15) + '=' + o.Name + '&' + 
                                                    'Name=' + o.Name + '&' +
                                                    'CF' + RR_AE_APPROVER.substring(0,15) + '=' + o.Owner.Name + '&' +
                                                    'CF' + RR_AE_APPROVER.substring(0,15) + '_lkid=' + o.OwnerId + '&' +
                                                    'CF' + RR_ACCOUNT.substring(0,15) + '=' + o.Account.Name + '&' +
                                                    'CF' + RR_ACCOUNT.substring(0,15) + '_lkid=' + o.AccountId;
            system.debug('DEBUG:: retURL: ' + retURL);
        }
        else
        {
            retURL = '/' + refReqKeyPrefix + '/e';
        }
        return null;
        
    }    
    
    public void search()
    {
        resultsMsg = 'No records to display';
        opptys = new list<Opportunity>();
        opptys2 = new list<opptyObj>();
        string soql = 'Select Id, Replication_Required__c, Name, StageName, CloseDate, Competition__c, Environment__c, Environment_detail__c, ' + 
                        'OwnerId, Owner.Name, Owner.Email, Reasons_for_Loss__c, Reasons_for_Win__c, Customer_Reference__c, ' + 
                        'Enviornment_Primary_Server__c, AccountId, Account.Name, Account.Industry, ' + 
                        'SE_Opportunity_Owner__c, Business_value__c, Technical_value__c, Operational_value__c, ' + 
                        '(select Id from Reference_Requests__r where Status__c != \'Rejected\' AND Status__c != \'Approved\')' +
                        'From Opportunity ';
        
        string whereClause = 'Where Name != null ';
        
        if(selectedStages.size()>0)
        {
            set<string> stages = new set<string>();
            boolean first = true;
            for(selectOption so: selectedStages)
            {
                stages.add(so.getLabel());
            }
            
            whereClause += 'AND StageName in: stages ' ;
                //+ '\'' + stages + '\'';
        }
        
        if(selectedIndustry != '--None--')
            whereClause += 'AND Account.Industry = ' + '\'' + String.escapeSingleQuotes(selectedIndustry) + '\'';
        
        if(selectedSubDivision != '--None--')
            whereClause += 'AND Sub_Division__c = ' + '\'' + String.escapeSingleQuotes(selectedSubDivision) + '\'';
        
        if(oppty.Environment__c != null)
            whereClause += 'AND Environment__c = ' + '\'' + String.escapeSingleQuotes(oppty.Environment__c) + '\'';
        
        if(oppty.Environment_detail__c != null)
            whereClause += 'AND Environment_detail__c = ' + '\'' + String.escapeSingleQuotes(oppty.Environment_detail__c) + '\'';
        
        if(oppty.Competition__c != null)
            whereClause += 'AND Competition__c = ' + '\'' + String.escapeSingleQuotes(oppty.Competition__c) + '\'';
        
        if(oppty.Enviornment_Primary_Server__c != null)
        	whereClause += 'AND Enviornment_Primary_Server__c = ' + '\'' + String.escapeSingleQuotes(oppty.Enviornment_Primary_Server__c) + '\'';
        
        if(oppty.Replication_Required__c != null)
            whereClause += 'AND Replication_Required__c = ' + '\'' + string.escapeSingleQuotes(oppty.Replication_Required__c) + '\'';
        
        if(selectedCR != '--None--')
            whereClause += 'AND Customer_Reference__c = ' + '\'' + String.escapeSingleQuotes(selectedCR) + '\'';
        
        
        string searchstring = soql + ' ' + whereClause;
        
        system.debug('DEBUG:: soql ' + soql);
        system.debug('DEBUG:: whereClause' + whereClause);
        system.debug('DEBUG:: searchstring' + searchstring);
        
        opptys = Database.query(searchstring);
        
        for(Opportunity o: opptys)
        {
            opptyObj op = new opptyObj(o);
            opptys2.add(op);
        }
        
        if(opptys.size() > 0 && opptys.size() <= 500)
        {
            resultsMsg = '';
        }
        if(opptys.size() == 0)
        {
            resultsMsg = 'Didn\'t find what you were looking for? Click the Request Reference button for help locating what you need.   ';
        	noneFound = true;
        }
        if(opptys.size()>500)
        {
            resultsMsg = 'There are too many records to display.  Please refine your search criteria.';
            opptys.clear();
            opptys2.clear();
        }
        
    }
    
   
    
}