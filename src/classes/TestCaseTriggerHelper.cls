@isTest
private class TestCaseTriggerHelper {
	
	@isTest static void offworkSupport() {
		TestDataFactory dataCreation = new TestDataFactory();
		String caseJSON = '{"attributes":{"type":"Case","url":"/services/data/v30.0/sobjects/Case/500E0000002nH2fIAE"},"Id":"500E0000002nH2fIAE","CreatedDate":"2014-07-05T17:54:26.000+0000"}';
		Case c = (Case) JSON.deserialize(caseJSON, Case.class );
		System.debug(c.createdDate);

		List<Case> insertedCases = new List<Case> ();
		insertedCases.add(c);
		Account testAccount = dataCreation.getAccount();
        insert testAccount;
        Contact testContact = dataCreation.getContact(testAccount);
        testContact.phone = '4086379711';
        insert testContact;
        Case insertedCase = dataCreation.createCase();
        insertedCase.accountId = testAccount.Id;
        insertedCase.contactId = testContact.Id;
        insertedCase.subject = 'Test';
		insert insertedCase;
		insertedCases.add(insertedCase);
		CaseComment insertedComment = new CaseComment (parentId = insertedCase.Id, commentBody = 'Test');
		insert insertedComment;
        Date today = System.today();
		Event testEvent = new Event(subject = 'On Call', IsVisibleInSelfService = true, StartDateTime = today.addDays(-2), EndDateTime  = today.addDays(2), ShowAs = 'OutOfOffice');
        insert testEvent;
		CaseTriggerHelper.offworkSupport(insertedCases);

	}
	@isTest static void offworkSupportTime() {
		Datetime createdTime = Datetime.newInstance(2014, 7, 5);
		CaseTriggerHelper.offworkSupport(createdTime);
	}

	@isTest static void isWeekend() {
		String dayString = 'Sat';
		CaseTriggerHelper.isWeekend(dayString);
		dayString = 'Sun';
		CaseTriggerHelper.isWeekend(dayString);
		dayString = 'Test';
		CaseTriggerHelper.isWeekend(dayString);
	}
	

}