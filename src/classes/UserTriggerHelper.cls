/*************************************************
Class: UserTriggerHelper
Description: This is the helper class for trigger UserTrigger
Auther: Christine Wang
Date:   7/3/2014

Revision History: 
----------------------------------------------------  
Auther: Sriram Swaminathan
Date:   7/23/2014   
Description: Introduced the Account Edit grant for Partner Users 
   
***************************************************/
public without sharing class UserTriggerHelper{

    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static boolean USER_TRIGGER_ENABLED = customSettings.User_Trigger_Enabled__c;
    
    // method fires on insert of user
    public static void methodOnInsert( List<User> newUsers ){
        
        if(USER_TRIGGER_ENABLED) {
            Map<Id, Boolean> contactIdToActiveStatusMap = new Map<Id, Boolean>(); 
            Set<ID> userIds = new Set<ID>(); 

            for( User user : newUsers ){
                System.debug('userType = ' + user.UserType);
                System.debug('contact = ' + user.ContactId);
                System.debug('status = ' + user.IsActive);
                // get associated contact for active partner user
                if( (user.UserType == 'Partner' || user.UserType == 'PowerPartner') && user.ContactId != null && user.IsActive ){
                    System.debug('I am in');
                    contactIdToActiveStatusMap.put( user.ContactId, true );
                }
                userIds.add(user.ID);
            }
            
            // update Portal_Access_Approved__c on associated contacts
            updatePortalAccessApprovedFieldOnContact( contactIdToActiveStatusMap );
            // Grant Account Edit Access to Partner Users
            grantAccountEditForPartnerUsers(userIds);
        }
    }
    
    // method fires on update of user
    public static void methodOnUpdate( List<User> newUsers, Map<Id, User> oldMap ){
      
        if(USER_TRIGGER_ENABLED) {
            Map<Id, Boolean> contactIdToActiveStatusMap = new Map<Id, Boolean>();
            Set<ID> userIds = new Set<ID>(); 
           
            for( User user : newUsers ){
                //if contact dipable partner user
                if( (user.UserType == 'Partner' || user.UserType == 'PowerPartner') && user.IsPortalEnabled!= oldMap.get(user.Id).IsPortalEnabled){
                    System.debug('I am in');
                    if( user.IsPortalEnabled){
                        contactIdToActiveStatusMap.put( user.ContactId, true );
                    }else{
                        contactIdToActiveStatusMap.put( user.ContactId, false );
                    }
                }
                // if partner user status changes, get associated contact for that user
                if( (user.UserType == 'Partner' || user.UserType == 'PowerPartner') && user.ContactId != null && user.IsActive != oldMap.get(user.Id).IsActive ){
                    System.debug('I am in');
                    if( user.IsActive ){
                        contactIdToActiveStatusMap.put( user.ContactId, true );
                    }else{
                        contactIdToActiveStatusMap.put( user.ContactId, false );
                    } 
                }
                userIds.add(user.ID);
            }
            System.debug('contactIdToActiveStatusMap = ' +contactIdToActiveStatusMap );
            
            // update Portal_Access_Approved__c on associated contacts
            updatePortalAccessApprovedFieldOnContact( contactIdToActiveStatusMap );
            // Grant Account Edit Access to Partner Users
            grantAccountEditForPartnerUsers(userIds); 
        }       
    }
    
    // method fires to update the Portal Access Approved field on contact
    @future
    public static void updatePortalAccessApprovedFieldOnContact( Map<Id, Boolean> contactIdToActiveStatusMap ){
        List<Contact> contacts = [ SELECT Portal_Access_Approved__c FROM Contact WHERE Id IN :contactIdToActiveStatusMap.keySet() ];
        System.debug('contacts = ' + contacts);
        for( Contact con : contacts ){
            con.Portal_Access_Approved__c = contactIdToActiveStatusMap.get( con.Id );
        }
        
        Database.update( contacts );
    }

    // Grant Account Edit Access to Partner Users
    @future
    public static void grantAccountEditForPartnerUsers(Set<ID> userIds){
        List<AccountShare> accountShares = new List<AccountShare>();
        Map<ID, Set<ID>> partnerUserIdsByAccountId = new Map<ID, Set<ID>>();
        Set<ID> partnerUserIds = new Set<ID>();
        Set<ID> partnerUserIdsWithAccountShare = new Set<ID>();
        for (User u : [ SELECT ID, ACCOUNTID 
                        FROM USER 
                        WHERE ID IN :userIds
                        AND (ISACTIVE = TRUE)
                        AND (ACCOUNTID != NULL)
                        AND (CONTACTID != NULL)
                        AND (CONTACT.ACCOUNTID != NULL)
                        AND (CONTACT.ACCOUNT.ISPARTNER = TRUE)
                        ]){
            if (partnerUserIdsByAccountId.containsKey(u.ACCOUNTID)){
                partnerUserIdsByAccountId.get(u.ACCOUNTID).add(u.ID);
            } else {
                partnerUserIdsByAccountId.put(u.ACCOUNTID, new Set<ID>{u.ID});
            }
            partnerUserIds.add(u.ID);
        }
        for (AccountShare aShare : [SELECT ID, USERORGROUPID, ACCOUNTID, ACCOUNTACCESSLEVEL
                                    FROM ACCOUNTSHARE
                                    WHERE ACCOUNTID IN :partnerUserIdsByAccountId.keySet()
                                    AND USERORGROUPID IN :partnerUserIds
                                    ]){
            if (aShare.ACCOUNTACCESSLEVEL.equals('Read')){
                aShare.ACCOUNTACCESSLEVEL = 'Edit';
                accountShares.add(aShare);
            }
            partnerUserIdsWithAccountShare.add(aShare.USERORGROUPID);
        }
        for (ID acctID : partnerUserIdsByAccountId.keySet()){
            for (ID partnerUserID : partnerUserIdsByAccountId.get(acctID)){
                if (!partnerUserIdsWithAccountShare.contains(partnerUserID)){
                    accountShares.add(new AccountShare(
                                                USERORGROUPID = partnerUserID
                                                ,ACCOUNTID = acctID
                                                ,ACCOUNTACCESSLEVEL = 'Edit'
                                                //,ROWCAUSE = 'Manual'
                                                //,CASEACCESSLEVEL = 'None'
                                                //,CONTACTACCESSLEVEL = 'None'
                                                ,OPPORTUNITYACCESSLEVEL = 'None'
                                            ) 
                                        );
                }
            }
        }
        upsert accountShares;
    }

}