Public class ZenTicket{
    public String id {get;set;}    
    public String subject {get;set;}
    public String type;
    public String status;
    public String priority;    
    public String description; 
    public String assignee {get;set;}   
    public String updated_at;
    public String created_at;
    public String status_updated_at;
    public String requester_id {get;set;}
    public String requester_name {get;set;}
    public String requester_email {get;set;}
    public String requester_organizationId {get;set;}
    public String resolution_time {get;set;}
    
    /*
    public String organization_id{get;set;}
    public List<String> collaborator_ids{get;set;}
    public String url{get;set;}
    public String has_incidents{get;set;}
    public String forum_topic_id{get;set;}
    public String submitter_id{get;set;}
    public String group_id{get;set;}
    public String external_id{get;set;}
    public List<String> tags{get;set;}
    public List<String> via{get;set;}
    */
    
    
    public List<ZenComment> comments {get;set;}
    
    public String getDescription(){
        return this.description.replace('\n','<br>');
    }
    public String getPriority(){
        /* XML: Deprecated
        if (this.priority == '0')
            return 'N/A';
        else if (this.priority == '1')
            return 'SEV 4';
        else if (this.priority == '2')
            return 'SEV 3';
        else if (this.priority == '3')
            return 'SEV 2';
        else if (this.priority == '4')
            return 'SEV 1';
        */
        if (this.priority == 'low')
            return 'SEV 4';
        else if (this.priority == 'normal')
            return 'SEV 3';
        else if (this.priority == 'high')
            return 'SEV 2';
        else if (this.priority == 'urgent')
            return 'SEV 1';       
        return 'Undefined';
    }
    
    public String getType(){
        
        /* XML: Deprecated
        if (this.type == '0')
            return 'N/A';
        else if (this.type == '1')
            return 'Question';
        else if (this.type == '2')
            return 'Incident';
        else if (this.type == '3')
            return 'Problem';
        else if (this.type == '4')
            return 'Task';
        */        
        if (this.type == 'question')
            return 'Question';
        else if (this.type == 'incident')
            return 'Incident';
        else if (this.type == 'problem')
            return 'Problem';
        else if (this.type == 'task')
            return 'Task';
        return 'Undefined';
    }

    public String getStatus(){
    	/* XML: Deprecated
        if (this.status == '0')
            return 'Open'; //New
        else if (this.status == '1')
            return 'Open';
        else if (this.status == '2')
            return 'Open'; //Pending
        else if (this.status == '3')
            return 'Closed'; //Solved
        else if (this.status == '4')
            return 'Closed';
        */    
        if (this.status == 'new')
            return 'Open';
        else if (this.status == 'open')
            return 'Open';
        else if (this.status == 'pending')
            return 'Open';
        else if (this.status == 'solved')
            return 'Closed';
        else if (this.status == 'closed')
            return 'Closed';
        return 'Undefined';
    }
    
    public Datetime getUpdatedAt(){
        DateTime GMTDateTime = datetime.valueOf(updated_at.substring(0,10) + ' ' + updated_at.substring(11,19));
        return datetime.newinstance(GMTDateTime.date(),GMTDateTime.time());  
    }
    
    public Datetime getCreatedAt(){
        return datetime.valueOf(created_at.substring(0,10) + ' ' + created_at.substring(11,19));   
    }
    
    public Datetime getStatusUpdatedAt(){
        return datetime.valueOf(status_updated_at.substring(0,10) + ' ' + status_updated_at.substring(11,19));   
    }
    
}