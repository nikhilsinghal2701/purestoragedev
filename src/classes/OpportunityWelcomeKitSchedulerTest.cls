@isTest
public class OpportunityWelcomeKitSchedulerTest
{
    static testMethod void testOpportunityWelcomeKitScheduler()
    {
        Apex_Code_Settings__c testSetting = getCustomSetting();
        Account testAccount = createAccount();
        
        createOpportunity( 'USD', System.today().addDays( -Integer.valueOf( testSetting.Welcome_Kit_First_Email_Notification__c )), testAccount );
        createOpportunity( 'USD', System.today().addDays( -Integer.valueOf( testSetting.Welcome_Kit_Second_Email_Notification__c )), testAccount );
        
        test.startTest();
        System.assertEquals( 'processed', OpportunityWelcomeKitScheduler.processClosedOpportunity() );
        test.stopTest();
    }
    
    
    public static Apex_Code_Settings__c getCustomSetting()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Welcome_Kit_First_Email_Notification__c = 14;
        cs.Welcome_Kit_Second_Email_Notification__c = 21;
        cs.Welcome_Kit_Stage__c = 'Stage 8 - Closed/Won';
        cs.Welcome_Kit_Email_Template__c = 'Welcome Kit Reminder';
        cs.Welcome_Kit_Go_Live_Date__c = system.today();
        
        Profile profileInfo = [SELECT Id FROM Profile WHERE Name = 'Marketing User' LIMIT 1];
        
        
        user testUser = new User();
        testUser.username = 'test@test.com1234567890';
        testUser.email = 'test@test.com123';
        testUser.LastName = 'test';
        testUser.EmailEncodingKey='UTF-8';
        testUser.LanguageLocaleKey='en_US';
        testUser.LocaleSidKey='en_US';
        testUser.TimeZoneSidKey='America/Los_Angeles';
        testUser.Alias = 'test';
        testUser.ProfileId = profileInfo.id;
        insert testUser;
        
        Group testGroup = new Group();
        testGroup.Name = 'test';
        insert testGroup;
        
        GroupMember testGroupMember = new GroupMember();
        testGroupMember.GroupId = testGroup.id;
        testGroupMember.UserOrGroupId = testUser.id;
        insert testGroupMember;
        
        cs.Welcome_Kit_Notification_Group_ID__c = testGroup.id;
        insert cs;   
        
        return cs;
    }
    
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account' );
        // To by-pass the trigger of zen
        myAccount.Support_Must_Not_Contact_End_Customer__c = true;
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate, Account testAccount )
    {
        Opportunity myOpp = new Opportunity( AccountId = testAccount.Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 8 - Closed/Won',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate,
                                             Customer_Reference__c = 'Public Reference',
                                             POC_Install_Date__c = closeDate,
                                             Reasons_for_Win__c = 'test',
                                             X23TB_required__c = 'No',
                                             Replication_Required__c = 'No',
                                             Business_value__c = 'test',
                                             Operational_value__c = 'test',
                                             Technical_value__c = 'test',
                                             Eligible_for_the_Love_Your_Storage_Prog__c = 'No',
                                             Competition__c = 'No Competition',
                                             Reason_s_for_Win_Loss__c = 'Pricing',
                                             SE_Opportunity_Owner__c = UserInfo.getUserId(),
                                             Forever_Flash_upgrade__c = 'some val',
                                             Welcome_Kit_Opt_Out__c = 'TBD' );
        
        insert myOpp;
        return myOpp;
    }
}