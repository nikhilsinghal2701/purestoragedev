public Class OpportunityExchangeVFProcessor
{
    public String errorMessages{ get; set; }
    public String successMessages{ get; set; }
    public String dateFilter{ get; set; }
    public Map<String, List<DatedConversionRate>> myConversionMapping;
    
    // Contstructor will fetch all of the conversion rates and store into the map
    public OpportunityExchangeVFProcessor()
    {
        successMessages = 'None';
        errorMessages = '';
        myConversionMapping = OpportunityExchangeProcessor.getConversionRate();
        System.debug( myConversionMapping );
    }
    
    
    // This will fetch all of the currency date range
    public String getFilter()
    {
        String filter = '';

        Set<String> filteredDates = new Set<String>();
        
        for (DatedConversionRate dcr : [SELECT StartDate, NextStartDate 
                                          FROM DatedConversionRate
                                      ORDER BY StartDate])
        {
            String dateRange = dcr.StartDate.format() + '-' + dcr.nextStartDate.format();
            
            // Using the set to filter out duplicates
            if (filteredDates.add( dateRange ))
            {
                // Excluding the default currency for all date range
                if (!dateRange.equals( '1/1/0001-12/31/9999'))
                {
                    filter += dateRange + ';';
                }
            }
        }
        
        return filter;
    }
    
    
    // In order to save SOQL, we will be using the map that was fetched from the constructor
    public void processOpportunities()
    {
        List<Opportunity> myOpportunities = new List<Opportunity>();
        
        String mySOQL = 'SELECT Id, amount, CurrencyIsoCode, CloseDate FROM Opportunity WHERE amount != null';
        
        Date startDate;
        Date nextStartDate;
        
        // If the administrator uses the filter, the method needs to parse the data
        if(dateFilter != null && dateFilter.length() > 0)
        {
            // Split the string into two start and next start string
            String[] splitDates = dateFilter.split( '-' );
            
            // Parse them into year, month and day string
            String[] splitStartDate = splitDates[0].split( '/' );
            String[] splitNextStartDate = splitDates[1].split( '/' );
            
            System.debug( splitNextStartDate );
            
            // Create date object for SOQL query
            startDate = Date.newinstance( Integer.valueOf( splitStartDate[2] ), Integer.valueOf( splitStartDate[1] ), Integer.valueOf( splitStartDate[0] ) );
            
            // Checking to see if the year is not 9999. If it is 9999, we will only have to fetch the latest one 
            if (Integer.valueOf( splitNextStartDate[2] ) != 9999)
            {
                nextStartDate = Date.newinstance( Integer.valueOf( splitNextStartDate[2] ), Integer.valueOf( splitNextStartDate[1] ), Integer.valueOf( splitNextStartDate[0] ) );
                mySOQL += ' AND CloseDate < :NextStartDate ';
            }
            
            mySOQL += ' AND CloseDate >= :startDate ';
            
        }
        
        // Using the SOQL string to query all of the matching opportunities
        for (Opportunity myOpp : Database.query( mySOQL ))
        {
            // Loop through all of the matching currency code
            for (DatedConversionRate dcr : myConversionMapping.get( myOpp.CurrencyIsoCode ))
            {
                System.debug( myOpp );
                
                if (dcr.StartDate <= myOpp.CloseDate && dcr.NextStartDate > myOpp.CloseDate)
                {
                    myOpp.Exch_Rate_USD__c = dcr.ConversionRate;
                    break;
                }
            }
            
            myOpp.Converted_Amount_USD__c = OpportunityExchangeProcessor.calculateOpportunityExchangeRate( myOpp.Amount, myOpp.Exch_Rate_USD__c );
            myOpportunities.add( myOpp );
        }
        
        // Process any of the data that can be procesesd
        Database.SaveResult[] processedList = Database.update( myOpportunities, false );
        
        errorMessages = '';
           
        Integer processedRecordCount = 0;
        
        // Display the error messages to the administrator
        for (Integer i=0; i < processedList.size(); i++)
        {
           if(!processedList.get(i).isSuccess())
           {
               errorMessages += '<li>ID:' + myOpportunities.get(i).Id + '<ul>';           
               
               for (Database.Error error : processedList.get(i).getErrors())
               {
                   errorMessages += '<li>' + error.getMessage() + '</li>';
               }
               
               errorMessages += '</ul></li>';
           }
           else
           {
               processedRecordCount++;
           }
        }
        
        if (errorMessages.length() > 0)
        {
            errorMessages = 'Error Messages:<br/><ul>' + errorMessages + '</ul>';
        }
        
        successMessages = processedRecordCount + ' records out of ' + myOpportunities.size() + ' have been processed.';
    }
}