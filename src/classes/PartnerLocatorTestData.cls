/*****************************************************************************
 * Description : Purpose of this class is to setup the test data for Partner Locator functionality
 * Author      : Sriram Swaminathan
 * Date        : 07/21/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorTestData {

    public final static Integer DEFAULT_SAMPLE_DATA_SIZE = 100;

    public final static String PARTNER_ACCOUNT_NAME_PREFIX = 'Partner Locator Test Account';
    public final static String PARTNER_USER_NAME_PREFIX = 'PartnerTst';
    public final static String PARTNER_NAME_PREFIX = 'Partner Locator Test';

	public final static String PROFILE_NAME_PRM_GLOBAL_PARTNER_PROFILE = 'PRM Global Partner Profile';

    public final static String PARTNER_SERVICE_AREA_US = 'United States';
    public final static String PARTNER_SERVICE_AREA_CANADA = 'Canada';
    public final static String PARTNER_SERVICE_AREA_MEXICO = 'Mexico';    

    public final static String US_STATE_CA = 'CA';
    public final static String US_STATE_AZ = 'AZ';
    public final static String US_STATE_NV = 'NV';

    public final static String MEXICO_STATE_CHIAPAS = 'Chiapas';
    public final static String MEXICO_STATE_SINALOA = 'Sinaloa';
    public final static String MEXICO_STATE_OAXACA = 'Oaxaca';

    public final static String CANADA_PROVINCE_ONTARIO = 'Ontario';
    public final static String CANADA_PROVINCE_QUEBEC = 'Quebec';
    public final static String CANADA_PROVINCE_ALBERTA = 'Alberta';    

    public final static Map<String, List<String>> countryStateMap =
                            new Map<String, List<String>>{
                                                        PARTNER_SERVICE_AREA_US => new List<String>{
                                                                                            US_STATE_CA
                                                                                            ,US_STATE_AZ
                                                                                            ,US_STATE_NV
                                                                                        }
                                                        ,PARTNER_SERVICE_AREA_CANADA => new List<String>{
                                                                                            CANADA_PROVINCE_ONTARIO
                                                                                            ,CANADA_PROVINCE_QUEBEC
                                                                                            ,CANADA_PROVINCE_ALBERTA
                                                                                        }  
                                                        ,PARTNER_SERVICE_AREA_MEXICO => new List<String>{
                                                                                            MEXICO_STATE_CHIAPAS
                                                                                            ,MEXICO_STATE_SINALOA
                                                                                            ,MEXICO_STATE_OAXACA
                                                                                        }                                                                                                                                                                               
                                                    };

    public final static String INDUSTRY_FOCUS_COMMERCIAL = 'Commercial';
    public final static String INDUSTRY_FOCUS_ENTERPRISE = 'Enterprise';
    public final static String INDUSTRY_FOCUS_FINANCIALS = 'Financials';
    public final static String INDUSTRY_FOCUS_EDUCATION = 'Education';
    public final static String INDUSTRY_FOCUS_TECHNOLOGY = 'Technology';

    public final static String APPLICATION_EXPERTISE_DATABASE = 'Database';
    public final static String APPLICATION_EXPERTISE_VIRTUAL_SERVER = 'Virtual Server Infrastructure';
    public final static String APPLICATION_EXPERTISE_VIRTUAL_DESKTOP = 'Virtual Desktop Infrastructure';

    public final static String TARGET_MARKET_SIZE_SMALL = 'Small';
    public final static String TARGET_MARKET_SIZE_MEDIUM = 'Medium';
    public final static String TARGET_MARKET_SIZE_LARGE = 'Large';
    public final static String TARGET_MARKET_SIZE_ENTERPRISE = 'Enterprise';    

    public final static String PLATFORMS_WINDOWS = 'Windows';
    public final static String PLATFORMS_LINUX = 'Linux';
    public final static String PLATFORMS_UNIX = 'Unix';
    public final static String PLATFORMS_ORACLE = 'Oracle';
    public final static String PLATFORMS_MICROSOFT_SQL_SERVER = 'Microsoft SQL Server';   

    public final static List<String> PARTNER_TIERS = new List<String>{'Platinum', 'Gold', 'Silver'}; 

    public static void setup(Integer sampleDataSize){

        sampleDataSize = (sampleDataSize<=0) ? DEFAULT_SAMPLE_DATA_SIZE : sampleDataSize;
        User sysAdmin = createSystemAdminUser();
        insert sysAdmin;

        System.runAs(sysAdmin) {
        
            Profile profile = [SELECT Id FROM Profile WHERE Name = :PROFILE_NAME_PRM_GLOBAL_PARTNER_PROFILE]; 

            List<Account> partnerAccounts = new List<Account>();
            List<String> partnerUserNames = new List<String>();
            List<Contact> partnerContacts = new List<Contact>();
            List<User> partnerUsers = new List<User>();
            List<Partner_Locator_Details__c> partnerLocatorDetails = new List<Partner_Locator_Details__c>();
            // work with partner accounts
            for (Integer i=0; i<sampleDataSize; i++){
                String partnerAccountName = getPartnerAccountName(i);
                String partnerTier = getPartnerTier();  
                partnerAccounts.add(createPartnerAccount(partnerAccountName, partnerTier));
            }
            insert partnerAccounts;
            for (Account partnerAccount : partnerAccounts){
                partnerAccount.ISPARTNER = TRUE;
            }
            update partnerAccounts;
            // work with partner user names
            for (Integer i=0; i<sampleDataSize; i++){
                partnerUserNames.add(getPartnerUserName(i));
            }
            // work with partner contacts
            for (Integer i=0; i<sampleDataSize; i++){
                partnerContacts.add(createPartnerContact(partnerAccounts.get(i), partnerUserNames.get(i)));
            }
            insert partnerContacts;
            // work with partner users
            for (Integer i=0; i<sampleDataSize; i++){
                String partnerUserName = getPartnerUserName(i);
                partnerUsers.add(createPartnerUser(partnerUserNames.get(i), profile, partnerContacts.get(i)));
            }
            insert partnerUsers;
            // work with partner locator details
            for (Integer i=0; i<sampleDataSize; i++){
                String partnerName = getPartnerName(i);
                String country = getCountry();
                String state = getState(country);
                List<String> industryFocus = getIndustryFocus();
                List<String> applicationExpertise = getApplicationExpertise();
                List<String> targetMarketSize = getTargetMarketSize();
                List<String> platforms = getPlatform();
                partnerLocatorDetails.add(createPartnerLocatorDetails(partnerAccounts.get(i)
                                                                ,partnerName
                                                                ,country
                                                                ,state
                                                                ,industryFocus
                                                                ,applicationExpertise
                                                                ,targetMarketSize
                                                                ,platforms)
                                        );            
            }
            insert partnerLocatorDetails;
        }
    } 

    public static User createSystemAdminUser() {
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        UserRole userRole = [SELECT Id FROM USERROLE WHERE Name = 'WW VP Sales'];
        User user = new User();
        user.Alias = 'sysadm';
        user.Email= 'sysadmin@purestorage.com';
        user.LastName = 'sysadmin';
        user.ProfileId = profile.Id;
        user.userName = 'sysadmin@purestorage.com';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.isActive = TRUE;
        user.userRoleId = userRole.ID;
        return user;        
    }        

    public static Account createPartnerAccount(String partnerAccountName, String partnerTier) {
        Account partnerAccount =
                 new ACCOUNT (NAME = partnerAccountName
                    ,INDUSTRY  = 'Test Industry'
                    //,OWNERID = partnerUser.ID
                    //,ISPARTNER = TRUE
                    ,XPSYNCED__c = 'N'
                    ,TYPE = 'Partner'
                    ,AUTHORIZED_PARTER__C = TRUE
                    ,AUTHORIZED_PARTNER__C = TRUE
                    ,PARTNER_TIER__C = partnerTier
                    ,COMPANY_SIZE__C = '0-499'
                    ,QBDIALER__DIALS__C = 0
                );
        //insert partnerAccount;
        //partnerAccount.ISPARTNER = TRUE;
        //update partnerAccount;
        return partnerAccount;          
    }  

    public static Contact createPartnerContact(Account partnerAccount, String partnerUserName) {
        Contact partnerContact =
                 new Contact (LASTNAME = partnerUserName
                    ,EMAIL  = partnerUserName + '@purestorage.com'
                    //,OWNERID = partnerUser.ID
                    ,ACCOUNTID = partnerAccount.ID
                    ,TITLE = partnerUserName
                    ,ACCOUNT_OWNER__C = partnerAccount.ownerID
                    ,LAST_NAME__C = partnerUserName
                    ,PRMCITY__C = 'PRM Test City'
                    ,PRMCOUNTRY__C = 'PRM Test Country'
                    ,PRMEMAIL__C = partnerUserName + '@purestorage.com'
                    ,PRM_PHONE__C = '111-000-9999'
                    ,PRMMOBILE__C = '000-999-1111'
                    ,PORTAL_ACCESS_APPROVED__C = TRUE
                    ,CONTACT_STATUS__C = 'Qualified'
                );
        //insert partnerContact; //DO NOT INSERT, JUST RETURN THE Contact Instance
        return partnerContact;          
    }

    public static User createPartnerUser(String partnerUserName, Profile profile, Contact partnerContact) {
        User user = new User();
        user.Alias = partnerUserName.substring(0, 6);
        user.Email= partnerUserName + '@purestorage.com';
        user.LastName = partnerUserName;
        user.ProfileId = profile.Id;
        user.userName = partnerUserName + '@purestorage.com';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.ContactId = partnerContact.ID;
        user.isActive = TRUE;
        //user.ACCOUNTID = partnerContact.ACCOUNTID; //Field is not writeable
        //insert user;  //DO NOT INSERT, JUST RETURN THE USER Instance
        return user;        
    }    

    public static Partner_Locator_Details__c createPartnerLocatorDetails(Account partnerAccount
                                                                            , String partnerName
                                                                            , String country
                                                                            , String state
                                                                            , List<String> industryFocus
                                                                            , List<String> applicationExpertise
                                                                            , List<String> targetMarketSize
                                                                            , List<String> platforms){
        Partner_Locator_Details__c d = new Partner_Locator_Details__c (
            Partner_Name__c = partnerName
            ,Partner_Account__c = partnerAccount.ID
            ,Status__c = 'Published'
            ,Primary_Location_Country__c = country
            ,Primary_Location_State_Province__c = (String.isBlank(state)?NULL:state)
            ,Industry_Focus__c = convertedListToMultiValuePicklist(industryFocus)
            ,Application_Expertise__c = convertedListToMultiValuePicklist(applicationExpertise)
            ,Published_Date__c = System.today()
            ,Target_Market_Size__c = convertedListToMultiValuePicklist(targetMarketSize)
            ,Platforms__c = convertedListToMultiValuePicklist(platforms)
            ,Company_Logo__c = '<img width="42" src="image.png" height="42" alt="User-added image" >'
        );
        //insert d;   //DO NOT INSERT, JUST RETURN THE Partner_Locator_Details__c Instance
        return d;
    }  

    private static String convertedListToMultiValuePicklist(List<String> values){
        String multiValue = '';
        for (String value : values){
            multiValue = multiValue + ( String.isBlank(multiValue)
                                                    ? value
                                                    : (';' + value) 
                                        );
        }
        return multiValue;
    }

    private static String getPartnerAccountName(Integer index){
        return PARTNER_ACCOUNT_NAME_PREFIX + ' ' + index;
    }  

    private static String getPartnerUserName(Integer index){
        return PARTNER_USER_NAME_PREFIX + '_' + index;
    }       

    private static String getPartnerName(Integer index){
        return PARTNER_NAME_PREFIX + '_' + index;
    }       


    private static String getPartnerTier() {
        Double n = Math.random();
        if (n < 0.33){
            return PARTNER_TIERS.get(0);
        } else if (n < 0.66){
            return PARTNER_TIERS.get(1);
        } else {
            return PARTNER_TIERS.get(2);
        }
    }

    private static List<String> getApplicationExpertise() {
        Double n = Math.random();
        if (n < 0.14){
            return new List<String>{APPLICATION_EXPERTISE_DATABASE};
        } else if (n < 0.28){
            return new List<String>{APPLICATION_EXPERTISE_VIRTUAL_SERVER};
        } else if (n < 0.42){
            return new List<String>{APPLICATION_EXPERTISE_VIRTUAL_DESKTOP};
        } else if (n < 0.56){
            return new List<String>{APPLICATION_EXPERTISE_DATABASE, APPLICATION_EXPERTISE_VIRTUAL_SERVER};
        } else if (n < 0.70){
            return new List<String>{APPLICATION_EXPERTISE_DATABASE, APPLICATION_EXPERTISE_VIRTUAL_DESKTOP};
        } else if (n < 0.84){
            return new List<String>{APPLICATION_EXPERTISE_VIRTUAL_SERVER, APPLICATION_EXPERTISE_VIRTUAL_DESKTOP};
        } else {
            return new List<String>{APPLICATION_EXPERTISE_DATABASE, APPLICATION_EXPERTISE_VIRTUAL_SERVER, APPLICATION_EXPERTISE_VIRTUAL_DESKTOP};
        }
    } 

    private static List<String> getIndustryFocus() {
        Double n = Math.random();
        if (n < 0.10){
            return new List<String>{INDUSTRY_FOCUS_COMMERCIAL};
        } else if (n < 0.20){
            return new List<String>{INDUSTRY_FOCUS_ENTERPRISE};
        } else if (n < 0.30){
            return new List<String>{INDUSTRY_FOCUS_FINANCIALS};
        } else if (n < 0.40){
            return new List<String>{INDUSTRY_FOCUS_EDUCATION};
        } else if (n < 0.50){
            return new List<String>{INDUSTRY_FOCUS_TECHNOLOGY};            
        } else if (n < 0.60){
            return new List<String>{INDUSTRY_FOCUS_COMMERCIAL, INDUSTRY_FOCUS_ENTERPRISE};
        } else if (n < 0.70){
            return new List<String>{INDUSTRY_FOCUS_FINANCIALS, INDUSTRY_FOCUS_EDUCATION};
        } else if (n < 0.80){
            return new List<String>{INDUSTRY_FOCUS_ENTERPRISE, INDUSTRY_FOCUS_FINANCIALS};
        } else if (n < 0.90){
            return new List<String>{INDUSTRY_FOCUS_COMMERCIAL, INDUSTRY_FOCUS_EDUCATION, INDUSTRY_FOCUS_TECHNOLOGY};            
        } else {
            return new List<String>{INDUSTRY_FOCUS_COMMERCIAL, INDUSTRY_FOCUS_ENTERPRISE, INDUSTRY_FOCUS_FINANCIALS};
        }
    }   

    private static List<String> getPlatform() {
        Double n = Math.random();
        if (n < 0.10){
            return new List<String>{PLATFORMS_WINDOWS};
        } else if (n < 0.20){
            return new List<String>{PLATFORMS_LINUX};
        } else if (n < 0.30){
            return new List<String>{PLATFORMS_UNIX};
        } else if (n < 0.40){
            return new List<String>{PLATFORMS_ORACLE};
        } else if (n < 0.50){
            return new List<String>{PLATFORMS_MICROSOFT_SQL_SERVER};            
        } else if (n < 0.60){
            return new List<String>{PLATFORMS_WINDOWS, PLATFORMS_LINUX};
        } else if (n < 0.70){
            return new List<String>{PLATFORMS_UNIX, PLATFORMS_ORACLE};
        } else if (n < 0.80){
            return new List<String>{PLATFORMS_WINDOWS, PLATFORMS_MICROSOFT_SQL_SERVER};
        } else if (n < 0.90){
            return new List<String>{PLATFORMS_ORACLE, PLATFORMS_LINUX, PLATFORMS_UNIX};            
        } else {
            return new List<String>{PLATFORMS_WINDOWS, PLATFORMS_MICROSOFT_SQL_SERVER, PLATFORMS_ORACLE};
        }
    }

    private static List<String> getTargetMarketSize() {
        Double n = Math.random();
        if (n < 0.10){
            return new List<String>{TARGET_MARKET_SIZE_SMALL};
        } else if (n < 0.20){
            return new List<String>{TARGET_MARKET_SIZE_MEDIUM};
        } else if (n < 0.30){
            return new List<String>{TARGET_MARKET_SIZE_LARGE};
        } else if (n < 0.40){
            return new List<String>{TARGET_MARKET_SIZE_ENTERPRISE};
        } else if (n < 0.50){
            return new List<String>{TARGET_MARKET_SIZE_SMALL, TARGET_MARKET_SIZE_MEDIUM};            
        } else if (n < 0.60){
            return new List<String>{TARGET_MARKET_SIZE_LARGE, TARGET_MARKET_SIZE_ENTERPRISE};
        } else if (n < 0.70){
            return new List<String>{TARGET_MARKET_SIZE_MEDIUM, TARGET_MARKET_SIZE_ENTERPRISE};
        } else if (n < 0.80){
            return new List<String>{TARGET_MARKET_SIZE_SMALL, TARGET_MARKET_SIZE_MEDIUM, TARGET_MARKET_SIZE_LARGE};
        } else if (n < 0.90){
            return new List<String>{TARGET_MARKET_SIZE_SMALL, TARGET_MARKET_SIZE_MEDIUM, TARGET_MARKET_SIZE_ENTERPRISE};            
        } else {
            return new List<String>{TARGET_MARKET_SIZE_SMALL, TARGET_MARKET_SIZE_MEDIUM, TARGET_MARKET_SIZE_LARGE, TARGET_MARKET_SIZE_ENTERPRISE};
        }
    } 

    private static String getCountry() {
        Double n = Math.random();
        if (n < 0.33){
            return PARTNER_SERVICE_AREA_US;
        } else if (n < 0.66){
            return PARTNER_SERVICE_AREA_CANADA;
        } else {
            return PARTNER_SERVICE_AREA_MEXICO;
        }
    } 

    private static String getState(String country) {
        Double n = Math.random();
        if (n < 0.33){
            return countryStateMap.get(country).get(0);
        } else if (n < 0.66){
            return countryStateMap.get(country).get(1);
        } else {
            return countryStateMap.get(country).get(2);
        }
    }           

}