@isTest( seeAllData = true )
private class TestLibrariesLandingPageController {

    static testMethod void testLibrariesLandingPageController() {
    	
		// getting the Field Integrity Exception - You cannot set custom fields or tags on Pure Storage, Inc. files. 
		// so querying the records 
    	
    	/*ContentDocument cd = [ SELECT Id, Title FROM ContentDocument LIMIT 1] ;
    	Content_Thumbnail__c ct = new Content_Thumbnail__c( Library__c = 'Sales',
    														Description_Rich__c = 'Sales',
    														Name = 'Sales',
    														Thumbnail_Type__c = 'Library');
    	insert ct;
    	ContentVersion cv = new ContentVersion( ContentDocumentId = cd.Id, Content_Thumbnail__c = ct.Id, 
    											CurrencyIsoCode = 'USD', Description = 'test', Featured_Asset__c = false,
    											TagCsv = 'Battlecard,Sell Sheets', ReasonForChange = 'test',
    											VersionData = Blob.valueof('tests'), Title = 'Sell Sheets', 
    											Origin = 'C', PathOnClient = 'Sell Sheet - Databases.pdf' );
  		insert cv;*/
  		Test.startTest();
  			LibrariesLandingPageController cont = new LibrariesLandingPageController();
  			System.assertEquals( 0, cont.pageNumbersList.size() );
  		Test.stopTest();
    }
    
    static testMethod void testLibrariesLandingPageControllerWithSearchText() {
    	
    	/*ContentDocument cd = [ SELECT Id, Title FROM ContentDocument LIMIT 1] ;
    	
    	Content_Thumbnail__c ct = new Content_Thumbnail__c( Library__c = 'Sales',
    														Description_Rich__c = 'Sales',
    														Name = 'Sales',
    														Thumbnail_Type__c = 'Library');
    	insert ct;
    	ContentVersion cv = new ContentVersion( ContentDocumentId = cd.Id, Content_Thumbnail__c = ct.Id, 
    											CurrencyIsoCode = 'USD', Description = 'test', Featured_Asset__c = false,
    											TagCsv = 'Battlecard,Sell Sheets', ReasonForChange = 'test',
    											VersionData = Blob.valueof('tests'), Title = 'Sell Sheets', 
    											Origin = 'C', PathOnClient = 'Sell Sheet - Databases.pdf' );
  		insert cv;*/
  		Test.startTest();
			PageReference pageRef = Page.Libraries_Landing_Page;
			pageRef.getParameters().put('SearchStr', 'te');
			Test.setCurrentPage( pageRef );
  			LibrariesLandingPageController cont = new LibrariesLandingPageController();
  			cont.searchResources();
  			System.assertEquals( 0, cont.pageNumbersList.size() );
  			cont.previous();
  			cont.next();
  		Test.stopTest();
    }
    static testMethod void testLibrariesLandingPageControllerWithLibrary() {
    	
    	/*ContentDocument cd = [ SELECT Id, Title FROM ContentDocument LIMIT 1] ;
    	
    	Content_Thumbnail__c ct = new Content_Thumbnail__c( Library__c = 'Sales',
    														Description_Rich__c = 'Sales',
    														Name = 'Sales',
    														Thumbnail_Type__c = 'Library');
    	insert ct;
    	ContentVersion cv = new ContentVersion( ContentDocumentId = cd.Id, Content_Thumbnail__c = ct.Id, 
    											CurrencyIsoCode = 'USD', Description = 'test', Featured_Asset__c = false,
    											TagCsv = 'Battlecard,Sell Sheets', ReasonForChange = 'test',
    											VersionData = Blob.valueof('tests'), Title = 'Sell Sheets', 
    											Origin = 'C', PathOnClient = 'Sell Sheet - Databases.pdf' );
  		insert cv;*/
  		Test.startTest();
			PageReference pageRef = Page.Libraries_Landing_Page;
			pageRef.getParameters().put('Lib', 'Sales');
			Test.setCurrentPage( pageRef );
  			LibrariesLandingPageController cont = new LibrariesLandingPageController();
  			cont.getLibraryResources();
  			cont.toggleSort();
  			System.assert( cont.pageNumbersList.size() >= 1 );
  		Test.stopTest();
    }
}