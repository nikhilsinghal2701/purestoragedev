/* Test Class for SC_CCContactsOnAllCaseTrigger
 * to test the functionality of adding the contact to the case team when a 
 * contact is marked to be "CC on All Emails"
 */

@isTest
private class SC_CCContactsOnAllCasesTriggerTest
{
    // test the functionality of adding contact to the case team
    private static testMethod void markContactToBeCCed()
    {
        // test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        // run as a TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);
        Contact con2 = SC_TestUtil.createContact(true, acc);
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
            Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

            Test.startTest();

            // update the CC checkbox on Contact
            con2.CC_On_All_Cases__c = true;
            update con2;

            Test.stopTest();

            // assert that the contact has been added as case team member
            system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCContactRole
                                                                            and MemberId = :con2.ID and ParentId = :c.Id]);
        }
    }

    // test the functionality of removing the contact from the case team
    private static testMethod void removeContactFromCCed()
    {
        Profile p = SC_TestUtil.getProfile('System Administrator');
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');

        
            Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

            Test.startTest();

            // insert the contact by checking the CC Email checkbox
            Contact con2 = SC_TestUtil.createContact(false, acc);
            con2.CC_On_All_Cases__c = true;
            insert con2;

            // now the contact should have been added as a case team member
            system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCContactRole
                                                                            and MemberId = :con2.ID and ParentId = :c.Id]);
            // now uncheck the flag
            con2.CC_On_All_Cases__c = false;
            update con2;

            Test.stopTest();
            
            // assert that the contact is removed from the case team
            system.assertEquals(0, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCContactRole
                                                                            and MemberId = :con2.ID and ParentId = :c.Id]);
        }
    }
}