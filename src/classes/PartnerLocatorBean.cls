/*****************************************************************************
 * Description : A custom bean for display of Partner Locator information on the front-end User Interface
 *
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/29/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorBean implements Comparable {

	private final static String PARTNER_TIER_PLATINUM = 'Platinum';
	private final static String PARTNER_TIER_GOLD = 'Gold';
	private final static String PARTNER_TIER_SILVER = 'Silver'; 
	private final static Set<String> PARTNER_TIERS = new Set<String>{PARTNER_TIER_PLATINUM, PARTNER_TIER_GOLD, PARTNER_TIER_SILVER};

	public String name { get; set; }
	public String applicationExpertise { get; set; }
	public String companyLogo { get; set; }
	public String contactInformationEmail { get; set; }
	public String contactInformationPhone { get; set; }
	public String contactInformationWebsiteURL { get; set; }
	public String description { get; set; }
	public String industryFocus { get; set; }
	public String partnerName { get; set; }
	public String partnerTier { get; set; }
	public String platforms { get; set; }
	public String primaryLocationCity { get; set; }
	public String primaryLocationCountry { get; set; }
	public String primaryLocationStateProvince { get; set; }
	public String primaryLocationStreet { get; set; }
	public String primaryLocationZipPostalCode { get; set; }
	public String targetMarketSize { get; set; }

	public 	PartnerLocatorBean (
								String name
								, String applicationExpertise
								, String companyLogo
								, String contactInformationEmail
								, String contactInformationPhone
								, String contactInformationWebsiteURL
								, String description
								, String industryFocus
								, String partnerName
								, String partnerTier
								, String platforms
								, String primaryLocationCity
								, String primaryLocationCountry
								, String primaryLocationStateProvince
								, String primaryLocationStreet
								, String primaryLocationZipPostalCode
								, String targetMarketSize
						) {
		this.name = name;
		this.applicationExpertise = applicationExpertise;
		this.companyLogo = companyLogo;
		this.contactInformationEmail = contactInformationEmail;
		this.contactInformationPhone = contactInformationPhone;
		this.contactInformationWebsiteURL = contactInformationWebsiteURL;
		this.description = description;
		this.industryFocus = industryFocus;
		this.partnerName = partnerName;
		this.partnerTier = partnerTier;
		this.platforms = platforms;
		this.primaryLocationCity = primaryLocationCity;
		this.primaryLocationCountry = primaryLocationCountry;
		this.primaryLocationStateProvince = primaryLocationStateProvince;
		this.primaryLocationStreet = primaryLocationStreet;
		this.primaryLocationZipPostalCode = primaryLocationZipPostalCode;
		this.targetMarketSize = targetMarketSize;	
	}

	public Integer compareTo(Object compareTo) {
		if (!(compareTo instanceof PartnerLocatorBean)){
			return -1;
		}
		PartnerLocatorBean p = (PartnerLocatorBean)compareTo;
		if (PARTNER_TIERS.contains(this.partnerTier) && !PARTNER_TIERS.contains(p.partnerTier)){
			return -1;
		}
		if (!PARTNER_TIERS.contains(this.partnerTier) && PARTNER_TIERS.contains(p.partnerTier)){
			return 1;
		}
		if (!PARTNER_TIERS.contains(this.partnerTier) && !PARTNER_TIERS.contains(p.partnerTier)){
			return (this.partnerName.compareTo(p.partnerName));
		}
		return (this.partnerTier.equals(p.partnerTier))
					?	(this.partnerName.compareTo(p.partnerName))
					:	(	(this.partnerTier.equals(PARTNER_TIER_PLATINUM))					// when this is platinum
								?	-1
								:	(	(this.partnerTier.equals(PARTNER_TIER_SILVER))			// when this is silver
										?	1
										:	(	(p.partnerTier.equals(PARTNER_TIER_PLATINUM))	// when this is gold and compareTo is platinum 
												?	1
												:	-1											// when this is gold and compareTo is silver
											)
									)
						);
	}

	public String getPrimaryLocation() {
		String line1 = (!String.isBlank(primaryLocationStreet))
						? primaryLocationStreet
						: '';
		String line2 = (!String.isBlank(primaryLocationCity))
						? ( primaryLocationCity + ( (!String.isBlank(primaryLocationStateProvince))
													? ( (', ' + primaryLocationStateProvince) + ((!String.isBlank(primaryLocationZipPostalCode))
																								? (' ' + primaryLocationZipPostalCode)
																								: '') )
													: (	(!String.isBlank(primaryLocationZipPostalCode))
														? (', ' + primaryLocationZipPostalCode)
														: ''
													)
												)
						)
						: ( (!String.isBlank(primaryLocationStateProvince))
													? ( primaryLocationStateProvince + ((!String.isBlank(primaryLocationZipPostalCode))
																								? (' ' + primaryLocationZipPostalCode)
																								: '') )
													: (	(!String.isBlank(primaryLocationZipPostalCode))
														? (' ' + primaryLocationZipPostalCode)
														: ''
													)
						);
		String line3 = (!String.isBlank(primaryLocationCountry))
						? primaryLocationCountry
						: '';
		return (
					(!String.isBlank(line1))
						? ( line1 + ( (!String.isBlank(line2))
									? ( ('<br/>' + line2) + ( (!String.isBlank(line3))
															? ('<br/>' + line3)
															: '' )
										)
									: (	(!String.isBlank(line3))
											? ('<br/>' + line3)
											: ''
										)
								)
							) 
						: ( (!String.isBlank(line2))
									? ( line2 + ( (!String.isBlank(line3))
															? ('<br/>' + line3)
															: '' )
										)
									: (	(!String.isBlank(line3))
											? (line3)
											: ''
										)
								)
			);

	}

	public String getContactLocation() {
		String line1 = (!String.isBlank(contactInformationPhone))
						? contactInformationPhone
						: '';
		String line2 = (!String.isBlank(contactInformationEmail))
						? ('<a href="mailto:' + contactInformationEmail + '" target="_top">' + contactInformationEmail + '</a>')
						: '';
		String line3 = (!String.isBlank(contactInformationWebsiteURL))
						? ('<a href="' 
								+ ( contactInformationWebsiteURL.startsWith('http') 
										? contactInformationWebsiteURL
										: ('http://' + contactInformationWebsiteURL)
								 )
								+ '" target="_blank">' + 'Partner Website' + '</a>')
						: '';
		return (
					(!String.isBlank(line1))
						? ( line1 + ( (!String.isBlank(line2))
									? ( ('\n' + line2) + ( (!String.isBlank(line3))
															? ('\n' + line3)
															: '' )
										)
									: (	(!String.isBlank(line3))
											? ('\n' + line3)
											: ''
										)
								)
							) 
						: ( (!String.isBlank(line2))
									? ( line2 + ( (!String.isBlank(line3))
															? ('\n' + line3)
															: '' )
										)
									: (	(!String.isBlank(line3))
											? (line3)
											: ''
										)
								)
			);

	}

	public String getPartnerLogo() {
		String regExPattern ='(width|height)="[0-9]*"'; 
		String resizeStr = ' width="' + System.Label.PARTNER_LOCATOR_COMPANY_LOGO_WIDTH + '"'
						+ ' height="' + System.Label.PARTNER_LOCATOR_COMPANY_LOGO_HEIGHT + '"' + ' >';
		String imgStr = (!String.isBlank(this.companyLogo)) ? this.companyLogo : '';
		imgStr = imgStr.replaceAll(regExPattern, '');
		imgStr = imgStr.replace('>', resizeStr);
		return imgStr;		
	}	

}