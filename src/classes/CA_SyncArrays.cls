/***********************************************
 * Description : Apex REST service for Cloud Assist Array Integration. This class can receive bulk data from Cloud Assist 
                 and upserts them as the standard REST API URLs won't support bulk
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

@RestResource(urlMapping='/SyncArrays/*')
global class CA_SyncArrays
{
    // get the JSON request, deserialize them into list of assets, splits the parent assets and child assets
    // upsert all the parent assets first and child next
    @HttpPost
    global static List<SyncArraysResponse> UpsertArrays()
    {
        RestRequest req = RestContext.request;
        
        // de-serialize incoming JSON from Cloud Assist into List of Assets
        List<Asset> incomingAssets = (List<Asset>) JSON.deserialize(req.requestBody.toString(), List<Asset>.Class);
        List<Asset> parentAssets = new List<Asset>();
        List<Asset> childAssets = new List<Asset>();
        
        List<Database.UpsertResult> parentResults;
        List<Database.UpsertResult> childResults;
        List<SyncArraysResponse> response = new List<SyncArraysResponse>();
        
        system.debug('-----> ' + req.requestBody.toString());
        system.debug('-----> ' + incomingAssets);
        
        // splits out the parent and child assets
        if(incomingAssets.size() > 0)
        {
            for(Asset a : incomingAssets)
            {
                if(a.Parent_Asset__r != null && a.Serial_Number__c != null)
                {
                    childAssets.add(a);
                }
                else
                {
                    parentAssets.add(a);
                }
            }
        }
        
        // upserts parent assets
        if(parentAssets.size() > 0)
        {
            parentResults = Database.upsert(parentAssets, Asset.Fields.Array_ID__c, false);
            
            for(Integer i=0; i < parentResults.size(); i++)
            {
                response.add(new SyncArraysResponse(parentResults[i].isSuccess(), parentResults[i].getErrors(), 
                                                    true, 
                                                    parentAssets[i].Array_ID__c, ''));
            }
        }
        
        // upserts child assets
        if(childAssets.size() > 0)
        {
            childResults = Database.upsert(childAssets, Asset.Fields.Serial_Number__c, false);
            
            for(Integer i=0; i < childResults.size(); i++)
            {
                response.add(new SyncArraysResponse(childResults[i].isSuccess(), childResults[i].getErrors(), 
                                                    false, 
                                                    '', childAssets[i].Serial_Number__c));
            }
        }
        
        return response;
    }
    
    // Response Wrapper
    global class SyncArraysResponse
    {
        global Boolean isSuccess;
        global string errorMsg;
        global string errorCode;
        global Boolean isParentArray;
        global string arrayID;
        global string serialNumber;
        
        global syncArraysResponse(Boolean flag, List<Database.Error> error, Boolean isParent, string aID, string sNo)
        {
            isSuccess = flag;
            if(error != null && error.size() > 0)
            {
                errorMsg = error[0].getMessage();
                errorCode = string.valueOf(error[0].getStatusCode());
            }
            
            isParentArray = isParent;
            arrayID = aID;
            serialNumber = sNo;
        }
    }
}