global class VLStatusBatchCampaign implements Database.Batchable<Campaign>, Database.AllowsCallouts{

Campaign [] AssetsToUpdate = [select Id,SurveyID__c, Survey_Name__c from Campaign where Survey_Status__c =: 'Approved'];

global Iterable<Campaign> start(database.batchablecontext BC){
    return (AssetsToUpdate);    
}

global void execute(Database.BatchableContext BC, List<Campaign> scope){
    for(Campaign a : scope){
            VLCampaignNomination.campaignNomination(a.Id,a.SurveyID__c,a.Survey_Name__c);
    }    
}

global void finish(Database.BatchableContext info){
    }//global void finish loop

}