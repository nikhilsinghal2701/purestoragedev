public class ZenComment{

    public String id {get;set;}
    public String author {get;set;}
    public String created_at;
    public String value;
    public String html_body {get;set;}
    public String isPublic {get;set;}
    public String type {get;set;}
    
  
    public String getValue(){
        return html_body;
        //return value.replace('\n','<br>');
    }
    
    public Datetime getCreatedAt(){
        return datetime.valueOf(created_at.substring(0,10) + ' ' + created_at.substring(11,18));   
    }
}