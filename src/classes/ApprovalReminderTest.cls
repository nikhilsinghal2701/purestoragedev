@isTest
private class ApprovalReminderTest {

    public static Account generateAccount()
    {
        Account retVal = new Account(Name = 'testAccount');
        return retVal;
    }
    
    public static Product2 generateProduct2()
    {
        Product2 retVal = new Product2(Name = 'testProduct', 
                                        Is_Hardware__c = true,
                                        Product_Cost__c = 100.00,
                                        IsActive = true);
        return retVal;
    }
    
    public static PricebookEntry generatePricebookEntry(id inputProduct2Id) 
    {
        Id pbId = Test.getStandardPricebookId();
        PricebookEntry retVal = new PricebookEntry(Pricebook2Id = pbId,
                                                    IsActive = true,
                                                    Product2Id = inputProduct2Id,
                                                    UnitPrice = 100.00,
                                                    CurrencyIsoCode = 'USD');
        return retVal;
    }
    
    public static Opportunity generateOpportunity(id inputAccountId)
    {
        Opportunity retVal = new Opportunity(Name = 'Some Oppty',
                                                AccountId = inputAccountId,
                                                CloseDate = System.Today(),
                                                CurrencyIsoCode = 'USD',
                                                StageName = 'TBD');
        return retVal;
    }
    
    public static OpportunityLineItem generateOLI(id inputOpportunity, id inputPricebookEntryId)
    {
        OpportunityLineItem retVal = new OpportunityLineItem(OpportunityId = inputOpportunity,
                                                                PricebookEntryId = inputPricebookEntryId,
                                                                Quantity = 1,
                                                                UnitPrice = 100.00);
        return retVal;
    }
    
    public static Quote generateQuote(id inputOpptyId)
    {
        Quote retVal = new Quote(Name = 'Some Quote',
                                 OpportunityId = inputOpptyId,
                                 Status = 'Draft',
                                 Pricebook2Id = Test.getStandardPricebookId());
        return retVal;
    }
    
    public static QuoteLineItem generateQLI(id inputQuoteId, id inputPBEId)
    {
        QuoteLineItem retVal = new QuoteLineItem(PriceBookEntryId = inputPBEId,
                                 QuoteId = inputQuoteId,
                                 Quantity = 1,
                                 UnitPrice = 100.00);
        return retVal;
    }
    
	static testMethod void testApprovalReminderFunctionality() {
		
        
		//create the opportunity
		/*
   		Opportunity oppty = new Opportunity(Name='testOppty', StageName = 'prospecting');
       	oppty.CloseDate = Date.today();
		oppty.OwnerId = UserInfo.getUserId();	
		oppty.Amount = Decimal.valueOf(50000);		
		insert oppty;		
		//check that the opportunity was inserted	
		System.assertNotEquals(null, oppty.id);
		List<Quote>quotesList = new List<Quote>();
		
        //to avoid mxd dml error
        system.runAs( new User(Id = UserInfo.getUserId()))
        {
		//create 10 quotes for the same user
		for(Integer i = 0; i<10; i++){
	       	//create the quote records related to the opportunity
	       	Quote quote = new Quote(Name='testQuote'+i, OpportunityId = oppty.Id);
	       	quotesList.add(quote);
		}
		insert quotesList;	
		//check that the quote was inserted		
		System.assertEquals(10, quotesList.size());
		//create the pubic group
       	Group publicGroup = new Group(Name = 'testGroup');
       	insert publicGroup;
       	//check that the public group was inserted
       	System.assertNotEquals(null, publicGroup.id);
       	
       	//create test user
       	//User u = generateUser('System Administrator', 'System Administrator', 'test', 'user');
        
       	//create a group memeber with the user id created and the group
       	GroupMember member = new GroupMember(GroupId = publicGroup.Id, UserOrGroupId = UserInfo.getUserId());
       	insert member;
       	//check that the group member was created
       	System.assertNotEquals(null, publicGroup.id);
       	//insert the created public group Id to th custom setting
       	Quotes_Reminders_Config__c customSettingValue = new Quotes_Reminders_Config__c(Name = publicGroup.Id);
       	insert customSettingValue;
       	*/
        
        //create the opportunity
        //to avoid mxd dml error
        system.runAs( new User(Id = UserInfo.getUserId()))
        {
			Account acct = generateAccount();
        	insert acct;
        	
        	Product2 p = generateProduct2();
        	insert p;
        
        	PricebookEntry pbe = generatePricebookEntry(p.Id);
        	insert pbe;
        
        	Opportunity o = generateOpportunity(acct.id);
        	o.OwnerId = UserInfo.getUserId();
        	insert o;
        	//check that the opportunity was inserted	
			System.assertNotEquals(null, o.id);
            
        	List<Quote>quotesList = new List<Quote>();
            for(Integer i = 0; i<10; i++){
        		Quote q = generateQuote(o.Id);
             	quotesList.add(q);  
            }
        	insert quotesList;
        	
            list<QuoteLineItem> qliList = new list<QuoteLineItem>();
            for(Quote qList: quotesList){
        		QuoteLineItem qli = generateQLI(qList.Id,pbe.Id);
        		qli.Discount = 59;
        		qli.Discount_for_Approval__c = 59;
                qliList.add(qli);
            }
        	insert qliList;
        
            //create the pubic group
            Group publicGroup = new Group(Name = 'testGroup');
            insert publicGroup;
            //check that the public group was inserted
            System.assertNotEquals(null, publicGroup.id);
            
            //create a group memeber with the user id created and the group
            GroupMember member = new GroupMember(GroupId = publicGroup.Id, UserOrGroupId = UserInfo.getUserId());
            insert member;
            //check that the group member was created
            System.assertNotEquals(null, publicGroup.id);
            
            //these settings are not being tested but be sure they are there
            Apex_Code_Settings__c cs = Apex_Code_Settings__c.getInstance();
            cs.Approval_Reminder_Hours_Criteria__c = 1;
            cs.Approval_Reminder_Template_Name__c = 'test';
            cs.Approval_Reminder_Group_Id__c = publicGroup.Id;
            insert cs;
            
       	//submit for approval all the quotes created
       	for(Quote q : quotesList){
	       	// Create an approval request for the quote
	        Approval.ProcessSubmitRequest req1 = 
	            new Approval.ProcessSubmitRequest();
	        req1.setComments('Submitting request for approval.');
	        req1.setObjectId(q.id);
	        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
	        // Submit the approval request for the account
	        Approval.ProcessResult result = Approval.process(req1);
	        // Verify the result
	        System.assert(result.isSuccess());
	        System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
       	}  
       	
       	Test.startTest();  
       	//============================TEST SCHEDULE JOB==========================================================================
       	// Schedule the test job  
        String jobId = System.schedule('TestScheduleApprovalReminderEmails', '0 0 9 * * ?', new ScheduleApprovalReminderEmails());                 
     	// Get the information from the CronTrigger API object  
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, 
           NextFireTime
 	      FROM CronTrigger WHERE id = :jobId]; 
     	// Verify the expressions are the same  
        System.assertEquals('0 0 9 * * ?', 
           ct.CronExpression);
		// Verify the job has not run  
       	System.assertEquals(0, ct.TimesTriggered);
       	//===========================END OF SCHEDULE JOB TESTING===============================================================
       	// TEST ApprovalReminderHelper
       	ApprovalReminderHelper.executeQuotesReminders();
       	//check that there's only one user to send email; not a great way to test because cant upd approval process approvers on the fly
       	//System.assertEquals(1, ApprovalReminderHelper.wrapperMap.size());
       	//check that the user has 10 quotes for approval
       	//System.assertEquals(10, ApprovalReminderHelper.wrapperMap.get(UserInfo.getUserId()).quotes.size()); 
     Test.stopTest();    
    // CRON test method
    }
    }
	
	 /*
    * creates and returns a User
    */
    // generate active users
    /*
    public static User generateUser(String profileName, String role, String firstName, String lastName) {   
        UserRole userRole = createUserRole(role);
        Profile profile = getProfileByName(profileName);
        
        insert userRole;
        
        // ensure userRole was created properly and expected profile was fetched
        System.assertNotEquals(userRole.Id, null); 
        System.assertNotEquals(profile.Id, null);
        
        //creates a user
        User user = new User(
            Username = firstName + '@test.com.psem',
            LastName = lastName,
            Email = firstName + '@test.com',
            Alias = firstName,
            CommunityNickname = firstName, 
            IsActive = true,
            TimeZoneSidKey = 'America/Los_Angeles',
            UserRoleId = userRole.Id,
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            ProfileId = profile.id);
 
        insert user;
        System.assertNotEquals(user.Id, null);
        
        return user;
    }
    
    // get profile by name
    public static Profile getProfileByName(String profileName) {
        return [Select Id, Name From Profile Where Name= :profileName];
    }
 
     // create a User Role
    public static UserRole createUserRole(String name) {
        return new UserRole(Name = name);
    }*/
	
}