/*****************************************************************************
 * Description : A class with utility methods or operations for Partner Locator functionality
 *
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/25/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorUtility {

	public static List<PartnerLocatorBean> convertDetailsToBeans(List<Partner_Locator_Details__c> details){
		List<PartnerLocatorBean> beans = new List<PartnerLocatorBean>();
		for (Partner_Locator_Details__c detail : details){
			beans.add(convertDetailToBean(detail));
		}
		beans.sort();
		return beans;
	}

	public static PartnerLocatorBean convertDetailToBean(Partner_Locator_Details__c detail){
		return new PartnerLocatorBean(
			detail.NAME
			, convertMultiValuePicklistToCommaDelimitedString(detail.APPLICATION_EXPERTISE__C)
			, detail.COMPANY_LOGO__C
			, detail.CONTACT_INFORMATION_EMAIL__C
			, detail.CONTACT_INFORMATION_PHONE__C
			, detail.CONTACT_INFORMATION_WEBSITE_URL__C
			, detail.DESCRIPTION__C
			, convertMultiValuePicklistToCommaDelimitedString(detail.INDUSTRY_FOCUS__C)
			, detail.PARTNER_NAME__C
			, detail.PARTNER_TIER__C
			, convertMultiValuePicklistToCommaDelimitedString(detail.PLATFORMS__C)
			, detail.PRIMARY_LOCATION_CITY__C
			, detail.PRIMARY_LOCATION_COUNTRY__C
			, detail.PRIMARY_LOCATION_STATE_PROVINCE__C
			, detail.PRIMARY_LOCATION_STREET__C
			, detail.PRIMARY_LOCATION_ZIP_POSTAL_CODE__C
			, convertMultiValuePicklistToCommaDelimitedString(detail.TARGET_MARKET_SIZE__C)
		);
	}	

	public static List<SelectOption> getPickListOptions(List<Schema.PicklistEntry> pEntries, SelectOption defaultOption, Set<String> exclusions){
		List<SelectOption> options = new List<SelectOption>();
		if (defaultOption!=NULL){
			options.add(defaultOption);
		}
		for (Schema.PicklistEntry pEntry : pEntries){
			if (pEntry.isActive() && !exclusions.contains(pEntry.getValue())){
				options.add(new SelectOption(pEntry.getValue(), pEntry.getLabel()) );
			}
		}
		return options;		
	}

    public static String convertMultiValuePicklistToCommaDelimitedString(String multiValuePicklistString){
        List<String> multiValueStringList = multiValuePicklistString.split(';');
        String retVal = '';
        for (String value : multiValueStringList){
            retVal = retVal + ( String.isBlank(retVal)
                                            ? value
                                            : (', ' + value) 
                                        );
        }
        return retVal;
    }

}