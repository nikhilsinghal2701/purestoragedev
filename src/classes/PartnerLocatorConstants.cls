/*****************************************************************************
 * Description : A constant class managing static values for Partner Locator functionality
 *
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/25/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorConstants {

	public static final SelectOption DEFAULT_SELECT_OPTION = new SelectOption('', 'Please Select...');
	public static final SelectOption DEFAULT_SELECT_OPTION_COUNTRY = new SelectOption('', 'Please Select a Country...');
	public static final SelectOption DEFAULT_SELECT_OPTION_STATE = new SelectOption('', 'Please Select a State...');

	public static final String PARTNER_LOCATOR_DETAILS_SOBJECT_NAME = 'Partner_Locator_Details__c';
	public static final String PRIMARY_LOCATION_COUNTRY_SOBJECTFIELD_NAME = 'Primary_Location_Country__c';
	public static final String PRIMARY_LOCATION_STATE_SOBJECTFIELD_NAME = 'Primary_Location_State_Province__c';

	public enum PARTNER_LOCATOR_SELECT_OPTION_FIELD {COUNTRY, STATE, INDUSTRYFOCUS, APPLICATIONEXPERTISE, PARTNERTIER}

	public static final Set<String> PARTNER_TIER_EXCLUSIONS = new Set<String>{'OTA'};

}