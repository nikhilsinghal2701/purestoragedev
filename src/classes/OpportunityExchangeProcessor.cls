global Class OpportunityExchangeProcessor
{
    // This method will fetch the conversion rate and calculate the USD amount
    // and return the calculated result
    global static Decimal calculateOpportunityExchangeRate( Decimal amount, Decimal rate )
    {
        Decimal calculatedAmount = 0;
                
        if (amount != null && rate != null)
        {
            calculatedAmount = amount / rate;
        }
        
        return calculatedAmount;
    }
    
    
    // This method will locate a closest conversionRate using currency ISO code and the close date
    // and return rate in Decimal object
    global static Decimal getConversionRate( String currencyIsoCode, Date closedDate )
    {
        // Search for conversion rate that is within the closed date
        DatedConversionRate myRate = [SELECT ConversionRate FROM DatedConversionRate WHERE IsoCode = :currencyIsoCode AND StartDate <= :closedDate AND NextStartDate > :closedDate];  
        return myRate.ConversionRate;
    }
    
    
    // This method will populate a map of Dated conversion rate to minimize the SOQL query
    // and return the result map
    global static Map<String, List<DatedConversionRate>> getConversionRate()
    {
        Map<String, List<DatedConversionRate>> sortedISOMap = new Map<String, List<DatedConversionRate>>();
        
        // Loops through all of the rate information
        for (DatedConversionRate dcr : [SELECT ConversionRate, IsoCode, StartDate, NextStartDate  FROM DatedConversionRate])
        {
            if (sortedISOMap.containsKey( dcr.IsoCode ))
            {
                sortedISOMap.get( dcr.IsoCode ).add( dcr );
            }
            else
            {
                sortedISOMap.put( dcr.IsoCode, new List<DatedConversionRate>() );
                sortedISOMap.get( dcr.IsoCode ).add( dcr );
            }
        }
        
        
        return sortedISOMap;
    }
}