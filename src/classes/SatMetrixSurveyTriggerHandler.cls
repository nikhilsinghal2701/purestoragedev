/* Description : Handler for SatMetrixSurveyTrigger
                 When a case is closed, this class will create a survey record 
                 for the case contact if there was no survey created for him in the last one month.
                 That survey record will then be picked up by SatMetrix to send out the survey.
 */

public class SatMetrixSurveyTriggerHandler
{
    // On after update
    public static void onAfterUpdate(Map<Id,Case> oldMap, Map<Id,Case> newMap)
    {
        Set<Id> caseIDs = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> contactsWithSurveyThisMonth = new Set<Id>();
        
        // if the case is closed, collect the case id and contact id
        for(Case c : newMap.values())
        {
            if(c.IsClosed != oldMap.get(c.Id).IsClosed && c.IsClosed && c.ContactId != null)
            {
                 caseIds.add(c.Id);
                 contactIds.add(c.ContactID);
            }
        }
        
        // get if there are any survey created in the last one month for those contacts
        for(Feedback__c s : [Select Id, Contact__c from Feedback__c where Contact__c in :contactIds and CreatedDate = LAST_N_DAYS:30])
        {
            contactsWithSurveyThisMonth.add(s.Contact__c);
        }
        
        // if the contact was not sent a survey in last one month and if the contact's account is not marked as "Support Must Not Contact Customer" and if the
        // contact's email is not pure storage internal, create a survey record
        for(Case c : [Select Id, ContactId, Contact.Email, Contact.AccountId, Contact.Account.Support_Must_Not_Contact_End_Customer__c, CaseNumber, Priority, OwnerId, Owner.Name from Case where Id In :caseIds])
        {
            if(!contactsWithSurveyThisMonth.contains(c.ContactId) && c.ContactID != null && c.Contact.AccountId != null && !c.Contact.Account.Support_Must_Not_Contact_End_Customer__c
                && c.Contact.Email!= null && !c.Contact.Email.contains(system.label.SC_SatMetrix_PureInteralEmail1) && !c.Contact.Email.contains(system.label.SC_SatMetrix_PureInteralEmail2))
            {
                SatmetrixNominationUtil util = new SatmetrixNominationUtil();
                Feedback__c feedback = util.nominateContactsForSupportSurvey(new Contact(Id = c.ContactId), c.CaseNumber, c.priority, c.Owner.Name, null);
            }
        }
    }
}