/*
Class: CommunityLandingHomeCtrl
Purpose:Ctrl for CommunityLandingHomeCtrl
Author: Jaya
Created Date: 11/25/2013 
*/
public with sharing class PRMCommunityLandingHomeCtrl 
{
    
    public String managerName{get;set;}
    public String managerEmail{get;set;}
    public User accountManager{get;set;}
    public Account account{get;set;}
    public void init()
    {
        Id userId = System.Userinfo.getUserId();
        User userRec = [SELECT Name, IsActive, Id, Email, AccountId FROM User WHERE Id =:userId];
        account  = [SELECT Id, OwnerId, Owner_Email__c , Owner.Name FROM Account WHERE Id =: userRec.AccountId]; 
        managerName = account.Owner.Name;
        managerEmail = account.Owner_Email__c;
    }
    
    public PageReference getAccount()
    {
        PageReference pref = null;
        pref = new PageReference('/'+account.Id);
        System.debug('getAccount:'+account);
        return pref;
    } 
    public PageReference getManager()
    {
        //<a href="mailto:youremailaddress">Email Me</a>
        return new PageReference('/'+account.OwnerId);
    }

}