/***********************************************
 * Description : Controller class for SC_EmailRelatedList page and SC_EmailMessageDetail Page.
                 This class will following funtionalities: 1) Show a custom email related list for internal users with
                 "send email" "reply" "reply to all" links, 2) collect all the email addresses to be CCed, 3) Show only
                 the customer-involved email messages to the customer
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

public without sharing class SC_EmailsRelatedListController
{
    // variable declarations
    public List<EmailMessage> emails {get;set;}
    public string redirectURL {get;set;}
    private Case c;
    private EmailMessage emailMessage;
    public User u {get;set;}
    public string CUSTOMER_USERTYPE  {get;set;}
    public string PARTNER_USERTYPE {get;set;}
    public string ccEmailStr {get;set;}
    public boolean isCustomer {get;set;}
    public string replyURL {get;set;}
    public string replyToAllURL {get;set;}
    public string forwardURL {get;set;}

    // constants
    private static final string SEND_EMAIL_URL = '_ui/core/email/author/EmailAuthor?';
    private static final string RETURL = 'retURL=%2F';
    private static final string CASE_LKP_ID = system.label.SC_SendEmailCaseLookupID;
    private static final string CONTACT_LKP_ID = system.label.SC_SendEMailContactLookupID;
    private static final string CC_FIELD_ID = system.label.SC_CCFieldID;
    private static final string EMAIL_ID = 'email_id';
    private static final string REPLY_TO_ALL = 'replyToAll';
    private static final string FORWARD = 'forward';
    private static final string CC_CONTACT_ROLENAME = system.label.SC_CCContactRole;
    private static final string CC_ACCOUNT_ROLENAME = system.label.SC_CCAccountRole;
    private static final string CASE_PREFIX = '500';
    private static final string EMAILMESSAGE_PREFIX = '02s';
    private static final string FROMDEFAULT_PARAMETER = system.label.SC_SendEmailFromAddressID;
    private static final string FROMDEFAULT_EMAIL = system.label.SC_DefaultFromEmailAddress;
    private static final string VFDETAILPAGE = '/apex/SC_EmailMessageDetailPage?id=';
    
    // constructor
    public SC_EmailsRelatedListController(ApexPages.StandardController controller)
    {
        // customer and partner user types
        CUSTOMER_USERTYPE = system.label.SC_CustomerCommunityUserType;
        PARTNER_USERTYPE = system.label.SC_PartnerCommunityUserType;

        // get the user details to check if the logged in user is an internal user or a customer or a partner
        getUserDetails();

        // if the controller is executed from the SC_EmailRelatedList Page
        if(controller.getId() != null && controller.getId().startsWith(CASE_PREFIX))
        {
            c = (Case) controller.getRecord();
            queryEmailMessages();
        }

        // if the controller is executed from the SC_EmailMessageDetail Page
        else if(controller.getId() != null && controller.getId().startsWith(EMAILMESSAGE_PREFIX))
        {
            emailMessage = (EmailMessage) controller.getRecord();
            c = emailMessage.Parent;
            formRedirectURLs();
        }
    }
    
    // query the user details
    public void getUserDetails()
    {
        u = [Select Id, Profile.Name, UserType, Email from User where Id = :userinfo.getuserId()];
    }

    // query email messages for the current case
    public void queryEmailMessages()
    {
        string emailStr = '';
        emails = new List<EmailMessage>();
        
        // if the user is a customer or a partner, query only the customer-involved email messages
        if(u.UserType == CUSTOMER_USERTYPE || u.UserType == PARTNER_USERTYPE)
        {
            emailStr = '%' + u.Email + '%';
            emails = [Select Id, ParentID, Status, Subject, ToAddress, MessageDate from EmailMessage where ParentID = :c.Id
                                                        and (FromAddress like : emailStr 
                                                            or CCAddress like : emailStr
                                                            or ToAddress like : emailStr) order by MessageDate desc];
        }

        // if the user is an internal user, show all the email messages
        else
        {
            emails = [Select Id, ParentID, Status, Subject, ToAddress, MessageDate from EmailMessage where ParentID = :c.Id order by MessageDate desc];
        }
    }
    
    // get all the emails to be CCed from the following sources: 1) one of the 10 Case Collaborator email field, 
    // 2) any contact playing CC Contact role in the case team, 3) Users who are playing Sales Engineer role
    // on the Case's Account Team, 4) collect any existing cc address in the exisitng email message
    public void getListofCCEmails(string selectedEmailID)
    {
        ccEmailStr = '';
        set<string> uniqueEmails = new set<string>();
        set<Id> conIds = new set<Id>();
        
        // collect from Case Collaborators
        if(c.Case_Collaborator_1__c != null && c.Case_Collaborator_1__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_1__c);
        }
        if(c.Case_Collaborator_2__c != null && c.Case_Collaborator_2__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_2__c);
        }
        if(c.Case_Collaborator_3__c != null && c.Case_Collaborator_3__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_3__c);
        }
        if(c.Case_Collaborator_4__c != null && c.Case_Collaborator_4__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_4__c);
        }
        if(c.Case_Collaborator_5__c != null && c.Case_Collaborator_5__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_5__c);
        }
        if(c.Case_Collaborator_6__c != null && c.Case_Collaborator_6__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_6__c);
        }
        if(c.Case_Collaborator_7__c != null && c.Case_Collaborator_7__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_7__c);
        }
        if(c.Case_Collaborator_8__c != null && c.Case_Collaborator_8__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_8__c);
        }
        if(c.Case_Collaborator_9__c != null && c.Case_Collaborator_9__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_9__c);
        }
        if(c.Case_Collaborator_10__c != null && c.Case_Collaborator_10__c != '')
        {
            uniqueEmails.add(c.Case_Collaborator_10__c);
        }
        
        // collect the contacts playing CC Contact role in the case team
        for(CaseTeamMember cm : [Select Id, MemberId from CaseTeamMember where ParentID = :c.Id and TeamRole.Name = :CC_CONTACT_ROLENAME])
        {
            conIds.add(cm.MemberId);
        }
        
        for(Contact con : [Select Email from Contact where Id in :conIds and Email != null])
        {
            uniqueEmails.add(con.Email);
        }
        
        // collect users playing Sales Engineer from the account team
        for(AccountTeamMember am : [Select Id, User.Email from AccountTeamMember where AccountId = :c.AccountId and TeamMemberRole = : CC_ACCOUNT_ROLENAME])
        {
            uniqueEmails.add(am.User.Email);
        }

        // Collect any existing CC address in an email message
        if(selectedEmailID != null && selectedEmailID != '')
        {
            EmailMessage em;
            
            // if the controller is executed from SC_EmailRelatedList page
            if(emailMessage == null)
            {
                em = [Select CCAddress from EmailMessage where Id = :selectedEmailID];
            }

            // if the controller is executed from SC_EmailMessageDetail Page
            else
            {
                em = emailMessage;
            }

            if(em.CCAddress != null && em.CCAddress != '')
            {
                for(string s : em.CCAddress.split(';'))
                {
                    if(s != null && s != '')
                        uniqueEmails.add(s.trim());
                }
            }
        }

        // form the cc email string
        for(string s : uniqueEmails)
        {
            if(ccEmailStr == '')
                ccEmailStr += s;
            else
                ccEmailStr += ',' + s;
        }
    }

    // form the send email url with CC email parameter and other standard email parameters
    public void sendEmail()
    {
        redirectURL = SEND_EMAIL_URL;
        
        getListOfCCEmails(null);
        
        redirectURL += CASE_LKP_ID + '=' + string.valueOf(c.Id).substring(0,15)
                         + '&' + RETURL + c.Id;
                         
        if(c.ContactId != null)
            redirectURL += ' &' + CONTACT_LKP_ID + '=' + c.ContactId
                            + '&rtype=003';
        
        redirectURL += '&' + FROMDEFAULT_PARAMETER + '=' + FROMDEFAULT_EMAIL;

        if(ccEmailStr != null && ccEmailStr != '')
        {
            redirectURL += '&' + CC_FIELD_ID + '=' + ccEmailStr;
        }
    }

    // form the reply & reply to all url with cc email parameter and other standard email parameters
    public void reply()
    {
        string selectedEmailID = ApexPages.currentPage().getParameters().get('selectedEmailId');
        string toAll = ApexPages.currentPage().getParameters().get('toAll');

        if(selectedEmailID != null)
        {
            redirectURL = SEND_EMAIL_URL;

            redirectURL += EMAIL_ID + '=' + selectedEmailID + '&' + REPLY_TO_ALL + '=' + toAll + '&' + RETURL + c.Id;

            redirectURL += '&' + FROMDEFAULT_PARAMETER + '=' + FROMDEFAULT_EMAIL;

            if(toAll == '0')
            {
                getListOfCCEmails(null);    
            }
            else if(toAll == '1')
            {
                getListOfCCEmails(selectedEmailID);
            }
            

            if(ccEmailStr != null && ccEmailStr != '')
            {
                redirectURL += '&' + CC_FIELD_ID + '=' + ccEmailStr;
            }
        }
    }

    // delete an existing email messages
    public void deleteEmail()
    {
        string selectedEmailID = ApexPages.currentPage().getParameters().get('selectedEmailId');
        
        if(selectedEmailID != null)
        {
            EmailMessage em = [Select Id from EmailMessage where Id = :selectedEmailID];

            try
            {
                Delete em;
                queryEmailMessages();
            }
            catch(Exception e)
            {
                Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
            }
        }
    }

    // form the urls for reply, reply to all and forward buttons on the SC_EmailMessageDetail Page
    public void formRedirectURLs()
    {
        replyURL = SEND_EMAIL_URL + EMAIL_ID + '=' + emailMessage.Id + '&' + REPLY_TO_ALL + '=0&' + 'retURL' + VFDETAILPAGE + emailMessage.Id +'&' + FROMDEFAULT_PARAMETER + '=' + FROMDEFAULT_EMAIL;
        replyToAllURL = SEND_EMAIL_URL + EMAIL_ID + '=' + emailMessage.Id + '&' + REPLY_TO_ALL + '=1&' + 'retURL' + VFDETAILPAGE + emailMessage.Id +'&' + FROMDEFAULT_PARAMETER + '=' + FROMDEFAULT_EMAIL;
        forwardURL = SEND_EMAIL_URL + EMAIL_ID + '=' + emailMessage.Id + '&' + FORWARD + '=1&' + 'retURL' + VFDETAILPAGE + emailMessage.Id +'&' + FROMDEFAULT_PARAMETER + '=' + FROMDEFAULT_EMAIL;
        
        getListOfCCEmails(null);
        if(ccEmailStr != null && ccEmailStr != '')
        {
            replyURL += '&' + CC_FIELD_ID + '=' + ccEmailStr;
            forwardURL += '&' + CC_FIELD_ID + '=' + ccEmailStr;
        }

        getListOfCCEmails(emailMessage.Id);
        if(ccEmailStr != null && ccEmailStr != '')
        {
            replyToAllURL += '&' + CC_FIELD_ID + '=' + ccEmailStr;
        }
    }
}