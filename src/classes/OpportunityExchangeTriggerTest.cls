@isTest( seeAllData=true )
public class OpportunityExchangeTriggerTest
{
    private static DatedConversionRate currencyRateGBPnow;
    private static DatedConversionRate currencyRateGBPprev;
    
    static testMethod void testOpportunityExchangeTrigger()
    {
        // Set the currency rates
        setupSettings();
        
        Opportunity myTestObject = createOpportunity( 'GBP', System.today() );
        addTestProducts( myTestObject );
        
        // Refetching the data to refresh the exchange rate and amount USD
        myTestObject = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount, Converted_Amount_USD__c, Exch_Rate_USD__c FROM Opportunity WHERE id = :myTestObject.id];
        
        // Pre-calculated amount for the opportunity
        Decimal calculatedAmount = (167.12 * 2) + (788.24 * 3);
        
        test.startTest();
        // Verifying the insert trigger
        System.assertEquals( currencyRateGBPnow.ConversionRate, myTestObject.Exch_Rate_USD__c );
        System.assertEquals( calculatedAmount / currencyRateGBPnow.ConversionRate, myTestObject.Converted_Amount_USD__c );
        
        // Verifying the update trigger (Close date update)
        myTestObject.CloseDate = currencyRateGBPprev.NextStartDate.addDays( -1 );
        update myTestObject;
        
        // Refetching the data to refresh the exchange rate and amount USD
        Opportunity myTestObject2 = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount, Converted_Amount_USD__c, Exch_Rate_USD__c FROM Opportunity WHERE id = :myTestObject.id];

        System.assertEquals( currencyRateGBPprev.ConversionRate, myTestObject2.Exch_Rate_USD__c );
        System.assertEquals( calculatedAmount / currencyRateGBPprev.ConversionRate, myTestObject2.Converted_Amount_USD__c );
        
        // Updating the Opportunity Line item quantities to change the amount
        OpportunityLineItem oli = [SELECT Quantity FROM OpportunityLineItem WHERE OpportunityId = :myTestObject.id AND Quantity = 2];
        oli.Quantity = 3;
        update oli;
        
        // Verifying the update trigger (Amount update)
        Decimal calculatedAmount2 = (167.12 * 3) + (788.24 * 3);
        myTestObject2.Amount = calculatedAmount2;
        update myTestObject2;
        
        // Refetching the data to refresh the exchange rate and amount USD
        Opportunity myTestObject3 = [SELECT AccountId, Name, StageName, CurrencyIsoCode, CloseDate, amount, Converted_Amount_USD__c, Exch_Rate_USD__c FROM Opportunity WHERE id = :myTestObject.id];
        
        System.debug( myTestObject );

        System.debug( calculatedAmount2 );
        System.debug( calculatedAmount );
        
        System.assertEquals( currencyRateGBPprev.ConversionRate, myTestObject3.Exch_Rate_USD__c );
        System.assertEquals( myTestObject3.Amount / currencyRateGBPprev.ConversionRate, myTestObject3.Converted_Amount_USD__c );
        test.stopTest();
    }
    
   
    public static void setupSettings()
    {
        currencyRateGBPnow = [SELECT ConversionRate, StartDate, NextStartDate FROM DatedConversionRate WHERE IsoCode = 'GBP' ORDER BY NextStartDate desc limit 1];
        currencyRateGBPprev = [SELECT ConversionRate, StartDate, NextStartDate FROM DatedConversionRate WHERE IsoCode = 'GBP' ORDER BY NextStartDate asc limit 1];
    }
   
    
    public static Account createAccount()
    {
        Account myAccount = new Account( Name = 'Testing Account', Support_Must_Not_Contact_End_Customer__c = true );
        insert myAccount;
        
        return myAccount;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate,
                                             amount = 2698.96,
                                             Converted_Amount_USD__c = 0,
                                             Exch_Rate_USD__c = 0 );
        
        insert myOpp;
        return myOpp;
    }
    
    
    public static void addTestProducts( Opportunity myOpp )
    {
        PricebookEntry pbe1 = createPriceBookEntry( 'Product 1', 167.12, myOpp.CurrencyIsoCode );
        PricebookEntry pbe2 = createPriceBookEntry( 'Product 2', 788.24, myOpp.CurrencyIsoCode );
        
        OpportunityLineItem oli1 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe1.Id, Quantity = 2, UnitPrice = 167.12 );
        OpportunityLineItem oli2 = new OpportunityLineItem( OpportunityId = myOpp.id, PricebookEntryId = pbe2.Id, Quantity = 3, UnitPrice = 788.24 );
        
        insert oli1;
        insert oli2;
    }
    
    
    public static PricebookEntry createPriceBookEntry( String productName, Decimal price, String currencyCode )
    {
        Product2 myProduct = new Product2(Name = productName);
        insert myProduct;

        PricebookEntry pbe = new PricebookEntry( isActive = true, Product2Id = myProduct.Id, Pricebook2Id = getStandardPriceBookID(), CurrencyIsoCode = currencyCode, UnitPrice = price );
        insert pbe;
        
        return pbe;
    }
    
    
    public static ID getStandardPriceBookID()
    {
        return [select Id from Pricebook2 where isActive = true and isStandard = true limit 1].ID;
    }
}