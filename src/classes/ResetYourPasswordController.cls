public class ResetYourPasswordController {
	public String newPassword { get; set;}
	public String oldPassword { get; set;}
	public String verifyNewPassword { get; set;}
	public Boolean passwordChanged {get;set;}

	public ResetYourPasswordController(){
		passwordChanged = false;
	}
	
	public PageReference resetPassword(){
		if( oldPassword == newPassword ){
			ApexPages.addMessage( new Apexpages.Message( Apexpages.Severity.ERROR, 'You cannot reuse your old Password' ) );
		}
		else if( verifyNewPassword != newPassword ){
			ApexPages.addMessage( new Apexpages.Message( Apexpages.Severity.ERROR, 'New password and verify New passwords should match' ) );
		}else{
			if( Site.changePassword(newPassword, verifyNewPassword, oldpassword) != null ){
				passwordChanged=true;				
			}
		}
		return null;
	}
	
}