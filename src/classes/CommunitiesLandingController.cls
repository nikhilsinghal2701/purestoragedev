/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        
        if(isCustomerCommunity())
        {
            System.debug('CommunitiesLandingController ... calling');
            
            //return new pageReference(system.label.SC_Lithium_SSO_URL);
            
            if(isGuestUser())
            {
                return new PageReference('/customers/SC_CustomerLoginPage');
            }
            
            ///* commented by Sorna: All the users will be redirected to Lithium now
            // if the current community is customer community, and if the logged in user's profile is
            // PS Lithium User Profile, redirect them to Lithium SSO URL;
            if(!isSVAR())
            {
                system.debug('-----> REDIRECT TO LITHIUM COMMUITY <-----');
                return new pageReference(system.label.SC_Lithium_SSO_URL);
            }
            
            // if the current community is customer community, and if the logged in user's profile is not 
            // PS Lithium User Profile, redirect them to the user's Account's detail page
            else
            {
                List<User> users = [Select Id, contactId, contact.AccountId from User where contactId != null
                                                                                       AND Contact.AccountId !=null
                                                                                       AND Id =: userinfo.getUserId() limit 1];
                System.debug('log portal user is  ... ' + users);
                if(users != null && !users.isEmpty()) {
                    return new pageReference('/customers/' + users[0].contact.AccountId);
                }
                
            }
            //*/
        }
        
        return Network.communitiesLanding();
    }
    
    // checks if the current community name is Customer Support Community
    public boolean isCustomerCommunity()
    {
        if(network.getNetworkId() != null)
        {
            string communityName = [Select Name from Network where ID = :network.getNetworkId()].Name;
            
            if(communityName == system.label.SC_Customer_Community_Name)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    // returns true if the logged in user's profile is PS Lithium User Profile
    public boolean isSVAR()
    {
        User u = [Select Lithium_Role__c from User where Id = :userInfo.getUserId()];
        
        if(u.Lithium_Role__c == system.label.SC_LithiumRole_SVAR)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public boolean isGuestUser()
    {
        Profile p = [Select Name from Profile where Id = :userInfo.getProfileId()];
        
        if(p.Name == 'Customer Support Community Profile')
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public CommunitiesLandingController() {}
}