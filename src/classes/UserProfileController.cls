public without sharing class UserProfileController {

    public String userProfileKey {get; set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public UserProfileController(ApexPages.StandardController stdController) {
    }

    public PageReference getUserLicense() {
        User user = [Select Id, Profile.UserLicense.LicenseDefinitionKey From User where Id =: UserInfo.getUserId() limit 1];
        userProfileKey = user.Profile.UserLicense.LicenseDefinitionKey;
        return null;
    }


}