/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestLeadTriggerHelper {

    static testMethod void myUnitTest() 
    {
        //
        TestDataFactory dataCreation = new TestDataFactory();
        Profile adminProfile = dataCreation.getProfile( 'System Administrator' );
        User adminUser = dataCreation.getUser( adminProfile, null );
        UserRole role = [SELECT Id FROM UserRole WHERE Name LIKE 'Executive Management' LIMIT 1];
        adminUser.UserRoleId = role.Id;
        adminUser.Username = 'test' + Math.random() + '@test123.com';
        User user ;
        system.runAs( adminUser ){
            Profile profileObj = dataCreation.getProfile('PRM Global Partner Profile');
            Account acct = dataCreation.getAccount();
            insert acct;
            Contact contact = dataCreation.getContact(acct);
            insert contact;
            user = dataCreation.getUser(profileObj, contact);
            // added .p3 to satisfy the validation rule on User You_must_append_username_with_p3
            user.userName = user.userName + '.p3';
        }
        
        System.runAs(user)
        {
            Lead lead = dataCreation.getLead();
            Test.startTest();
                insert lead;
            Test.stopTest();
        }
         
       
    }
}