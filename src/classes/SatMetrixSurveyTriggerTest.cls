@isTest
private class SatMetrixSurveyTriggerTest
{
    
    private static testMethod void testSurveyCreation()
    {
        Profile p = SC_TestUtil.getProfile('System Administrator');
        system.runAs(SC_TestUtil.createUser(p))
        {
            Account acc = SC_TestUtil.createAccount(true);
            Contact con1 = SC_TestUtil.createContact(true, acc);
    
            Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                        'Installed', 
                                                        'Controller 24/7 4HR 1 Year Domestic Support');

            Case c1 = SC_TestUtil.createCase(false, acc, con1, parentArray);
            Case c2 = SC_TestUtil.createCase(false, acc, con1, parentArray);
            List<Case> cases = new List<Case>();
            cases.add(c1);
            cases.add(c2);
            insert cases;
            Test.startTest();

            c1.Status = system.label.SC_CaseClosedStatusValue;
            c1.Disposition__c = 'Workaround';
            update c1;

            //system.assertEquals(1, [Select count() from Feedback__c where Contact__c = :con1.Id], 'One Survey should be created for the Case Contact after Case is closed');

            c2.Status = system.label.SC_CaseClosedStatusValue;
            c2.Disposition__c = 'Workaround';
            update c2;

            Test.stopTest();

            //system.assertEquals(1, [Select count() from Feedback__c where Contact__c = :con1.Id], 'Only one Survey should be created for a Contact in a month');
        }
    }
}