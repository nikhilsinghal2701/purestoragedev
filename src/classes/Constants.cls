/*
Class: Constants
Author: Jaya
Purpose: to avoid duble submission to approval process.
*/
public with sharing class Constants
{
	public static final String OPPORTUNITY_EXTENSION_STATUS_ELIGIBLE = 'Eligible';
	public static final String OPPORTUNITY_EXTENSION_STATUS_NOT_ELIGIBLE = 'Not Eligible';
	public static final String OPPORTUNITY_EXTENSION_STATUS_PENDING = 'Pending Approval';
	public static final String OPPORTUNITY_EXTENSION_STATUS_APPROVED = 'Approved';
	public static final String OPPORTUNITY_EXTENSION_STATUS_REJECTED = 'Rejected';
	public static final String LEAD_GLOBAL_PARTNER_DEAL = 'Global Partner Deal Registration';
	public static final String PRM_GLOBAL_PARTNER_PROFILE = 'PRM Global Partner Profile';
    Private static boolean approvalFlag = false;
    public static boolean hasalreadyapproval() {
        return approvalFlag;
    }
    public static void setalreadyapproval() {
        approvalFlag = true;
    }
}