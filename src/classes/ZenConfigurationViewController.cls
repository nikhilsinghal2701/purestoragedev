public class ZenConfigurationViewController {

    public ZenConfiguration__c configuration {set; get;}

    public ZenConfigurationViewController(){
        this.configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);    
    } 
    
    public PageReference save() {
        update this.configuration;
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Saved Successfully.');
        ApexPages.addMessage(myMsg);
        return null;
    }
}