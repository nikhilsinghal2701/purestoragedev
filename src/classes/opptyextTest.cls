@isTest
public class opptyextTest
{

    static testMethod void testCustomerReferencePage()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Account_Upsert_Address_Enabled__c = true;
        insert cs;
        
        // Create a test opportunity with exiting action item
        Opportunity testOpp = createOpportunity( 'USD', System.today() );
        
        test.startTest();
        // instantiate the controller and assert that the defaults set
        ApexPages.StandardController controller = new ApexPages.StandardController(testOpp);
        opptyext oController = new opptyext(controller);
        oController.getAddrs();
        oController.closePopup();
        oController.cancelAddr();
        
        oController.selectedId = '';
        oController.shippingAccountId = testOpp.AccountId;
        oController.shippingCity = 'Washington';
        oController.shippingState = 'DC';
        oController.shippingCountry = 'United States';
        oController.shippingPostalCode = '20001';
        oController.shippingContact = 'Jane';
        oController.shippingContactPhone = '202-867-5309';
        oController.shippingStreet = '123 Constitution Ave';
        oController.saveAddr();
        oController.clearAddr();
        oController.needsShippingStage(testOpp.StageName);
        
            
        test.stopTest();
        
        list<Address__c> addr = [select Id from Address__c where Account__c =: testOpp.AccountId];
        system.assertEquals(2, addr.size());
    
    }
    
    public static Account createAccount()
    {
        Account a = new Account(Name = 'Some Account',
                               	ShippingStreet = '123 Main St',
                                ShippingCity = 'Washington',
                               	ShippingState = 'DC',
                               	ShippingPostalCode = '20001',
                                ShippingCountry = 'United States',
                               	Ship_To_Contact_Name__c = 'Joe',
                               	Ship_To_Contact_Phone__c = '867-5309');
        insert a;
        
        return a;
    }
    
    
    public static Opportunity createOpportunity( String currencyCode, Date closeDate )
    {
        Opportunity myOpp = new Opportunity( AccountId = createAccount().Id, 
                                             Name = 'Some Test',
                                             StageName = 'Stage 1 - Prequalified',
                                             CurrencyIsoCode = currencyCode,
                                             CloseDate = closeDate);
        
        insert myOpp;
        return myOpp;
    }
}