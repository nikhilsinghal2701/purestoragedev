/***********************************************
 * Description : Controller class for SC_CaseDocuments. This class retrieves the Attachments from the Current Case
                 and the case's account. It also uploads case's attachments to case's account if chosen by user.
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/
public with sharing class SC_CaseDocumentsController
{
    // variable declarations
    public List<AttachmentWrapper> enterpriseDocs {get;set;}
    public List<AttachmentWrapper> caseDocs {get;set;}
    
    public string selectedOption {get;set;}
    public list<SelectOption> options {get;set;}

    public list<SC_Enterprise_Document_Fields__c> attachmentFields {get;set;}
    
    // current case record
    private Case c;

    // querystr - will be populated by values from custom setting
    private string queryStr;

    // constants
    private static final string SELECTOPTION_ICONS = 'Icons';
    private static final string SELECTOPTION_LISTVIEW = 'List View';
    private static final string ENTERPRISEDOC_PREFIX = system.label.SC_EnterpriseDocumentPrefix;
    private static final string IMAGECONTENT_IDENTIFIER = 'image';
    private static final string DISPLAY_BLOCK = 'display: block;';
    private static final string DISPLAY_NONE = 'display: none;';
    
    // constructor
    public SC_CaseDocumentsController(ApexPages.StandardController controller)
    {
        if(Apexpages.currentPage().getParameters().get('id') != null)
        {
            c = (Case) controller.getRecord();

            // set icons as default option
            selectedOption = SELECTOPTION_ICONS;

            // select options for radio buttons
            options = new list<SelectOption>();
            options.add(new SelectOption(SELECTOPTION_ICONS,SELECTOPTION_ICONS));
            options.add(new SelectOption(SELECTOPTION_LISTVIEW,SELECTOPTION_LISTVIEW));

            // query custom setting to form the query str
            retrieveAttachmentFields();

            // call init() to query the account's and case's attachments
            init();
        }
        else
        {
            Apexpages.addMessage(new ApexPages.Message(APEXPAGES.SEVERITY.INFO, system.label.SC_NoDocuments));
        }
    }
    
    // queries the attachments from case and case's account
    public void init()
    {
        enterpriseDocs = new List<AttachmentWrapper>();
        caseDocs = new List<AttachmentWrapper>();

        if(c.AccountID != null)
        {
            // query attachment from case's account
            for(Attachment a : Database.query(queryStr + 'where ParentId = \'' + c.AccountId + '\' and Name like \'' + ENTERPRISEDOC_PREFIX + '%' + '\' order by CreatedDate desc'))
            {
                enterpriseDocs.add(new AttachmentWrapper(a, false));
            }
        }

        // query attachment from case
        for(Attachment a : Database.query(queryStr + 'where ParentId = \'' + c.Id + '\' order by CreatedDate desc'))
        {
            caseDocs.add(new AttachmentWrapper(a, false));
        }
        
        // if there are no attachments, show message to user
        if(enterpriseDocs.size() == 0 && caseDocs.size() == 0)
        {
            Apexpages.addMessage(new ApexPages.Message(APEXPAGES.SEVERITY.INFO, system.label.SC_NoDocuments));
        }
    }
    
    // finds the case attachments chosen by the user and uploads them to case's account
    public void uploadDocsToEnterprise()
    {
        List<Attachment> attachments = new List<Attachment>();
        Attachment doc;
        set<Id> docIds = new set<Id>();
        
        try
        {
            // collects the case attachments chosen by user
            for(AttachmentWrapper a : caseDocs)
            {
                if(a.IsChecked)
                {
                    docIds.add(a.doc.Id);
                }
            }
            
            // queries the necessary fields of the attachments chosen by user
            for(Attachment a : [Select Id, ParentID, Name, Body, Description, IsPrivate, ContentType from Attachment where Id in :docIds])
            {
                doc = a.clone(false, true, false, false);
                doc.ParentID = c.AccountID;
                doc.Name = ENTERPRISEDOC_PREFIX + ' ' + a.Name;
                attachments.add(doc);
            }
            
            // uploads attachments to case's account
            if(attachments.size() > 0)
            {
                insert attachments;
                init();
            }
        }
        catch(Exception e)
        {
            Apexpages.addMessage(new ApexPages.Message(APEXPAGES.SEVERITY.ERROR, e.getMessage()));
        }
    }
    
    // allows user to cancel their selection
    public void cancelSelection()
    {
        for(AttachmentWrapper a : caseDocs)
        {
            if(a.IsChecked)
            {
                a.IsChecked = false;
            }
        }
    }
    
    // retrieves rows from SC_Enterprise_Document_Fields__c custom setting and forms the query str
    public void retrieveAttachmentFields()
    {
        attachmentFields = new List<SC_Enterprise_Document_Fields__c>();
        queryStr = 'Select Id, ContentType';

        for(SC_Enterprise_Document_Fields__c  f : [Select Name, Label__c, Order__c, Type__c from SC_Enterprise_Document_Fields__c order by Order__c asc])
        {
            attachmentFields.add(f);
            queryStr += ', ' + f.Name;
        }

        queryStr += ' from Attachment ';
    }

// wrapper class to show the attachments in the VF page
public class AttachmentWrapper
    {
        public Attachment doc {get;set;} // the attachment
        public boolean isChecked {get;set;} // checkbox to allow users to select the attachments they want to upload to account
        public string name {get;set;} // holds the name of the account attachment without "(Enterprise)" prefix
        public integer size {get;set;} // size of the attachment in kb
        public string imgStyle {get;set;} // style to either show or hide the default image vs actual image
        public string applicationStyle {get;set;} // style to either show or hide the default image vs actual image
        
        public AttachmentWrapper(Attachment a, Boolean flag)
        {
            doc = a;
            isChecked = flag;
            
            // removes the "(Enterprise)" prefix from the account attachment name
            if(a.Name.contains(ENTERPRISEDOC_PREFIX))
            {
                name = a.Name.substring(a.Name.indexOf(ENTERPRISEDOC_PREFIX) + 12);
            }
            
            // converts the size (in bytes) to kilo bytes
            if(a.BodyLength > 0)
            {
                size = a.bodyLength/1024;
            }
            else
            {
                size = 0;
            }

            // if attachment is an image, show that image as thumbnail; if it's any other format, show a generic document image as thumbnail
            if(a.ContentType.startsWith(IMAGECONTENT_IDENTIFIER))
            {
                imgStyle = DISPLAY_BLOCK;
                applicationStyle = DISPLAY_NONE;
            }
            else
            {
                imgStyle = DISPLAY_NONE;
                applicationStyle = DISPLAY_BLOCK;
            }
        }
    }

}