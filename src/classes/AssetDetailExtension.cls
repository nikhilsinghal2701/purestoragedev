public without sharing class AssetDetailExtension {

	public ContentVersion currentContentVersion { get; set; }
	
	public Boolean isSubscribed { get; set; }
	public String searchString { get; set; }
	public String tags { get; set; }
	
	public AssetDetailExtension( Apexpages.Standardcontroller con ){
		currentContentVersion = ( ContentVersion ) con.getRecord();
		currentContentVersion = [ SELECT Id, Title, FileType, Description, TagCsv, Content_Thumbnail__r.Image__c, 
										 ContentDocumentId, Content_Thumbnail__r.Library__c, ContentUrl
								  FROM ContentVersion
								  WHERE isLatest = true AND Id =: currentContentVersion.Id ];	
		List<EntitySubscription> subscriptions = [ SELECT Id 
												   FROM EntitySubscription 
												   WHERE parentId =: currentContentVersion.ContentDocumentId 
												   		 AND SubscriberId =: Userinfo.getUserId() LIMIT 10];
		if( subscriptions == null || subscriptions.isEmpty() ){
			isSubscribed = false;
		}else{
			isSubscribed = true;
		}
		searchString = Apexpages.currentPage().getParameters().get('searchStr');
		tags = '';
		if( currentContentVersion.TagCsv != null ){
			for( string tag : currentContentVersion.TagCsv.split(',') ){
				tags += tag + ', ';
			}
			tags = tags.substring( 0, tags.length() - 2 );
		}
		
	}
	
	public PageReference subscribe(){
		List<EntitySubscription> subscriptions = [ SELECT Id 
												   FROM EntitySubscription 
												   WHERE parentId =: currentContentVersion.ContentDocumentId 
												   		 AND SubscriberId =: Userinfo.getUserId() LIMIT 10];
		if( subscriptions == null || subscriptions.isEmpty() ){
			if( isSubscribed ){
				EntitySubscription subscription = new EntitySubscription();
				subscription.ParentId = currentContentVersion.ContentDocumentId;
				subscription.SubscriberId = Userinfo.getUserId();
				insert subscription;
			}
		}else{
			if( !isSubscribed ){
				delete subscriptions;
			}
		}
		return null;
	}
}