public class Util {
    public static final String CONFIG_NAME ='Default';
    public static final String String_TYPE ='String';
    public static final String PURE_STORAGE_HELP_DESK_EMAIL = 'daizhixia@gmail.com';
    public static ATST_Settings__c getSettings () {
        ATST_Settings__c atstSettings = null;
        Map<String, ATST_Settings__c> settingsMap =  ATST_Settings__c.getAll();
        if(settingsMap != null && settingsMap.containsKey(Util.CONFIG_NAME)) {
            atstSettings  = settingsMap.get(Util.CONFIG_NAME);
        }
        return atstSettings;
    } 

    /*
        Email util
        send single email to specific user 
     */ 
    
    public static void sendAdminEmail(String sSubject, String sMessage, string adminEmail) {
        if (sMessage != '') {
               List<String> toAddresses = new list<String>();
               toAddresses.add(adminEmail);
               Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
               mail.setToAddresses(toAddresses);
               mail.setSubject(sSubject);
               mail.setPlainTextBody(sMessage);
               Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
       }
    }
 
    /*
    public static void sendTemplateEmail(List<User> users, List<String> toAddresses, String templateId) {
        if(!users.isEmpty()) {
            try{
                //email
                List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
                for(User emailUser: users) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    if(emailUser.IsActive) {
                        email.setTargetObjectId(emailUser.ContactId);
                        if(!toAddresses.isEmpty()) {
                            email.setToAddresses(toAddresses);
                        }
                        email.setTemplateId(templateId);
                    emails.add(email);
                }
                Messaging.sendEmail(emails);
                }
            }
            catch(EmailException e) {
                Util.SendAdminEmail('Send Email with template failed', e.getStackTraceString() + '\n' +  e.getMessage(), PURE_STORAGE_HELP_DESK_EMAIL);
            }
        }
    }
    */


}