@isTest (SeeAllData=true)
public class VLOppBatchTest{

    static testMethod void testOpportunityNomination(){    
         Database.BatchableContext BC;
         List<OpportunityHistory> lstOppHist = new List<OpportunityHistory>();
         Test.startTest();         
         VLStatusBatchOpportunity b = new VLStatusBatchOpportunity();
         Iterable<OpportunityHistory> itrOppHist = b.start(BC);
         Iterator<OpportunityHistory> iterator = itrOppHist.iterator();
         while(iterator.hasNext()){
             lstOppHist.add(iterator.next());         
         }
         b.execute(BC,lstOppHist);
         b.finish(BC);
         //database.executebatch(b,1); 
         Test.stopTest();    
    }
}