/****************************************************************************************
  Class: TestUserTrigger 
  Test Class for User Trigger
  Author - Christine Wang
  Date - 7/3/2014
  Revision History
----------------------------------------------------  
  Auther: Sriram Swaminathan
  Date:   7/23/2014   
  Description: Introduced the Account Edit grant for Partner Users   
****************************************************************************************/

@isTest
private class TestUserTrigger {

    static testMethod void testMethodOnInsert(){

        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.User_Trigger_Enabled__c = true;
        insert cs;

        // create an account
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        acc.IsPartner = true;
        update acc;
        
        // create a contact
        Contact contact = new Contact(LastName = 'Test',
                                      Email = 'test123@test.com',
                                      AccountId = acc.Id);
        insert contact;
        
        // create a user
        User u;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            Profile p = [SELECT Id FROM Profile WHERE Name LIKE '%Partner%' Limit 1];
            u = new User( Alias = 'standt', 
                          Email='test123@test.com', 
                          EmailEncodingKey='UTF-8', 
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='test123@test19.p3',
                          IsActive = true,
                          ContactId = contact.Id);
             insert u;
         }

         Contact con = [ SELECT Portal_Access_Approved__c FROM Contact WHERE Id = :contact.Id ];
         System.assertEquals( false, con.Portal_Access_Approved__c );
    }
    
    static testMethod void testMethodOnUpdate(){

        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.User_Trigger_Enabled__c = true;
        insert cs;

        // create an account
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        acc.IsPartner = true;
        update acc;
        
        // create a contact
        Contact contact = new Contact(LastName = 'Test',
                                      Email = 'test123@test.com',
                                      AccountId = acc.Id);
        insert contact;
        
        // create a user
        User u;
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            Profile p = [SELECT Id FROM Profile WHERE Name LIKE '%Partner%' Limit 1];
            u = new User( Alias = 'standt', 
                          Email='test123@test.com', 
                          EmailEncodingKey='UTF-8', 
                          LastName='Testing',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='test123@test19.p3',
                          ContactId = contact.Id);
             insert u;
         }
         
         u.IsActive = true;
         
         System.runAs(u) {
             Test.StartTest();
                 update u;
             Test.StopTest();
         }
         
         Contact con = [ SELECT Portal_Access_Approved__c FROM Contact WHERE Id = :contact.Id ];
         System.assertEquals( true, con.Portal_Access_Approved__c );
    }

  /**
    * Purpose of this test method is to test the Account Edit grant for the Partner Users
    *
    */
    @isTest 
    static void testAccountEditPartnerUser(){

        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.User_Trigger_Enabled__c = true;
        insert cs;      

        Integer total = 2;
        String ACCOUNT_NAME = 'AcctEditTest';
        String ACCOUNT_NAME_MODIFIED = 'AcctModifiedTest';

        // create an account
        Account acc = new Account(Name = ACCOUNT_NAME);
        insert acc;
        acc.IsPartner = true;
        update acc;
        
        List<Contact> contacts = new List<Contact>();

        for (Integer i=0; i<total; i++){
        // create a contact
          contacts.add(new Contact(LastName = ACCOUNT_NAME + i,
                                      Email = ACCOUNT_NAME + i + '@test.com',
                                      AccountId = acc.Id)
                      );
        }
        insert contacts;
        
        // create a user
        Profile p = [SELECT Id FROM Profile WHERE Name LIKE '%Partner%' Limit 1];
        List<User> users = new List<User>();
        
        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
            for (Integer i=0; i<total; i++){
              users.add(new User(Alias = 'accEd' + i, 
                                Email=ACCOUNT_NAME + i + '@test.com',
                                EmailEncodingKey='UTF-8', 
                                LastName='Testing',
                                LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', 
                                ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', 
                                UserName=ACCOUNT_NAME + i + '@test.p3',
                                IsActive = true,
                                ContactId = contacts.get(i).Id)
                        );
            }
        
            insert users;
        }

        User user1 = users.get(0);
        User user2 = users.get(1);

        AccountShare user1AcctShare = [SELECT ID, USERORGROUPID, ACCOUNTID, ACCOUNTACCESSLEVEL, OPPORTUNITYACCESSLEVEL, ROWCAUSE
                                        FROM ACCOUNTSHARE
                                        WHERE USERORGROUPID = :user1.ID
                                        AND ACCOUNTID = :acc.ID];
        System.assertEquals(TRUE, user1AcctShare!=NULL, 'Expecting an Account Share to be created for User ' + user1.NAME); 
        System.assertEquals('Manual', user1AcctShare.ROWCAUSE, 'Expecting an Account Share row cause to be Manual');                                        
        user1AcctShare.ACCOUNTACCESSLEVEL = 'Read';
        update user1AcctShare;        

        System.runAs(user1) {
            Account a1 = [SELECT ID, NAME FROM ACCOUNT WHERE NAME = :ACCOUNT_NAME LIMIT 1];
            System.assertEquals(TRUE, a1!=NULL, 'Expect the User to have read access to Account');
            a1.Name = ACCOUNT_NAME_MODIFIED;
            try {
              update a1;
            }catch(DmlException de){
              System.assertEquals(StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY
                                ,de.getDmlType(0)
                                ,'Expecting the account to be read only for user1');
            }
        }

        user1 = [SELECT ID, EMAIL FROM USER WHERE ID = :user1.ID];
        user1.Email = ACCOUNT_NAME + '1' + '@test1.com';
        update user1;

        System.runAs(user2) {
            Account a2 = [SELECT ID, NAME FROM ACCOUNT WHERE NAME = :ACCOUNT_NAME LIMIT 1];
            System.assertEquals(TRUE, a2!=NULL, 'Expect the User to have read access to Account');
            a2.Name = ACCOUNT_NAME_MODIFIED;
            update a2;
            a2 = [SELECT ID, NAME FROM ACCOUNT WHERE NAME = :ACCOUNT_NAME_MODIFIED LIMIT 1];
            System.assertEquals(TRUE, a2!=NULL, 'Expect the User to have read access to Account');  
            System.assertEquals(ACCOUNT_NAME_MODIFIED, a2.NAME, 'Expect the User to have edit access to Account');          
        }        
    }    
}