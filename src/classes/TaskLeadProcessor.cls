public class TaskLeadProcessor {

    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    private static datetime LEAD_SLA_GO_LIVE_DT = customSettings.Lead_Status_Transitions_Go_Live_Date__c;
    
    private static string getLeadKeyPrefix()
    {
        String retVal = '' ;
        Schema.DescribeSObjectResult l = Lead.sObjectType.getDescribe();
        return retVal = l.getKeyPrefix();
    
    }
    
    public static void computeNumberOfActivities(list<Task> newTasks, map<id,Task> mapNewIdTasks)
    {        
        set<id> leadIds = new set<id>();
        string leadKey = getLeadKeyPrefix();
        for(Task t: newTasks)
        {
           //if the t.WhatId's lead prefix is leadKeyPrefix
           if(t.WhoId != null)
           {
               system.debug('DEBUG:: t.WhoId: ' + t.WhoId); 
               system.debug('DEBUG:: leadKey: ' + leadKey);   
               string tempid = t.WhoId;
               system.debug('DEBUG:: t.WhoId.substr: ' + tempid.substring(0,3));
               if(tempid.substring(0,3) == leadKey)
                   leadIds.add(t.WhoId);
           }
        }
          
        if(leadIds.size() > 0)
        {    
            //only count the tasks that are Createddate greater than the Lead Opened date/time
            map<id,datetime> leadOpenMap = new map<id,datetime>(); 
            map<id,decimal> leadNumActMap = new map<id,decimal>(); 
            for(Lead l: [select Id, Lead_Opened__c from Lead where id in: leadIds])
            {
                leadOpenMap.put(l.Id, l.Lead_Opened__c);
                
            }
            //we are handling legacy data difftly.  the num of related tasks is artificially seeded so we 
            //dont want to count the tasks.  We use the LEAD_SLA_GO_LIVE_DT to distinguish these.
            //instead we use the seeded number plus any new tasks since the seed date to get the total number.
            for(Lead l: [select Id, Lead_Opened__c, Number_of_Related_Activities_Hist__c from Lead where id in: leadIds AND Lead_Opened__c =: LEAD_SLA_GO_LIVE_DT])
            {
                leadNumActMap.put(l.Id,l.Number_of_Related_Activities_Hist__c);
            }
            
            system.debug('DEBUG:: leadIds.size: ' + leadIds.size());
            
            list<Task> allTasksAllLeads = new list<Task>([select id, CreatedDate, isClosed, Status, WhoId from Task where WhoId in: leadIds]);
            map<id,list<Task>> leadTasks = new map<id,list<Task>>();
            
            for(id i: leadOpenMap.keySet())
            {
                list<Task> leadTask = new list<Task>();
                for(Task t: allTasksAllLeads)
                {
                    if(leadOpenMap.get(t.WhoId)!= null)
                    {
                        system.debug('DEBUG:: t.CreatedDate: ' + t.CreatedDate);
                        system.debug('DEBUG:: leadOpenMap.get(t.WhoId): ' + leadOpenMap.get(t.WhoId));
                        if(t.CreatedDate >= leadOpenMap.get(t.WhoId) && t.IsClosed == true) //should prolly be the date it was completed
                        {
                            leadTask.add(t);
                        }
                    }
                }
                
                leadTasks.put(i,leadTask);

            }
            
            map<id,decimal> numOfActPerLead = new map<id,decimal>();
            for(id i: leadTasks.keySet())
            {
                decimal x = 0;
                for(list<Task> ts: leadTasks.values())
                {
                    for(Task t: ts)
                    {
                        x=x+1;
                    }
                }
                decimal y = 0;
                if(leadNumActMap.get(i)!= null)
                {
                    y = leadNumActMap.get(i);
                }
                system.debug('DEBUG:: x is: ' + x);
                system.debug('DEBUG:: y is: ' + y);
                numOfActPerLead.put(i,x+y);
            }
            
            if(numOfActPerLead.size()> 0)
            {
                list<Lead> leadsToUpd = new list<Lead>();
                for(Lead ld: [select id, Number_Of_Related_Activities__c from Lead where id in: numOfActPerLead.keySet()])
                {
                    system.debug('DEBUG:: getting here');
                    
                    ld.Number_of_Related_Activities__c = numOfActPerLead.get(ld.Id);
                    leadsToUpd.add(ld);
                }
                
                system.debug('DEBUG:: leadsToUpd.size(): ' + leadsToUpd.size());
                
                if(leadsToUpd.size()>0)
                    update leadsToUpd;
            }
        }
        
    }
    
}