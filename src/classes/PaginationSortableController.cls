public abstract class PaginationSortableController {
	private Integer DEFAULT_PAGE_SIZE = 3;
  	private Integer DEFAULT_PAGE_START_NUMBER = 1;
  
	// The string containing the column to filter by
  	public String sortBy { get; set; }
  	// The string containing the direction (ASC, DESC)
  	public String sortDir { get; set; }
  	public Integer pageNumber { get; set; }
  	public Integer pageSize { get; set; }
  
	public List<sObject> pageRecords { get; set; }
  	public List<sObject> records { 
    	get {
	      		if (records == null){
	        	loadRecords();
	      	}
	      	return records;
    	}
    	set;
  	}

	public String entityId {
    	get;
    	set {
      		entityId = value;
      		records = null;
      		viewData();       
    		}
  	}

  	public abstract void loadRecords();

  	public void sort() { 
    	records = null;
    	firstPage();
  	}

  	public PaginationSortableController(){
	    pageNumber = DEFAULT_PAGE_START_NUMBER;
	    pageSize = DEFAULT_PAGE_SIZE;
	    records = null;
	    viewData();
  	}
  
  	public String sitePrefix {
    	get {
      		return Site.getPrefix();
    	}
  	} 

  	public Boolean hasPrevious {
    	get {
      		return (pageNumber > 1);
    	}
  	}

  	public Boolean hasNext {
    	get {
      		if (records == null) {
        		return false;
      		} else {
        		return ((pageNumber * pageSize) < records.size());
      		}
    	}
  	}

	public Integer totalPageNumber {
    	get {
      		Integer calculatedTotalPageNumber = 0;
      		if (records ==null || records.isEmpty()){
        		calculatedTotalPageNumber = 0;
      		}else{
        		Integer RECORD_SIZE = Integer.valueOf(records.size());
        		Integer remainder = math.mod(RECORD_SIZE, pageSize);
        		if (remainder == 0){
          			calculatedTotalPageNumber = RECORD_SIZE / pageSize;
        		}else{
          			calculatedTotalPageNumber = (RECORD_SIZE / pageSize) + 1;
    			}
      		}	
      		return calculatedTotalPageNumber;     
    	}
  	}

  	public PageReference viewData() { 
    	bindData(pageNumber);
    	return null;
  	}


  	private void bindData(Integer newPageIndex){
    	try {
      		if (records == null || records.isEmpty()){
        		loadRecords();
      		}
	    	pageRecords = new List<sObject>();
    	  	Transient Integer counter = 0;
      	  	Transient Integer min = 0;
      		Transient Integer max = 0;
        
      		if (newPageIndex <= 1){           //first page
        		min = 0;
        		max = pageSize;
      		}else if (newPageIndex >= totalPageNumber){ //last page
        		min = (totalPageNumber-1) * pageSize;
        		max = Integer.valueOf(records.size());
      		}else {
        		min = (newPageIndex - 1) * pageSize;
        		max = newPageIndex * pageSize;
      		}       
	      	for(sObject a : records){
    			counter++;
        		if (counter > min && counter <= max){
          			pageRecords.add(a);
        		}
      		}
      		pageNumber = newPageIndex;        

      		if (pageRecords == null || pageRecords.isEmpty()) {
        		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Records not available.'));
      		}
    	} catch(Exception ex) {
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
    	}
  	}

  	public void firstPage() {
    	bindData(1);
  	}
 
  	public void lastPage() {
    	bindData(totalPageNumber);
  	}

  	public void nextPage() {
    	bindData(pageNumber + 1);
	}

  	public void previousPage() {
    	bindData(pageNumber - 1);
  	}

  	public void goToPage() {
    	bindData((pageNumber<1)
        	? 1
          	:(  (pageNumber>totalPageNumber)
            ? totalPageNumber
            : pageNumber));
	}   
}