/*****************************************************************************
 * Description : This Apex class is leveraged in Partner Locator functionality
 *			
 *				 Purpose of this class is to hold the search paramaters selected by the end user during Partner Locator Search
 *
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/25/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorSearchBean {

	private static final PartnerLocatorService service = PartnerLocatorFactory.getInstance();

	public PartnerLocatorSearchBean(PartnerLocatorContext ctx){
		this.ctx = ctx;
	}

	public PartnerLocatorContext ctx { get; set; }

	public List<SelectOption> countries {
		get {
			return ctx.isContextSearch() 
				? service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.COUNTRY
											, NULL
											, PartnerLocatorConstants.DEFAULT_SELECT_OPTION)
				: service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.COUNTRY
											, NULL
											, PartnerLocatorConstants.DEFAULT_SELECT_OPTION_COUNTRY);
		}
		set;
	}

	public String selectedCountry {
		get;
		set;
	}	

	public List<SelectOption> statesOrProvinces {
		get {
			return service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.STATE
											, selectedCountry
											, PartnerLocatorConstants.DEFAULT_SELECT_OPTION_STATE);
		}
		set;
	}

	public String selectedStateOrProvince {
		get;
		set;
	}	

	public List<SelectOption> industryFocusOptions {
		get {
			return service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.INDUSTRYFOCUS
											, NULL
											, PartnerLocatorConstants.DEFAULT_SELECT_OPTION);
		}
		set;
	}	

	public String selectedIndustryFocus {
		get;
		set;
	}	

	public List<SelectOption> applicationExpertiseOptions {
		get {
			return service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.APPLICATIONEXPERTISE
											, NULL
											, PartnerLocatorConstants.DEFAULT_SELECT_OPTION);
		}
		set;
	}

	public String selectedApplicationExpertise {
		get;
		set;
	}

	public List<SelectOption> partnerTierOptions {
		get {
			return service.getSelectOptions(PartnerLocatorConstants.PARTNER_LOCATOR_SELECT_OPTION_FIELD.PARTNERTIER
											, NULL
											, NULL);
		}
		set;
	}

	public List<String> selectedPartnerTiers {
		get {
			if (selectedPartnerTiers==NULL){
				selectedPartnerTiers = new List<String>();
			}
			return selectedPartnerTiers;
		}
		set;
	}

}