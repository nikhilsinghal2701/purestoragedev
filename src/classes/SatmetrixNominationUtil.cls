public class SatmetrixNominationUtil{        
    
     public Feedback__c nominateContactsForSupportSurvey(Contact con, String strZDTicketNumber, String strZDSeverityLevel,
     String strZDCSRName, String strZDNumDaysToResolve) {
        Feedback__c feedback;
        Long lgSeed = System.currentTimeMillis();
        lgSeed = lgSeed + 1;            
        feedback = new Feedback__c();
        feedback.Name = 'P_' + lgSeed;
        feedback.Contact__c = con.Id; //ContactName
        
        feedback.Status__c = 'Nominated';               
        feedback.DataCollectionId__c= 'PURESTORAGE_68307';            
        feedback.DataCollectionName__c = 'Pure Storage Support Satisfaction Survey';
        
        feedback.ZD_Ticket_Number__c = strZDTicketNumber;
        feedback.ZD_Ticket_CSR_Name__c = strZDCSRName;
        feedback.ZD_Severity_Level__c = strZDSeverityLevel;
        feedback.ZD_No_Days_to_Resolve__c = strZDNumDaysToResolve;            
        
        insert feedback;
        return feedback;
    }
    
          /** TEST METHOD **/
      @isTest   
      static void testNominateContactsForSupportSurvey(){
       Contact contact = createTestContact();      
       new SatmetrixNominationUtil().nominateContactsForSupportSurvey(contact, '12345', 'level1', 'SMX Test ZDCSRName', '10');
       clearTestContact();     
    }
    
      static Contact createTestContact(){
      
        Account a = new Account(Name='SMX Test Account');
        insert a;
        
        Contact c1 = new Contact(FirstName='SMX TestFName1', LastName='SMX TestLName1', AccountID=a.id, Email='this.is.a.smx.test@test.com');
        insert c1;
        
        return c1;
    }
     
      static void clearTestContact(){
       List<Contact> contacts = [SELECT Id from CONTACT WHERE FirstName = 'SMX TestFName1'];
       delete contacts;
       List<Account> accounts = [SELECT Id from ACCOUNT WHERE NAME = 'SMX Test Account'];
       delete accounts;
     }   
}