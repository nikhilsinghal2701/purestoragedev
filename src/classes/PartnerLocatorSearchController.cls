/*****************************************************************************
 * Description : Search Controller Class for Partner Locator functionality
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/27/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorSearchController {

	public PartnerLocatorSearchController() {
		ctx = new PartnerLocatorContext();
		partnerLocatorSearchBean = new PartnerLocatorSearchBean(ctx);
	}

	public PartnerLocatorSearchController getThis() { 
		return this; 
	}	

	public PartnerLocatorContext ctx { 
		get;
		set;
	}	

	public PartnerLocatorSearchBean partnerLocatorSearchBean {
		get;
		set;
	}

	public PartnerLocatorBean partnerLocatorBean {
		get;
		set;
	}

	public void navigateToSearch() {
		ctx.setContextAsSearch();
	}	

	public void navigateToList() {
		ctx.setContextAsList();
	}	

	public void navigateToDetails(PartnerLocatorBean bean) {
		this.partnerLocatorBean = bean;
		ctx.setContextAsDetails();
	}

	public void search() {
		searchEvent = TRUE;
		ctx.setContextAsList();
	}

	private boolean searchEvent {
		get;
		set;
	}

	public boolean isSearchEvent() {
		return searchEvent;
	}

	public void resetSearchEvent() {
		searchEvent = FALSE;
	}			

}