/* test class for SC_CaseTriggerHandler
   to test the functionality of splitting comma separated emails into 
   10 individual email fields and creation of a "CC on All Cases" marked Contacts 
   as a case team member
 */

@isTest
private class SC_CaseTriggerHandlerTest
{
    // test the case team creation functionality
    private static testMethod void createCase()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);
        
        // create a contact and update the CC On All Cases checkbox to true
        Contact con2 = SC_TestUtil.createContact(true, acc);
        con2.CC_On_All_Cases__c = true;
        update con2;

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
            Test.startTest();

            // create a case
            Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
            c.Case_Collaborators__c = 'CCEmail1@testclass.com, CCEmail2@testclass.com, CCEmail3@testclass.com,'
                                    + 'CCEmail4@testclass.com, CCEmail5@testclass.com, CCEmail6@testclass.com,'
                                    +'CCEmail7@testclass.com, CCEmail8@testclass.com, CCEmail9@testclass.com,'
                                    + 'CCEmail10@testclass.com';
            insert c;

            Test.stopTest();

            // assert that the con2 is added as a case team member
            system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCContactRole
                                                                            and MemberId = :con2.ID and ParentId = :c.Id]);
        }
    }
    
    // To test the scenario when a case is created with standard account field populate,
    // the SE and AE of the Account (Account team members) should be copied to the case
    private static testMethod void createCaseWithStandardAccount()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
            Test.startTest();

            // create a case
            Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
            insert c;

            Test.stopTest();

            // assert that the SE is added as a case team member
            system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCAccountRole
                                                                            and MemberId = :u1.ID and ParentId = :c.Id]);
        }
    }
    
    // To test the scenario when a case is created with Cloud Assist account field populate,
    // the SE and AE of the Account (Account team members) should be copied to the case
    private static testMethod void createCaseWithCloudAssistAccount()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
            Test.startTest();

            // create a case
            Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
            c.AccountId = null;
            c.Cloud_Assist_Account__c = acc.Id;
            insert c;

            Test.stopTest();

            // assert that the SE is added as a case team member
            system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCAccountRole
                                                                            and MemberId = :u1.ID and ParentId = :c.Id]);
        }
    }
    
    // To test the scenario when a case is created with a wrong account by mistake,
    // TSE can update it back to correct account, 
    // and the SE and AE of the second Account (Account team members) should be copied to the case
    private static testMethod void updateCaseWithStandardAccount()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc1 = SC_TestUtil.createAccount(true);
        
        Contact con1 = SC_TestUtil.createContact(true, acc1);
        
        Account acc2 = SC_TestUtil.createAccount(true);
        
        Contact con2 = SC_TestUtil.createContact(true, acc2);

        Asset parentArray1 = SC_TestUtil.createArray(true, 'testclass.array', acc1, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Asset parentArray2 = SC_TestUtil.createArray(true, 'testclass.array', acc2, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        
        SC_TestUtil.createAccountTeamMember(acc1, u1);
        
        User u2 = SC_TestUtil.createUser(p1);
        u2.UserName = 'accountSE@testclass.com2';
        u2.Email = 'SE@testclass.com2';
        insert u2;
        
        SC_TestUtil.createAccountTeamMember(acc2, u2);
        
            Test.startTest();

            // create a case
            Case c = SC_TestUtil.createCase(false, acc1, con1, parentArray1);
            insert c;
            
            system.assertEquals(true, SC_CaseTriggerHandler.isInsert, 'The flag has to be true to make sure it does not create the case team member for workflow updates');
            
            SC_CaseTriggerHandler.isInsert = false;
            
            c.AccountId =  acc2.Id;
            c.ContactId = con2.Id;
            c.AssetId = parentArray2.Id;
            update c;

            Test.stopTest();

            // cannot assert, as there will 2 future calls one from insert and one from update got executed at that same time
            // after stop test
            //system.assertEquals(1, [Select count() from CaseTeamMember where TeamRole.Name = :system.label.SC_CCAccountRole
                                                                            //and MemberId = :u2.ID and ParentId = :c.Id]);
        }
    }

    // test the splitting of comma separated email functionality
    private static testMethod void testCaseCollaborators()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
            Test.startTest();

            // create a test case with populating the comma separated emails
            Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
            c.Case_Collaborators__c = 'ccemail1@testclass.com, ccemail2@testclass.com, ccemail3@testclass.com,'
                                    + 'ccemail4@testclass.com, ccemail5@testclass.com, ccemail6@testclass.com,'
                                    + 'ccemail7@testclass.com, ccemail8@testclass.com, ccemail9@testclass.com,'
                                    + 'ccemail10@testclass.com';
            insert c;

            // query the individual email fields of the inserted case
            c = [Select Case_COllaborator_1__c, Case_Collaborator_10__c from Case where Id = :c.Id];

            // assert that the first email field is populated with the first email in the comma separated field
            system.assertEquals('ccemail1@testclass.com', c.Case_COllaborator_1__c, 
                                            'Comma separated emails entered in Case_Collaborators__c should be split into each email field');
            // assert that the last email field is populated with the last email in the comma separated field
            system.assertEquals('ccemail10@testclass.com', c.Case_COllaborator_10__c, 
                                            'Comma separated emails entered in Case_Collaborators__c should be split into each email field');
            
            // test the same functionality by updating the comma separated email field and also to test functionality when
            // only one email is entered without any comma
            c.Case_Collaborators__c = 'ccemail1@updatedcase.com';
            update c;

            Test.stopTest();

            // query the individual email fields of the updated case
            c = [Select Case_COllaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_10__c from Case where Id = :c.Id];

            // assert that the first email has the updated email
            system.assertEquals('ccemail1@updatedcase.com', c.Case_COllaborator_1__c, 
                                            'Comma separated emails entered in Case_Collaborators__c should be split into each email field');
            // assert that the other email fields are nulled out
            system.assertEquals(null, c.Case_COllaborator_2__c,
                                            'Comma separated emails entered in Case_Collaborators__c should be split into each email field');
            system.assertEquals(null, c.Case_COllaborator_10__c,
                                            'Comma separated emails entered in Case_Collaborators__c should be split into each email field');
        }
    }
}