/***********************************************
 * Description : Custom sort class to sort on parts of an array - used in SC_SendRMAController and SC_SendRMA Page
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/
global class PartsMatrixSort implements Comparable {

    public SC_SendRMAController.Components item1 {get;set;}
    
    // Constructor 
    
    public PartsMatrixSort(SC_SendRMAController.Components item) {
        item1 = item;
    } 
    
    // method to compare to parts based on name, spare description and slot no
    global Integer compareTo(Object compareTo) {
        // Cast argument to MerchandiseWrapper 
        PartsMatrixSort p = (PartsMatrixSort)compareTo;
        SC_SendRMAController.Components item2 = p.item1;
        
        // The return value of 0 indicates that both elements are equal. 
    
        Integer returnValue = 0;

        // 1) sort on name ex: CT-0, CT-1, SH-0, SH-1
        if (item1.ChildAssetName > item2.ChildAssetName) {
            returnValue = 1;
        } else if (item1.ChildAssetName < item2.ChildAssetName) {
            returnValue = -1;
        }
        
        // 2) sort on spare description ex: CRU: CRU: Bezel FA-300, CRU: Controller Power Supply
        else if(item1.ChildAssetName == item2.ChildAssetName)
        {
            if (item1.part.Spare_Description__c > item2.part.Spare_Description__c) {
                returnValue = 1;
            } else if (item1.part.Spare_Description__c < item2.part.Spare_Description__c) {
                returnValue = -1;
            }
            
            // 3) sort on slot ex: 0, 1, 2
            else if(item1.part.Spare_Description__c == item2.part.Spare_Description__c)
            {
                if(item1.Slot != 'N/A' && item2.Slot != 'N/A')
                {
                    if (Integer.ValueOf(item1.Slot) > Integer.ValueOf(item2.Slot)) {
                        returnValue = 1;
                    } else if (Integer.ValueOf(item1.Slot) < Integer.ValueOf(item2.Slot)) {
                        returnValue = -1;
                    }
                }
            }
        }
        
        return returnValue;       
    }
}