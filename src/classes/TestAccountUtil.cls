@isTest
private class TestAccountUtil {
    
    static testMethod void svarAccountShare() {
        TestDataFactory dataCreation = new TestDataFactory();
        UserLicense license = dataCreation.getLicense('PID_Partner_Community_Login');
        System.debug('license  is ...' + license);
        Profile profile = dataCreation.getPrifleByLicense('PID_Partner_Community_Login');
        System.debug('profile  is ...' + profile);
        Account testAccount = dataCreation.getAccount();
        insert testAccount;
        // svar Account 1, and related data preparation
        Account svarAccount1 = dataCreation.getAccount();
        svarAccount1.SVAR__c = true;
        insert svarAccount1;
        
        Contact testContact1 = dataCreation.getContact(svarAccount1);
        insert testContact1;
        User communityUser1 = dataCreation.getUser(profile, testContact1);
        communityUser1.userName = 'testU@testorg.com.p3';
        insert communityUser1;
        System.debug('communityUser1  is ...' + communityUser1);

        // svar Account 2, and related data preparation
        Account svarAccount2 = dataCreation.getAccount();
        svarAccount2.SVAR__c = true;
        insert svarAccount2;
        Contact testContact2 = dataCreation.getContact(svarAccount2);
        insert testContact2;
        User communityUser2 = dataCreation.getUser(profile, testContact2);
        communityUser2.LastName = 'Dai test';
        communityUser2.UserName = 'fred.dai@test.com.test.p3';
        insert communityUser2;
        System.debug('communityUser2  is ...' + communityUser2);
        Test.startTest();
        testAccount.SVAR_Account__c = svarAccount1.id;
        update testAccount;
        testAccount.SVAR_Account__c = svarAccount2.id;
        update testAccount;
        List<AccountShare> shares = [Select Id, AccountId, UserOrGroupId From AccountShare where AccountId =: testAccount.id and RowCause = 'Manual'];
        List<User> expectedUsers = [Select Id, userRoleId From User where Id =: communityUser2.id limit 1];
        List<Group> expectedGroups = [Select Id, relatedId From Group where relatedId =: expectedUsers[0].userRoleId and type = 'Role' limit 1];
        for(AccountShare share: shares) {
            System.assertEquals(share.accountId, testAccount.Id);
            System.assertEquals(share.UserOrGroupId, expectedGroups[0].id);
        }
        Test.stopTest();
        
    }
    
}