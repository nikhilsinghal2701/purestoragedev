public class AccountProcessor
{

    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static boolean ADDR_UPSERT_ENABLED = customSettings.Account_Upsert_Address_Enabled__c;
    public static boolean ADDR_SEGMENT_SYNC_ENABLED = customSettings.Account_Segment_Sync_Enabled__c;
    public static map<id,Address__c> mapAccountWithAddr = new map<id,Address__c>();
    
    public void processRecords (List<Account> newRecs, List<Account> oldRecs, Map<Id,Account> newMap, Map<Id,Account> oldMap, boolean isBefore, boolean isAfter)
    {
        if(isAfter)
        {
            if(ADDR_SEGMENT_SYNC_ENABLED)
            {
                set<id> setAccountIdsWithPossibleSegmentChange = new set<id>();
                list<Account> listAccountWithSegmentChange = new list<Account>();
                for(Account a: newRecs)
                {
                    if(oldMap != null)
                    {
                        if(oldMap.get(a.Id).Segment_Calculated__c != newMap.get(a.Id).Segment_Calculated__c)
                            setAccountIdsWithPossibleSegmentChange.add(a.Id);
                    }
                }
                if(setAccountIdsWithPossibleSegmentChange.size()>0)
                {
                    for(Account a: [select id, Segment__c, Segment_Calculated__c,Description from Account where id in: setAccountIdsWithPossibleSegmentChange])
                    {
                        SYSTEM.DEBUG('DEBUG:: Segment__c' + 'a.Segment__c = ' + a.Segment__c + 'newMap.get(a.Id).Segment_Calculated__c = ' + newMap.get(a.Id).Segment_Calculated__c);
                        a.Segment__c = newMap.get(a.Id).Segment_Calculated__c;
                        listAccountWithSegmentChange.add(a);
                        SYSTEM.DEBUG('DEBUG:: Segment__c' + 'a.Segment__c = ' + a.Segment__c);
                    }
                }
        		update listAccountWithSegmentChange;
        	}
        }
        if(ADDR_UPSERT_ENABLED)
        {
            boolean addrChanged = false;
            list<Account> listAccountWithAddrChange = new list<Account>();
            set<id> setAccountIdsWithAddrChange = new set<id>();   
     
            for(Account a: newRecs)
            {
                if(oldMap != null)//updates
                {
                    if(oldMap.get(a.Id).ShippingStreet != newMap.get(a.Id).ShippingStreet)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).ShippingCity != newMap.get(a.Id).ShippingCity)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).ShippingState != newMap.get(a.Id).ShippingState)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).ShippingPostalCode != newMap.get(a.Id).ShippingPostalCode)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).ShippingCountry != newMap.get(a.Id).ShippingCountry)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).Ship_To_Contact_Name__c != newMap.get(a.Id).Ship_To_Contact_Name__c)
                        addrChanged = true;
                    
                    if(oldMap.get(a.Id).Ship_To_Contact_Phone__c != newMap.get(a.Id).Ship_To_Contact_Phone__c)
                        addrChanged = true;
                }
                else
                {
                    //inserts
                    addrChanged = true;
                }
                
                if(addrChanged == true)
                {
                    listAccountWithAddrChange.add(a);  
                    setAccountIdsWithAddrChange.add(a.Id);                
                }
                
                if(setAccountIdsWithAddrChange.size()>0)
                {
                    list<Address__c> existingPrimaryAddrs = [select Id, Primary__c, Account__c, Street__c, 
                                                             City__c, State_Province__c, Country__c, Postal_Code__c,
                                                             Location_Contact__c, Location_Contact_Phone__c
                                         from Address__c
                                         where Primary__c = true
                                         and Account__c in: setAccountIdsWithAddrChange ];
                    
                    for (Address__c addr: existingPrimaryAddrs)
                    {
                        mapAccountWithAddr.put(addr.Account__c, addr);
                    }
                    
                    doAddrUpdates(listAccountWithAddrChange);    
                }   
            }
        }
    }
    
    public void doAddrUpdates (list<Account> recsToProcess)
    {
        list<Address__c> addrsForUpsert = new list<Address__c>();
        for(Account a: recsToProcess)
        {
            //only create an address if there is some min addr info
            if(a.ShippingStreet != null && a.ShippingStreet != '')
            {
            	Address__c addr = generateAddress(a.Id, true, 'Shipping', a.ShippingStreet, a.ShippingCity, a.ShippingState, a.ShippingPostalCode, a.ShippingCountry, a.Ship_To_Contact_Name__c, a.Ship_To_Contact_Phone__c);   
            	addrsForUpsert.add(addr);
            }
        }
        
        upsert addrsForUpsert;
    }
    
    public static Address__c generateAddress (id accountId, boolean isDefault, string addrType, string streetAddress, string city, string stateProv, string PostalCode, string country, string conName, string conPhone)
    {
        Address__c newAddr;
        if(mapAccountWithAddr.get(accountId)!= null)
        {
            newAddr = mapAccountWithAddr.get(accountId);
        }
        else
        {
            newAddr = new Address__c();
        }
        
        String addrName;
        if(streetAddress != '')
        {
            addrName = streetAddress;
        }
        else if(city != '')
        {
            addrName = city;
        }
        else if(stateProv != '')
        {
            addrName = stateProv;
        }
        
        
        newAddr.Primary__c = isDefault;
        newAddr.Name = addrName;
        newAddr.Account__c = accountId;
        newAddr.Street__c = streetAddress;
        newAddr.City__c = city;
        newAddr.State_Province__c = stateProv;
        newAddr.Postal_Code__c = postalCode;
        newAddr.Country__c = country;
        newAddr.Location_Contact__c = conName;
        newAddr.Location_Contact_Phone__c = conPhone;
            
        return newAddr;

    }
   
}