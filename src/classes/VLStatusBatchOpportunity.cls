global class VLStatusBatchOpportunity implements Database.Batchable<OpportunityHistory>, Database.AllowsCallouts{

OpportunityHistory[] AssetsToUpdate = [select OpportunityId, StageName from OpportunityHistory where CreatedDate = TODAY order by OpportunityId asc, CreatedDate asc];

global Iterable<OpportunityHistory> start(database.batchablecontext BC){
    if(Test.isRunningTest()){
        return prepareOppTestData();
    }
    return (AssetsToUpdate);    
}

global void execute(Database.BatchableContext BC, List<OpportunityHistory> scope){
    List<Account> lstAccounts = [select id, XPSynced__c from Account where Id in (select AccountId from Opportunity where LastModifiedDate = TODAY)];
    for(Account acc: lstAccounts){
     acc.XPSynced__c = 'N';
    }
    update lstAccounts;
    
    OpportunityContactRole[] arrOppContactRole = [select OpportunityId, ContactId from OpportunityContactRole where IsPrimary = true and OpportunityId in (select OpportunityId from OpportunityHistory where CreatedDate = TODAY)];
    Id idOpp = null;
    String strStageCurrent = '';
    String strStagePrevious = '';
    Long lgSeed = System.currentTimeMillis();
    List<Feedback__c> lstFeedback = new List<Feedback__c>();
    Set<ID> idArrInMemOppIDList = new Set<ID>();
    for(OpportunityHistory o : scope){
             if(idOpp != o.OpportunityId){
                 idOpp = o.OpportunityId;
                 strStagePrevious = '';
                 strStageCurrent = o.StageName;
             }else{
                strStagePrevious = strStageCurrent;
                strStageCurrent = o.StageName;
                if(strStageCurrent != strStagePrevious){
                    if(strStagePrevious == 'Stage 4 - POC/EVAL Engaged'){
                        if(strStageCurrent == 'Stage 5 - POC/EVAL Complete' ||
                           strStageCurrent == 'Stage 6 - Commit' ||
                           strStageCurrent == 'Stage 8 - Closed/Won' ||
                           strStageCurrent == 'Stage 8 - Closed/Lost' ||
                           strStageCurrent == 'Stage 8 - Closed/No Decision'){   
                               if(idArrInMemOppIDList.add(o.OpportunityId)){
                                   // Opportunity has moved from POC Engaged stage to valid next survey states. Send a pure storage inc survey
                                    System.debug('Let\'s send survey PURESTORAGE_99896');                    
                                    lgSeed += 1;
                                    List<Feedback__c> lstDeltaFeedback = VLOpportunityNomination.opportunityNomination(lgSeed, o.OpportunityId, arrOppContactRole, 'PURESTORAGE_99896','Pure Storage Inc');                                                                
                                    lstFeedback.addAll(lstDeltaFeedback);
                               }
                            }
                    }else{
                    if(strStageCurrent == 'Stage 8 - Closed/Won' ||
                           strStageCurrent == 'Stage 8 - Closed/Lost'){  
                               if(idArrInMemOppIDList.add(o.OpportunityId)){ 
                                    // Opportunity has moved from final state. Send a customer satisfaction survey
                                    System.debug('Let\'s send survey PURESTORAGE_34846');
                                    lgSeed += 1; 
                                    List<Feedback__c> lstDeltaFeedback = VLOpportunityNomination.opportunityNomination(lgSeed, o.OpportunityId, arrOppContactRole, 'PURESTORAGE_34846','Pure Storage Customer Satisfaction Survey');                                
                                    lstFeedback.addAll(lstDeltaFeedback);
                                }
                            }
                    }
             }   
        }    
    }    
    insert lstFeedback;
}

global void finish(Database.BatchableContext info){
    }//global void finish loop
    

static id createEnvironmentRec(id OppId, id AcctId)
    {
        Environment__c e = new Environment__c(Name = 'testEnv', 
                                         Opportunity__c = OppId,
                                        Account__c = AcctId,
                                        Vendor_Product__c = 'None');
        insert e;
        return e.Id; 
        
    }
    
static Iterable<OpportunityHistory> prepareOppTestData(){
    Account acc = new Account(Name = 'Test Account',Support_Must_Not_Contact_End_Customer__c=true);
    insert acc;
    
    Contact con = new Contact(Email='santosh.hanaji@satmetrix.com', LastName='Hanaji', CurrencyIsoCode='USD',AccountID=acc.ID);
    insert con;
    
    Opportunity opp = new Opportunity();
    opp.name = 'Test Opp 1';
    opp.AccountID = acc.id;
    opp.CloseDate =  dateTime.now().date();
    opp.StageName = 'Stage 1 - Prequalified';
    opp.Type = 'Existing Business';
    opp.ForecastCategoryName = 'Pipeline';
    opp.CurrencyIsoCode = 'USD';
    opp.NextStep = 'Step 3';
    opp.Channel_Led_Deal__c = false;
    
    insert opp;
    
    OpportunityContactRole ocr = new OpportunityContactRole();
    ocr.ContactId = con.Id;
    ocr.OpportunityId = opp.Id;
    ocr.Role = 'Decision Maker';
    ocr.IsPrimary = true;
    insert ocr;
    
    opp.Reason_s_for_Win_Loss__c = 'Support';
    opp.Customer_Reference__c = 'Test';
    opp.Eligible_for_the_Love_Your_Storage_Prog__c = 'No';
    opp.Environment__c = 'Test';
    opp.Is_the_Environment_Virtualized__c = 'Test';
    opp.Competition__c = 'Test';
    opp.Eval_Stage__c = 'POC Installed';
    opp.HW_Eval_Agreement_Completed__c = 'Yes';
    opp.Site_Survey_Completed__c = 'Yes';
    opp.SE_Opportunity_Owner__c = UserInfo.getUserId();
    //opp.Reasons_for_Loss__c = 'Test';
    opp.Reasons_for_Win__c = 'Test';
    opp.Business_value__c = 'Test';
    opp.Operational_value__c = 'Test';
    opp.Technical_value__c = 'Test' ;
   // opp.Reasons_for_Loss__c = 'Test';
    opp.Replication_Required__c = 'true';
    opp.X23TB_required__c = 'true';
    //environment reqt
    createEnvironmentRec(opp.Id, opp.AccountId); 
    opp.StageName = 'Stage 4 - POC/EVAL Engaged';
    update opp;
    
    opp.StageName = 'Stage 8 - Closed/Won';
    update opp;
    
    opp.Amount = 10.00;    
    update opp;
    
    Opportunity opp1 = new Opportunity();
    opp1.name = 'Test Opp 1';
    opp1.AccountID = acc.id;
    opp1.CloseDate =  dateTime.now().date();
    opp1.StageName = 'Stage 1 - Prequalified';
    opp1.Type = 'Existing Business';
    opp1.ForecastCategoryName = 'Pipeline';
    opp1.CurrencyIsoCode = 'USD';
    opp1.NextStep = 'Step 3';
    opp1.Channel_Led_Deal__c = false;
    opp1.Replication_Required__c = 'true';
    opp1.X23TB_required__c = 'true';
    
    insert opp1;
    
    OpportunityContactRole ocr1 = new OpportunityContactRole();
    ocr1.ContactId = con.Id;
    ocr1.OpportunityId = opp1.Id;
    ocr1.Role = 'Decision Maker';
    ocr1.IsPrimary = true;
    insert ocr1;
    
    opp1.Reason_s_for_Win_Loss__c = 'Support';
    opp1.Customer_Reference__c = 'Test';
    opp1.Eligible_for_the_Love_Your_Storage_Prog__c = 'No';
    opp1.Environment__c = 'Test';
    opp1.Is_the_Environment_Virtualized__c = 'Test';
    opp1.Competition__c = 'Test';
    opp1.Eval_Stage__c = 'POC Installed';
    opp1.HW_Eval_Agreement_Completed__c = 'Yes';
    opp1.Site_Survey_Completed__c = 'Yes';
    opp1.SE_Opportunity_Owner__c = UserInfo.getUserId();
    //opp1.Reasons_for_Loss__c = 'Test';
    opp1.Reasons_for_Win__c = 'Test';
    opp1.Business_value__c = 'Test';
    opp1.Operational_value__c = 'Test';
    opp1.Technical_value__c = 'Test' ;
   // opp1.Reasons_for_Loss__c = 'Test';
    createEnvironmentRec(opp1.Id, opp1.AccountId); 
    opp1.StageName = 'Stage 8 - Closed/Won';
    update opp1;    

    opp1.Amount = 10.00;    
    update opp1;
    
    List<OpportunityHistory> lstOppHist = [select OpportunityId, StageName from OpportunityHistory where OpportunityId =: opp.Id or OpportunityId =: opp1.Id order by OpportunityId asc, StageName asc];
    
    System.debug('Test Code - Size of Opportunity History List: ' + lstOppHist.size());
    
    return lstOppHist;

   }

}