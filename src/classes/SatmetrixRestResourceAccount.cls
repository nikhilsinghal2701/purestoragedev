@RestResource(urlMapping='/Account/*')
global with sharing class SatmetrixRestResourceAccount {

@HttpPost
global static String doPost(Decimal decAvgNPS) {    
    RestRequest req = RestContext.request;
    String strCompanyId= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    strCompanyId = EncodingUtil.urlDecode(strCompanyId,'UTF-8');
    return updateAccount(strCompanyId,decAvgNPS);
   }
   
 static String updateAccount(String strCompanyId, Decimal decAvgNPS){
 
    if(strCompanyId == null || strCompanyId == '')
     return 'Failure';
    else{ 
        Account account= [SELECT ID,Name,NPSScore__c FROM Account where ID =:strCompanyId];
        if(account == null)
            return 'Failure';
        else{   
            account.NPSScore__c = decAvgNPS;
            update account;
            return 'Success';
          }
       }
  }   
  
  
  /****** Test Methods start here *******/
  
  @isTest(SeeAllData=true)
  static void testDoPost(){
     String strAccountId = prepareTestData();
     System.RestContext.request = new RestRequest();
     RestContext.request.requestURI = '/Account/' + strAccountId;
     SatmetrixRestResourceAccount.doPost(9);     
     clearTestData();
  }
  
  static String prepareTestData(){  
    Account a = new Account(Name='SMX Test Account');
    insert a;
    return a.Id;
  }
  
  static void clearTestData(){
    Account a = [SELECT Id from ACCOUNT WHERE Name = 'SMX Test Account'];
    delete a;
  }
}