public class LeadStatusTransitionsProcessor 
{
    private static final string OPEN = 'Open';
    private static final string SP1 = 'Working 1';
    private static final string SP2 = 'Working 2';
    private static final string SP3 = 'Working 3';
    private static final string SP4 = 'Working 4';
    private static final string SP5 = 'Working 5';
    private static final string SP6 = 'Working 6';
    private static final string NURTURE = 'Nurture';
    private static final string FORECAST = 'Forecast';
    private static final string CHANNEL = 'Channel';
    private static final string QUALIFIED = 'Qualified';
    private static final string DEAD = 'Dead';
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static boolean STATUS_RULES_ENABLED = customSettings.Lead_Status_Transitions_Enabled__c;
    public static string EXCLUDED_GROUP = customSettings.Lead_Status_Transitions_Exception_Group__c;
    //these two flags help control wether apex script invokes the status transition rules or not.  taskIsTest is a backdoor for apex test methods to always invoke the rules
    public static boolean taskIsApex = false;
    public static boolean taskIsTest = test.isRunningTest();
    
    public static boolean isExcluded()
    {
        boolean retVal = false;
        for(GroupMember gm: [Select UserOrGroupId, GroupId From GroupMember Where GroupId =:EXCLUDED_GROUP])
        {
            if(System.UserInfo.getUserId() == gm.UserOrGroupId)
            {
                retVal = true;
            }
        }
        return retVal;
    }
    
    public static void checkStatusTransition(list<Lead> newRecs, map<id,Lead> oldRecsMap)
    {
        system.debug('DEBUG:: customSettings.Lead_Status_Transitions_Enabled__c: ' + customSettings.Lead_Status_Transitions_Enabled__c);
        system.debug('DEBUG:: isExcluded(): ' + isExcluded());
        if(STATUS_RULES_ENABLED == true)
        {
            if(isExcluded() == false)
            {
                //use this line to allow apex unit tests to invoke the transition rules; otherwise they are ignored
                if(taskIsTest == true)
                    taskIsApex = false;
                    
            for(Lead l: newRecs)
            {
                system.debug('DEBUG:: old Status: ' + oldRecsMap.get(l.Id).Status);
                system.debug('DEBUG:: new Status: ' + l.Status);
                system.debug('DEBUG:: # Related Activities: ' + l.Number_of_Related_Activities__c);
                
                if(oldRecsMap.get(l.Id).Status == OPEN )
                {
                    if(taskIsApex || (l.Status == SP1 && l.Number_of_Related_Activities__c == 1) || l.Status == OPEN || l.Status == DEAD || l.Status == QUALIFIED)
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Dead.  Please complete a related task to advance the status.');
                    }
                }
                

                if(oldRecsMap.get(l.Id).Status == SP1 )
                {
                    if(taskIsApex || l.Status == SP1 || (l.Status == SP2 && l.Number_of_Related_Activities__c == 2) || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                
                
                if(oldRecsMap.get(l.Id).Status == SP2 )
                {
                    if(taskIsApex || l.Status == SP2 || (l.Status == SP3 && l.Number_of_Related_Activities__c == 3) || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                
                if(oldRecsMap.get(l.Id).Status == SP3 )
                {
                    if(taskIsApex || l.Status == SP3 || (l.Status == SP4 && l.Number_of_Related_Activities__c == 4) || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                
                if(oldRecsMap.get(l.Id).Status == SP4 )
                {
                    if(taskIsApex || l.Status == SP4 || (l.Status == SP5 && l.Number_of_Related_Activities__c == 5) || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                
                if(oldRecsMap.get(l.Id).Status == SP5 )
                {
                    if(taskIsApex || l.Status == SP5 || (l.Status == SP6 && l.Number_of_Related_Activities__c == 6) || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                
                if(oldRecsMap.get(l.Id).Status == SP6 )
                {
                    if(taskIsApex || l.Status == SP6 || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture, Forecast, or Dead.');
                    }
                }
                

                if(oldRecsMap.get(l.Id).Status == NURTURE)
                {
                    if(taskIsApex || l.Status == NURTURE || l.Status == FORECAST || l.Status == QUALIFIED || l.Status == DEAD )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Forecast or Dead.');
                    }
                }
                

                if(oldRecsMap.get(l.Id).Status == FORECAST)
                {
                    if(taskIsApex || l.Status == FORECAST || l.Status == NURTURE || l.Status == QUALIFIED || l.Status == DEAD )
                    {}
                    else
                    {
                        l.addError('The status may only be manually updated to Nurture or Dead.');
                    }
                }
                

                /*if(oldRecsMap.get(l.Id).Status == CHANNEL)
                {
                    if(taskIsApex || l.Status == CHANNEL || l.Status == SP1 || l.Status == QUALIFIED || l.Status == DEAD || l.Status == NURTURE || l.Status == FORECAST)
                    {}
                    else
                    {
                        l.addError('The status may only be updated to Forecast, Nurture, Qualified or Dead.');
                    }
                }*/
                

                if(oldRecsMap.get(l.Id).Status == QUALIFIED )
                {
                    if(taskIsApex || l.Status == QUALIFIED || l.Status == NURTURE || l.Status == OPEN || l.Status == DEAD || l.Status == FORECAST)
                    {}
                    else
                    {
                        l.addError('The status may only be updated to Open, Forecast, Nurture or Dead.');
                    }
                }
                
                //if oldvalue was Dead, can only go to Open
                if(oldRecsMap.get(l.Id).Status == DEAD)
                {
                    if(taskIsApex || l.Status == DEAD)
                    {}
                    else
                    {
                        l.addError('The status may not be updated manually.');
                    }
                }
                
            }   
            }
        }
    }
    
}