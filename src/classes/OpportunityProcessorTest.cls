@isTest
private class OpportunityProcessorTest {

    public Id testContactId {get;set;}
    
    public static void updateExistingCustomSettings()
    {
        Apex_Code_Settings__c cs = Apex_Code_Settings__c.getInstance();
        cs.Oppty_Gartner_VR_Enabled__c = true;
        cs.Oppty_Gartner_VR_Msg__c = 'Gartner Error';
        cs.Oppty_Gartner_VR_Stage_Number__c = 4;

    }
    
    public static void setUpCustomSettings()
    {
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Oppty_Primary_Contact_Trigger_Enabled__c = true;
        cs.Oppty_Primary_Contact_Trigger_Msg__c = 'Some Error';
        cs.Oppty_Primary_Contact_Trigger_Stage__c = 2;
        cs.Oppty_Gartner_VR_Enabled__c = true;
        cs.Oppty_Gartner_VR_Msg__c = 'Gartner Error';
        cs.Oppty_Gartner_VR_Stage_Number__c = 2;
        cs.Oppty_Gartner_VR_Amount__c = 250000;
        cs.Account_Upsert_Address_Enabled__c = true;
        insert cs;
    }

    public static Account createAcct()
    {
        Account acct = new Account(Name = 'Some Account',
                               	ShippingStreet = '123 Main St',
                                ShippingCity = 'Washington',
                               	ShippingState = 'DC',
                               	ShippingPostalCode = '20001',
                                ShippingCountry = 'United States',
                               	Ship_To_Contact_Name__c = 'Joe',
                               	Ship_To_Contact_Phone__c = '867-5309',
                                Support_Must_Not_Contact_End_Customer__c = true);

        //Account acct = new Account(Name = 'Some Test Account', Support_Must_Not_Contact_End_Customer__c = true);
        insert acct;
        return acct;
    }
    
    public static Contact createCon()
    {
        Contact cont = new Contact (AccountId = createAcct().Id, FirstName = 'Some', LastName = 'Test', Email = 'sometest@sometest.test');
        insert cont;
        return cont;    
    }

    public static Opportunity createOppty()
    {
        Opportunity oppty = new Opportunity(AccountId = createAcct().Id, 
                                            Name = 'Some Test', 
                                            StageName = 'Stage 1 - Prequalified',
                                            CloseDate = System.today());
        insert oppty;
        return oppty;
    }
    
    public static PricebookEntry createPBE(string currCode)
    {
        Product2 p = new Product2(Name = 'Some Product');
        insert p;
        Pricebook2 pb = [select Id from Pricebook2 where isActive = true and isStandard = true limit 1];
        PricebookEntry pbe = new PricebookEntry(isActive = true, Product2Id = p.Id, Pricebook2Id = pb.Id, CurrencyIsoCode = currCode, UnitPrice = 250000.00);
        insert pbe;
        return pbe;
    }
    
    public static OpportunityLineItem createOLI (Id OpptyId, string currCode)
    {
        PricebookEntry pbe = createPBE(currCode);
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = OpptyId, PricebookEntryId = pbe.Id, Quantity = 1);
        oli.UnitPrice = pbe.UnitPrice;
        insert oli;
        return oli;
    }
    
    static testMethod void testOpportunityDefaultAddress()
    {
        test.startTest();
        setUpCustomSettings();
        Opportunity testoppty = createOppty();
        test.stopTest();
        Opportunity o = [select Id, Shipping_Address__c from Opportunity where Id =: testoppty.Id];
    	system.assertNotEquals(null, o.Shipping_Address__c);
        
    }
    
    static testMethod void testgetExchangeRateMap()
    {
        OpportunityProcessor op = new OpportunityProcessor();
        set<string> currencies = new set<string>();
        currencies.add('EUR');
        map<string,double> rates = new map<string,double>();
        rates = op.getExchangeRateMap(currencies);
        system.assert(rates != null);
        system.assert(rates.get('EUR') > 0);
    }
    
    @isTest(SeeAllData=true)
    static void testcheckGartnerRequiredFieldNegative()
    {
        string msg = '';
        updateExistingCustomSettings();
        Opportunity testoppty = createOppty();
        testoppty.CurrencyIsoCode = 'EUR';
        testoppty.HW_Eval_Agreement_Completed__c = 'Yes';
        testoppty.Replication_Required__c = 'Yes';
        testoppty.X23TB_required__c = 'Yes';
        testoppty.NextStep = 'Step 1';
        update testoppty;
        createEnvironmentRec(testoppty.Id, testoppty.AccountId);
        OpportunityLineItem testoli = createOLI(testoppty.Id, 'EUR');
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = testoppty.Id, ContactId = createCon().Id);
        insert ocr;
        testoli.Quantity = 10;
        update testoli;
        
        test.startTest();
            
            testoppty.StageName = 'Stage 4 - Qualified';
            
            try
            {
                update testoppty;
            }
            catch (DMLException e)
            {
                msg = e.getMessage();
            }
            system.assertNotEquals('',msg);
            system.assert(msg.contains(OpportunityProcessor.GARTNER_VR_MSG));
        test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    static void testcheckGartnerRequiredField()
    {
        string msg = '';
        updateExistingCustomSettings();
        Opportunity testoppty = createOppty();
        testoppty.SE_Opportunity_Owner__c = Userinfo.getUserId();
        testoppty.HW_Eval_Agreement_Completed__c = 'Yes';
        testoppty.Replication_Required__c = 'Yes';
        testoppty.X23TB_required__c = 'Yes';
        testoppty.NextStep = 'Step 1';
        update testoppty;
        
        OpportunityLineItem testoli = createOLI(testoppty.Id, 'USD');
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = testoppty.Id, ContactId = createCon().Id);
        insert ocr;
        createEnvironmentRec(testoppty.Id, testoppty.AccountId);
        test.startTest();
            
            testoppty.StageName = 'Stage 4 - Qualified';
            
            try
            {
                update testoppty;
            }
            catch (DMLException e)
            {
                msg = e.getMessage();
            }
            system.assertEquals('',msg);
            system.assert(!msg.contains(OpportunityProcessor.GARTNER_VR_MSG));
        test.stopTest();
    }
    
    static id createEnvironmentRec(id OppId, id AcctId)
    {
        Environment__c e = new Environment__c(Name = 'testEnv', 
                                       	Opportunity__c = OppId,
                                      	Account__c = AcctId,
                                      	Vendor_Product__c = 'None');
        insert e;
        return e.Id;
        
    }
    
    static testMethod void testgetStageNumber()
    {
        OpportunityProcessor op = new OpportunityProcessor();
        Integer val = op.getStageNumber('Stage 1 - bla');
        system.AssertEquals(1,val);
        val = op.getStageNumber('Stage 2 - bla');
        system.AssertEquals(2,val);
        val = op.getStageNumber('Stage 3 - bla');
        system.AssertEquals(3,val);
        val = op.getStageNumber('Stage 4 - bla');
        system.AssertEquals(4,val);
        val = op.getStageNumber('Stage 5 - bla');
        system.AssertEquals(5,val);
        val = op.getStageNumber('Stage 6 - bla');
        system.AssertEquals(6,val);
        val = op.getStageNumber('Stage 7 - bla');
        system.AssertEquals(7,val);
        val = op.getStageNumber('Stage 8 - bla');
        system.AssertEquals(8,val);
        val = op.getStageNumber('Stage 9 - bla');
        system.AssertEquals(0,val);
        val = op.getStageNumber('Stage 10 - bla');
        system.AssertEquals(0,val);
    }

    static testMethod void testOpportunityWithoutCons() 
    {
        string msg = '';
        setUpCustomSettings();
        Opportunity testoppty = createOppty();
        test.startTest();
            
            testoppty.StageName = 'Stage 2 - Qualified';
            try
            {
                update testoppty;
            }
            catch (DMLException e)
            {
                msg = e.getMessage();
            }
            system.assertNotEquals('',msg);
            system.assert(msg.contains(OpportunityProcessor.OCR_TRIGGER_MSG));
        test.stopTest();
    }
    
    static testMethod void testOpportunityClone()
    {
        test.startTest();
        string msg = 'success';
        setUpCustomSettings();
        Opportunity testoppty = createOppty();
        system.assertNotEquals('',testoppty.System_Id__c);
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = testoppty.Id, ContactId = createCon().Id, isPrimary = true, Role='Decision Maker');
        insert ocr;
        testoppty.StageName = 'Stage 2 - Qualified';
        testoppty.System_Id__c = (string.valueof(testoppty.id).substring(0,15));
        update testoppty;
        
            Opportunity newopp = testoppty.clone(false,false);
        try
        {    
            insert newopp;
        }
        catch(DMLException e)
        {
            msg = e.getMessage();
        }
        system.assertEquals('success',msg);
        
        list<OpportunityContactRole> ocrCloned = new list<OpportunityContactRole>([select Id, OpportunityId from OpportunityContactRole where OpportunityId =: newopp.Id]);
        system.assert(ocrCloned.size()>0);
        test.stopTest();
    }
    
    
    static testMethod void testOpportunityWithCons()
    {
        string msg = 'success';
        setUpCustomSettings();
        Opportunity testoppty = createOppty();
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = testoppty.Id, ContactId = createCon().Id);
        insert ocr;
        test.startTest();
            
            testoppty.StageName = 'Stage 2 - Qualified';
            try
            {
                update testoppty;
            }
            catch (DMLException e)
            {
                msg = e.getMessage();
            }
            system.assertNotEquals('',msg);
            system.assertEquals('success',msg);
        test.stopTest();
    }
}