public class QuoteProcessor
{
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static boolean QUOTE_AUTOSUBMIT_ENABLED = customSettings.Quote_Autosubmit_Enabled__c;
    public static final string PENDING_GVP = 'Approved - Pending GVP';
    public static final string PENDING_GVP_PRES = 'Approved - Pending GVP + Pres';
    public static boolean QUOTE_APPROVAL_COPY_ENABLED = customSettings.Quote_Approval_Copy_Enabled__c;

    public void copyApprovalDetails(Map<Id, Quote> oldMap, Map<Id, Quote> newMap)
    {
        if(QUOTE_APPROVAL_COPY_ENABLED)
        {
            set<id> quotesToProcess = new set<id>();
            //Check if Quote update is because of an Approval
                for(Quote aux : newMap.values()){
                    if(aux.Status != oldMap.get(aux.Id).Status && aux.Status == 'Approved'){
                        //toProcessQuotes.put(aux.Id, aux);
                        quotesToProcess.add(aux.Id);
                    }
                }
            if(quotesToProcess.size()>0)
                copyApprovalDetailsFuture(quotesToProcess);
        }
    }
    
    @future
    static void copyApprovalDetailsFuture(set<id> idsToProcess)
    {
        //moved this method to future because the approvals seem to be delayed in getting updates so sometimes they wont copy ugh
        //Map to hold which Quotes to process, we need it in case of Bulk updates
        Map<Id, Quote> toProcessQuotes = new Map<Id, Quote>();
        
        for(Quote q: [select Id, OpportunityId from Quote where id in: idsToProcess])
        {
            toProcessQuotes.put(q.Id, q);
        }
		if(toProcessQuotes.size()>0)
       	{
        	//List to hold all Approval_Process_Comments__c records before insert
            List<Approval_Process_Comments__c> toInsertApprovalProcesses = new List<Approval_Process_Comments__c>();
            
            //Check if tracking action needs to take place
            if(toProcessQuotes.keySet().size() > 0){
                system.debug('DEBUG:: toProcessQuotes: ' + toProcessQuotes);
                //delete copied comments first
                List<Approval_Process_Comments__c> existingCopiedComments = [select Id from Approval_Process_Comments__c where Quote__c in: toProcessQuotes.keySet()];
                system.debug('DEBUG:: existingCopiedComments.size: ' + existingCopiedComments.size());
                system.debug('DEBUG:: existingCopiedComments: ' + existingCopiedComments);
                if(existingCopiedComments.size()>0)
                    delete existingCopiedComments;
                
                List<ProcessInstance> theProcessInstances = [SELECT Id, TargetObjectId, SystemModstamp, (Select SystemModstamp, StepStatus, StepNodeId, ProcessInstanceId, OriginalActorId, Id, CreatedDate, CreatedById, Comments, ActorId From Steps  Where StepStatus = 'Approved' order by SystemModstamp) FROM ProcessInstance WHERE TargetObjectId IN : toProcessQuotes.keySet() order by SystemModstamp desc];
                system.debug('DEBUG:: theProcessInstances: ' + theProcessInstances);
                //Need Set In case there are many apprvals for the same Quote
                Set<Id> processedQuotes = new Set<Id>();
                for(ProcessInstance pi : theProcessInstances){
                    //Check if the quote was already processed
                    //if(!processedQuotes.contains(pi.TargetObjectId)){
                        processedQuotes.add(pi.TargetObjectId);
                        if(pi.Steps.size() > 0){
                            //Add only the first Step info which reflects last Approval processed approved for that Quote
                            //actually process them all
                            for(ProcessInstanceStep auxStep: pi.Steps)
                            {
                            //ProcessInstanceStep auxStep = pi.Steps[0];
                            	Approval_Process_Comments__c auxQuoteApproval = new Approval_Process_Comments__c();
                                auxQuoteApproval.Comments__c = auxStep.Comments;
                                auxQuoteApproval.Date_Time_of_the_approval__c = auxStep.SystemModstamp;
                            	auxQuoteApproval.Opportunity__c = toProcessQuotes.get(pi.TargetObjectId).OpportunityId;
                            	auxQuoteApproval.Quote__c = pi.TargetObjectId;
                            	auxQuoteApproval.Approver__c = auxStep.ActorId;
                            	toInsertApprovalProcesses.add(auxQuoteApproval);
                            }
                        }
                    //}
                }
                //Insert All Approval_Process_Comments__c records
                if(toInsertApprovalProcesses.size() > 0){
                    insert toInsertApprovalProcesses;
                }
            }
        }
    }
    
    public void processRecords(List<Quote> newRecs, List<Quote> oldRecs, Map<Id,Quote> newMap, Map<Id,Quote> oldMap)
    {
        list<Quote> recsToProcess = new list<Quote>();
        
        for(Quote q: newRecs)
        {
            if(oldMap == null)//inserts
            {
                if(newMap.get(q.Id).Status == PENDING_GVP || newMap.get(q.Id).Status == PENDING_GVP_PRES)
                    recsToProcess.add(q);
            }
                
            
            if(oldMap != null)//updates
            {
                if(oldMap.get(q.Id).Status != newMap.get(q.Id).Status)
                {
                    if(newMap.get(q.Id).Status == PENDING_GVP || newMap.get(q.Id).Status == PENDING_GVP_PRES)
                        recsToProcess.add(q);
                }
            }   
        }
        
        if(recsToProcess.size()>0)
        {
            submitQuote(recsToProcess);
        }    
    }
    
    
    public void submitQuote(list<Quote> inputRecs)
    {
        if(QUOTE_AUTOSUBMIT_ENABLED)
        {
            for(Quote q: inputRecs)
            {
                Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
                app.setObjectId(q.id);
                Approval.ProcessResult result = Approval.process(app);    
            }
        }
    }
    
    
    /*for (Quote q : trigger.new) 
    {
        if(trigger.OldMap.get(q.id).Status != trigger.NewMap.get(q.Id).Status && trigger.Newmap.get(q.Id).Status == '1st Approval')
        {
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
    
            app.setObjectId(q.id);
    
            Approval.ProcessResult result = Approval.process(app);
        }
    }*/
    
    /*public void processRecords (List<QuoteLineItem> newRecs, List<QuoteLineItem> oldRecs, Map<Id,QuoteLineItem> newMap, Map<Id,QuoteLineItem> oldMap)
    {
        list<QuoteLineItem> recsToProcess = new list<QuoteLineItem>();
        
        
        
        
        for(QuoteLineItem qli: newRecs)
        {
            if(oldMap == null)
                recsToProcess.add(qli);
            
            if(oldMap != null)
            {
                if(oldMap.get(qli.Id).TotalPrice != newMap.get(qli.Id).TotalPrice)
                {
                    recsToProcess.add(qli);
                }
            }   
        }
        
        if(recsToProcess.size()>0)
            updateQLI(recsToProcess);        
    }*/
    
    /*
    public void updateQLI(list<QuoteLineItem> inputRecs)
    {
        if(QLI_CONVERTED_AMT_ENABLED)
        {
            
            set<string> currencies = new set<string>();
            for(QuoteLineItem qli: inputRecs)
            {
                currencies.add(qli.CurrencyIsoCode);
            }
            
            list<QuoteLineItem> qliForUpdate = new list<QuoteLineItem>();
            map<string,double> exchRatesMap = getExchangeRateMap(currencies);
            for(QuoteLineItem qli: inputRecs)
            {
                if(qli.TotalPrice != null)
                {
                    qli.Converted_Amount_USD__c = qli.TotalPrice / exchRatesMap.get(qli.CurrencyIsoCode);
                    qliForUpdate.add(qli);
                }
            
            }
        }
    }*/
    
    /*
    public map<string,double> getExchangeRateMap(set<string> inputCurrency)
    {
        map<string,double> retExchRates = new map<string,double>();
        for(CurrencyType ct: [select IsoCode, ConversionRate FROM CurrencyType where isActive = true and IsoCode in: inputCurrency])
        {
            retExchRates.put(ct.IsoCode, ct.ConversionRate);
        }
        system.debug('DEBUG:: retExchRates: ' + retExchRates);
        return retExchRates;
    }*/
}