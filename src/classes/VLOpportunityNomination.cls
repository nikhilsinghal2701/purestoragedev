global class VLOpportunityNomination {
    global static List<Feedback__c> opportunityNomination(Long lgSeed, ID idOpportunity, OpportunityContactRole[] arrOppContactRole, String strDataCollectionId,String strDataCollectionName){
        List<Feedback__c> listFeedback_e = [select Id from Feedback__c where Opportunity__c =: idOpportunity];
        List<Feedback__c> lstFeedback = new List<Feedback__c>();
        if(listFeedback_e.isEmpty()) {            
            for(OpportunityContactRole oppContactRole: arrOppContactRole){                
                if(oppContactRole.OpportunityId == idOpportunity){
                    Feedback__c feedback = new Feedback__c();  
                    feedback.Name = 'P_' + lgSeed;
                    feedback.Opportunity__c = idOpportunity;
                    feedback.Contact__c = oppContactRole.ContactID;                
                    feedback.DataCollectionId__c = strDataCollectionId;                       
                    feedback.DataCollectionName__c = strDataCollectionName;
                    feedback.Status__c = 'Nominated'; 
                    lstFeedback.add(feedback);              
                }
            }          
        }
        return lstFeedback;
    }
}