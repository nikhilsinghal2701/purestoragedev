global class VLStatusPartySchedulerContext implements Schedulable{
    global VLStatusPartySchedulerContext(){}
    global void execute(SchedulableContext ctx){
        VLStatusBatchParty b = new VLStatusBatchParty();
        database.executebatch(b,1);        
    }
    static testMethod void testExecute() {
    Test.startTest();
    VLStatusPartySchedulerContext schedulerContext= new VLStatusPartySchedulerContext();
    String schedule = '0 0 6 * * ?';
    system.schedule('Scheduled Party Update', schedule, schedulerContext);
    test.stopTest();
    }
}