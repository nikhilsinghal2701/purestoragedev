/** 
* Class for parsing CSV and return results. Do validations and Return leads
* @author Nikhil Singhal 
* @created Date:11/15/2013 
* */
public with sharing class PRM_MDF_ParseCsvtoLead {
	
public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
	List<List<String>> allFields = new List<List<String>>();

	// replace instances where a double quote begins a field containing a comma
	// in this case you get a double quote followed by a doubled double quote
	// do this for beginning and end of a field
	contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
	// now replace all remaining double quotes - we do this so that we can reconstruct
	// fields with commas inside assuming they begin and end with a double quote
	contents = contents.replaceAll('""','DBLQT');
	// we are not attempting to handle fields with a newline inside of them
	// so, split on newline to get the spreadsheet rows
	List<String> lines = new List<String>();
	try {
		lines = contents.split('\n');
	} catch (System.ListException e) {
		System.debug('Limits exceeded?' + e.getMessage());
	}
	Integer num = 0;
	for(String line : lines) {
		// check for blank CSV lines (only commas)
		if (line.replaceAll(',','').trim().length() == 0) break;
		
		List<String> fields = line.split(',');	
		List<String> cleanFields = new List<String>();
		String compositeField;
		Boolean makeCompositeField = false;
		for(String field : fields) {
			if (field.startsWith('"') && field.endsWith('"')) {
				cleanFields.add(field.replaceAll('DBLQT','"'));
			} else if (field.startsWith('"')) {
				makeCompositeField = true;
				compositeField = field;
			} else if (field.endsWith('"')) {
				compositeField += ',' + field;
				cleanFields.add(compositeField.replaceAll('DBLQT','"'));
				makeCompositeField = false;
			} else if (makeCompositeField) {
				compositeField +=  ',' + field;
			} else {
				cleanFields.add(field.replaceAll('DBLQT','"'));
			}
		}
		
		allFields.add(cleanFields);
	}
	if (skipHeaders) allFields.remove(0);
	return allFields;		
}

public static string validate(List<String> str,map<string,id> leadMap) {
	 String status = label.PRM_MDF_LeadOk;
	 if(str[1] == null || str[1] == '' || str[1].isWhitespace()){
	 	status=System.label.PRM_MDF_LeadNameBlank; 
	 }else if(str[2] == null || str[2] == '' || str[2].isWhitespace()){
	 	status=System.label.PRM_MDF_LeadBlank; 
	 }else if(!checkEmailFormat(str[2].trim())){
	 	status=System.label.PRM_MDF_LeadInvalidEmail; 
	 }else if(!leadMap.isEmpty()){
	 	if(leadMap.containskey(str[2].toUpperCase().trim())){
	 	status = System.label.PRM_MDF_DuplicateLead;	
	 	}else{
	 	status = System.label.PRM_MDF_LeadOk;
	 	}
	 }else{
	 	status = System.label.PRM_MDF_LeadOk;
	 }
	 return status;
}

public static Boolean checkEmailFormat(String email) {
        String emailRegEx = '[a-zA-Z0-9\\.\\!\\#\\$\\%\\&\\*\\/\\=\\?\\^\\_\\+\\-\\`\\{\\|\\}\\~\'._%+-]+@[a-zA-Z0-9\\-.-]+\\.[a-zA-Z]+';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        Boolean result = MyMatcher.matches();
        return result;
    }

}