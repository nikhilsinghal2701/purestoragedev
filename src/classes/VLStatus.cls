global class VLStatus
{
    global static void FeedbackUpdate(String strFbkId)
    {
        Feedback__c fbk = new Feedback__c();
        
        fbk = [Select Feedback__c.Contact__c,Feedback__c.Name,Feedback__c.DataCollectionId__c, Feedback__c.Opportunity__c, Feedback__c.ZD_Ticket_Number__c, Feedback__c.ZD_Ticket_CSR_Name__c, Feedback__c.ZD_Severity_Level__c, Feedback__c.ZD_No_Days_to_Resolve__c  from Feedback__c where Feedback__c.Name =: strFbkId];
                        
        String strStatus = '';
        String strXPEnterpriseID = '';
        String strCreateParticipantURL = '';
        String strXPServer = '';
        String strXPSecurityToken = '';
        
        if(!Test.isRunningTest()){
            strXPEnterpriseID = SMXConfiguration__c.getValues('Configuration').ID_XP_ENTERPRISE__c;
            strXPSecurityToken = SMXConfiguration__c.getValues('Configuration').XP_SECURITYTOKEN__c;
            strXPServer = SMXConfiguration__c.getValues('Configuration').XP_SERVER__c;
            strCreateParticipantURL= SMXConfiguration__c.getValues('Configuration').URL_CREATE_PARTICIPANT_SERVICE__c;
        }
        Contact c = [select Name,Account.Id, Email,FirstName,LastName,Salutation,Title,Department,Phone,MobilePhone from Contact where Id =: fbk.Contact__c];
        Account a = [select Id,Name,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Type from Account where Id =: c.Account.Id ];
        
        Datetime currentDate = System.now();
        
        String strCurrentDate = currentDate.format('yyyy-MM-dd') + 'T' + currentDate.format('HH:mm:ss') + '.00';
       
       //create or update party
        //code that does callout stuff and sets the fields
        HttpRequest req = new HttpRequest();
        req.setTimeout(120000);

        req.setMethod('POST');
        req.setHeader('Host',strXPServer);
        
        req.setHeader('Connection','keep-alive');
        req.setEndpoint(strCreateParticipantURL);
        
        req.setHeader('Content-Type','text/xml');
        req.setHeader('DataEncoding','UTF-8');
        String strXML = '';
        
        if(fbk.Opportunity__c != null){
        System.debug('Processing a win/loss survey for' + fbk.Opportunity__c);
        // Opportunity oppty = [select Opportunity.Owner.Username, Opportunity.Environment__c from Opportunity where Opportunity.Name =: fbk.Opportunity__c];  
        Opportunity oppty = [select Opportunity.Owner.Username, Opportunity.Environment__c, Opportunity.StageName from Opportunity where Id =: fbk.Opportunity__c];  
        String strStageName='Won';
        
        if (oppty.StageName == 'Stage 8 - Closed/Won')
        {
         strStageName='Won';
        }
       
        else if (oppty.StageName=='Stage 8 - Closed/Lost')
        {
        strStageName='Lost';
        }
        else
        strStageName='';
     
        strXML = '<?xml version="1.0" encoding="utf-8"?>' +
                        '<utilService><companyIdfier>'+
                        + strXPEnterpriseID +
                        '</companyIdfier>'+        
                        '<securityToken>'+
                        + strXPSecurityToken +
                        '</securityToken>' + 
                        '<sendMail>Y</sendMail>'+
                        '<communicationType>INVITATION</communicationType>'+
                        '<fbkProvider announcementCommSentYN="Y" companyIdfier="'+a.Id+'" commBouncedYN="N" dataFormStartedYN="N" dataFormSubmittedYN="N" datacollectionIdfier="'+fbk.DataCollectionId__c+'" defaultFbkLocaleCode="en_US" enabledYN="Y" fbkLocaleSelectEnabledYN="Y" followupContactPartyIdfier="CRM" invitationCommSentYN="N" markedToResendCommYN="N" optedOutYN="N" personIdfier="'+fbk.Contact__c+'" preferTextOnlyEmailYN="N" preferredFbkMediumTypeCode="EMAIL" primaryAddrCityName="'+escapeXML(a.BillingCity)+'" primaryAddrCountryCode="'+escapeXML(a.BillingCountry)+'" primaryAddrLine1="'+escapeXML(a.BillingStreet)+'" primaryAddrLine2="" primaryAddrPostalCode="'+escapeXML(a.BillingPostalCode)+'" primaryAddrStateCode="'+escapeXML(a.BillingState)+'" primaryEmailAddrText="'+c.Email+'" primaryPhoneNo="'+escapeXML(c.Phone)+'" primaryMobilePhoneNo="'+escapeXML(c.MobilePhone)+'" providerIdfier="'+fbk.Name+'" reminder1CommSentYN="N" reminderCommSentYN="N" thankyouCommSentYN="N" personIdentifierTypeCode="PERSON_ID" personIdentifier="'+fbk.Contact__c+'">'+
                        '<fbkProviderT-list>'+  
                        '<fbkProviderT localeCode="en_US" nameT="'+escapeXML(c.FirstName)+' '+escapeXML(c.LastName)+'" personFNameT=" '+ escapeXML(c.FirstName)+'" personLNameT=" '+escapeXML(c.LastName)+'" personMNameT="" personNameSuffixT="" personPrimaryJobTitleT=" '+escapeXML(c.Title) +'" personSalutationT=" '+escapeXML(c.Salutation)+'" sourceLocaleCode="en_US" translatedTimestamp="'+strCurrentDate+'"/>'+
                        '</fbkProviderT-list>'+
                        '<fbkProviderX-list>'+  
                        '<fbkProviderX enabledYN="Y" String1="'+oppty.Owner.Username+'" String2="'+oppty.Environment__c+'" String6="'+strStageName+'" String300="'+fbk.Contact__c+'" String299="'+a.Id+'"/>'+
                        '</fbkProviderX-list></fbkProvider></utilService>';
        }else if (fbk.ZD_Ticket_Number__c != null || fbk.ZD_Ticket_Number__c != '') {
        strXML = '<?xml version="1.0" encoding="utf-8"?>' +
                        '<utilService><companyIdfier>'+
                        + strXPEnterpriseID +
                        '</companyIdfier>'+        
                        '<securityToken>'+
                        + strXPSecurityToken +
                        '</securityToken>' + 
                        '<sendMail>Y</sendMail>'+
                        '<communicationType>INVITATION</communicationType>'+
                        '<fbkProvider announcementCommSentYN="Y" companyIdfier="'+a.Id+'" commBouncedYN="N" dataFormStartedYN="N" dataFormSubmittedYN="N" datacollectionIdfier="'+fbk.DataCollectionId__c+'" defaultFbkLocaleCode="en_US" enabledYN="Y" fbkLocaleSelectEnabledYN="Y" followupContactPartyIdfier="CRM" invitationCommSentYN="N" markedToResendCommYN="N" optedOutYN="N" personIdfier="'+fbk.Contact__c+'" preferTextOnlyEmailYN="N" preferredFbkMediumTypeCode="EMAIL" primaryAddrCityName="'+escapeXML(a.BillingCity)+'" primaryAddrCountryCode="'+escapeXML(a.BillingCountry)+'" primaryAddrLine1="'+escapeXML(a.BillingStreet)+'" primaryAddrLine2="" primaryAddrPostalCode="'+escapeXML(a.BillingPostalCode)+'" primaryAddrStateCode="'+escapeXML(a.BillingState)+'" primaryEmailAddrText="'+c.Email+'" primaryPhoneNo="'+escapeXML(c.Phone)+'" primaryMobilePhoneNo="'+escapeXML(c.MobilePhone)+'" providerIdfier="'+fbk.Name+'" reminder1CommSentYN="N" reminderCommSentYN="N" thankyouCommSentYN="N" personIdentifierTypeCode="PERSON_ID" personIdentifier="'+fbk.Contact__c+'">'+
                        '<fbkProviderT-list>'+  
                        '<fbkProviderT localeCode="en_US" nameT="" personFNameT=" '+ escapeXML(c.FirstName)+'" personLNameT=" '+escapeXML(c.LastName)+'" personMNameT="" personNameSuffixT="" personPrimaryJobTitleT=" '+escapeXML(c.Title) +'" personSalutationT=" '+escapeXML(c.Salutation)+'" sourceLocaleCode="en_US" translatedTimestamp="'+strCurrentDate+'"/>'+
                        '</fbkProviderT-list>'+
                        '<fbkProviderX-list>'+  
                        '<fbkProviderX enabledYN="Y" String3="'+fbk.ZD_Ticket_CSR_Name__c+'"  String4="'+fbk.ZD_No_Days_to_Resolve__c+'"  String5="'+fbk.ZD_Severity_Level__c+'"  String300="'+fbk.Contact__c+'" String299="'+a.Id+'"/>'+
                        '</fbkProviderX-list></fbkProvider></utilService>';
        }else{
        strXML = '<?xml version="1.0" encoding="utf-8"?>' +
                        '<utilService><companyIdfier>'+
                        + strXPEnterpriseID +
                        '</companyIdfier>'+        
                        '<securityToken>'+
                        + strXPSecurityToken +
                        '</securityToken>' + 
                        '<sendMail>Y</sendMail>'+
                        '<communicationType>INVITATION</communicationType>'+
                        '<fbkProvider announcementCommSentYN="Y" companyIdfier="'+a.Id+'" commBouncedYN="N" dataFormStartedYN="N" dataFormSubmittedYN="N" datacollectionIdfier="'+fbk.DataCollectionId__c+'" defaultFbkLocaleCode="en_US" enabledYN="Y" fbkLocaleSelectEnabledYN="Y" followupContactPartyIdfier="CRM" invitationCommSentYN="N" markedToResendCommYN="N" optedOutYN="N" personIdfier="'+fbk.Contact__c+'" preferTextOnlyEmailYN="N" preferredFbkMediumTypeCode="EMAIL" primaryAddrCityName="'+escapeXML(a.BillingCity)+'" primaryAddrCountryCode="'+escapeXML(a.BillingCountry)+'" primaryAddrLine1="'+escapeXML(a.BillingStreet)+'" primaryAddrLine2="" primaryAddrPostalCode="'+escapeXML(a.BillingPostalCode)+'" primaryAddrStateCode="'+escapeXML(a.BillingState)+'" primaryEmailAddrText="'+c.Email+'" primaryPhoneNo="'+escapeXML(c.Phone)+'" primaryMobilePhoneNo="'+escapeXML(c.MobilePhone)+'" providerIdfier="'+fbk.Name+'" reminder1CommSentYN="N" reminderCommSentYN="N" thankyouCommSentYN="N" personIdentifierTypeCode="PERSON_ID" personIdentifier="'+fbk.Contact__c+'">'+
                        '<fbkProviderT-list>'+  
                        '<fbkProviderT localeCode="en_US" nameT="" personFNameT=" '+ escapeXML(c.FirstName)+'" personLNameT=" '+escapeXML(c.LastName)+'" personMNameT="" personNameSuffixT="" personPrimaryJobTitleT=" '+escapeXML(c.Title) +'" personSalutationT=" '+escapeXML(c.Salutation)+'" sourceLocaleCode="en_US" translatedTimestamp="'+strCurrentDate+'"/>'+
                        '</fbkProviderT-list>'+
                        '<fbkProviderX-list>'+  
                        '<fbkProviderX enabledYN="Y" String300="'+fbk.Contact__c+'" String299="'+a.Id+'"/>'+
                        '</fbkProviderX-list></fbkProvider></utilService>';
        }
        
        req.setBody(strXML);

        Http http = new Http();
        
        try
        {
            HTTPResponse httpResponse;
            if(!Test.isRunningTest()){
             httpResponse = http.send(req);
             }else{
             httpResponse = new HttpResponse();
             httpResponse.setBody(returnTestXML());
             httpResponse.setStatusCode(testHttpStatusCode);             
            }
            if ((httpResponse.getStatusCode()) == 200)
            {
                String xmlString = httpResponse.getBody();
                XMLDom doc= new XMLDom(xmlString);
                XMLDom.Element compDataXML = (XMLDom.Element)doc.getElementByTagName('webserviceresponse');
                if(compDataXML != null)
                {
                    String strResultCode, strResultDescription,resultValue;
                    XMLDom.Element code = (XMLDom.Element)compDataXML.getElementByTagName('code');
                    if(code != null)
                    strResultCode= code.nodeValue;
                    XMLDom.Element description = (XMLDom.Element)compDataXML.getElementByTagName('description');
                    if(description != null)
                    {
                        strResultDescription= description.nodeValue;
                        Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                        feedback.Status__c = 'Success';
                        feedback.StatusDescription__c = strResultDescription;
                        update feedback;
                        strStatus = 'Success';                        
                    }
                    if (strResultCode == '0')
                    {
                        for(XMLdom.Element ee:compDataXML.getElementsByTagName('row'))
                        {
                            resultValue = ee.getAttribute('value');
                        }
                        if (resultValue == 'No Send Rule is applied for the provider')
                        {
                            Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                            feedback.Status__c = 'Failure';
                            feedback.StatusDescription__c = 'No send rule applied';
                            update feedback;
                            strStatus = 'Success';
                            strStatus = 'Failure';
                            
                        }
                        if (resultValue != 'No Send Rule is applied for the provider')
                        {
                            Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                            feedback.Status__c = 'Success';
                            feedback.StatusDescription__c = 'Participant Created';
                            update feedback;
                            strStatus = 'Success';                            
                        }
                    }
                    if (strResultCode <> '0')
                    {
                        Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                        
                        feedback.Status__c = 'Failure';
                        feedback.StatusDescription__c = strResultDescription;
                        update feedback;
                        strStatus = 'Failure';                        
                    }
                }
            }
            if ((httpResponse.getStatusCode()) <> 200)
            {
                String strMessage = httpResponse.getStatus();
                Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
                
                feedback.Status__c = 'Failure';
                feedback.StatusDescription__c = strMessage;
                update feedback;
                strStatus = 'Failure';
                
            }
        }
        catch(System.CalloutException e)
        {
            
            String strMessage = e.getMessage();
            Feedback__c feedback = [select Name, Status__c,StatusDescription__c from Feedback__c where Name = :fbk.Name];
            
            feedback.Status__c = 'Failure';
            feedback.StatusDescription__c = strMessage;
            update feedback;
            strStatus = 'Failure';
            
        }
    }
   
    public static String escapeXML(String strInput){
          String strReturnValue = '';
          if (strInput != null && strInput != '')
          {
            strInput = strInput.replace('"','&quot;');
            strInput = strInput.replace('<','&lt;');
            strInput = strInput.replace('>','&gt;');
            strInput = strInput.replace('&','&amp;');    
            strReturnValue = strInput;
          }
          return strReturnValue;  
        }


   /******** test methods ******/
    static Integer testCaseNum;
    static Integer testHttpStatusCode = 200;
    
    public static String returnTestXML(){
      if(testCaseNum == 1){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>0</code><description></description><row value="success"></row></webserviceresponse>';
      }else if(testCaseNum  == 2){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>0</code><description></description><row value="No Send Rule is applied for the provider"></row></webserviceresponse>';        
      }else if(testCaseNum  == 3){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }else if(testCaseNum  == 4){
        testHttpStatusCode = 404;
        return '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }
      return '';
    }
    public static String escapeForCompanyEmail(String strInput){
          String strReturnValue = '';
          if (strInput != null && strInput != '')
          {
            strInput = strInput.replace('"','');
            strInput = strInput.replace('<','');
            strInput = strInput.replace('>','');
            strInput = strInput.replace('&','&amp;');  
            strInput = strInput.replace(' ','');  
            strInput = strInput.replace('.','');  
            strInput = strInput.replace(',','');   
            strReturnValue = strInput;
          }
          return strReturnValue;  
        }
        
   @isTest(SeeAllData=true)
   static void testFeedbackUpdate(){   
    String strFeedbackID = prepareTestData();
    for(Integer i = 1; i<=4; i++){
        testCaseNum = i;
        FeedbackUpdate(strFeedbackID);
        }    
       cleanupTestData(); 
    }
      
 static String prepareTestData()
  {
    Account a = new Account(Name='SMX Test Account',BillingCity='BillingCity',BillingCountry = 'BillingCountry',BillingPostalCode = 'BillingPostalCode',BillingState = 'BillingState',BillingStreet = 'BillingStreet',Type = 'Type');
    insert a;
    
    Contact c = new Contact(FirstName='SMX TestFName1', LastName='SMX TestLName1', AccountID=a.id, Email='this.is.a.smx.test@verafin.com', Salutation = 'Mr', Title = 'Title',Department ='Department',Phone='9999999',MobilePhone='11111111');
    insert c;    
        
    List <Feedback__c> feedbackList = new List<Feedback__c>();
    Feedback__c feedback = new Feedback__c();
    feedback.Name = 'TEST_CRM_12345';
    feedback.Contact__c = c.Id; //ContactName
    feedback.DataCollectionId__c = '123456';
    feedback.Status__c = 'Test_Nominated';               
    feedback.DataCollectionName__c = 'Test Survey Name';
    feedback.PrimaryScore__c = 9;
    feedback.PrimaryComment__c = 'Test comment';
    feedback.Status__c = 'Test Status';
    feedback.StatusDescription__c = 'Test Description';
    feedback.SurveyDetailsURL__c = '';
    feedbackList.add(feedback);    

    insert feedbackList;    
    
    return feedback.Name;
  }  
  
  static void cleanupTestData(){
   
   Contact c = [SELECT Id FROM CONTACT WHERE FirstName = 'SMX TestFName1']; 
   delete c;
   
   Account a = [SELECT Id FROM ACCOUNT WHERE NAME='SMX Test Account'];
   delete a;

  }
   
}