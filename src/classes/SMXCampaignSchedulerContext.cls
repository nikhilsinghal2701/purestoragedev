global class SMXCampaignSchedulerContext implements Schedulable{
    global SMXCampaignSchedulerContext(){}
    global void execute(SchedulableContext ctx){
        SMXProcessCampaignBatch b = new SMXProcessCampaignBatch();
        database.executebatch(b);        
    }
    
}