/*****************************************************************************
 * Description : Purpose of this class is to test the List Controller for Partner Locator functionality
 * Author      : Sriram Swaminathan
 * Date        : 07/30/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
@isTest
private class PartnerLocatorListControllerTest {
	
	@isTest 
	static void listTest() {

		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);

		PartnerLocatorSearchController searchCtrl = new PartnerLocatorSearchController();
		searchCtrl.navigateToSearch();
		String country = 'United States';
		searchCtrl.partnerLocatorSearchBean.selectedCountry = country;
		searchCtrl.search();

		PartnerLocatorListController listCtrl = new PartnerLocatorListController();
		listCtrl.partnerLocatorSearchController = searchCtrl;
		Integer count = getCount(' PRIMARY_LOCATION_COUNTRY__C = \'' + country + '\'' );
		System.assertEquals(count
							, listCtrl.recordSize
							, 'Expected count of Partner Locator List records for search by country ' + country + ' is ' +  count);					
		System.assertEquals(TRUE
							, listCtrl.pageRecords.size()>0
							, 'Expected count of Page records for search by country ' + country + ' is greater than zero');
		System.assertEquals(FALSE
							, listCtrl.hasPrevious
							, 'Expected to be on the first page of the list');	
		System.assertEquals(TRUE
							, listCtrl.totalPageNumber>0
							, 'Expected the total page number for search by country ' + country + ' is greater than zero');		
		listCtrl.onCountryChange();
		listCtrl.onStateOrProvinceChange();	
		listCtrl.selectedPartnerLocatorName = listCtrl.pageRecords.get(0).name;
		listCtrl.navigateToDetails();
		System.assertEquals(listCtrl.selectedPartnerLocatorName
							, searchCtrl.partnerLocatorBean.name
							, 'Expected the Partner Locator bean to be ' + listCtrl.selectedPartnerLocatorName);
		String primaryLocation = searchCtrl.partnerLocatorBean.getPrimaryLocation();
		String contactLocation = searchCtrl.partnerLocatorBean.getContactLocation();
		String partnerLogo = searchCtrl.partnerLocatorBean.getPartnerLogo();
		listCtrl.lastPage();
		listCtrl.nextPage();
		listCtrl.goToPage();
		listCtrl.previousPage();
		listctrl.firstPage();		
	}
	
	/*
	* Return count for the input filter criteria
	*/
	private static Integer getCount(String filterSql) {
		Integer cnt = 0;
		String sql = ' SELECT COUNT(ID) cnt FROM PARTNER_LOCATOR_DETAILS__C '
					// + ' WHERE PARTNER_NAME__C LIKE \'' +  PartnerLocatorTestData.PARTNER_NAME_PREFIX + '%' + '\''
					+ ' WHERE ' + ' STATUS__C = \'Published\' ' + ' AND ' + filterSql;
		for (AggregateResult result: Database.query(sql)){
			cnt =  (Integer)result.get('cnt');
		}
		return cnt;
	} 
	
}