global class VLStatusSchedulerContextOpportunity implements Schedulable{
    global VLStatusSchedulerContextOpportunity(){}
    global void execute(SchedulableContext ctx){
        VLStatusBatchOpportunity b = new VLStatusBatchOpportunity();
        database.executebatch(b);        
    }
    static testMethod void testExecute() {
    Test.startTest();
    VLStatusSchedulerContextOpportunity schedulerContext= new VLStatusSchedulerContextOpportunity ();
    String schedule = '0 0 4 * * ?';
    system.schedule('Scheduled Opportunity Update', schedule, schedulerContext);
    test.stopTest();
    }
}