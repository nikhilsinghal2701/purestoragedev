global with sharing class SC_CustomerLoginController 
{
    global String username {get; set;}
    global String password {get; set;}

    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        system.debug('------> '+ username + '   ' + password);
        return Site.login(username, password, startUrl);
    }

   global SC_CustomerLoginController () {}

}