@isTest
private class ZenTest{

    static testMethod void zenTestComponents(){
        
        
        ZenConfiguration__c configuration = new ZenConfiguration__c();
        configuration = new ZenConfiguration__c();
        configuration.Zendesk_Username__c = 'testUser';
        configuration.Zendesk_Password__c = 'testPassword';
        configuration.Zendesk_URL__c = 'http://testURL.com';
        insert configuration;
        
        ZenConfigurationViewController configurationController = new ZenConfigurationViewController(); 
        
        ZenConfiguration__c configurationInstance = configurationController.configuration;        
        System.assertEquals(configurationInstance.Zendesk_Username__c,'testUser');
        
        configurationInstance.Zendesk_Username__c = 'testUserNew';
        configurationController.save();
        System.assertEquals(configurationInstance.Zendesk_Username__c,'testUserNew');
         
        //////////////////////////////////////////////////////////////////////////////////////////////// 
            
        PageReference pageRef = Page.ZenTicketList;
        Test.setCurrentPageReference(pageRef);
        
        ZenTicketListViewController ticketListViewController = new ZenTicketListViewController();
        ticketListViewController.isApexTest = true;
        
        List<ZenTicket> tickets = ticketListViewController.getTickets();
        
        ticketListViewController.sortExpression='';        
        System.assertEquals(ticketListViewController.getSortDirection(),'desc');
        
        ticketListViewController.sortExpression='created_at';        
        System.assertEquals(ticketListViewController.getSortDirection(),'asc');
        System.assertEquals(ticketListViewController.sortExpression,'created_at');
        
        ticketListViewController.sortExpression='created_at';
        System.assertEquals(ticketListViewController.getSortDirection(),'desc');
        
        ticketListViewController.setSortDirection('asc');
        System.assertEquals(ticketListViewController.getSortDirection(),'asc');
        
        ticketListViewController.reloadData();
        
        System.assertEquals(ticketListViewController.getPageNumber() ,1);  
        System.assertEquals(ticketListViewController.getPageSize() ,15);
        System.assertEquals(ticketListViewController.getTotalPageNumber () ,2);
        
        ticketListViewController.setSevLevel(':4');
        System.assertEquals(ticketListViewController.getSevLevel() ,':4');
        
        ticketListViewController.setStatusLevel(':4');
        System.assertEquals(ticketListViewController.getStatusLevel() ,':4');
        
        System.assertEquals(ticketListViewController.getPreviousButtonEnabled() ,true);
        System.assertEquals(ticketListViewController.getNextButtonDisabled() ,false);
        
        ticketListViewController.nextBtnClick();
        ticketListViewController.previousBtnClick();
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        
        PageReference pageRef2 = Page.ZenTicketDetails;
        Test.setCurrentPageReference(pageRef2);
        
        ZenTicketDetailsViewController ticketDetailsViewController = new ZenTicketDetailsViewController();
        ZenTicket ticket = ticketDetailsViewController.ticket;
        ticket.comments.get(0).getCreatedAt();
        System.assertEquals(ticket.comments.get(0).getValue(),'test'); 
        
        System.assertEquals(ticket.getDescription(),'test');
        
        ticket.priority = 'low';        
        System.assertEquals(ticket.getPriority(),'SEV 4');
        
        ticket.priority = 'normal';        
        System.assertEquals(ticket.getPriority(),'SEV 3');
        
        ticket.priority = 'high';        
        System.assertEquals(ticket.getPriority(),'SEV 2');
        
        ticket.priority = 'urgent';        
        System.assertEquals(ticket.getPriority(),'SEV 1');
        
        ticket.priority = '';        
        System.assertEquals(ticket.getPriority(),'Undefined');
        
        ticket.type = 'question';        
        System.assertEquals(ticket.getType(),'Question');
        
        ticket.type = 'incident';        
        System.assertEquals(ticket.getType(),'Incident');
        
        ticket.type = 'problem';        
        System.assertEquals(ticket.getType(),'Problem');
        
        ticket.type = 'task';        
        System.assertEquals(ticket.getType(),'Task');
        
        ticket.type = '';        
        System.assertEquals(ticket.getType(),'Undefined');
        
        ticket.status = 'new';
        System.assertEquals(ticket.getStatus(),'Open');
        
        ticket.status = 'open';        
        System.assertEquals(ticket.getStatus(),'Open');
        
        ticket.status = 'pending';        
        System.assertEquals(ticket.getStatus(),'Open');
        
        ticket.status = 'solved';        
        System.assertEquals(ticket.getStatus(),'Closed');
        
        ticket.status = 'closed';        
        System.assertEquals(ticket.getStatus(),'Closed');
        
        ticket.status = '';        
        System.assertEquals(ticket.getStatus(),'Undefined');
        
        ticket.getUpdatedAt();
        ticket.getCreatedAt();
         
       //////////////////////////////////////////////////////////////////////////////////////////////// 
            
        PageReference pageRef3 = Page.ZenTicketListForAccount;
        Test.setCurrentPageReference(pageRef3);
        
        Account account = new Account (name='Test Organization',Zendesk_Organization__c='Test Organization',Zendesk_Organization_Id__c = '20597908');

        insert account;
        
        ApexPages.StandardController sc = new ApexPages.standardController(account);
        
        ZenTicketListForAccountViewController ticketListForAccountViewController = new ZenTicketListForAccountViewController (sc);
        
        
        ticketListForAccountViewController.isApexTest = true;
        
        List<ZenTicket> accountTickets = ticketListForAccountViewController.getTickets();
        
        ticketListForAccountViewController.sortExpression='';        
        System.assertEquals(ticketListForAccountViewController.getSortDirection(),'desc');
        
        ticketListForAccountViewController.sortExpression='created_at';        
        System.assertEquals(ticketListForAccountViewController.getSortDirection(),'asc');
        System.assertEquals(ticketListForAccountViewController.sortExpression,'created_at');
        
        ticketListForAccountViewController.sortExpression='created_at';
        System.assertEquals(ticketListForAccountViewController.getSortDirection(),'desc');
        
        ticketListForAccountViewController.setSortDirection('asc');
        System.assertEquals(ticketListForAccountViewController.getSortDirection(),'asc');
        
        ticketListForAccountViewController.reloadData();
        
        System.assertEquals(ticketListForAccountViewController.getPageNumber() ,1);  
        System.assertEquals(ticketListForAccountViewController.getPageSize() ,5);
        System.assertEquals(ticketListForAccountViewController.getTotalPageNumber () ,4);
        
        ticketListForAccountViewController.setSevLevel(':urgent');
        System.assertEquals(ticketListForAccountViewController.getSevLevel() ,':urgent');
        
        ticketListForAccountViewController.setStatusLevel(':urgent');
        System.assertEquals(ticketListForAccountViewController.getStatusLevel() ,':urgent');
        
        System.assertEquals(ticketListForAccountViewController.getPreviousButtonEnabled() ,true);
        System.assertEquals(ticketListForAccountViewController.getNextButtonDisabled() ,false);
        
        ticketListForAccountViewController.nextBtnClick();
        ticketListForAccountViewController.previousBtnClick();
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
                
        //ZenSurveyPoster.loadTickets(true); 
        
        ///////////////////////////////////////////////////////////////////////////////////////////////
        
        Account newAccount = new Account (name='isTest');
        insert newAccount;
        
        Contact contact = new Contact (FirstName='Test',LastName='isTest',Email='test@istest.com',account=newAccount,accountId=newAccount.id);
        insert contact;
        
        // Meeting_took_place__c to satisfy the validation rule: AE_TQL_Approve_YES
        Opportunity opportunity = new Opportunity (Name='XYZ Opportunity',Account=newAccount,accountId=newAccount.id,CloseDate=System.Today()+7,StageName='Stage 1 - Prequalified',
                                                   AE_TQL_Approved__c='YES', Meeting_took_place__c='No');
        Insert opportunity;
        
        OpportunityContactRole contactRole = new OpportunityContactRole( contactid=contact.id, OpportunityId=opportunity.id);
        insert contactRole;
        
        opportunity.StageName = 'Stage 4 - POC/EVAL Engaged';   
        opportunity.HW_Eval_Agreement_Completed__c = 'Yes';
        opportunity.Site_Survey_Completed__c = 'Yes';           
        opportunity.Eligible_for_the_Love_Your_Storage_Prog__c = 'Yes'; 
        opportunity.Reasons_for_Loss__c = 'Pricing';
        opportunity.Reasons_for_Win__c = 'Test';
        opportunity.Competition__c = 'EMC';
        opportunity.Reason_s_for_Win_Loss__c = 'Pricing';
        opportunity.Customer_Reference__c = 'Public Reference';
        opportunity.SE_Opportunity_Owner__c = '005600000015YYK';
        //02-06-14:added the following to accomodate new validation rules that cause the test to fail
        opportunity.Replication_Required__c = 'No';
        opportunity.X23TB_required__c = 'No';
        opportunity.Distributor__c = 'None';
        //end 02-06-14 changes
        update opportunity;
        
        
        //ZenWebServiceCallout.createOrganization('Test Account','OpportunityId');
        //ZenWebServiceCallout.createEndUser('Test User', 'test@users.com','OrgId');
        
        
    }
}