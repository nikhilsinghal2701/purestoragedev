/* Test Class for SC_EmailRelatedListController
 * to test the send email related list for both internal users & customers and the email detail page
 */

@isTest
private class SC_EmailRelatedListControllerTest
{
    // test the email related list for internal users and auto populated CC emails field on sending any email from case
    private static testMethod void emailRelatedListAndSendEmail()
    {
        // create test data
        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        // create a test SE as Account team member who should alse be CC'ed
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
        // populate the collaborator field on the case as they will alse to be CC'ed
        c.Case_Collaborators__c = 'CCEmail1@testclass.com, CCEmail2@testclass.com, CCEmail3@testclass.com,'
                                    + 'CCEmail4@testclass.com, CCEmail5@testclass.com, CCEmail6@testclass.com,'
                                    +'CCEmail7@testclass.com, CCEmail8@testclass.com, CCEmail9@testclass.com,'
                                    + 'CCEmail10@testclass.com';
        insert c;
        
        // set a contact on Case Account to be cc'ed
        Contact con2 = SC_TestUtil.createContact(false, acc);
        con2.CC_On_All_Cases__c = true;
        insert con2;

        // create test email messages to be appeared on the email related list
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test second email'));
        insert emailMessages;
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            c = [Select Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, Contact.Email, ContactID, AccountId from Case where Id = :c.Id];
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(c));
            // assert that the related list showed all the email messages of the case
            system.assertEquals(2, obj.emails.size(), 'There should be 2 emails in the related list');

            // get all the CC Emails and redirect the user to the standard send email page with cc emails on the url parameters
            obj.sendEmail();
            
            // asset that the case collaborators, account SE and contact has been Cc'ed
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_1__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_10__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(con2.Email.toLowerCase()), 'Contact on Case Team should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(u1.Email.toLowerCase()), 'Account SE should be added to CC field');

            Test.stopTest();
        }
    }
    
    // test the email related list for internal users and auto populated CC emails field on reply to any email from case
    private static testMethod void testReply()
    {
        // create test data
        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        
        // create a test SE as Account team member who should alse be CC'ed
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
        // populate the collaborator field on the case as they will alse to be CC'ed
        c.Case_Collaborators__c = 'CCEmail1@testclass.com, CCEmail2@testclass.com, CCEmail3@testclass.com,'
                                    + 'CCEmail4@testclass.com, CCEmail5@testclass.com, CCEmail6@testclass.com,'
                                    +'CCEmail7@testclass.com, CCEmail8@testclass.com, CCEmail9@testclass.com,'
                                    + 'CCEmail10@testclass.com';
        insert c;
        
        // set a contact on Case Account to be cc'ed
        Contact con2 = SC_TestUtil.createContact(false, acc);
        con2.CC_On_All_Cases__c = true;
        insert con2;

        // create test email messages to be appeared on the email related list
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test second email'));
        insert emailMessages;
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            c = [Select Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, Contact.Email, ContactID, AccountId from Case where Id = :c.Id];
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(c));
            
            // if user has chosen to reply for the first email in the related list
            Apexpages.currentPage().getParameters().put('selectedEmailId', obj.emails[0].Id);
            Apexpages.currentPage().getParameters().put('toAll', '0');
            obj.reply();
            
            // asset that the case collaborators, account SE and contact has been Cc'ed
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_1__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_10__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(con2.Email.toLowerCase()), 'Contact on Case Team should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(u1.Email.toLowerCase()), 'Account SE should be added to CC field');
            
            string previousCCAddress = 'ccAddress@testclass.com';
            system.assert(!obj.redirectURL.toLowerCase().contains(previousCCAddress.toLowerCase()), 'On reply, people who were CCed already should not be added to the CC field');
            Test.stopTest();
        }
    }
    
    // test the email related list for internal users and auto populated CC emails field on reply to all to any email from case
    // where any existing emails in the email conversation should also be added to the cc field
    private static testMethod void testReplyToAll()
    {
        // create test data
        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        
        // create a test SE as Account team member who should alse be CC'ed
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
        // populate the collaborator field on the case as they will alse to be CC'ed
        c.Case_Collaborators__c = 'CCEmail1@testclass.com, CCEmail2@testclass.com, CCEmail3@testclass.com,'
                                    + 'CCEmail4@testclass.com, CCEmail5@testclass.com, CCEmail6@testclass.com,'
                                    +'CCEmail7@testclass.com, CCEmail8@testclass.com, CCEmail9@testclass.com,'
                                    + 'CCEmail10@testclass.com';
        insert c;
        
        // set a contact on Case Account to be cc'ed
        Contact con2 = SC_TestUtil.createContact(false, acc);
        con2.CC_On_All_Cases__c = true;
        insert con2;

        // create test email messages to be appeared on the email related list
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test second email'));
        insert emailMessages;
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            c = [Select Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, Contact.Email, ContactID, AccountId from Case where Id = :c.Id];
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(c));
            
            // if user has chosen to reply all for the first email in the related list
            Apexpages.currentPage().getParameters().put('selectedEmailId', obj.emails[0].Id);
            Apexpages.currentPage().getParameters().put('toAll', '1');
            obj.reply();
            
            // asset that the case collaborators, account SE and contact has been Cc'ed
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_1__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(c.Case_Collaborator_10__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.redirectURL.toLowerCase().contains(con2.Email.toLowerCase()), 'Contact on Case Team should be added to CC field');
            
            // also assert that any existing CC emails in that email conversation is also copied as it is a reply to all
            string previousCCAddress = 'ccAddress@testclass.com';
            system.assert(obj.redirectURL.toLowerCase().contains(previousCCAddress.toLowerCase()), 'On reply to all, people who were CCed already should also be added to the CC field');

            Test.stopTest();
        }
    }
    
    // test delete email functionality - but this functionality is deprecated for now
    private static testMethod void testDeleteEmail()
    {
        // create test data
        Profile p = SC_TestUtil.getProfile('System Administrator');
        Account acc = SC_TestUtil.createAccount(true);

        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test second email'));
        insert emailMessages;
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            c = [Select Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, Contact.Email, ContactID, AccountId from Case where Id = :c.Id];
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(c));
            
            Apexpages.currentPage().getParameters().put('selectedEmailId', obj.emails[0].Id);
            obj.deleteEmail();
            
            // assert that one email is deleted
            system.assertEquals(1, obj.emails.size(), 'One email should have been deleted');

            Test.stopTest();
        }
    }
    
    // // test the email related list for customers which shoudl only show the public emails
    private static testMethod void testEmailRelatedListAsCustomer()
    {
        // test data
        Profile p = SC_TestUtil.getProfile('PS Customer Community User');
        Account acc = SC_TestUtil.createAccount(true);

        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        List<EmailMessage> emailMessages = new List<EmailMessage>();
        // public email - as from address is the customer's email - should be shown to the customer
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'testuser@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        // private email - as the customer is not involved in this email - should not be shown to the customer
        emailMessages.add(SC_TestUtil.createEmailMessage(false, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test second email'));
        insert emailMessages;
        
        c = [Select Case_Collaborator_1__c, Case_Collaborator_2__c, Case_Collaborator_3__c,
                        Case_Collaborator_4__c, Case_Collaborator_5__c, Case_Collaborator_6__c,
                        Case_Collaborator_7__c, Case_Collaborator_8__c, Case_Collaborator_9__c,
                        Case_Collaborator_10__c, Contact.Email, ContactID, AccountId from Case where Id = :c.Id];
        
        // run as customer
        system.runAs(SC_TestUtil.createCustomerUser(p, con1))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(c));
            
            // assert that the user only sees the public emails
            system.assertEquals(1, obj.emails.size(), 'Only public emails should be shown to the customer');

            Test.stopTest();
        }
    }
    
    // test the email detail page
    private static testMethod void testEmailDetailPage()
    {
        // test data
        Profile p = SC_TestUtil.getProfile('Engineering Support');
        Account acc = SC_TestUtil.createAccount(true);
        
        // add an account team member with a role of SE who has also to be CC'ed
        Profile p1 = SC_TestUtil.getProfile('System Administrator');
        User u1 = SC_TestUtil.createUser(p1);
        u1.UserName = 'accountSE@testclass.com';
        u1.Email = 'SE@testclass.com';
        insert u1;
        SC_TestUtil.createAccountTeamMember(acc, u1);
        
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(false, acc, con1, parentArray);
        // populate case collaborator fields as they have to be copied as well
        c.Case_Collaborators__c = 'CCEmail1@testclass.com, CCEmail2@testclass.com, CCEmail3@testclass.com,'
                                    + 'CCEmail4@testclass.com, CCEmail5@testclass.com, CCEmail6@testclass.com,'
                                    +'CCEmail7@testclass.com, CCEmail8@testclass.com, CCEmail9@testclass.com,'
                                    + 'CCEmail10@testclass.com';
        insert c;
        
        // contact to be copied as well
        Contact con2 = SC_TestUtil.createContact(false, acc);
        con2.CC_On_All_Cases__c = true;
        insert con2;

        // create a test email msg
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(SC_TestUtil.createEmailMessage(true, c.Id, 'fromAddress@testclass.com', 
                                                    'toaddress@testclass.com', 'ccAddress@testclass.com', 'Test first email'));
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
            Test.startTest();

            Test.setCurrentPage(Page.SC_EmailRelatedList);
            
            ApexPages.currentPage().getParameters().put('id', emailMessages[0].Id);
            
            EmailMessage em = [Select Id, ParentId, CCAddress, Parent.AccountId, Parent.ContactId,
                                      Parent.Case_Collaborator_1__c, Parent.Case_Collaborator_2__c, Parent.Case_Collaborator_3__c,
                                      Parent.Case_Collaborator_4__c, Parent.Case_Collaborator_5__c, Parent.Case_Collaborator_6__c,
                                      Parent.Case_Collaborator_7__c, Parent.Case_Collaborator_8__c, Parent.Case_Collaborator_9__c,
                                      Parent.Case_Collaborator_10__c from EmailMessage where ID = :emailMessages[0].Id];
            
            SC_EmailsRelatedListController obj = new SC_EmailsRelatedListController(new Apexpages.standardController(em));
            
            // assert all the urls
            system.assert(obj.replyToAllURL.toLowerCase().contains(em.Parent.Case_Collaborator_1__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.replyToAllURL.toLowerCase().contains(em.Parent.Case_Collaborator_10__c.toLowerCase()), 'Case Collaborators should be added to CC field');
            system.assert(obj.replyToAllURL.toLowerCase().contains(con2.Email.toLowerCase()), 'Contact on Case Team should be added to CC field');
            
            string previousCCAddress = 'ccAddress@testclass.com';
            system.assert(obj.replyToAllURL.toLowerCase().contains(previousCCAddress.toLowerCase()), 'On reply to all, people who were CCed already should also be added to the CC field');

            Test.stopTest();
        }
    }
}