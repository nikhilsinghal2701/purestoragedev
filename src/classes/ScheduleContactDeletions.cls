global class ScheduleContactDeletions implements Schedulable {
 	
	global void execute(SchedulableContext ctx) 
    {   
		ContactDeletionProcessor.executeContactSoftDeletion();
	}

}