/*****************************************************************************
 * Description : Purpose of this class is to setup the test data for Partner Locator functionality
 * Author      : Sriram Swaminathan
 * Date        : 07/21/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
@isTest
private class PartnerLocatorServiceTest {

	private static final PartnerLocatorService service = PartnerLocatorFactory.getInstance();
	
	//@isTest 
	static void setup() {
		PartnerLocatorTestData.setup(-1);
		List<Partner_Locator_Details__c> details = new List<Partner_Locator_Details__c>();
		String partnerNamePrefix = PartnerLocatorTestData.PARTNER_NAME_PREFIX + '%';
		for (Partner_Locator_Details__c detail : [	SELECT ID, PARTNER_NAME__C
													FROM PARTNER_LOCATOR_DETAILS__C
													WHERE PARTNER_NAME__C LIKE :partnerNamePrefix
												]){
			details.add(detail);
		}
		System.assertEquals(PartnerLocatorTestData.DEFAULT_SAMPLE_DATA_SIZE
							, details.size()
							, 'Expected count of Partner Locator Details is ' +  PartnerLocatorTestData.DEFAULT_SAMPLE_DATA_SIZE);			
	}

	@isTest 
	static void testSearchByCountry() {
		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		String country = 'United States';
		searchBean.selectedCountry = country;
		List<PartnerLocatorBean> results = service.search(searchBean);
		Integer count = getCount(' PRIMARY_LOCATION_COUNTRY__C = \'' + country + '\'' );
		System.assertEquals(count
							, results.size()
							, 'Expected count of Partner Locator Details for search by country ' + country + ' is ' +  count);
		System.assertEquals(TRUE
							, !searchBean.countries.isEmpty()
							, 'Expected countries select option to be not empty' );											
	}

	@isTest 
	static void testSearchByCountryAndState() {
		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		String country = 'United States';
		String state = 'CA';
		searchBean.selectedCountry = country;
		searchBean.selectedStateOrProvince = state;
		List<PartnerLocatorBean> results = service.search(searchBean);
		Integer count = getCount(' PRIMARY_LOCATION_COUNTRY__C = \'' + country + '\'' + ' AND PRIMARY_LOCATION_STATE_PROVINCE__C = \'' + state + '\'');
		System.assertEquals(count
							, results.size()
							, 'Expected count of Partner Locator Details for search by country, state - ' + country + ',' + state + ' is ' +  count);	
		System.assertEquals(TRUE
							, !searchBean.statesOrProvinces.isEmpty()
							, 'Expected statesOrProvinces select option to be not empty' );										
	}

	@isTest 
	static void testSearchByApplicationExpertise() {
		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		String applicationExpertise = 'Database';
		searchBean.selectedApplicationExpertise = applicationExpertise;
		List<PartnerLocatorBean> results = service.search(searchBean);
		Integer count = getCount(' APPLICATION_EXPERTISE__C INCLUDES (\'' + applicationExpertise + '\')' );
		System.assertEquals(count
							, results.size()
							, 'Expected count of Partner Locator Details for search by applicationExpertise ' + applicationExpertise + ' is ' +  count);			
		System.assertEquals(TRUE
							, !searchBean.applicationExpertiseOptions.isEmpty()
							, 'Expected Application Expertise select option to be not empty' );			
	}	

	@isTest 
	static void testSearchByIndustryFocus() {
		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		String industryFocus = 'Commercial';
		searchBean.selectedIndustryFocus = industryFocus;
		List<PartnerLocatorBean> results = service.search(searchBean);
		Integer count = getCount(' INDUSTRY_FOCUS__C INCLUDES (\'' + industryFocus + '\')' );
		System.assertEquals(count
							, results.size()
							, 'Expected count of Partner Locator Details for search by industryFocus ' + industryFocus + ' is ' +  count);	
		System.assertEquals(TRUE
							, !searchBean.industryFocusOptions.isEmpty()
							, 'Expected Industry Focus select option to be not empty' );										
	}		

	@isTest 
	static void testSearchByPartnerTier() {
		Integer sampleDataSize = 5;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		List<String> partnerTiers = new List<String>();
		for (SelectOption o : searchBean.partnerTierOptions){
			partnerTiers.add(o.getValue());
		}
		searchBean.selectedPartnerTiers.addAll(partnerTiers);
		List<PartnerLocatorBean> results = service.search(searchBean);
		System.assertEquals(sampleDataSize
							, results.size()
							, 'Expected count of Partner Locator Details for search by partner tier is ' +  sampleDataSize);			
	}

	@isTest 
	static void testGetPartnerLocatorByName() {
		Integer sampleDataSize = 1;
		PartnerLocatorTestData.setup(sampleDataSize);
		PartnerLocatorSearchBean searchBean = new PartnerLocatorSearchBean(new PartnerLocatorContext());
		List<PartnerLocatorBean> results = service.search(searchBean);
		String attributeName = '';
		for (PartnerLocatorBean b : results){
			if (b.partnerName.startsWith(PartnerLocatorTestData.PARTNER_NAME_PREFIX)){
				attributeName = b.NAME;
			}
		}
		PartnerLocatorBean partnerLocatorBean = service.getPartnerLocatorByName(attributeName);
		System.assertEquals(TRUE
							, (partnerLocatorBean!=NULL && partnerLocatorBean.NAME.equals(attributeName))
							, 'Expecting a Partner Locator Bean with name attribute as ' +  attributeName);			
	}	

	/*
	* Return count for the input filter criteria
	*/
	private static Integer getCount(String filterSql) {
		Integer cnt = 0;
		String sql = ' SELECT COUNT(ID) cnt FROM PARTNER_LOCATOR_DETAILS__C '
					// + ' WHERE PARTNER_NAME__C LIKE \'' +  PartnerLocatorTestData.PARTNER_NAME_PREFIX + '%' + '\''
					+ ' WHERE ' + ' STATUS__C = \'Published\' ' + ' AND ' + filterSql;
		for (AggregateResult result: Database.query(sql)){
			cnt =  (Integer)result.get('cnt');
		}
		return cnt;
	} 

	
}