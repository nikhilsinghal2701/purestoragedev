public with sharing class ApprovalReminderHelper {
	
	public static Map<Id, userQuotesWrapper> wrapperMap;
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static final string EMAIL_TEMPLATE_NAME = customSettings.Approval_Reminder_Template_Name__c;
    public static final decimal AGE_CRITERIA = customSettings.Approval_Reminder_Hours_Criteria__c;
    public static final string APPROVERS_GROUP = customSettings.Approval_Reminder_Group_Id__c;
	
	/*
	* Method used to execute the batch class if needed
	*/
	public static void executeQuotesReminders(){
		populateApproverWrappers();
		//check if there are users to send email
		if(wrapperMap != null && wrapperMap.keySet().size() > 0){
			ApprovalReminderEmails ARE = new ApprovalReminderEmails();
			// the batch size can be no larger than 10 due to current apex single email limits
			integer batchSize = 10; 
			database.executebatch(ARE, batchSize);
		}
	}
	
	/*
	* Method used to generate the wrapper records where we store 
	* the user information and quotes pending for approval
	*/
	public static void populateApproverWrappers(){
		//Get Custom setting to Identify Groups of Users that need to be part of the reminder
		//List<Quotes_Reminders_Config__c> relatedGroups = Quotes_Reminders_Config__c.getall().values();
		Set<String> groupIds = new Set<String>();
		set<Id>userIds = new set<Id>();
		/*for(Quotes_Reminders_Config__c aux:relatedGroups){
			//store the public gropus ids that are added to the custom setting
			groupIds.add(String.valueOf(aux.Name));
		}*/
		//query the group members assigned to the specific groups stored in the custom setting
		List<GroupMember> theGroupMembers = [Select UserOrGroupId, GroupId From GroupMember Where GroupId =:APPROVERS_GROUP];
		//make sure to get only the ones that are users
		for(GroupMember aux:theGroupMembers){
			//Vaidate that the ID is a User ID
			if(String.valueOf(aux.UserOrGroupId).substring(0, 3) == '005'){
				userIds.add(aux.UserOrGroupId);
			}
		}
		Datetime submittedDate;
		Integer AGE_CRITERIA_INT = AGE_CRITERIA.intValue();
		//added logic for the test methods
		if (test.IsRunningTest() == true) {
			submittedDate = system.now();
		}
		else{
			submittedDate = system.now().addHours(-AGE_CRITERIA_INT);
		}
		 
		//Query Process Instances records that are "Pending", related to Quotes object and submitted for approval 24 hours ago or more
		List<ProcessInstance> processItems = new List<ProcessInstance>();
		processItems = [SELECT Id, TargetObject.Name, TargetObjectID, TargetObject.Type, SystemModstamp, 
                        (SELECT Id, ActorID, Actor.email, Actor.Name, SystemModstamp,CreatedDate
                         		FROM WorkItems 
                         		WHERE SystemModstamp  <= :submittedDate
                        		ORDER BY SystemModstamp desc) 
                        	FROM ProcessInstance WHERE Status = 'Pending' 
                        	AND IsDeleted = False 
                        	AND TargetObject.Type = 'Quote'];  
		//Loop for Process and get Actors that are part of User IDs previously filtered
		wrapperMap = new Map<Id, userQuotesWrapper>();
		if(processItems.size() > 0){
			for(ProcessInstance pi : processItems){
                if(pi.WorkItems.size() > 0){
                	ProcessInstanceWorkitem wi = pi.WorkItems[0];
					//check that the process instance records have an approver associated
					if(userIds.contains(wi.ActorId)){
                    
					//create the wrapper records with the user information and his quotes pending approval
					if(!wrapperMap.containsKey(wi.ActorId)){
						userQuotesWrapper wrapperRecord = new userQuotesWrapper(wi.Actor.Name, wi.ActorID, wi.Actor.email, wi.CreatedDate);
						wrapperRecord.quotes.add(pi);
						wrapperMap.put(wi.ActorId, wrapperRecord);
					}
					else{
						wrapperMap.get(wi.ActorId).quotes.add(pi);
					}
				}
				}
            }
			}
	}
	
	/*
	* Method used to send email to all the users that are members of specific groups and have quotes pending approval
	*/
	public static void SendReminderEmail(userQuotesWrapper wrapperRecord) {        
         EmailTemplate emailTemplateID;
     
     	//query the email template directly	
     	emailTemplateID = [Select Subject, Name, Body From EmailTemplate e where Name =: EMAIL_TEMPLATE_NAME];
 
    	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();     
    	String[] toAddresses;
    
		// get the current salesforce instance to build the link with
	    string domain = URL.getSalesforceBaseUrl().toExternalForm();
       
   		email.setTargetObjectId(wrapperRecord.userId);
   		email.setSaveAsActivity(false);
	    // set the subject on the email
	    email.setSubject(emailTemplateID.Subject);
  		
  		String bodyText = emailTemplateID.Body;
  		//replace with the user Name
  		bodyText = bodyText.replace('[User],', wrapperRecord.userName + ', <br/>');
  		//start creating the quotes table that is going to be shown in the email body
  		String quotesTable = '<br /><br /><table rowClasses="odd,even" border="1" cellspacing="2" cellpadding="2"><tr><td width="40%"><b>Quote Name</b></td><td width="45%"><b>Detail</b></td><td width="15%"><b>Pending Since</b></td></tr>';
  		//loop the quotes related to the user to build the table
  		for(ProcessInstance pi : wrapperRecord.quotes){
  			string targetLink = domain + '/' + string.valueof(pi.TargetObjectID);
            DateTime d = pi.Workitems[0].CreatedDate;
			string timeStr = d.format('MMMMM dd, yyyy hh:mm:ss a z');
  			quotesTable += '<tr><td width="40%">' + pi.TargetObject.Name + '</td><td width="45%"><a href=\'' + targetLink + '\'>' + targetLink + '</a></td>' + '<td width="15%">' + timeStr +  '</td></tr>';
  		}
  		quotesTable +='</table><br /><br />';
  		//replace with the table created
  		bodyText = bodyText.replace('[TableQuote]', quotesTable);
	    // set the body of the email
	    email.setHTMLBody(bodyText);                     
	    
	    // send our email by creating an array of emails and calling the send email method.
	    Messaging.SingleEmailMessage[] EmailsToSend = new Messaging.SingleEmailMessage[] { email };
	    Messaging.sendEmail(EmailsToSend);
    } // end send reminder email
    
    
	public class userQuotesWrapper{
    	public List<ProcessInstance>quotes {get;set;}
    	String userName{get;set;}
    	String userId{get;set;}
    	String userEmail{get;set;}
        Datetime pendingDate {get;set;}
    	
    	public userQuotesWrapper(String name, Id usrid, String email, Datetime inputDate){
    		userName = name;
    		userId = usrid;
    		userEmail = email;
            pendingDate = inputDate;
    		quotes = new List<ProcessInstance>();
    	}
    }
}