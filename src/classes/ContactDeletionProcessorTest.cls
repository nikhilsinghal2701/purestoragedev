@istest
public class ContactDeletionProcessorTest 
{
	static testMethod void testDeleteContact()
    {
        Account a = new Account(Name = 'HIDDEN ACCT TEST', BillingCountry = 'US');
        insert a;
        
        Account b = new Account(Name = 'ACCT TEST', BillingCountry = 'US');
        insert b;
        
        Apex_Code_Settings__c cs = new Apex_Code_Settings__c();
        cs.Hidden_Account_Id__c = a.Id;
        cs.Hidden_Owner_Id__c = System.UserInfo.getUserId();
        insert cs;
        
        Contact c = new Contact(LastName = 'HIDEME', FirstName = 'TEST', AccountId = b.Id);
        insert c;
        system.assertNotEquals(null, c.Id);
     	
        //this indicates that the contact should be soft deleted
        c.Delete__c = true;
        update c;
        
        test.startTest();
        //this method executes the soft deletion by reassigning to the hidden acct
        ContactDeletionProcessor.executeContactSoftDeletion();
        Contact softDeleted = [select id, AccountId from Contact where id =: c.Id];
        system.assertEquals(a.Id, softDeleted.AccountId);
        
        //this indicates that the contact should be un-soft deleted
        softDeleted.Delete__c = false;
        update softDeleted;
        
        //the contact should be restored to the prior account
        Contact unSoftDeleted = [select id, AccountId from Contact where id =: c.Id];
        system.assertEquals(b.Id, unSoftDeleted.AccountId);
        test.stopTest();
        
        
        
    }
        
        
        
}