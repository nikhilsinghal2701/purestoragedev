public class ZenTicketListViewController {

    public boolean isApexTest = false;
    
    private List<ZenTicket> tickets = new List<ZenTicket>();
    private Map<String,ZenUser> usersMap = new Map<String,ZenUser>{};
    
    private String sortDirection = 'desc';
    private String sortExp = 'created_at';

    private Integer totalTickets = 0;    
    private Integer pageNumber = 1;
    private Integer pageSize = 15; 
    private Integer totalPageNumber = 0;
    
    //private String sevLevel = ':4';   //Changed By Zendesk 
    private String sevLevel = ':urgent';
    
    //private String statusLevel = '<4'; //Changed By Zendesk
    private String statusLevel = '<solved';
    
    private String SERVICE_JSON_URL; 
    private String SERVICE_XML_URL; 
    private String USERNAME;
    private String PASSWORD;
    
    public ZenTicketListViewController(){

        ZenConfiguration__c configuration = [SELECT Zendesk_Username__c,Zendesk_URL__c,Zendesk_Password__c FROM ZenConfiguration__c].get(0);
                      
        //this.SERVICE_XML_URL = configuration.Zendesk_URL__c; //Deprecated
        this.SERVICE_JSON_URL = configuration.Zendesk_URL__c + '/api/v2';       
        this.USERNAME = configuration.Zendesk_Username__c;
        this.PASSWORD = configuration.Zendesk_Password__c;
           
    } 

    public String sortExpression
    {
     get
     {
        return sortExp;
     }
     set
     {
       //if the column is clicked on then switch between Ascending and Descending modes
       if (value == sortExp)
         sortDirection = (sortDirection == 'asc')? 'desc' : 'asc';
       else
         sortDirection = 'asc';
       sortExp = value;
     }
    }
    
    public String getSortDirection()
    {
    //if not column is selected 
    if (sortExpression == null || sortExpression == '')
      return 'desc';
    else
     return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    public Integer getPageNumber()
    {
        return pageNumber;
    }
    
    public Integer getPageSize()    
    {    
        return pageSize;   
    }
    
    public Boolean getPreviousButtonEnabled()    
    {    
        return !(pageNumber > 1);
    }
    
    public Boolean getNextButtonDisabled()    
    {    
        if (tickets == null) 
            return true;    
        else
            return ((pageNumber * pageSize) >= totalTickets );    
    }
    
    public Integer getTotalPageNumber()    
    {    
        if (tickets != null)   
        {        
            totalPageNumber = totalTickets / pageSize;            
            Integer mod = totalTickets - (totalPageNumber * pageSize);            
            if (mod > 0)            
                totalPageNumber++;        
        }
        if(totalPageNumber == 0) totalPageNumber++;
        return totalPageNumber;    
    }
    
    public void setSevLevel(String value)
    {
        sevLevel = value;
    }
    
    public String getSevLevel()
    {
        return sevLevel;
    }
    
    public void setStatusLevel(String value)
    {
        statusLevel = value;
    }
    
    public String getStatusLevel()
    {
        return statusLevel;
    }
     
    public PageReference nextBtnClick() {
        pageNumber++;
        tickets = loadTickets();  
        return null;   
    }
    
    public PageReference previousBtnClick() {
        pageNumber--;
        tickets = loadTickets();  
        return null;       
    }

    public PageReference reloadData() {
        pageNumber = 1;
        if(sevLevel == '>0') sortExp = 'priority';
        else sortExp = 'created_at';
        tickets = loadTickets();
        return null;
    }
    
    public PageReference resortData() {
        pageNumber = 1;
        tickets = loadTickets();
        return null;
    }
    
    public List<ZenTicket> getTickets() {
        return loadTickets();
    }
    
    public List<ZenTicket> loadTickets() {
                 
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');
 
        String url = SERVICE_JSON_URL+'/search.json?query=type:ticket+priority'+sevLevel+'+status'+statusLevel+'&per_page='+pageSize+'&page='+pageNumber+'&sort_by='+sortExpression+'&sort_order='+sortDirection;
        //String url = SERVICE_XML_URL+'/search.xml?query=type:ticket+priority'+sevLevel+'+status'+statusLevel+'&per_page='+pageSize+'&page='+pageNumber+'&order_by='+sortExpression+'&sort='+sortDirection;
        system.debug('url is: '+url);
        req.setEndpoint(url);
        
        Blob headerValue = Blob.valueOf(USERNAME+':'+PASSWORD);
         
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader); 
        
        
        if(isApexTest){
            HTTPResponse response = new HTTPResponse();
            
            //response.setBody('XML');  //Deprecated
            //return parseXML(response); //Deprecated
            
            response.setBody('{"results":[{"url":"https://pure.zendesk.com/api/v2/tickets/11229.json","id":11229,"external_id":null,"via":{"channel":"email","source":{"from":{"address":"picpure001-ct0@barclays.com","name":"Picpure001-ct0"},"to":{"address":"support@purestorage.com","name":"Pure Storage Support"},"rel":null}},"created_at":"2013-10-29T17:12:23Z","updated_at":"2013-10-29T17:46:26Z","type":"incident","subject":"Info - PICPURE001: Storage consumption has reached 84.43% of usable capacity [25]","description":"I/O performance may degrade when consumed storage is above the array rated capacity.  Severity: Info  UTC Time: 2013 Oct 29 17:11:58 Array Time: 2013 Oct 29 13:11:58 EDT Array Name: PICPURE001 Domain: barclays.com Suggested Action: Consider increasing capacity by adding a storage shelf, or an additional FlashArray to increase both capacity and performance.  Purity Version: 3.2.2 Array ID: 11297093091695047510:18225793936655040712 Controller Name: ct0 Variables: (below) Used: 15719263910912 Total: 18618569143092 Percent Usage: 84.43   _______________________________________________  This message is for information purposes only, it is not a recommendation, advice, offer or solicitation to buy or sell a product or service nor an official confirmation of any transaction. It is directed at persons who are professionals and is not intended for retail customer use. Intended for recipient only. This message is subject to the terms at: www.barclays.com/emaildisclaimer.  For important disclosures, please see: www.barclays.com/salesandtradingdisclaimer regarding market commentary from Barclays Sales and/or Trading, who are active market participants; and in respect of Barclays Research, including disclosures relating to specific issuers, please see http://publicresearch.barclays.com.  _______________________________________________","priority":"urgent","status":"closed","recipient":"support@pure.zendesk.com","requester_id":256204479,"submitter_id":256204479,"assignee_id":376801621,"organization_id":20919772,"group_id":128914,"collaborator_ids":[262714444,224412438,384303852,109487771,266672990],"forum_topic_id":null,"problem_id":null,"has_incidents":false,"due_at":null,"tags":["201309192027_b8a39ca_r5","40","closed_by_merge","other"],"custom_fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":""},{"id":21171050,"value":""},{"id":20007202,"value":"201309192027_b8a39ca_r5"},{"id":20010486,"value":"other"},{"id":20520488,"value":"na"},{"id":22064668,"value":""},{"id":20018901,"value":"PURE-17729"},{"id":21666866,"value":"40"},{"id":22064678,"value":""},{"id":21092354,"value":null},{"id":21101625,"value":false},{"id":21072274,"value":""},{"id":20172612,"value":"Neil driving RA - top priority to get back up"}],"satisfaction_rating":null,"sharing_agreement_ids":[],"fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":""},{"id":21171050,"value":""},{"id":20007202,"value":"201309192027_b8a39ca_r5"},{"id":20010486,"value":"other"},{"id":20520488,"value":"na"},{"id":22064668,"value":""},{"id":20018901,"value":"PURE-17729"},{"id":21666866,"value":"40"},{"id":22064678,"value":""},{"id":21092354,"value":null},{"id":21101625,"value":false},{"id":21072274,"value":""},{"id":20172612,"value":"Neil driving RA - top priority to get back up"}],"followup_ids":[],"ticket_form_id":47948,"result_type":"ticket"},{"url":"https://pure.zendesk.com/api/v2/tickets/10027.json","id":10027,"external_id":null,"via":{"channel":"web","source":{"from":{},"to":{},"rel":null}},"created_at":"2013-10-07T01:35:37Z","updated_at":"2013-10-14T18:01:17Z","type":"incident","subject":"Series of failover events","description":"Hello Morten,    For the last 50 minutes or so PureS2 has been failing over between controllers. I would like to stop this as fast as possible and need a Remote Assist to accomplish this.    To provide a Remote Assist, please do the following:    Go to the GUI: System > Configuration > Support Connectivity > Remote Assist > Connect  OR   CLI: purearray remoteassist --connect    Once I can confirm recovery, I will escalate to engineering and we will provide regular updates going forward. Apologies for further problems and thanks for your patience.    Regards,  ","priority":"urgent","status":"closed","recipient":null,"requester_id":234164215,"submitter_id":104482931,"assignee_id":104482931,"organization_id":25862613,"group_id":128914,"collaborator_ids":[299582036,233733454,248678660],"forum_topic_id":null,"problem_id":null,"has_incidents":false,"due_at":null,"tags":["10","201309192027_b8a39ca_r5","failover","jiradaily","no","no_updates","software_fix","vmware"],"custom_fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":null},{"id":21171050,"value":"no_updates"},{"id":20007202,"value":"201309192027_b8a39ca_r5"},{"id":20010486,"value":"vmware"},{"id":20520488,"value":"5"},{"id":22064668,"value":"failover"},{"id":20018901,"value":"PURE-17212"},{"id":21666866,"value":"10"},{"id":22064678,"value":"software_fix"},{"id":21092354,"value":null},{"id":21101625,"value":false},{"id":21072274,"value":"no"},{"id":20172612,"value":"review data, open RA"}],"satisfaction_rating":null,"sharing_agreement_ids":[],"fields":[{"id":20009798,"value":null},{"id":20010481,"value":null},{"id":21119624,"value":null},{"id":21171050,"value":"no_updates"},{"id":20007202,"value":"201309192027_b8a39ca_r5"},{"id":20010486,"value":"vmware"},{"id":20520488,"value":"5"},{"id":22064668,"value":"failover"},{"id":20018901,"value":"PURE-17212"},{"id":21666866,"value":"10"},{"id":22064678,"value":"software_fix"},{"id":21092354,"value":null},{"id":21101625,"value":false},{"id":21072274,"value":"no"},{"id":20172612,"value":"review data, open RA"}],"followup_ids":[],"ticket_form_id":null,"result_type":"ticket"}],"facets":null,"next_page":"https://pure.zendesk.com/api/v2/search.json?page=2&per_page=2&query=type%3Aticket+priority%3Aurgent+status%3Esolved&sort_by=created_at&sort_order=desc","previous_page":null,"count":19}');
            return parseJSON(response);
        }
        return parseJSON(http.send(req));
        //return parseXML(http.send(req)); //Deprecated
    }
    
    /* Deprecated
    private List<ZenTicket> parseXML(HTTPResponse response){
        
        List<ZenTicket> tickets = new List<ZenTicket>(); 
        dom.Document doc = response.getBodyDocument();
        totalTickets = Integer.valueOf(doc.getRootElement().getAttribute('count',null));
        dom.XmlNode [] nodes = doc.getRootElement().getChildElements(); 
        for(dom.XMLNode node :nodes){
            ZenTicket ticket = new ZenTicket();
            dom.XmlNode [] nodeItems = node.getChildElements();
            for(dom.XMLNode nodeItem :nodeItems){
                if(nodeItem.getName() == 'nice-id') 
                    ticket.id = nodeItem.getText();
                else if(nodeItem.getName() == 'subject') 
                    ticket.subject = nodeItem.getText();
                else if(nodeItem.getName() == 'ticket-type-id') 
                    ticket.type = nodeItem.getText();
                else if(nodeItem.getName() == 'status-id') 
                    ticket.status = nodeItem.getText();
                else if(nodeItem.getName() == 'priority-id') 
                    ticket.priority = nodeItem.getText();
                else if(nodeItem.getName() == 'assignee-id'){
                    if(!this.usersMap.containsKey(nodeItem.getText()))
                        usersMap.put(nodeItem.getText(),new ZenUser(nodeItem.getText(),isApexTest));
                    ticket.assignee = (usersMap.get(nodeItem.getText())).name;  
                }
                else if(nodeItem.getName() == 'updated-at') 
                    ticket.updated_at = nodeItem.getText();
                else if(nodeItem.getName() == 'created-at') 
                    ticket.created_at = nodeItem.getText();
            }
            tickets.add(ticket);
        }
        return tickets;
    }
    */
                             
    private List<ZenTicket> parseJSON(HTTPResponse response){    
        
        List<ZenTicket> tickets = new List<ZenTicket>(); 
        JSONParser parser = JSON.createParser(response.getBody());
        
        //system.debug(resp.getBody());

        while (parser.nextToken() != null)  
        {  
			if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'count'))  
            {  
               parser.nextToken();      
               totalTickets = Integer.valueOf(parser.getText());  
            }
            if (parser.getCurrentToken() == JSONToken.START_ARRAY)  
            {  
               
                while (parser.nextToken() !=  JSONToken.END_ARRAY)  
                {  
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT)  
                    {  
                          
                        ZenTicket ticket = new ZenTicket();  
                        
                        while (parser.nextToken() !=  JSONToken.END_OBJECT)  
                        {  
                              
                                if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'id'))  
                                {  
                                    parser.nextToken();      
                                    ticket.id= parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'subject'))  
                                {  
                                    parser.nextToken();  
                                    ticket.subject = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'type'))  
                                {  
                                    parser.nextToken();  
                                    ticket.type = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'status'))  
                                {  
                                    parser.nextToken();  
                                    ticket.status = parser.getText();  
                                } 
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'priority'))  
                                {  
                                    parser.nextToken();  
                                    ticket.priority = parser.getText();  
                                } 
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'updated_at'))  
                                {  
                                    parser.nextToken();  
                                    ticket.updated_at = parser.getText();  
                                }  
                                else if((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&(parser.getText().tolowercase() == 'created_at'))  
                                {  
                                    parser.nextToken();  
                                    ticket.created_at = parser.getText();  
                                }
                                else if(parser.getCurrentToken() == JSONToken.FIELD_NAME)  
                                {  
                                    parser.nextToken();
                                    if(parser.getCurrentToken() == JSONToken.START_ARRAY){
                                        while(parser.nextToken() != JSONToken.END_ARRAY){
                                            //Ignore nested Arrays
                                        }
                                    }else if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                            //Ignore nested objects
                                            if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                while(parser.nextToken() != JSONToken.END_OBJECT){
                                                    //Ignore double nested objects
                                                    if(parser.getCurrentToken() == JSONToken.START_OBJECT){
                                                        while(parser.nextToken() != JSONToken.END_OBJECT){
                                                            //Ignore triple nested objects
                                                        }
                                                    }
                                                }
                                            }
                                        
                                        }
                                    }else{
                                        //Ignore Other Tokens
                                    }
                                }                               
                        } 
                        
                        system.debug('Ticket number: ' + ticket.id); 
                        tickets.add(ticket);  
                    }  
                }  
            }  
        }  
        return tickets;
    }    
}