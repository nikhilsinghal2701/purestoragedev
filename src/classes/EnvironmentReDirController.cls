public class EnvironmentReDirController 
{
	public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance(); 
    public static final string ENV_OPP_FIELD_ID = customSettings.Env_Opp_Field_Id__c;
    public static final string ENV_ACCT_FIELD_ID = customSettings.Env_Acct_Field_Id__c;
    public string refReqKeyPrefix {get;set;}
    public string retURL {get;set;}
    public id oppId {get;set;}
    public id acctId {get;set;}
    
    public EnvironmentReDirController()
    {
        Schema.DescribeSObjectResult r = Environment__c.sObjectType.getDescribe();
        refReqKeyPrefix = r.getKeyPrefix();
        Id id = apexpages.currentpage().getparameters().get('id');
        oppId = apexpages.currentpage().getparameters().get('oppId');
        acctId = apexpages.currentpage().getparameters().get('acctId');
        
    }
    
    public pageReference reDirect()
    {

        if(oppId != null)
        {
            Opportunity o = [select Id, Name, AccountId, Account.Name, OwnerId, Owner.Name from Opportunity where Id =: oppId];
            retURL = '/' + refReqKeyPrefix + '/e?CF' + ENV_OPP_FIELD_ID.substring(0,15) + '_lkid=' + o.Id + '&' + 
                                                    'CF' + ENV_OPP_FIELD_ID.substring(0,15) + '=' + EncodingUtil.urlEncode(o.Name, 'UTF-8') + '&' + 
                                                    'Name=' + EncodingUtil.urlEncode(o.Name, 'UTF-8') + '&' +
                                                    'CF' + ENV_ACCT_FIELD_ID.substring(0,15) + '=' + EncodingUtil.urlEncode(o.Account.Name, 'UTF-8') + '&' +
                                                    'CF' + ENV_ACCT_FIELD_ID.substring(0,15) + '_lkid=' + o.AccountId + '&' + 
                									'saveURL=' + '/' + oppId + '&' + 
                									'cancelURL=' + '/' + oppId + '&' + 
                									'retURL=' + '/' + oppId;
            system.debug('DEBUG:: retURL: ' + retURL);
        }
        else
        {
            retURL = '/' + refReqKeyPrefix + '/e' ;
        }
        
        PageReference reDirPage = new PageReference(retURL);
        reDirPage.setRedirect(true);
        return reDirPage;
        
    }
    
}