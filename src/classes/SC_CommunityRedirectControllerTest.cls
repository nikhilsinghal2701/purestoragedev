/* Test Class for SC_CommunityRedirectController
 * to test the functionality of redirecting the users to the correct landing page when the users 
 * lands on community from Lithium
 */

@isTest
private class SC_CommunityRedirectControllerTest
{
	// internal users should be taken to the home page of the support community
	private static testMethod void testRedirectAsInternalUser()
	{
		// as an internal user profile
		Profile p = SC_TestUtil.getProfile('System Administrator');

		// run as internal user
		system.runAs(SC_TestUtil.createUser(p))
		{
			Test.startTest();

			// set the page as the landing page
			Test.setCurrentPage(Page.SC_CommunityRedirect);
			SC_CommunityRedirectController obj = new SC_CommunityRedirectController();
			PageReference pageRef = obj.redirectUsersToAccount();

			Test.stopTest();

			// assert that the user is taken to the home page of support community
			system.assert(pageRef.getURL().contains(system.label.SC_CommunityEmployeeRedirectURL), 
									'Internal users should be taken home page of support community');
		}
	}

	// User's of lithium role as Visitor should not access support community, so an error msg is shown
	private static testMethod void testRedirectAsVisitor()
	{
		// create test account and contact
		Profile p = SC_TestUtil.getProfile('PS Lithium Community User');
		Account acc = SC_TestUtil.createAccount(true);
		Contact con = SC_TestUtil.createContact(true, acc);

		// run as visitor
		system.runAs(SC_TestUtil.createCustomerUser(p, con))
		{
			Test.startTest();

			// set the page as the landing page
			Test.setCurrentPage(Page.SC_CommunityRedirect);
			SC_CommunityRedirectController obj = new SC_CommunityRedirectController();
			PageReference pageRef = obj.redirectUsersToAccount();

			Test.stopTest();

			// assert that the insufficient privilege msg is shown
			system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_CommunityVisitorErrorMsg), 
									'Lithium Users should get insufficient privilege error message');
		}
	}

	// User's of lithium role as Customers should be taken to their account detail page
	private static testMethod void testRedirectAsCustomer()
	{
		// create test account and contact
		Profile p = SC_TestUtil.getProfile('PS Customer Community User');
		Account acc = SC_TestUtil.createAccount(true);
		Contact con = SC_TestUtil.createContact(true, acc);

		// run as Customer
		system.runAs(SC_TestUtil.createCustomerUser(p, con))
		{
			Test.startTest();

			// set the page as the landing page
			Test.setCurrentPage(Page.SC_CommunityRedirect);
			SC_CommunityRedirectController obj = new SC_CommunityRedirectController();
			PageReference pageRef = obj.redirectUsersToAccount();

			Test.stopTest();

			// assert that the customers are taken to their account detail page
			system.assert(pageRef.getURL().contains(acc.Id), 
									'Customer users should be taken to their Account page');
		}
	}
}