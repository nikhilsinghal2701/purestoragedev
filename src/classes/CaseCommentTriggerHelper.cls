/**
* Class: CaseCommentTriggerHelper
* Author: Fred
* Created Date: 06/26/2014
* Description: A helper class when CaseComment trigger fires
*/
public without sharing class CaseCommentTriggerHelper {

    public static final String CASE_EMAIL_ALIAS = 'daizhixia@gmail.com';

    public static final String PID_CUSTOMER_COMMUNITY_LOGIN = 'PID_Customer_Community_Login';

    public static final String SYSTEM_ADMINISTRATOR = 'System Administrator';

    public static final String PURE_STORAGE_SUPPORT = 'Purestorage Support';
    //from custom setting

    /*
        when new case comment get inserted, will check the case owner, 
        if the owner is out of office, will send out email to the case alias;
        1. create a dummy contact and put case email alias value in the contact email;
        2. create a custom setting, put the contact id in the custom setting;
     */
    public static void eventProcess(List<CaseComment> caseComments) {

        List<UserLicense> licenses = [Select Id, LicenseDefinitionKey from UserLicense WHERE LicenseDefinitionKey =: PID_CUSTOMER_COMMUNITY_LOGIN limit 1];
        
        // Sorna: added SVARs to the query
        Map<Id, User> customUsers = new Map<Id, User>([Select Id FROM User where (User.Profile.UserLicenseId =: licenses[0].id or Lithium_Role__c = :system.label.SC_LithiumRole_SVAR)
                                                                              AND ContactId != null
                                                                              AND IsActive = true ]);
        System.debug('customUsers are ...' + customUsers);
        Set<Id> caseId = new Set<Id>();
        for(CaseComment caseComment : caseComments) {
            //customer user create a comment 
            if(customUsers.get(caseComment.CreatedById) != null) {
                caseId.add(caseComment.parentId);
            }
        }
        if(caseId.isEmpty()) {
            return;
        }
        Map<Id, Id> caseOwnerMap = new Map<Id, Id> ();

        for(Case eventCase :[Select Id, ownerId FROM Case where Id IN: caseId limit 50000]) {
            if(string.valueOf(eventCase.ownerId).startsWith('005'))
                caseOwnerMap.put(eventCase.Id, eventCase.ownerId);          
        }
        
        if(caseOwnerMap == null || caseOwnerMap.isEmpty()) {
            return;
        }
        System.debug('case owner map is ...' + caseOwnerMap);
        Set<Id> eventOwnerId = new Set<Id> ();
        Set<Id> businessOwnerId = new Set<Id> ();
        for(Event event: [Select Id, ownerId, Subject From Event where ownerId IN: caseOwnerMap.values() And (ShowAs = 'OutOfOffice' OR Subject = 'Business Hours') AND StartDateTime <=: System.now() AND EndDateTime >=: System.now() limit 50000]) {
            if(event.Subject == 'Business Hours') {
                businessOwnerId.add(event.ownerId);
            }else {
                eventOwnerId.add(event.ownerId);
            }
        }
        System.debug('EventOwnerId is ...' + eventOwnerId);
        System.debug('businessOwnerId is ...' + businessOwnerId );

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        List<EmailTemplate> templates = [Select Id from EmailTemplate where DeveloperName = 'Out_of_the_office_Case_Notification' limit 1];
        for(Id id : caseOwnerMap.keySet()) {
            System.debug('right condition result is ...' + !businessOwnerId.contains(caseOwnerMap.get(Id)));
            if(eventOwnerId.contains(caseOwnerMap.get(id)) || !businessOwnerId.contains(caseOwnerMap.get(Id))) {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(Label.Case_Contact_Email);
                email.setReplyto(Label.Pure_Storage_Support_Email);
                email.setSenderDisplayName(PURE_STORAGE_SUPPORT);
                email.setWhatId(id);
                email.setTemplateId(templates[0].id);
                email.saveAsActivity = false;
                emails.add(email);
            }
        }
        try{
            // Emails are sent only if it production org. If you want to send it from this sandbox,
            // update the SC_ProductionOrgId with this sandbox's 18 digit org id
            if(userinfo.getOrganizationId() == system.label.SC_ProductionOrgId)
                Messaging.sendEmail(emails);
        }
        catch(EmailException e) {
                Util.SendAdminEmail('CaseCommentTriggerHelper Send Email to Case Alias failed', e.getStackTraceString() + '\n' +  e.getMessage(), Label.Help_Desk_Email);
        }
    }
}