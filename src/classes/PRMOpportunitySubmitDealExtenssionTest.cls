/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRMOpportunitySubmitDealExtenssionTest {

    static testMethod void regHandlerTest() {
		TestDataFactory rec = new TestDataFactory();
		Account acct=rec.getAccount();
		insert acct;
		
		Opportunity opp=rec.createOpportunity(acct.id,Constants.OPPORTUNITY_EXTENSION_STATUS_ELIGIBLE);
		test.startTest();
		PageReference pageRef = Page.PRM_Opportunity_Submit_Deal_Extension;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PRMOpportunitySubmitDealExtenssion controller = new PRMOpportunitySubmitDealExtenssion(sc);
        controller.submitForApproval();
        test.stopTest();
	}
	   static testMethod void regHandlerTest2() {
		TestDataFactory rec = new TestDataFactory();
		Account acct=rec.getAccount();
		insert acct;
		
		Opportunity opp=rec.createOpportunity(acct.id,Constants.OPPORTUNITY_EXTENSION_STATUS_PENDING);
		test.startTest();
		PageReference pageRef = Page.PRM_Opportunity_Submit_Deal_Extension;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PRMOpportunitySubmitDealExtenssion controller = new PRMOpportunitySubmitDealExtenssion(sc);
        controller.submitForApproval();
        test.stopTest();
	}
	 static testMethod void regHandlerTest3() {
		TestDataFactory rec = new TestDataFactory();
		Account acct=rec.getAccount();
		insert acct;
		
		Opportunity opp=rec.createOpportunity(acct.id,Constants.OPPORTUNITY_EXTENSION_STATUS_NOT_ELIGIBLE);
		test.startTest();
		PageReference pageRef = Page.PRM_Opportunity_Submit_Deal_Extension;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PRMOpportunitySubmitDealExtenssion controller = new PRMOpportunitySubmitDealExtenssion(sc);
        controller.submitForApproval();
        test.stopTest();
	}
	 static testMethod void regHandlerTest4() {
		TestDataFactory rec = new TestDataFactory();
		Account acct=rec.getAccount();
		insert acct;
		
		Opportunity opp=rec.createOpportunity(acct.id,Constants.OPPORTUNITY_EXTENSION_STATUS_APPROVED);
		test.startTest();
		PageReference pageRef = Page.PRM_Opportunity_Submit_Deal_Extension;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PRMOpportunitySubmitDealExtenssion controller = new PRMOpportunitySubmitDealExtenssion(sc);
        controller.submitForApproval();
        test.stopTest();
	}
	 static testMethod void regHandlerTest5() {
		TestDataFactory rec = new TestDataFactory();
		Account acct=rec.getAccount();
		insert acct;
		
		Opportunity opp=rec.createOpportunity(acct.id,Constants.OPPORTUNITY_EXTENSION_STATUS_REJECTED);
		test.startTest();
		PageReference pageRef = Page.PRM_Opportunity_Submit_Deal_Extension;
        pageRef.getParameters().put('id', String.valueOf(opp.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        PRMOpportunitySubmitDealExtenssion controller = new PRMOpportunitySubmitDealExtenssion(sc);
        controller.submitForApproval();
        test.stopTest();
	}
}