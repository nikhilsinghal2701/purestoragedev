/*****************************************************************************
 * Description : Purpose of this class is to test the Search Controller for Partner Locator functionality
 * Author      : Sriram Swaminathan
 * Date        : 07/30/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
@isTest
private class PartnerLocatorSearchControllerTest {
	
	@isTest 
	static void searchTest() {
		Integer sampleDataSize = 20;
		PartnerLocatorTestData.setup(sampleDataSize);

		PartnerLocatorSearchController ctrl = new PartnerLocatorSearchController();
		System.assertEquals(TRUE, ctrl.getThis()!=NULL, 'Expected an instance of the search controller');	
		ctrl.navigateToSearch();
		System.assertEquals(TRUE, ctrl.ctx.isContextSearch(), 'Expected the context to be search');		
		ctrl.partnerLocatorSearchBean.selectedCountry = 'United States';		
		ctrl.search();
		System.assertEquals(TRUE, ctrl.isSearchEvent(), 'Expected an search event');	
		ctrl.resetSearchEvent();
		ctrl.navigateToList();
		ctrl.navigateToDetails(null);
	}
	
}