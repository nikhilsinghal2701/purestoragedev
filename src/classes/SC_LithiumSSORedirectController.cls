/* Controller class for SC_LithiumSSORedirct Page
 * this class will fetch the customer community Id
 */

public class SC_LithiumSSORedirectController
{
    // community ID
    public string communityID {get;set;}
    
    // constructor
    public SC_LithiumSSORedirectController()
    {
        try
        {
            Network community = [Select Id, Name from Network where Name = :system.label.SC_Customer_Community_Name];
            communityId = community.Id;
            
            // switch to customer community link will only work with 15 digit community Id
            communityId = communityId.substring(0, 15);
        }
        catch(Exception e)
        {
            Apexpages.addMessage(new Apexpages.Message(APEXPAGES.SEVERITY.ERROR, 'No community with name ' + system.label.SC_Customer_Community_Name + ' is available'));
        }
    }
}