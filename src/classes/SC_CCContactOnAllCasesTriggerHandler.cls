/***********************************************
 * Description : Handler for SC_CCContactOnAllCasesTrigger. It has after insert & after update methods to 
                 create/remove CC enabled/disabled contacts as case team members
 * Author      : Sorna (Perficient)
 * CreatedDate : 06/26/2014
 ***********************************************/

public without sharing class SC_CCContactOnAllCasesTriggerHandler
{
    public static final string CC_CONTACT_ROLENAME = system.label.SC_CCContactRole;
    
    // on after insert, if the contact is marked to be CCed, create the contact as case team member
    public static void onAfterInsert(Map<Id, Contact> newMap)
    {
        Set<Id> accIdsToAddCaseTeam = new Set<Id>();
        
        for(Contact c : newMap.values())
        {
            if(c.AccountId != null && c.CC_On_All_Cases__c)
            {
                accIdsToAddCaseTeam.add(c.AccountId);
            }
        }
        
        if(accIdsToAddCaseTeam.size() > 0)
        {
            createCaseTeam(newMap, accIdsToAddCaseTeam);
        }
    }
    
    // on after update, if the contact is marked to be CCed, create the contact as case team member
    // if the contact is unchecked from CC, delete the contact from the case team
    public static void onAfterUpdate(Map<Id, Contact> oldMap, Map<Id, Contact> newMap)
    {
        Set<Id> accIdsToAddCaseTeam = new Set<Id>();
        Set<Id> conIdsToRemoveCaseTeam = new Set<Id>();
        
        for(Contact c : newMap.values())
        {
            if(c.AccountId != null && c.CC_On_All_Cases__c != oldMap.get(c.Id).CC_On_All_Cases__c
                && c.CC_On_All_Cases__c)
            {
                accIdsToAddCaseTeam.add(c.AccountId);
            }
            else if(c.AccountId != null && c.CC_On_All_Cases__c != oldMap.get(c.Id).CC_On_All_Cases__c
                && !c.CC_On_All_Cases__c)
            {
                conIdsToRemoveCaseTeam.add(c.Id);
            }
        }
        
        // to create contact as case team
        if(accIdsToAddCaseTeam.size() > 0)
        {
            createCaseTeam(newMap, accIdsToAddCaseTeam);
        }
        
        // to remove contact as case team
        if(conIdsToRemoveCaseTeam.size() > 0)
        {
            deleteCaseTeam(conIdsToRemoveCaseTeam);
        }
    }
    
    // creates case team members
    public static void createCaseTeam(Map<Id, Contact> contactsMap, Set<Id> accIds)
    {
        Map<Id, Account> accountsMap = new Map<Id, Account>();
        List<CaseTeamMember> members = new List<CaseTeamMember>();
        List<Database.SaveResult> results;
        
        Id caseTeamRoleID = getCCContactRoleID();
        
        // query all the cases of the account
        accountsMap = new Map<Id, Account>([Select Id, (Select Id from Cases) from Account where Id in :accIds]);
        
        for(Contact con : contactsMap.values())
        {
            if(accountsMap.get(con.AccountId).Cases != null && accountsMap.get(con.AccountId).Cases.size() > 0)
            {
                for(Case c : accountsMap.get(con.AccountId).Cases)
                {
                    members.add(populateCaseTeamMemberValues(c, con, caseTeamRoleID));
                }
            }
        }
        
        // add error
        if(members.size() > 0)
        {
            results = Database.insert(members, false);
            
            for(Integer i = 0; i < results.size(); i++)
            {
                if(!results[i].IsSuccess())
                {
                    trigger.newMap.get(members[i].MemberId).addError(results[i].getErrors()[0].getMessage());
                }
            }
        }
    }
    
    // removes contact from the case team
    public static void deleteCaseTeam(Set<Id> conIds)
    {
        List<CaseTeamMember> members = new List<CaseTeamMember>();
        List<Database.DeleteResult> results;
        
        Id caseTeamRoleID = getCCContactRoleID();
        
        members = [Select Id, MemberId from CaseTeamMember where MemberId in :conIds and TeamRoleId = :caseTeamRoleID];
        
        if(members.size() > 0)
        {
            results = Database.delete(members, false);
            
           for(Integer i = 0; i < results.size(); i++)
            {
                if(!results[i].IsSuccess())
                {
                    trigger.newMap.get(members[i].MemberId).addError(results[i].getErrors()[0].getMessage());
                }
            }
        }
    }
    
    // populate case team member record
    public static CaseTeamMember populateCaseTeamMemberValues(Case c, Contact con, ID caseTeamRoleID)
    {
        CaseTeamMember cm = new CaseTeamMember(ParentId = c.ID, MemberId = con.Id, TeamRoleID = caseTeamRoleID);
        return cm;
    }
    
    // gets the contact role for CC enabled contacts
    public static Id getCCContactRoleID()
    {
        try
        {
            CaseTeamRole ctr = [Select Id from CaseTeamRole where Name = :CC_CONTACT_ROLENAME];
            return ctr.Id;
        }
        catch(Exception e)
        {
            for(Case c : (List<Case>) trigger.new)
            {
                c.addError(e);
            }
            
            return null;
        }
    }
}