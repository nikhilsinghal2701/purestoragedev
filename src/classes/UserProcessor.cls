public class UserProcessor 
{
    public static final Apex_Code_Settings__c customSettings = Apex_Code_Settings__c.getInstance();
    public static boolean USER_TRIGGER_ENABLED = customSettings.User_Trigger_Enabled__c;
    
    public void processRecords (List<User> newRecs, List<User> oldRecs, Map<Id,User> newMap, Map<Id,User> oldMap)
    {
        if(USER_TRIGGER_ENABLED)
        {
            list<User> usersToUpd = new list<User>();
            set<id> userIdsInserted = new set<id>();
            for(User u: newRecs)
            {
                userIdsInserted.add(u.Id);
            }
            for(User u: [select Id, CreatedDate from User where Id in: userIdsInserted])
            {
                u.User_Created_Date__c = system.today();
                usersToUpd.add(u);
            }
            update usersToUpd;
        }
    
    }
}