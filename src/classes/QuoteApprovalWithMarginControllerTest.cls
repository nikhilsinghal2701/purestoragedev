@isTest
private class QuoteApprovalWithMarginControllerTest
{
public static Account generateAccount()
    {
        Account retVal = new Account(Name = 'testAccount');
        return retVal;
    }
    
    public static Product2 generateProduct2()
    {
        Product2 retVal = new Product2(Name = 'testProduct', 
                                        Is_Hardware__c = true,
                                        Product_Cost__c = 100.00,
                                        IsActive = true);
        return retVal;
    }
    
    public static Pricebook2 generatePricebook2()
    {
        Pricebook2 pb = [select Id from Pricebook2 limit 1];
        return pb;
    }
    
    public static PricebookEntry generatePricebookEntry(id inputProduct2Id) 
    {
        Pricebook2 pb = [select Id from Pricebook2 limit 1];
        PricebookEntry retVal = new PricebookEntry(Pricebook2Id = pb.Id,
                                                    IsActive = true,
                                                    Product2Id = inputProduct2Id,
                                                    UnitPrice = 100.00,
                                                    CurrencyIsoCode = 'USD');
        return retVal;
    }
    
    public static Opportunity generateOpportunity(id inputAccountId)
    {
        Opportunity retVal = new Opportunity(Name = 'Some Oppty',
                                                AccountId = inputAccountId,
                                                CloseDate = System.Today(),
                                                CurrencyIsoCode = 'USD',
                                                StageName = 'TBD');
        return retVal;
    }
    
    public static OpportunityLineItem generateOLI(id inputOpportunity, id inputPricebookEntryId)
    {
        OpportunityLineItem retVal = new OpportunityLineItem(OpportunityId = inputOpportunity,
                                                                PricebookEntryId = inputPricebookEntryId,
                                                                Quantity = 1,
                                                                UnitPrice = 100.00);
        return retVal;
    }
    
    public static Quote generateQuote(id inputOpptyId)
    {
        Quote retVal = new Quote(Name = 'Some Quote',
                                 OpportunityId = inputOpptyId,
                                 Status = 'Draft',
                                 Pricebook2Id = generatePricebook2().Id);
        return retVal;
    }
    
    public static QuoteLineItem generateQLI(id inputQuoteId, id inputPBEId)
    {
        QuoteLineItem retVal = new QuoteLineItem(PriceBookEntryId = inputPBEId,
                                 QuoteId = inputQuoteId,
                                 Quantity = 1,
                                 UnitPrice = 100.00);
        return retVal;
    }
    
    @istest(SeeAllData = true)
    static void testController()
    {
        Account acct = generateAccount();
        insert acct;
        
        Product2 p = generateProduct2();
        insert p;
        
        PricebookEntry pbe = generatePricebookEntry(p.Id);
        insert pbe;
        
        Opportunity o = generateOpportunity(acct.id);
        insert o;
        
        OpportunityLineItem oli = generateOLI(o.Id, pbe.Id);
        insert oli;
        
        Quote q = generateQuote(o.Id);
        insert q;
        
        QuoteLineItem qli = generateQLI(q.Id, pbe.Id);
        insert qli;
        
        Test.startTest();
    		QuoteApprovalWithMarginController qc = new QuoteApprovalWithMarginController();
        	qc.quoteId = q.Id;
        	qc.getMyQuoteLineItem();
        	system.assert(qc.qliList.size()>0);
        	qc.getmyMargin();
        	system.assert(qc.getmyMargin() == 0);
    	Test.stopTest();
    }        
        

}