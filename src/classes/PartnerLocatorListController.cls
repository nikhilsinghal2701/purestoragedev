/*****************************************************************************
 * Description : Class that controls a paginated list of Partner Locator Detail records
 * Author      : Sriram Swaminathan (Perficient)
 * Date        : 07/27/2014
 * Version     : 1.0
 *
 * Version History : 
 * 
 *****************************************************************************/
public class PartnerLocatorListController {
	
	private static final PartnerLocatorService service = PartnerLocatorFactory.getInstance();
	protected Integer DEFAULT_PAGE_SIZE = 10;
	protected Integer DEFAULT_PAGE_START_NUMBER = 1;

	public PartnerLocatorListController() {
		pageNumber = DEFAULT_PAGE_START_NUMBER;
	}

	/**
	 * Set the Partner Locator Search Controller
	 *
	 */
	public PartnerLocatorSearchController partnerLocatorSearchController {
		get;
		set {
			partnerLocatorSearchController = value;
			if (partnerLocatorSearchController.isSearchEvent()){
				firstPage();
				partnerLocatorSearchController.resetSearchEvent();
				partnerLocatorSearchController.partnerLocatorSearchBean.selectedStateOrProvince = '';
			}
			loadRecords();
		}
	}
	/**
	 * Return the paginated list of Partner Locator records
	 *
	 */	
	/*public List<Partner_Locator_Bean__c> partnerLocatorRecords {
		get {
			partnerLocatorRecords = new List<Partner_Locator_Bean__c>();
			if (stdSetCtrl == null){
				loadRecords();
			}
			for (sObject obj : stdSetCtrl.getRecords()){
				partnerLocatorRecords.add((Partner_Locator_Bean__c)obj);
			}
			return partnerLocatorRecords;
		}
		set;
	}*/		
	/**
	 *	This method loads the Partner Locator records into the Custom bean objects of type PartnerLocatorBean
	 */	
	public void loadRecords() {
		records = new List<PartnerLocatorBean>();
		records.addAll(service.search(partnerLocatorSearchController.partnerLocatorSearchBean));
	}

	/**
	 *	Variable maintained for the Partner Locator Detail record selected in the front-end view
	 */		
	public String selectedPartnerLocatorName { 
		get; 
		set; 
	}
   /**	
	*	Perform the action for Country change event
	*
	*/	
	public void onCountryChange() {
		partnerLocatorSearchController.partnerLocatorSearchBean.selectedStateOrProvince = '';
		loadRecords();
		firstPage();
	}
   /**	
	*	Perform the action for State or Province change event
	*
	*/		
	public void onStateOrProvinceChange() {
		loadRecords();
		firstPage();
	}
   /**	
	*	Navigate to Partner Locator Details 
	*
	*/
	public void navigateToDetails() {
		partnerLocatorSearchController.navigateToDetails(service.getPartnerLocatorByName(selectedPartnerLocatorName));
	}

	public Integer pageNumber { 
		get;
		set;
	}	

	public Integer pageSize { 
		get {
			return (pageSize==NULL?DEFAULT_PAGE_SIZE:pageSize);
		}
		set {
			pageSize = value;
		}
	}	
	
	public List<PartnerLocatorBean> pageRecords { 
		get {
			pageRecords = new List<PartnerLocatorBean>();
			Transient Integer counter = 0;
			Transient Integer min = 0;
			Transient Integer max = 0;
				
			if (pageNumber <= 1){ 					//first page
				min = 0;
				max = pageSize;
			}else if (pageNumber >= totalPageNumber){ //last page
				min = (totalPageNumber-1) * pageSize;
				max = Integer.valueOf(records.size());
			}else {
				min = (pageNumber - 1) * pageSize;
				max = pageNumber * pageSize;
			}				
			for(PartnerLocatorBean a : records){
				counter++;
				if (counter > min && counter <= max){
					pageRecords.add(a);
				}
			}
			return pageRecords;			
		}
		set;
	}

	public List<PartnerLocatorBean> records { 
		get {
			if (records == null){
				loadRecords();
			}
			return records;			
		}
		set;
	}	

	public Integer recordSize {
		get {
			return records.size();
		}
	}
	
	public Boolean hasPrevious {
		get {
			return ( (pageNumber > 1) && (pageNumber <= totalPageNumber) );
		}		
	}


	public Boolean hasNext {
		get {
			if (records == null) {
				return false;
			} else {
				return ((pageNumber * pageSize) < records.size());
			}
		}		
	}

	public Integer totalPageNumber {
		get {
			Integer calculatedTotalPageNumber = 0;
			if (records ==null || records.isEmpty()){
				calculatedTotalPageNumber = 0;
			}else{
				Integer RECORD_SIZE = Integer.valueOf(records.size());
				Integer remainder = math.mod(RECORD_SIZE, pageSize);
				if (remainder == 0){
					calculatedTotalPageNumber = RECORD_SIZE / pageSize;
				}else{
					calculatedTotalPageNumber = (RECORD_SIZE / pageSize) + 1;
				}
			}
			return calculatedTotalPageNumber;			
		}
	}

 	public void firstPage() {
 		System.debug('PartnerLocatorListController class - firstPage method');
		pageNumber = 1;
 	}
 
 	public void lastPage() {
 		System.debug('PartnerLocatorListController class - lastPage method');
		pageNumber = totalPageNumber;
 	}

	public void nextPage() {
		System.debug('PartnerLocatorListController class - nextPage method');
		pageNumber = ((pageNumber + 1)>totalPageNumber)?totalPageNumber:(pageNumber + 1);
	}


	public void previousPage() {
		System.debug('PartnerLocatorListController class - previous page method');
		pageNumber = ((pageNumber - 1)<1)?1:(pageNumber - 1);
	}
	
	public void goToPage() {
		System.debug('PartnerLocatorListController class - goToPage method : Page Number ' + pageNumber);
		pageNumber = (	(pageNumber < 1)
						? 	1
						:	(	(pageNumber > totalPageNumber)
								?	totalPageNumber
								:	pageNumber
							)
					);
	
	}

	public void pagePlus2() {
		pageNumber = ((pageNumber + 2)>totalPageNumber)?totalPageNumber:(pageNumber + 2);
	}		

}