/*************************************************
Class: PartnerLocatorDetailsTriggerHelper
Description: This is the helper class for trigger PartnerLocatorDetailsTrigger
Auther: Christine Wang
Date:   8/25/2014

Revision History: 
----------------------------------------------------     
   
***************************************************/
public without sharing class PartnerLocatorDetailsTriggerHelper{
    // method fires on insert of Partner Locator Detail
    public static void populateChannelAccountManagerOnInsert (List<Partner_Locator_Details__c> newLocators) {
        List<Id> newLocatorIdList = new List<Id>();
        List<Partner_Locator_Details__c> locatorList = new List<Partner_Locator_Details__c>();
        List<Id> LocatorOwnerIdList = new List<Id>();
        Map<Id, Id> userIdToAccountIdMap = new Map<Id, Id>();
        Map<Id, Id> userIdToAccountOwnerMap = new Map<Id, Id>();
        List<Partner_Locator_Details__c> updateLocators = new List<Partner_Locator_Details__c>();
        
        // Get inserted Partner Locator Details
        for (Partner_Locator_Details__c locator : newLocators) {           
            newLocatorIdList.add(locator.Id);
        }
        locatorList = [SELECT CreatedById, Channel_Account_Manager__c, Partner_Account__c FROM Partner_Locator_Details__c WHERE Id IN :newLocatorIdList];
        
        // Get Partner Id
        for (Partner_Locator_Details__c locator : locatorList) {           
            System.debug('CreatedById = ' + locator.CreatedById);
            LocatorOwnerIdList.add(locator.CreatedById);
        }
        
        // Get Partner Account Owner Id
        if (LocatorOwnerIdList.size() > 0) {
            for (User u : [SELECT ContactId, Contact.AccountId, Contact.Account.OwnerId FROM User WHERE Id IN :LocatorOwnerIdList]) {
                System.debug('u = ' + u);
                userIdToAccountIdMap.put(u.Id, u.Contact.AccountId);
                userIdToAccountOwnerMap.put(u.Id, u.Contact.Account.OwnerId);
            }
        }
        
        // Populate the Channel Account Manager with the Partner Account Owner
        if (!userIdToAccountOwnerMap.isEmpty()) {
            for (Partner_Locator_Details__c locator : locatorList) {
                if (userIdToAccountOwnerMap.get(locator.CreatedById) != null && userIdToAccountIdMap.get(locator.CreatedById) != null) {
                    locator.Channel_Account_Manager__c = userIdToAccountOwnerMap.get(locator.CreatedById);
                    locator.Partner_Account__c = userIdToAccountIdMap.get(locator.CreatedById);
                    updateLocators.add(locator);
                }
            }
        }
        
        update updateLocators;
    }

}