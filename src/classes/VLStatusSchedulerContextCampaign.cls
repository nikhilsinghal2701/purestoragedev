global class VLStatusSchedulerContextCampaign implements Schedulable{
    global VLStatusSchedulerContextCampaign(){}
    global void execute(SchedulableContext ctx){
        VLStatusBatchCampaign b = new VLStatusBatchCampaign();
        database.executebatch(b);        
    }
    static testMethod void testExecute() {
    Test.startTest();
    VLStatusSchedulerContextCampaign schedulerContext= new VLStatusSchedulerContextCampaign ();
    String schedule = '0 0 4 * * ?';
    system.schedule('Scheduled Campaign Update', schedule, schedulerContext);
    test.stopTest();
    }
}