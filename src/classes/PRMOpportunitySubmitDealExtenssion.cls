/*
Class: PRMOpportunitySubmitDealExtenssion
Purpose: submits record to approval process.
Author: Jaya
*/
public class PRMOpportunitySubmitDealExtenssion 
{

    public Opportunity opportunityREC{get;set;} 
    public PRMOpportunitySubmitDealExtenssion(ApexPages.StandardController stdController)
    {
        opportunityREC = (Opportunity)stdController.getRecord();
        System.debug('opportunityREC:'+opportunityREC);
    }
    public PageReference submitForApproval()
    {
        Approval.ProcessSubmitRequest req;
        Approval.ProcessResult approvalResult;
        ProcessInstance pendingPI;
        
        opportunityREC = [SELECT Id, Extension_Status__c FROM Opportunity WHERE Id =: opportunityREC.Id];
        if(opportunityREC.Extension_Status__c == Constants.OPPORTUNITY_EXTENSION_STATUS_ELIGIBLE)
        {
            System.debug('opportunityREC:'+opportunityREC);
            try{
                pendingPI = [SELECT Id,TargetObjectID FROM ProcessInstance 
                                WHERE  TargetObjectId =: opportunityREC.Id AND Status=:'Pending'];              
            }
            catch(Exception e){}
            if(pendingPI == null)
            {
                System.debug('no pending');
                req = new Approval.ProcessSubmitRequest();
                req.setComments('Submitting request for approval.');
                req.setObjectId(opportunityREC.Id);
                approvalResult = Approval.process(req);
                System.debug('approval Request:'+approvalResult);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Status_Eligible));
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Pending_Approval));
            }
        }
        else
        {
        	if(opportunityREC.Extension_Status__c == Constants.OPPORTUNITY_EXTENSION_STATUS_PENDING)
        	{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Pending_Approval));
        	}
        	if(opportunityREC.Extension_Status__c == Constants.OPPORTUNITY_EXTENSION_STATUS_NOT_ELIGIBLE)
        	{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Status_Not_Eligible));
        	}
        	if(opportunityREC.Extension_Status__c == Constants.OPPORTUNITY_EXTENSION_STATUS_APPROVED)
        	{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Status_Approved));
        	}
        	if(opportunityREC.Extension_Status__c == Constants.OPPORTUNITY_EXTENSION_STATUS_REJECTED)
        	{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.PRM_Extension_Status_Rejected));
        	}
        }
        return null;        
    }   
}