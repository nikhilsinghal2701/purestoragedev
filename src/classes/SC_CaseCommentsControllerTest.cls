/* Test class for SC_CaseCommentsControllerTest
   to test the creation of public and private comments
 */

@isTest
private class SC_CaseCommentsControllerTest
{
    // test the creation of private cases comments
    private static testMethod void testCreatePrivateComment()
    {
        // insert test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        
        // run as a TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con = SC_TestUtil.createContact(true, acc);
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
            // create test case
            Case c = SC_TestUtil.createCase(true, acc, con, parentArray);

            Test.startTest();

            Test.setCurrentPage(Page.SC_CaseCommentsCustomView);
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_CaseCommentsController obj = new SC_CaseCommentsController(new ApexPages.StandardController(c));
            obj.selectedOption = 'Private';
            obj.commentBody = 'Test Case Comment';
            obj.insertCaseComment();

            Test.stopTest();

            // assert that a private case comment is added to the case
            system.assertEquals(1, [Select count() from CaseComment where IsPublished = :false and ParentID = :c.Id], 
                                    'One private case comment should have been created');
        }
    }

    // test the creation of public case comments
    private static testMethod void testCreatePublicComment()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        // run as a TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con = SC_TestUtil.createContact(true, acc);
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
            Case c = SC_TestUtil.createCase(true, acc, con, parentArray);

            Test.startTest();

            Test.setCurrentPage(Page.SC_CaseCommentsCustomView);
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_CaseCommentsController obj = new SC_CaseCommentsController(new ApexPages.StandardController(c));
            obj.selectedOption = 'Public';
            obj.commentBody = 'Test Case Comment';
            obj.insertCaseComment();

            Test.stopTest();

            // assert that a public case comment is added to the case
            system.assertEquals(1, [Select count() from CaseComment where IsPublished = :true and ParentID = :c.Id], 
                                    'One public case comment should have been created');
        }
    }

    // test the validation error msgs
    private static testMethod void testCommentRequiredErrorMsg()
    {
        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        // run as a TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con = SC_TestUtil.createContact(true, acc);
        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        
            Case c = SC_TestUtil.createCase(true, acc, con, parentArray);

            Test.startTest();

            Test.setCurrentPage(Page.SC_CaseCommentsCustomView);
            ApexPages.currentPage().getParameters().put('id', c.Id);

            SC_CaseCommentsController obj = new SC_CaseCommentsController(new ApexPages.StandardController(c));
            obj.selectedOption = 'Private';
            obj.insertCaseComment();

            Test.stopTest();

            // if the case comment body is not entered, an error msg should be shown in the page and the case comments should not be created
            system.assertEquals(0, [Select count() from CaseComment where IsPublished = :false and ParentID = :c.Id]);
            system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_EnterComments), 
                                    'Comments Requried Error Msg should be added to the page');
        }
    }
}