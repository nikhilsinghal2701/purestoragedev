global class VLStatusSchedulerContext implements Schedulable{
    global VLStatusSchedulerContext(){}
    global void execute(SchedulableContext ctx){
        VLStatusBatch b = new VLStatusBatch();
        database.executebatch(b,1);        
    }
    static testMethod void testExecute() {
        Test.startTest();
        VLStatusSchedulerContext schedulerContext = new VLStatusSchedulerContext();
        String schedule = '0 0 8 * * ?';
        system.schedule('Scheduled Update', schedule, schedulerContext);
        test.stopTest();
    }
}