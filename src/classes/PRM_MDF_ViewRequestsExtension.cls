public with sharing class PRM_MDF_ViewRequestsExtension {
	public List<MDF_Request__c> mdfRequestList {get;set;}
	public string allFields{get;set;}
	Set<String> myObjectFieldAPINames {get;set;}
	String Uid = UserInfo.getUserId();
	
	public PRM_MDF_ViewRequestsExtension(ApexPages.StandardController controller) {
		  // Get a map of all fields available to you on the MyObject__c table/object
    	// keyed by the API name of each field
    Map<String,Schema.SObjectField> myObjectFields  = MDF_Request__c.SObjectType.getDescribe().fields.getMap();
    
    // Get a Set of the field names
    myObjectFieldAPINames = myObjectFields.keyset();
    
    // Print out the names to the debug log
    for (String s : myObjectFieldAPINames) {
        allFields += s + '\n';
    }
	
	List<String> fieldsList = new List<String>(myObjectFieldAPINames);
    String query = 'SELECT ';
    // Add in all but the last field, comma-separated
    for (Integer i = 0; i < fieldsList.size()-1; i++) {
               query += fieldsList[i] + ',';
    }
    // Add in the final field
    query += fieldsList[fieldsList.size()-1];
    // Complete the query
    query += ' FROM MDF_Request__c WHERE ownerid =:Uid';
    
    // Perform the query (perform the SELECT *)
    mdfRequestList = Database.query(query);
	}

}