global class VLStatusParty
{
    global static void PartyCreate_Update(String strPartyId)
    {
        Account acc = new Account();
        acc = [select Id,Name,AccountNumber,XPSynced__c from Account where Id = :strPartyId];
        String strCreatePartyURL = '';
        String strXPServer = '';
        String strXPSecurityToken = '';
        
        String strStatus = '';
        String strXPEnterpriseID = '';
        
        if(!Test.isRunningTest()){
            strXPEnterpriseID = SMXConfiguration__c.getValues('Configuration').ID_XP_ENTERPRISE__c;
            strXPServer = SMXConfiguration__c.getValues('Configuration').XP_SERVER__c;
            strCreatePartyURL = SMXConfiguration__c.getValues('Configuration').URL_CREATEPARTY_SERVICE__c;
            strXPSecurityToken = SMXConfiguration__c.getValues('Configuration').XP_SECURITYTOKEN__c;
        }
        Datetime currentDate = System.now();
        
        String strCurrentDate = currentDate.format('yyyy-MM-dd') + 'T' + currentDate.format('HH:mm:ss') + '.00';
        
        //construct an HTTP request
        HttpRequest req = new HttpRequest();
        req.setTimeout(120000);
        req.setMethod('POST');
        req.setHeader('Host',strXPServer);
        
        req.setHeader('Connection','keep-alive');
        req.setEndpoint(strCreatePartyURL);
        req.setHeader('Content-Type','text/xml');
        req.setHeader('DataEncoding','UTF-8');
        string strXML = '<?xml version="1.0" encoding="utf-8"?>' +   
              '<utilService><companyIdfier>'+
              + strXPEnterpriseID +
              '</companyIdfier>'+               
              '<securityToken>'+
              + strXPSecurityToken +
              '</securityToken>' + 
              '<party enterprisePartyYN="N" primaryIdfier="'+escapeXML(acc.Id)+'" partyTypeCode="ORG" primaryEmailAddressIdfier="comapany@'+escapeXML(acc.Id)+'.com" personIdentifier="'+escapeXML(acc.Id)+'@'+escapeForCompanyEmail(acc.Name)+'.com"  enabledYN="Y" updateYN ="Y"><partyT-list><partyT nameT="'+escapeXML(acc.Name)+'" descriptionT="'+escapeXML(acc.Id)+'" localeCode="en_US" sourceLocaleCode="en_US" translatedTimestamp="'+ strCurrentDate +'"/>'+
              '</partyT-list></party></utilService>';
        System.debug(strXML);      
        req.setBody(strXML);  
        
        Http http = new Http();
        try
        {
            HTTPResponse httpResponse;
            if(!Test.isRunningTest()){
             httpResponse = http.send(req);
             }else{
              httpResponse = new HttpResponse();
              httpResponse.setBody(returnTestXML());
              httpResponse.setStatusCode(testHttpStatusCode); 
             }
            if ((httpResponse.getStatusCode()) == 200)
            {
                String xmlString = httpResponse.getBody();
                XMLDom doc= new XMLDom(xmlString);
                XMLDom.Element compDataXML = (XMLDom.Element)doc.getElementByTagName('webserviceresponse');
                if(compDataXML != null)
                {
                    String strResultCode, strResultDescription;
                    XMLDom.Element code = (XMLDom.Element)compDataXML.getElementByTagName('code');
                    if(code != null)
                    strResultCode= code.nodeValue;
                    XMLDom.Element description = (XMLDom.Element)compDataXML.getElementByTagName('message');
                    if(description != null)
                    strResultDescription= description.nodeValue;
                    if (strResultCode == '0')
                    {
                        Account partyUpdate = [select Id,XPSynced__c from Account where Id = :acc.Id];
                        partyUpdate.XPSynced__c = 'Y';
                        update partyUpdate;
                        strStatus = 'Success';                      
                    }
                    if (strResultCode <> '0')
                    {
                        strStatus = 'Failure';
                        Account partyUpdate = [select Id,XPSynced__c from Account where Id = :acc.Id];
                        partyUpdate.XPSynced__c = 'N';
                        update partyUpdate;                                             
                    }
                }
            }
            if ((httpResponse.getStatusCode()) <> 200)
            {
                String strMessage = httpResponse.getStatus();
                Account partyUpdate = [select Id,XPSynced__c from Account where Id = :acc.Id];
                partyUpdate.XPSynced__c = 'N';
                update partyUpdate;
                strStatus = 'Failure';              
            }
        }
        catch(System.CalloutException e)
        {
            String strMessage = e.getMessage();
            strStatus = 'Failure';          
        }
    }
    
     public static String escapeXML(String strInput){
      String strReturnValue = '';
      if (strInput != null && strInput != '')
      {
        strInput = strInput.replace('"','&quot;');
        strInput = strInput.replace('<','&lt;');
        strInput = strInput.replace('>','&gt;');
        strInput = strInput.replace('&','&amp;');    
        strReturnValue = strInput;
      }
      return strReturnValue;  
    }


    public static String escapeForCompanyEmail(String strInput){
      String strReturnValue = '';
      if (strInput != null && strInput != '')
      {
        strInput = strInput.replace('"','');
        strInput = strInput.replace('<','');
        strInput = strInput.replace('>','');
        strInput = strInput.replace('&','&amp;');  
        strInput = strInput.replace(' ','');  
        strInput = strInput.replace('.','');  
        strInput = strInput.replace(',','');   
        strReturnValue = strInput;
      }
      return strReturnValue;  
    }
    
   /******** test methods ******/
   static Integer testCaseNum;
   static Integer testHttpStatusCode = 200;
    
   public static String returnTestXML(){
      if(testCaseNum == 1){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>0</code><description></description><row value="success"></row></webserviceresponse>';
      }else if(testCaseNum  == 2){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>0</code><description></description><row value="No Send Rule is applied for the provider"></row></webserviceresponse>';        
      }else if(testCaseNum  == 3){
        testHttpStatusCode = 200;
        return '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }else if(testCaseNum  == 4){
        testHttpStatusCode = 404;
        return '<webserviceresponse><code>-1</code><description></description><row value=""></row></webserviceresponse>';        
      }
      return '';
    }
        
  @isTest(SeeAllData=true)
   static void testFeedbackUpdate(){   
    String strPartyID = prepareTestData();
    for(Integer i = 1; i<=4; i++){
        testCaseNum = i;
        PartyCreate_Update(strPartyID);
        }    
       cleanupTestData(); 
    }
    
  static String prepareTestData()
  {
    Account a = new Account(Name='SMX Test Account',BillingCity='BillingCity',BillingCountry = 'BillingCountry',BillingPostalCode = 'BillingPostalCode',BillingState = 'BillingState',BillingStreet = 'BillingStreet',Type = 'Type', XPSynced__c = 'N');
    insert a;
    
    return a.Id;
  }  
  
  static void cleanupTestData(){
   Account a = [SELECT Id FROM ACCOUNT WHERE NAME='SMX Test Account'];
   delete a;
  }
    
}