/*
Class: LeadTriggerHandler
Author: Jaya
Created Date: 11/20/2013
*/
public class LeadTriggerHandler 
{
	
	public void beforeInsertAction( List<Lead> leadRecList)
	{
		// get record type Global Partner Deal
		 Schema.Describesobjectresult leadRecordType = Schema.Sobjecttype.Lead;
         Map<String,Schema.RecordTypeInfo> leadRecordTypeMap = leadRecordType.getRecordTypeInfosByName();
         RecordTypeInfo gpRecordTypeId = leadRecordTypeMap.get(Constants.LEAD_GLOBAL_PARTNER_DEAL);
        
		 Id globalRecTypeId = gpRecordTypeId.getRecordTypeId();
		 Id runningUsrId = System.Userinfo.getUserId();
		 try{
			 User runningUser = [SELECT Id, Name, IsPortalEnabled, IsActive, Contact.AccountId, Contact.Account.Name,
			 						ContactId, Contact.Account.OwnerId 
			 						From User 
			 						WHERE Id = :runningUsrId AND IsActive = true AND IsPortalEnabled = true];
			
			Contact contactRec = [SELECT Id, Name, AccountId, Account.OwnerId FROM Contact WHERE Id = :runningUser.ContactId];
					 						
			// assigine the account to the
			if(runningUser != null && contactRec != null)
			{ 
				for(Lead leadRec: leadRecList)
				{
					leadRec.PRM_Partner_Account__c = contactRec.AccountId;//runningUser.Account.Name;
					leadRec.Partner_Contact__c = runningUser.ContactId;
					leadRec.Channel_Account_Manager__c = contactRec.Account.OwnerId;
				}
			}
		 }
		 catch(Exception e){}
	}
	public void afterInsertAction(List<Lead> leadsList)
	{
		Schema.Describesobjectresult leadRecordType = Schema.Sobjecttype.Lead;
        Map<String,Schema.RecordTypeInfo> leadRecordTypeMap = leadRecordType.getRecordTypeInfosByName();
        RecordTypeInfo gpRecordTypeId = leadRecordTypeMap.get(Constants.LEAD_GLOBAL_PARTNER_DEAL);
        
		Id globalRecTypeId = gpRecordTypeId.getRecordTypeId();
		
		if (!Constants.hasalreadyapproval()) 
		{ 
	    	List<Approval.ProcessSubmitRequest> approvalRequestList = new List<Approval.ProcessSubmitRequest>();
	    	Approval.ProcessSubmitRequest req;
	     	List<Approval.ProcessResult> approvalResults;
	     	ProcessInstance[] pendingPInstance = [SELECT Id,TargetObjectID FROM ProcessInstance WHERE  TargetObjectId in :leadsList AND Status=:'Pending' ];
	     	Map<Id, ProcessInstance> pendingPIMap = New Map<id, ProcessInstance>();
	      //Build Map
	        for(ProcessInstance pPI : pendingPInstance)
	        {
	            pendingPIMap.put(pPI.TargetObjectID, pPI);
	        }
	      	for(Lead l : leadsList)
	        {
	      		if( l.recordTypeId == globalRecTypeId && pendingPIMap.get(l.id) == NULL && l.Channel_Account_Manager__c != null)
	            {        
		            req = new Approval.ProcessSubmitRequest();
		            req.setComments('Submitting request for approval.');
		            req.setObjectId(l.Id);
		            approvalRequestList.add(req); 
	            } 
            
        	}
            if(approvalRequestList.size() > 0)
            {
             System.debug('************ Approvalid2: ************ ' +approvalRequestList);
             Constants.setalreadyapproval(); 
             approvalResults = Approval.process(approvalRequestList);
            }
              Constants.setalreadyapproval();              
          
          }
	}

}