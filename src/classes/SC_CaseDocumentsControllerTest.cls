/* Test class for SC_CaseDocumentsController 
   to test the functionalities of showing case and enterprise documents to a TSE,
   in a iconic view and list view and providing the option for the TSE to upload 
   any case document to the enterprise
 */

@isTest
private class SC_CaseDocumentsControllerTest
{
    // test the functionalities of showing case documents and entperise documents
    // and uploading case document to enterprise
    private static testMethod void createCaseDocumentsAndUploadtoAccount()
    {
        // load custom setting data from static resource
        Test.loadData(SC_Enterprise_Document_Fields__c.sObjectType, 'SC_TestClassEnterpriseDocFields');

        // create test records
        Profile p = SC_TestUtil.getProfile('System Administrator');
        
        // run as TSE
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        // create 2 types of attachments: 1) as image 2) as format other than an image
        // creating 2 types of attachments would help in testing the thumbnails for iconic view
        List<Attachment> attachments = new List<Attachment>();
        attachments.add(SC_TestUtil.createattachment(false, c.Id, 'image/png'));
        attachments.add(SC_TestUtil.createattachment(false, c.Id, 'application/pdf'));
        insert attachments;

        
            Test.startTest();

            Test.setCurrentPage(Page.SC_CaseDocuments);
            ApexPages.currentPage().getParameters().put('id', c.Id);
            SC_CaseDocumentsController obj = new SC_CaseDocumentsController(new ApexPages.StandardController(c));

            // 2 case documents should be rendered now
            system.assertEquals(2, obj.caseDocs.size(), 'There should be two case documents');

            // testing the upload functionality
            obj.caseDocs[1].IsChecked = true;
            // testing cancel selection functionality
            obj.cancelSelection();
            system.assertEquals(false, obj.caseDocs[1].IsChecked, 'Cancel Selection functionality');

            obj.caseDocs[0].IsChecked = true;
            // upload
            obj.uploadDocsToEnterprise();

            Test.stopTest();

            // assert that one document is uploaded to the enterprise and 2 documents still under case documents
            system.assertEquals(1, obj.enterpriseDocs.size(), 'There should be one enterprise document');
            system.assertEquals(2, obj.caseDocs.size(), 'There should be two case documents');
        }
    }

    // test the validation error msgs
    private static testMethod void TestNoDocumentsErrorMsg1()
    {
        Test.startTest();

        Test.setCurrentPage(Page.SC_CaseDocuments);
        SC_CaseDocumentsController obj = new SC_CaseDocumentsController(new ApexPages.StandardController(new Case()));

        Test.stopTest();

        // on the new case page, there won't even be the case id on the controller, but as this page is a 
        // custom console component on the case layout, we need to show no documents available msg
        system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_NoDocuments), 
                                    'No Documents Available Msg should be added to the page');
    }

    // teest the validation msgs
    private static testMethod void TestNoDocumentsErrorMsg2()
    {
        // create test data
        Test.loadData(SC_Enterprise_Document_Fields__c.sObjectType, 'SC_TestClassEnterpriseDocFields');

        Profile p = SC_TestUtil.getProfile('System Administrator');
        
        // run as a TSE without
        system.runAs(SC_TestUtil.createUser(p))
        {
        Account acc = SC_TestUtil.createAccount(true);
        Contact con1 = SC_TestUtil.createContact(true, acc);

        Asset parentArray = SC_TestUtil.createArray(true, 'testclass.array', acc, 'testarrayID', '3.4', 
                                                    'Installed', 
                                                    'Controller 24/7 4HR 1 Year Domestic Support');
        Case c = SC_TestUtil.createCase(true, acc, con1, parentArray);

        
            Test.startTest();

            Test.setCurrentPage(Page.SC_CaseDocuments);
            ApexPages.currentPage().getParameters().put('id', c.Id);
            SC_CaseDocumentsController obj = new SC_CaseDocumentsController(new ApexPages.StandardController(c));

            Test.stopTest();

            // when no attachment is available on the case as well as enterprise, show no documents msg
            system.assert(Apexpages.getMessages()[0].getDetail().contains(system.label.SC_NoDocuments), 
                                    'No Documents Available Msg should be added to the page');
        }
    }
}