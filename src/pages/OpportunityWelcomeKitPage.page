<apex:page id="myPage" controller="OpportunityWelcomeKitController" showHeader="false" applyHtmlTag="false">
    <style>
        .bPageBlock
        {
          width:1050px !important;
        }
        .selected
        {
          font-weight: bold;
          float: left;
        }
    </style>
    
    <apex:form id="myForm">
        <apex:pageBlock title="Welcome Kit Information" id="pageBlock">
            <apex:pageBlockButtons id="topButtons" location="top">
                <apex:commandButton id="newContact" action="{!newContact}"
                                    rerender="selectedContact,options,clearSection" value="Add New Contact"
                                    title="Click this button to create a new Contact for the Opportunity's Account to receive the Welcome Kit"
                                    status="loading1" immediate="true" oncomplete="configureSelectedContact( '' );" />
                <apex:actionStatus id="loading1" onstart="loading(true)" onstop="loading(false)" />
                <apex:outputpanel id="clearSection">
                    <apex:commandButton id="clearContact" action="{!clearContact}"
                                        rerender="selectedContact,options,clearSection" value="Clear Contact" rendered="{!displayClearBtn}"
                                        title="Click this button to clear the Welcome Kit contact information"
                                        onclick="confirm( 'Are you sure you want to clear the contact?' );"
                                        status="loading12" immediate="true" oncomplete="configureSelectedContact( '' );" />
                    <apex:actionStatus id="loading12" onstart="loading(true)" onstop="loading(false)" />
                </apex:outputPanel>
                <apex:commandButton id="save" status="loadingForSave" styleClass="options" rerender="selectedContact,errorDetection,errorMessage"
                                    title="Click this button to Save the Welcome Kit"
                                    value="Save"   action="{!save}"   oncomplete="hideDialog(false);" />
                <apex:commandButton id="cancel" status="loadingForCancel" styleClass="options" 
                                    title="Click this button to Cancel any of your changes"
                                    value="Cancel" action="{!cancel}" oncomplete="hideDialog(true);" />
                <apex:actionStatus id="loadingForSave" onstart="loading(true)" onstop="loading(false)" />
                <apex:actionStatus id="loadingForCancel" onstart="loading(true)" onstop="loading(false)" />
            </apex:pageBlockButtons>
            <apex:pageBlockSection id="oppInfo" showHeader="true" title="Opportunity Information" columns="2">
                <apex:outputField value="{!opp.Name}" />
                <apex:outputField value="{!opp.CloseDate}" />
                <apex:outputText label="Account Name" value="{!accountName}" />            
                <apex:outputField value="{!opp.StageName}" />
                <apex:inputField value="{!opp.Welcome_Kit_Opt_Out__c}" onchange="processWelcomeKitDropdown( j$( this ).val() );" />
                <apex:actionFunction name="updateOpp" action="{!saveOpp}" immediate="true" oncomplete="hideDialog(false);">
                    <apex:param name="welcomeKitVal" assignTo="{!welcomeKitOptOutVal}" value="" />
                </apex:actionFunction>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection id="contactInfo" showHeader="true" title="Contact Information" columns="1">
                <apex:pageBlockTable id="contacts" value="{!oppContacts}" var="cont" rowClasses="odd,even">
                    <apex:column >
                        <apex:facet name="header">Action</apex:facet>
                        <apex:commandLink action="{!selectContact}" rerender="selectedContact,options,clearSection" value="Select" status="loading2" immediate="true" oncomplete="configureSelectedContact( j$( this ).next().data( 'id' ) );">
                            <apex:param name="contactID" value="{!cont.id}" assignTo="{!selectedContactID}" />
                        </apex:commandLink>
                        <div class="selected" data-id="{!cont.id}"></div>
                        <apex:actionStatus id="loading2" onstart="loading(true)" onstop="loading(false)" />                        
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">First Name</apex:facet>
                        <apex:outputField value="{!cont.firstName}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Last Name</apex:facet>
                        <apex:outputField value="{!cont.LastName}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Street</apex:facet>
                        <apex:outputField value="{!cont.MailingStreet}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">City</apex:facet>
                        <apex:outputField value="{!cont.MailingCity}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">State/Province</apex:facet>
                        <apex:outputField value="{!cont.MailingState}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Postal Code</apex:facet>
                        <apex:outputField value="{!cont.MailingPostalCode}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Country</apex:facet>
                        <apex:outputField value="{!cont.MailingCountry}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Email</apex:facet>
                        <apex:outputText value="{!cont.Email}" />
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">Phone</apex:facet>
                        <apex:outputField value="{!cont.Phone}" />
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlockSection>

            <apex:pageBlockSection id="selectedContact" showHeader="true" title="Selected Contact Information" columns="2">
                <apex:messages id="errorMessage" style="background:pink;padding:5px;"/>
                <apex:inputHidden id="blank0"/>
                <apex:inputField value="{!selectedContact.firstName}" taborderhint="1" />
                <apex:inputField value="{!selectedContact.Email}" required="true" taborderhint="8" />
                <apex:inputField value="{!selectedContact.LastName}" taborderhint="2" />
                <apex:inputField value="{!selectedContact.Phone}" taborderhint="9" />
                <apex:inputField value="{!selectedContact.MailingStreet}" required="true" taborderhint="3" />
                <apex:inputField value="{!oaic.Notes__c}" taborderhint="10" />
                <apex:inputField value="{!selectedContact.MailingCity}" required="true" taborderhint="4" />
                <apex:inputField value="{!selectedContact.T_Shirt_Size__c}" taborderhint="11" />
                <apex:inputField value="{!selectedContact.MailingState}" taborderhint="5" />
                <apex:inputHidden id="blank2"/>
                <apex:inputField value="{!selectedContact.MailingPostalCode}" required="true" taborderhint="6" />
                <apex:inputHidden id="blank3"/>
                <apex:inputField value="{!selectedContact.MailingCountry}" taborderhint="7" />        
                <apex:inputHidden id="errorDetection" value="{!errorDetected}" />
            </apex:pageBlockSection>
            
            <div id="spinner" class="contentLoading">
                <div style="text-align: center;">
                    <img src="/img/loading.gif" alt="Loading graphic" /> Processing...
                </div>
            </div>
        </apex:pageBlock>
    </apex:form>
    <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css"/>
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"/>
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"/>
    
    <script type="text/javascript">
        j$ = jQuery.noConflict();
        
        j$(document).ready(function () {
            j$( "#spinner" ).hide();
            
            if (!{!existingWelcomeKit})
            {
                j$( '[id="myPage:myForm:pageBlock:selectedContact"]' ).hide();
                j$( '.options' ).hide();
            }
            else
            {
                //twistSection( document.getElementById( 'myPage:myForm:pageBlock:contactInfo' ).childNodes[0].childNodes[0] );
            }
            
            j$( '.options' ).click( function() {
                j$( '.btn' ).hide();
            });
            
            configureSelectedContact( '{!selectedContact.id}' );
        });
        
        
        function configureSelectedContact( id )
        {   
            j$( '.selected' ).each( function() {
                if (j$( this ).data( 'id' ) == id)
                {
                    j$( '.selected' ).find('span').remove();
                    j$( this ).append( '<span class="ui-icon ui-icon-check large"></span>' );
                    j$( this ).prev().text( 'Selected' ).addClass( 'selected' );
                }
                else
                {
                    j$( this ).empty();
                    j$( this ).prev().text( 'Select' ).removeClass( 'selected' );
                }
            });
        }
        
        function loading( val )
        {
            if (val)
            {
                if (j$( document.getElementById( 'myPage:myForm:pageBlock:contactInfo' ).childNodes[0].childNodes[0] ).hasClass( 'hideListButton' ))
                {
                    //twistSection( document.getElementById( 'myPage:myForm:pageBlock:contactInfo' ).childNodes[0].childNodes[0] );
                }
            
                j$( "#spinner" ).show();
                j$( '.options' ).hide();
            }
            else
            {
                j$( "#spinner" ).hide();
                j$( '.options' ).show();
            }
        }
        
        
        function processWelcomeKitDropdown( val )
        {
            if ( val != '' )
            {
                updateOpp( val );
            }
        }
                
        
        function hideDialog(isCancel)
        {
            if(isCancel)
            {
                var parent_url = GetURLParameter( 'parentURL' );
                
                // Making sure that parentURL was passed from the custom button
                if (parent_url != null)
                {
                    parent_url = decodeURIComponent( parent_url );
                    top.postMessage( '{"message": "close"}', parent_url ); 
                }      
            }
            
            else
            {
                var errorFound = j$( '[id="myPage:myForm:pageBlock:selectedContact:errorDetection"]' ).val();
                var pageErrorFound = j$( '[id="myPage:myForm:pageBlock:selectedContact:errorMessage"]' ).length;
                
                if(errorFound != 'true' && pageErrorFound == 0)
                {
                    var parent_url = GetURLParameter( 'parentURL' );
                    parent_url = decodeURIComponent( parent_url );
                    top.postMessage( '{"message": "close"}', parent_url );
                }
                else
                {
                    j$( '.btn' ).show();
                }
            }
        }
        
        
        function GetURLParameter( parameter )
        {
            var urlParams = window.location.search.substring( 1 );
            var parameters = urlParams.split( "&" );
            
            for (var i = 0; i < parameters.length; i++) 
            {
                var parameterArray = parameters[ i ].split( "=" );
                if (parameterArray[0] == parameter) 
                {
                    return parameterArray[ 1 ];
                }
            }
        }
    </script>
</apex:page>