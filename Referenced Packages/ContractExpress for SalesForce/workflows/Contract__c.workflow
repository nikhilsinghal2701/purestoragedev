<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ApproveContract</fullName>
        <field>ApprovalProcessStatus__c</field>
        <formula>&quot;Wait for Signature&quot;</formula>
        <name>Approve Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>InitiateContractApproval</fullName>
        <field>ApprovalProcessStatus__c</field>
        <formula>&quot;Wait for Approval&quot;</formula>
        <name>Initiate Contract Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecallContract</fullName>
        <field>ApprovalProcessStatus__c</field>
        <formula>&quot;Wait for Request&quot;</formula>
        <name>Recall Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RejectContract</fullName>
        <field>ApprovalProcessStatus__c</field>
        <formula>&quot;Stopped&quot;</formula>
        <name>Reject Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
