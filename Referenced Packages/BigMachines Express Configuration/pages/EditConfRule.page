<apex:page standardController="QConfig__Configuration_Rule__c" extensions="QConfig.EditConfigurationRuleCtrl" tabStyle="QConfig__Model_Definition__c" 
	title="Configuration Rule: {!IF(theRule.Name != '', theRule.Name, 'New Configuration Rule')}">
	<style>
	.attrToggle{
        vertical-align: middle;
        padding-bottom: 5px;
        margin-left:4px;
        margin-right:2px;
    }
	.Operand {
		min-width:180px;
	}
	.requiredInput .requiredBlock {
		background-color: #c00;
		position: absolute;
		left: -4px;
		width: 3px;
		top: 1px;
		bottom: 1px;
	}
	.helptext a {
	    position:relative;
	    text-decoration: none !important;
	}
	span.helptext {
		display: inline-block;
		width: 0;
	}
	.helptext a span {
	    display: none;
	}
	.helptext a:hover span {
	    display: block;
	    position:absolute;
	    padding:2px 5px;
	    left: 1.5em;
	    bottom: 1.1em;
	    width:15em;
	    z-index:4500;
	    border:1px solid orange;
	    background-image:none;
	    background-color:#FEFDB9;
	    color:black;
	    word-wrap: break-word;
	    text-align:left;
	    font-weight:normal;
	}
	.helptext img {
	    margin-right:0.25em;
	    padding:0px;
	    opacity:0.2;
	    filter:alpha(opacity=40);
	    vertical-align: middle;
	    display: inline-block;
	}
	.helptext img:hover {
	    opacity:1.0;
	    filter:alpha(opacity=100);
	}
	</style>
    <script>
   		function disableButton() { 
	        //disable save buttons to prevent mashing.
	        var buttons = document.getElementsByClassName('saveButton');
	        for(var i=0; i < buttons.length; i++){
	            buttons[i].disabled = true;
			}
	    }
    </script>
    <apex:form id="WholePage">
        <apex:sectionHeader title="Configuration Rule Edit" subtitle="{!theRule.Model_Definition__r.Name}">
            <apex:outputLink value="/{!theRule.QConfig__Model_Definition__c}" target="_parent"
      		  style="text-decoration: none; font-size: 11px;">&lt;&lt; Back to Configuration Blueprint</apex:outputLink><br /><br />
        </apex:sectionHeader>
        <apex:PageMessages id="Errors" />
        <apex:pageBlock title="Configuration Rule Details" id="GeneralInfo">
            <apex:pageBlockButtons location="top">
                <apex:commandButton styleClass="saveButton" action="{!save}" value="Save" rerender="WholePage" onclick="disableButton();"/>
                <apex:commandButton styleClass="saveButton" action="{!quickSave}" value="Quick Save" rerender="WholePage" onclick="disableButton();"/>
                <apex:commandButton styleClass="saveButton" action="{!saveNew}" value="Save & New" rerender="WholePage" onclick="disableButton();"/>
                <apex:commandButton styleClass="saveButton" action="{!saveClone}" value="Save & Clone" rerender="WholePage" onclick="disableButton();"/>
                <apex:commandButton action="{!cancel}" value="Cancel"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Information" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Name" />
                    <apex:outputPanel styleClass="requiredInput" layout="block" >
						<apex:outputPanel styleClass="requiredBlock" layout="block"/>
	                    <apex:inputField id="nameEntry" value="{!theRule.name}" />                        
	                </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Active" />
                    <apex:inputCheckbox id="activeBox" value="{!theRule.QConfig__Is_Active__c}" selected="{!checkActive}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Description" />
                    <apex:inputTextarea id="descriptionEntry" value="{!theRule.QConfig__Description__c}" rendered="{!$ObjectType.QConfig__Configuration_Rule__c.fields.QConfig__Description__c.Createable && $ObjectType.QConfig__Configuration_Rule__c.fields.QConfig__Description__c.Updateable}" rows="2" cols="50"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Rule Type" />
                   	<apex:outputPanel styleClass="requiredInput" layout="block" >
						<apex:outputPanel styleClass="requiredBlock" layout="block"/>
	                    <apex:inputField id="ruleType" value="{!theRule.QConfig__Rule_Type__c}" >
	                        <apex:actionSupport event="onchange" rerender="Errors, theActionBlock, PropertiesArea, theConditionBlock, theProfileBlock" />
	                    </apex:inputField>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                	<apex:outputLabel value="{!$ObjectType.QConfig__Configuration_Rule__c.fields.QConfig__Profile_Based__c.Label}" >
                		<span class="helptext">
							<apex:outputLink value="javascript:return false;" >
								<apex:image url="{!$Resource.QConfig__helpBlock}"/>
								<span>
									Make this rule active only on selected user profiles.
								</span>
							</apex:outputLink>
						</span>
					</apex:outputLabel>
                	<apex:inputField id="profileBased" value="{!theRule.QConfig__Profile_Based__c}" >
	                	<apex:actionSupport event="onchange" rerender="GeneralInfo" />
	                </apex:inputField>
                </apex:pageBlockSectionItem>
	            <apex:pageBlockSectionItem rendered="{!theRule.QConfig__Profile_Based__c}" id="ProfilesItem">
	            	<apex:outputLabel value="Profiles"/>
	            	<apex:selectList id="profilesList" size="7" multiselect="true" value="{!selectedProfiles}" >
	            		<apex:selectOptions value="{!activeProfiles}"/>
	            	</apex:selectList>
            	</apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Conditions" id="theConditionBlock" >
        	<apex:pageBlockButtons location="top" >
                <apex:commandButton action="{!addCondRow}" value="Add Condition" id="addCondition" disabled="{!theConditions.size > 24}" reRender="theConditionBlock, Errors"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="This Rule Activates When" columns="1" >
            	<apex:pageBlockTable id="ConditionRows" value="{!theConditions}" var="cond"  >
            		<apex:column headerValue="" width="4%" id="ActionColumn">
	                    <apex:commandLink value="Delete" id="deleteRow" action="{!deleteCondRow}" immediate="true" reRender="theConditionBlock, Errors">
	                        <apex:param name="index" assignTo="{!tmpRowIndex}" value="{!cond.ruleIndex}"/>
	                    </apex:commandLink>
	                </apex:column>
	                <apex:column headerValue="Row" value="{!cond.ruleIndex}" width="3em" id="IndexColumn"/>
	                <apex:column headerValue="Field" id="FieldColumn" style="max-width: 33%;">
	                    <apex:selectList id="CondFieldSelect" size="1" value="{!cond.attrDef}" >
	                        <apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Field--"/>
	                        <apex:selectOptions value="{!IF(!RuleIsFieldAssignment, AllFieldOptions, AllNonFormulaFieldOptions)}" />
	                        <apex:actionSupport event="onchange" reRender="ConditionRows" />
	                    </apex:selectList>
	                </apex:column>
	                <apex:column headerValue="Operator" width="200px" id="OperatorColumn">
	                    <apex:selectList id="ConditionOperator" size="1" value="{!cond.operator}" style="width:195px;" >
	                    	<apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Operator--"/>
	                        <apex:selectOptions value="{!conditionOperatorsMap[allFields[cond.attrDef].Attribute_Type__c]}"/>
	                        <apex:actionSupport event="onchange" reRender="ConditionRows" />
	                    </apex:selectList>
	                </apex:column>
	                <apex:column headerValue="Compare To" id="CompareColumn">
	                    <apex:image url="{!IF(cond.IsAttribute, $Resource.QConfig__spr_toggle_field, $Resource.QConfig__spr_toggle_value)}" styleClass="attrToggle" id="toggleCompareTo" >
	                        <apex:actionSupport event="onclick" action="{!cond.toggleIsAttribute}" reRender="theConditionBlock"/>
	                    </apex:image>
	                </apex:column>
	                <apex:column headerValue="Operand" id="OperandColumn">
	                    <!-- MultiPicklists -->
	                    <apex:selectList id="MultiPick" value="{!cond.multipickValue}" multiselect="true" styleClass="Operand" rendered="{!!cond.IsAttribute && allFields[cond.attrDef].Attribute_Type__c='Picklist (Multi-select)'}" >
	                        <apex:selectOptions value="{!AttrPicklistOptions[cond.attrDef]}"/>
	                        <apex:actionSupport event="onchange" reRender="MultiPick" />
	                    </apex:selectList>
	                    <!-- Regular Picklists -->
	                    <apex:selectList id="picklist" value="{!cond.theCond[conditionStaticOperandFieldName[allFields[cond.attrDef].Attribute_Type__c]]}" size="1" styleClass="Operand" rendered="{!!cond.IsAttribute && allFields[cond.attrDef].Attribute_Type__c='Picklist'}" >
	                        <apex:selectOptions value="{!AttrPicklistOptions[cond.attrDef]}"/>
	                        <apex:actionSupport event="onchange" reRender="picklist" />
	                    </apex:selectList>
	                    <!-- All other static value fields -->
						<apex:inputField label="Operand" id="staticOperandNums" value="{!cond.theCond[conditionStaticOperandFieldName[allFields[cond.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!cond.IsAttribute && OR(allFields[cond.attrDef].Attribute_Type__c='Number',allFields[cond.attrDef].Attribute_Type__c='Currency',allFields[cond.attrDef].Attribute_Type__c='Percent')}" />
						<apex:inputField label="Operand" id="staticOperandCheckbox" value="{!cond.theCond[conditionStaticOperandFieldName[allFields[cond.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!cond.IsAttribute && allFields[cond.attrDef].Attribute_Type__c='Checkbox' && !OR(cond.operator = 'is checked', cond.operator = 'is not checked')}" />
						<apex:inputField label="Operand" id="staticOperandDate" value="{!cond.theCond[conditionStaticOperandFieldName[allFields[cond.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!cond.IsAttribute && allFields[cond.attrDef].Attribute_Type__c='Date'}" />
						<apex:inputField label="Operand" id="staticOperandText" value="{!cond.theCond[conditionStaticOperandFieldName[allFields[cond.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!cond.IsAttribute && allFields[cond.attrDef].Attribute_Type__c='Text'}" />
						<!-- Dynamic values (matched to another field) -->
	                    <apex:selectList id="fieldOperand" value="{!cond.attrOperand}" rendered="{!cond.IsAttribute}" size="1" styleClass="Operand">
	                    	<apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Field--"/>
	                        <apex:selectOptions value="{!IF(!RuleIsFieldAssignment, fieldsByType[allFields[cond.attrDef].Attribute_Type__c], NonFormulaFieldsByType[allFields[cond.attrDef].Attribute_Type__c])}"  />
	                        <apex:actionSupport event="onchange" reRender="fieldOperand" />
	                    </apex:selectList>
	                </apex:column>
            	</apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Condition Summary" columns="1" id="summarySection">
	            <apex:pageBlockSectionItem >
	            	<apex:outputLabel value="Grouping"/>
	            	<apex:inputText id="GroupingBox" value="{!theRule.QConfig__Message_Formula__c}" rendered="{!$ObjectType.QConfig__Configuration_Rule__c.fields.QConfig__Message_Formula__c.Createable && $ObjectType.QConfig__Configuration_Rule__c.fields.QConfig__Message_Formula__c.Updateable}" size="60">
	            		<apex:actionSupport event="onchange" rerender="summarySection" action="{!validateGroupingInline}"/>
	            	</apex:inputText>
	            </apex:pageBlockSectionItem>
	            <apex:pageMessage id="GroupingStatus" strength="2" severity="error" summary="{!groupingMsg}" rendered="{!!ISBLANK(groupingMsg)}"/>
	            <apex:pageBlockSectionItem >
	                <apex:outputLabel value="Quick Controls" />
	                <apex:outputPanel >
	                    <apex:commandButton action="{!andALL}" value="AND all"  rerender="summarySection"/>
	                    <apex:commandButton action="{!orALL}" value="OR all"   rerender="summarySection"/>
	                </apex:outputPanel>
	            </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:pageBlock title="Actions" id="theActionBlock">
            <apex:pageBlockButtons location="top">
                <apex:commandbutton id="newActionRow" action="{!addActionRow}" value="Add New Action" disabled="{!theActions.size > 99}" reRender="theActionBlock, Errors, PropertiesArea"/>
            </apex:pageBlockButtons>
            <apex:pageBlockSection title="Actions Taken" columns="1" >
	            <apex:pageBlockTable id="ActionTable" value="{!theActions}" var="action">
	                <apex:column width="3%" id="ActionDeleteCol" headerValue="">
	                    <apex:commandLink value="Delete" id="deleteActionRow" action="{!deleteActionRow}" reRender="theActionBlock, Errors, PropertiesArea" immediate="true">
	                        <apex:param name="index" assignTo="{!tmpRowIndex}" value="{!action.executionOrder}"/>
	                    </apex:commandLink>
	                </apex:column>
	                <apex:column headerValue="Row" width="3em" value="{!action.executionOrder}"/> 
	                <apex:column headerValue="Field" id="ActionFieldColumn" style="max-width: 33%;">
	                   <apex:selectList id="ActionFieldSelect" size="1" value="{!action.attrDef}" >
	                       <apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Field--"/>
	                       <apex:selectOptions value="{!IF(RuleIsHiding, AllFieldOptions, AllNonFormulaFieldOptions)}" />
	                       <apex:actionSupport action="{!passOnActionProperties}" event="onchange" reRender="ActionTable, PropertiesArea" >
	                       		<apex:param name="index" assignTo="{!tmpRowIndex}" value="{!action.executionOrder}"/>
	                       </apex:actionSupport>
	                   </apex:selectList>
	                </apex:column>
	                <apex:column headerValue="Type" id="FieldType" width="130px">
	                    <apex:outputText value="{!IF(allFields[action.attrDef].Attribute_Type__c == INVALID_VALUE, '', allFields[action.attrDef].Attribute_Type__c )}" />
	                </apex:column>
	                <apex:column headerValue="Operator" id="ActionOperator" rendered="{!OR(RuleIsConstraint, RuleIsFieldAssignment)}" width="200px">
	                    <apex:selectList id="ActionOperatorSelect" size="1" value="{!action.operator}" style="width:195px;">
	                    	<apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Operator--"/>
	                        <apex:selectOptions value="{!IF(RuleIsConstraint, constraintOperatorsMap[allFields[action.attrDef].Attribute_Type__c], assignmentOperatorsMap[allFields[action.attrDef].Attribute_Type__c])}" />
	                        <apex:actionSupport event="onchange" reRender="ActionOperatorSelect" />
	                    </apex:selectList>
	                </apex:column>
	                <apex:column headerValue="Act On" id="ActionActOn" rendered="{!OR(RuleIsConstraint, RuleIsFieldAssignment)}" >
	                 <apex:image url="{!IF(action.IsAttribute, $Resource.QConfig__spr_toggle_field, $Resource.QConfig__spr_toggle_value)}" styleClass="attrToggle" id="toggleActionCompareTo" >
	                     <apex:actionSupport event="onclick" action="{!action.toggleIsAttribute}" reRender="theActionBlock"/>
	                 </apex:image>
	                </apex:column>
	                <apex:column headerValue="Operand" id="ActionOperand" rendered="{!OR(RuleIsConstraint, RuleIsFieldAssignment)}">
	                	<!-- MultiPicklists -->
	                        <apex:selectList id="MultiPick" value="{!action.multipickValue}" multiselect="true" styleClass="Operand" rendered="{!!action.IsAttribute && allFields[action.attrDef].Attribute_Type__c='Picklist (Multi-select)'}" >
	                            <apex:selectOptions value="{!AttrPicklistOptions[action.attrDef]}"/>
	                            <apex:actionSupport event="onchange" reRender="MultiPick" />
	                        </apex:selectList>
	                        <!-- Regular Picklists -->
	                        <apex:selectList id="picklist" value="{!action.theAction[actionStaticOperandFieldName[allFields[action.attrDef].Attribute_Type__c]]}" size="1" styleClass="Operand" rendered="{!!action.IsAttribute && allFields[action.attrDef].Attribute_Type__c='Picklist'}" >
	                            <apex:selectOptions value="{!AttrPicklistOptions[action.attrDef]}"/>
	                            <apex:actionSupport event="onchange" reRender="picklist" />
	                        </apex:selectList>
	                        <!-- All other static value fields -->
							<apex:inputField label="Operand" id="staticOperandNums" value="{!action.theAction[actionStaticOperandFieldName[allFields[action.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!action.IsAttribute && OR(allFields[action.attrDef].Attribute_Type__c='Number',allFields[action.attrDef].Attribute_Type__c='Currency',allFields[action.attrDef].Attribute_Type__c='Percent')}" />
							<apex:inputField label="Operand" id="staticOperandCheckbox" value="{!action.theAction[actionStaticOperandFieldName[allFields[action.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!action.IsAttribute && allFields[action.attrDef].Attribute_Type__c='Checkbox'}" />
							<apex:inputField label="Operand" id="staticOperandDate" value="{!action.theAction[actionStaticOperandFieldName[allFields[action.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!action.IsAttribute && allFields[action.attrDef].Attribute_Type__c='Date'}" />
							<apex:inputField label="Operand" id="staticOperandText" value="{!action.theAction[actionStaticOperandFieldName[allFields[action.attrDef].Attribute_Type__c]]}" styleClass="Operand" rendered="{!!action.IsAttribute && allFields[action.attrDef].Attribute_Type__c='Text'}" />
							<!-- Dynamic values (matched to another field) -->
	                        <apex:selectList id="fieldOperand" value="{!action.attrOperand}" rendered="{!action.IsAttribute}" size="1" styleClass="Operand">
	                        	<apex:selectOption itemValue="{!INVALID_VALUE}" itemLabel="--Select Field--"/>
	                            <apex:selectOptions value="{!NonFormulaFieldsByFlexType[allFields[action.attrDef].Attribute_Type__c]}" />
	                            <apex:actionSupport event="onchange" reRender="fieldOperand" />
	                        </apex:selectList>
	                </apex:column>
	                <apex:column headerValue="Execution Order" rendered="{!RuleIsFieldAssignment}">
	                	<apex:inputText label="Execution Order" id="ExecutionOrder" value="{!action.index}" style="max-width: 6.5em;" />
	                </apex:column>
	            </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <!-- MESSAGES -->
        <apex:outputPanel id="PropertiesArea">
	        <apex:pageBlock id="FieldProperties" title="Field Properties" rendered="{!OR(RuleIsConstraint, RuleIsFieldAssignment)}">
	            <apex:pageBlockTable value="{!MessageCandidates}" var="theMessage" id="table">
	                <apex:column headerValue="Field" value="{!allFields[theMessage.attrDef].Name}" width="25%" />
	                <apex:column headerValue="Read Only" rendered="{!RuleIsFieldAssignment}" >
	                	<apex:inputField value="{!theMessage.theAction.QConfig__Is_Mandatory__c}" id="readOnly">
	                		<apex:actionSupport action="{!propagateProperties}" event="onchange" reRender="theActionBlock, Errors, PropertiesArea">
	                       		<apex:param name="index" assignTo="{!tmpRowIndex}" value="{!theMessage.executionOrder}"/>
	                       </apex:actionSupport>
	                	</apex:inputField>
	                </apex:column>
	                <apex:column headerValue="Message">
	                	<apex:inputField id="FieldMessage" value="{!theMessage.theAction.QConfig__Attribute_Message__c}" style="width: 22em;">
	                		<apex:actionSupport action="{!propagateProperties}" event="onchange" reRender="theActionBlock, Errors, PropertiesArea" >
	                       		<apex:param name="index" assignTo="{!tmpRowIndex}" value="{!theMessage.executionOrder}"/>
	                       </apex:actionSupport>
	                	</apex:inputField>
	                </apex:column>
	            </apex:pageBlockTable>
	        </apex:pageBlock>
        </apex:outputPanel>
        
        
    </apex:form>
</apex:page>