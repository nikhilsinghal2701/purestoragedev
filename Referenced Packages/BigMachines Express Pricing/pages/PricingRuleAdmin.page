<apex:page controller="BMXP.PricingRuleAdminCtrl" sidebar="false" tabStyle="BMXP__Pricing_Rule__c" title="Pricing Rules: {!ruleListTitle}">
    <apex:includeScript value="{!URLFOR($Resource.BMXB__jQueryUI, '/js/jquery-1.7.2.min.js')}"  />
    <apex:includeScript value="{!URLFOR($Resource.BMXB__jQueryUI, '/js/jquery-ui-1.8.22.custom.min.js')}"  />
    
    <style>
    .searchBox{
        margin-right:4px;
    }
    .bmx-data-left{
		text-align: left;
		padding-left: 3pt;
	}
	.bmx-data-center{
		text-align: center;
	}
	.bmx-data-right{
		text-align: right;
		padding-right: 3pt;
	}
	.bmx-data-right > div, .bmx-data-left > div, .bmx-data-center > div{
		white-space: normal;
	}
    </style>
    <script type="text/javascript">
        j$ = jQuery.noConflict();
        j$(document).ready(function() {
            setPlaceHolder();
        });

        function setPlaceHolder(){
            var friendBoxTitle;
            var boxTitle;
            
            if(('{!JSINHTMLENCODE(ruleType)}' == 'MaintenanceProduct')||('{!JSINHTMLENCODE(ruleType)}' == 'OptionalMaintenance')){
                friendBoxTitle = 'Maintenance Product';
                boxTitle = 'Base Product';
            }else if('{!JSINHTMLENCODE(ruleType)}' == 'AssociatedProduct'){
                friendBoxTitle = 'Associated Product';
                boxTitle = 'Base Product';
            }else{
                boxTitle = 'Product';
                friendBoxTitle = '';
            }
            
            j$("[id='{!$Component.SearchForm.SearchBoxes.AccountBox}']").attr("placeholder", "Account");
            j$("[id='{!$Component.SearchForm.SearchBoxes.ProductBox}']").attr("placeholder", boxTitle);
            j$("[id='{!$Component.SearchForm.SearchBoxes.AssociatedProductBox}']").attr("placeholder", friendBoxTitle);
        }

        function suppressEnter(ev){
            if(window.event && window.event.keyCode == 13 || ev.which == 13){
                return false;
            } else {
                return true;
            }
        }
        
    </script>

    <apex:form id="SearchForm">
        <c:PricingNavigation />
        <apex:pageMessage rendered="{!ruleType == null && !fatalFailure}" severity="info" strength="3" title="Unable to determine Rule Type" summary="Please select a Rule Type from the navigation bar above." />
        <br/>
        <apex:variable var="MaintTypeRule" value="{!OR(ruleType == 'MaintenanceProduct',ruleType=='OptionalMaintenance')}"/>
		<apex:variable var="MaintOrAssoc" value="{!OR(MaintTypeRule,ruleType == 'AssociatedProduct')}"/>
		<apex:variable var="QtyBracketsEnabled" value="{!OR(ruleType=='Volume',ruleType=='Tiered')}"/>
        <apex:pagemessages id="Errors" />
        <apex:pageBlock id="SearchBoxes" rendered="{!ruleType != null && !isCustom}">
            <apex:outputLabel value="Filter by:" styleClass="searchBox"/> 
            <apex:outputLabel for="AccountBox" value="Account:" styleClass="searchBox" rendered="{!ruleType == 'AccountSpecific'}"/>
            <apex:inputText value="{!accountFilter}" id="AccountBox" onkeypress="return suppressEnter(event);" rendered="{!ruleType == 'AccountSpecific'}" styleClass="searchBox"/>
            <apex:outputLabel for="ProductBox" value="{!IF(MaintOrAssoc,'Base Product:','Product:')}" styleClass="searchBox"/>
            <apex:inputText value="{!productFilter}" id="ProductBox" onkeypress="return suppressEnter(event);" styleClass="searchBox"/>
            <apex:outputLabel for="AssociatedProductBox" rendered="{!MaintOrAssoc}" styleClass="searchBox" value="{!IF(ruleType=='AssociatedProduct', 'Associated Product:', IF(MaintTypeRule, 'Maintenance Product:', ''))}"/>
            <apex:inputText value="{!associatedProdFilter}" id="AssociatedProductBox" onkeypress="return suppressEnter(event);" rendered="{!MaintOrAssoc}" styleClass="searchBox"/>
            <apex:commandButton value="Go" reRender="theListBlock,stats" action="{!filterList}"/>
            <apex:commandButton value="Clear" reRender="theListBlock,SearchBoxes" action="{!showDefaultList}" oncomplete="setPlaceHolder();"/>
            (Total records fitting current filter criteria:&nbsp;<apex:outputText id="stats" value="{!totalSize}"/>)
        </apex:pageBlock>
        <apex:pageBlock id="theListBlock" rendered="{!ruleType <> null}" mode="detail" >
            <apex:inlineEditSupport event="ondblclick" disabled="false" resetFunction="resetInlineEdit()" />
            <apex:pageBlockButtons location="top">
                <apex:commandButton value="New Pricing Rule" id="newPricingRuleButton" action="{!URLFOR($Action.Pricing_Rule__c.New, null, ["rType"=ruleType] )}"/>
                <apex:commandButton value="Quick Save" action="{!quickSave}" id="quickSaveButton" />
                <apex:commandButton value="Mass Create" action="{!URLFOR($Page.PricingMassAdmin, null, ["rType"=ruleType])}" id="massAdmin" immediate="true" rendered="{!isCustom}" />
                <apex:commandButton value="Cancel" action="{!URLFOR($Page.PricingRuleAdmin, null, ["rType"=ruleType])}" id="cancelButton" immediate="true" />                
            </apex:pageBlockButtons>
            
            <apex:pageBlockSection title="{!ruleListTitle}" columns="1" collapsible="false" id="TableSection" >
	            <apex:actionFunction name="refreshTable" action="{!populateTable}"/>
	            <apex:pageBlockTable value="{!theRows}" var="row" rules="rows" id="rows">
	                <apex:column headerValue="Actions" style="width:67px;min-width:67px;" styleClass="bmx-data-left" headerClass="bmx-data-left" >
	                    <apex:outputLink style="margin-right:3px; text-decoration:none;" value="{!URLFOR($Action.Pricing_Rule__c.Edit, row.theRule.Id)}">
	                    Edit
	                    </apex:outputLink>
                    	|
                    	<apex:commandLink style="margin-left:3px; text-decoration:none;" value="Del" immediate="true" action="{!URLFOR($Action.Pricing_Rule__c.Delete, row.theRule.Id, [retURL=URLFOR($Page.PricingRuleAdmin, null, ["rType"=ruleType])])}" oncomplete="refreshTable" rerender="stats,rows,myButtons"/>
	                </apex:column>                
	                <apex:column headerValue="{!$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Account__c.label}" headerClass="bmx-data-left" styleClass="bmx-data-left" rendered="{!ruleType == 'AccountSpecific'}">
	                    <apex:outputField value="{!row.theRule.BMXP__Account__c}"/>
	                </apex:column>
	                <apex:column headerValue="{!$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__All_Products__c.label}" headerClass="bmx-data-center" styleClass="bmx-data-center" style="width:64px;" rendered="{!ruleType == 'AccountSpecific'}">
	                    <apex:outputField value="{!row.theRule.BMXP__All_Products__c}"/>
	                </apex:column>
	                <apex:column headerValue="{!IF(MaintOrAssoc,'Base Product',$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Product__c.label)}" styleClass="bmx-data-left" headerClass="bmx-data-left" rendered="{!!isCustom}">
	                    <apex:outputField value="{!row.theRule.BMXP__Product__c}"/>
	                </apex:column>
						<apex:column headerValue="{!IF(OR(ruleType == 'MaintenanceProduct',ruleType=='OptionalMaintenance'),'Maintenance Product',$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Associated_Product__c.label)}" styleClass="bmx-data-left" headerClass="bmx-data-left" rendered="{!MaintOrAssoc}">
	                    <apex:outputField value="{!row.theRule.BMXP__Associated_Product__c}"/>
	                </apex:column>
	                
	                <apex:repeat value="{!conditions}" var="cond" id="dynamicCols" rendered="{!isCustom}">
	                	<apex:column id="col" styleClass="{!cond.ColumnStyle}" headerClass="{!cond.ColumnStyle}" >
	                		<apex:facet name="header">
		                		<apex:outputText value="{!cond.label}"/>
	                		</apex:facet>
	                		<apex:outputField id="OperandValue" value="{!row.operands[cond.index].Value__c}" rendered="{!cond.pickVals == null && !cond.isBoolean}" />
	                		<apex:inputCheckbox id="OperandCheckValue" value="{!row.operands[cond.index].Value__c}" rendered="{!cond.isBoolean}" />
	                		<apex:selectList id="OperandPickValue" value="{!row.operands[cond.index].Value__c}" size="1" rendered="{!cond.pickVals != null}" >
	                			<apex:selectOptions value="{!cond.pickVals}" />
	                		</apex:selectList>
	                	</apex:column>
	                </apex:repeat>
	                
	                <apex:column headerValue="Min. Qty." rendered="{!QtyBracketsEnabled}" styleClass="bmx-data-right" headerClass="bmx-data-right" style="width:96px;">
	                    <apex:outputField value="{!row.theRule.BMXP__Min_Quantity__c}"/>
	                </apex:column>
	                <apex:column headerValue="Max. Qty." rendered="{!QtyBracketsEnabled}" styleClass="bmx-data-right" headerClass="bmx-data-right" style="width:96px;">
	                    <apex:outputField value="{!row.theRule.BMXP__Max_Quantity__c}"/>
	                </apex:column>
	                <apex:column headerValue="Base Price Source" styleClass="bmx-data-left" headerClass="bmx-data-left" rendered="{!MaintTypeRule}">
	                    <apex:outputField value="{!row.theRule.BMXP__Base_Price_Source__c}"/>
	                </apex:Column>
	                <apex:column headerValue="{!IF(MaintTypeRule,'Base Product List Price',IF((ruleType == 'AssociatedProduct'),'Associated Product List Price','List Price'))}" value="{!prefix} {!row.listP}" style="width:96px;"  styleClass="bmx-data-right" headerClass="bmx-data-right" rendered="{!!isCustom}"/>                
	                <apex:column headerValue="{!IF(MaintTypeRule,'Fee',$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Discount__c.label)}" style="width:96px;" styleClass="bmx-data-right" headerClass="bmx-data-right">
	                    <apex:outputField value="{!row.theRule.BMXP__Discount__c}"/>
	                </apex:column>
	                <apex:column headerValue="{!IF(MaintTypeRule,'Fee Type',$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Discount_Type__c.label)}" style="width:96px;" styleClass="bmx-data-left" headerClass="bmx-data-left">
	                    <apex:selectList size="1" value="{!row.discountType}">
	                        <apex:selectOptions value="{!discTypes}"/>
	                    </apex:selectList>
	                </apex:column>                
	                <apex:column headerValue="{!IF(MaintTypeRule,'Maintenance Fee','Modified Price')}" value="{!prefix} {!row.salesP}" style="width:96px;" styleClass="bmx-data-right" headerClass="bmx-data-right" rendered="{!!isCustom}"/>
	                <apex:column headerValue="{!$ObjectType.BMXP__Pricing_Rule__c.fields.BMXP__Active__c.label}" style="width:48px;" styleClass="bmx-data-center" headerClass="bmx-data-center">
	                    <apex:outputField value="{!row.theRule.BMXP__Active__c}"/>
	                </apex:column>
	                <apex:column headerValue="{!$ObjectType.BMXP__Pricing_Rule__c.fields.LastModifiedDate.label}" value="{!row.theRule.LastModifiedDate}" style="width:128px;min-width:128px;" styleClass="bmx-data-right" headerClass="bmx-data-right"/>
	            </apex:pageBlockTable>
            </apex:pageBlockSection>
            <apex:pagemessages />
            <apex:pageBlockButtons location="bottom" >
                <apex:outputPanel id="myButtons">
                    <apex:commandButton action="{!Beginning}" title="Beginning" value="<<" disabled="{!disablePrevious}" reRender="theListBlock"/>
                    <apex:commandButton action="{!Previous}" title="Previous" value="<" disabled="{!disablePrevious}" reRender="theListBlock"/>    
                    Page {!PageNumber} of {!TotalPages}  
                    <apex:commandButton action="{!Next}" title="Next" value=">" disabled="{!disableNext}" reRender="theListBlock"/>
                    <apex:commandButton action="{!End}" title="End" value=">>" disabled="{!disableNext}" reRender="theListBlock"/><br/> 
                    <apex:outputText value="Results per page: " />
                    <apex:selectList value="{!RowsPerPage}" size="1" id="pageSize"> 
                        <apex:selectOption itemLabel="25" itemValue="25"/>
                        <apex:selectOption itemLabel="50" itemValue="50"/>
                        <apex:selectOption itemLabel="100" itemValue="100"/>
                    </apex:selectList>
                    <apex:commandButton value="Change" action="{!changeListSize}" reRender="theListBlock"/> 
                </apex:outputPanel>
            </apex:pageBlockButtons>
        </apex:pageBlock>
    </apex:form>
</apex:page>